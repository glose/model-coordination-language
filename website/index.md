# Models Coordination Language

The Models Coordination Language, or MCL, is a Domain Specific Language (DSL) to specify the integration "glue" beetwen executable models.


Please look at:
- The update site for installing and trying the latest version of MCL language as an enduser: [https://glose.gitlabpages.inria.fr/model-coordination-language/updatesite/latest](https://glose.gitlabpages.inria.fr/model-coordination-language/updatesite/latest)
