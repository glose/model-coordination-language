# how to manage the docker images


cf. https://gitlab.inria.fr/glose/flight-controller-language-demonstrator/container_registry

from this directory

```
docker login registry.gitlab.inria.fr
docker build -t registry.gitlab.inria.fr/glose/flight-controller-language-demonstrator/ubuntu-maven-openjdk8-graphviz .
docker push registry.gitlab.inria.fr/glose/flight-controller-language-demonstrator/ubuntu-maven-openjdk8-graphviz
```