/**
 */
package fr.inria.glose.mbilang.provider;

import fr.inria.glose.mbilang.MbilangFactory;
import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.Port;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.mbilang.Port} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PortItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDirectionPropertyDescriptor(object);
			addTemporalreferencePropertyDescriptor(object);
			addNaturePropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addRTDPropertyDescriptor(object);
			addMonitoredPropertyDescriptor(object);
			addInitValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_direction_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_direction_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__DIRECTION, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Temporalreference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTemporalreferencePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_temporalreference_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_temporalreference_feature",
								"_UI_Port_type"),
						MbilangPackage.Literals.PORT__TEMPORALREFERENCE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Nature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_nature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_nature_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__NATURE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_type_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_type_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__TYPE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the RTD feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRTDPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_RTD_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_RTD_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__RTD, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Monitored feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMonitoredPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_monitored_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_monitored_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__MONITORED, true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Init Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_Port_initValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_Port_initValue_feature", "_UI_Port_type"),
						MbilangPackage.Literals.PORT__INIT_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MbilangPackage.Literals.PORT__IOEVENTS);
			childrenFeatures.add(MbilangPackage.Literals.PORT__PROPERTIES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Port.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Port"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Port) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Port_type")
				: getString("_UI_Port_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Port.class)) {
		case MbilangPackage.PORT__DIRECTION:
		case MbilangPackage.PORT__NATURE:
		case MbilangPackage.PORT__TYPE:
		case MbilangPackage.PORT__RTD:
		case MbilangPackage.PORT__MONITORED:
		case MbilangPackage.PORT__INIT_VALUE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MbilangPackage.PORT__IOEVENTS:
		case MbilangPackage.PORT__PROPERTIES:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(
				createChildParameter(MbilangPackage.Literals.PORT__IOEVENTS, MbilangFactory.eINSTANCE.createUpdated()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__IOEVENTS,
				MbilangFactory.eINSTANCE.createReadyToRead()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__IOEVENTS,
				MbilangFactory.eINSTANCE.createTriggered()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__PROPERTIES,
				MbilangFactory.eINSTANCE.createZeroCrossingDetection()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__PROPERTIES,
				MbilangFactory.eINSTANCE.createExtrapolation()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__PROPERTIES,
				MbilangFactory.eINSTANCE.createInterpolation()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__PROPERTIES,
				MbilangFactory.eINSTANCE.createDiscontinuityLocator()));

		newChildDescriptors.add(createChildParameter(MbilangPackage.Literals.PORT__PROPERTIES,
				MbilangFactory.eINSTANCE.createHistoryProvider()));
	}

}
