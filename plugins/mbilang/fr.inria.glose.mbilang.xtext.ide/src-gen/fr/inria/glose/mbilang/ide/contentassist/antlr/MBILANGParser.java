/*
 * generated by Xtext 2.21.0
 */
package fr.inria.glose.mbilang.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.inria.glose.mbilang.ide.contentassist.antlr.internal.InternalMBILANGParser;
import fr.inria.glose.mbilang.services.MBILANGGrammarAccess;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class MBILANGParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(MBILANGGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, MBILANGGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getModelPropertyAccess().getAlternatives(), "rule__ModelProperty__Alternatives");
			builder.put(grammarAccess.getIOEventAccess().getAlternatives(), "rule__IOEvent__Alternatives");
			builder.put(grammarAccess.getEStringAccess().getAlternatives(), "rule__EString__Alternatives");
			builder.put(grammarAccess.getPortPropertyAccess().getAlternatives(), "rule__PortProperty__Alternatives");
			builder.put(grammarAccess.getEDoubleAccess().getAlternatives_4_0(), "rule__EDouble__Alternatives_4_0");
			builder.put(grammarAccess.getEBooleanAccess().getAlternatives(), "rule__EBoolean__Alternatives");
			builder.put(grammarAccess.getDataNatureAccess().getAlternatives(), "rule__DataNature__Alternatives");
			builder.put(grammarAccess.getDataTypeAccess().getAlternatives(), "rule__DataType__Alternatives");
			builder.put(grammarAccess.getPortDirectionAccess().getAlternatives(), "rule__PortDirection__Alternatives");
			builder.put(grammarAccess.getTemporalReferenceAccess().getAlternatives(), "rule__TemporalReference__Alternatives");
			builder.put(grammarAccess.getInterfaceAccess().getGroup(), "rule__Interface__Group__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_0(), "rule__Interface__Group_3_0__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_1(), "rule__Interface__Group_3_1__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_2(), "rule__Interface__Group_3_2__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_3(), "rule__Interface__Group_3_3__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_3_3(), "rule__Interface__Group_3_3_3__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_4(), "rule__Interface__Group_3_4__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_4_3(), "rule__Interface__Group_3_4_3__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_5(), "rule__Interface__Group_3_5__0");
			builder.put(grammarAccess.getInterfaceAccess().getGroup_3_5_3(), "rule__Interface__Group_3_5_3__0");
			builder.put(grammarAccess.getPortAccess().getGroup(), "rule__Port__Group__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_0(), "rule__Port__Group_4_0__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_1(), "rule__Port__Group_4_1__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_2(), "rule__Port__Group_4_2__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_3(), "rule__Port__Group_4_3__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_4(), "rule__Port__Group_4_4__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_5(), "rule__Port__Group_4_5__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_6(), "rule__Port__Group_4_6__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_7(), "rule__Port__Group_4_7__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_8(), "rule__Port__Group_4_8__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_9(), "rule__Port__Group_4_9__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_9_3(), "rule__Port__Group_4_9_3__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_10(), "rule__Port__Group_4_10__0");
			builder.put(grammarAccess.getPortAccess().getGroup_4_10_3(), "rule__Port__Group_4_10_3__0");
			builder.put(grammarAccess.getModelTemporalReferenceAccess().getGroup(), "rule__ModelTemporalReference__Group__0");
			builder.put(grammarAccess.getModelTemporalReferenceAccess().getGroup_4(), "rule__ModelTemporalReference__Group_4__0");
			builder.put(grammarAccess.getEDoubleAccess().getGroup(), "rule__EDouble__Group__0");
			builder.put(grammarAccess.getEDoubleAccess().getGroup_4(), "rule__EDouble__Group_4__0");
			builder.put(grammarAccess.getUpdatedAccess().getGroup(), "rule__Updated__Group__0");
			builder.put(grammarAccess.getReadyToReadAccess().getGroup(), "rule__ReadyToRead__Group__0");
			builder.put(grammarAccess.getTriggeredAccess().getGroup(), "rule__Triggered__Group__0");
			builder.put(grammarAccess.getZeroCrossingDetectionAccess().getGroup(), "rule__ZeroCrossingDetection__Group__0");
			builder.put(grammarAccess.getExtrapolationAccess().getGroup(), "rule__Extrapolation__Group__0");
			builder.put(grammarAccess.getInterpolationAccess().getGroup(), "rule__Interpolation__Group__0");
			builder.put(grammarAccess.getDiscontinuityLocatorAccess().getGroup(), "rule__DiscontinuityLocator__Group__0");
			builder.put(grammarAccess.getHistoryProviderAccess().getGroup(), "rule__HistoryProvider__Group__0");
			builder.put(grammarAccess.getRollbackAccess().getGroup(), "rule__Rollback__Group__0");
			builder.put(grammarAccess.getSuperDenseTimeAccess().getGroup(), "rule__SuperDenseTime__Group__0");
			builder.put(grammarAccess.getDeltaStepSizeAccess().getGroup(), "rule__DeltaStepSize__Group__0");
			builder.put(grammarAccess.getInterfaceAccess().getNameAssignment_1(), "rule__Interface__NameAssignment_1");
			builder.put(grammarAccess.getInterfaceAccess().getModelAssignment_3_0_1(), "rule__Interface__ModelAssignment_3_0_1");
			builder.put(grammarAccess.getInterfaceAccess().getFMUPathAssignment_3_1_1(), "rule__Interface__FMUPathAssignment_3_1_1");
			builder.put(grammarAccess.getInterfaceAccess().getGemocExecutablePathAssignment_3_2_1(), "rule__Interface__GemocExecutablePathAssignment_3_2_1");
			builder.put(grammarAccess.getInterfaceAccess().getPortsAssignment_3_3_2(), "rule__Interface__PortsAssignment_3_3_2");
			builder.put(grammarAccess.getInterfaceAccess().getPortsAssignment_3_3_3_1(), "rule__Interface__PortsAssignment_3_3_3_1");
			builder.put(grammarAccess.getInterfaceAccess().getTemporalreferencesAssignment_3_4_2(), "rule__Interface__TemporalreferencesAssignment_3_4_2");
			builder.put(grammarAccess.getInterfaceAccess().getTemporalreferencesAssignment_3_4_3_1(), "rule__Interface__TemporalreferencesAssignment_3_4_3_1");
			builder.put(grammarAccess.getInterfaceAccess().getPropertiesAssignment_3_5_2(), "rule__Interface__PropertiesAssignment_3_5_2");
			builder.put(grammarAccess.getInterfaceAccess().getPropertiesAssignment_3_5_3_1(), "rule__Interface__PropertiesAssignment_3_5_3_1");
			builder.put(grammarAccess.getPortAccess().getNameAssignment_2(), "rule__Port__NameAssignment_2");
			builder.put(grammarAccess.getPortAccess().getVariableNameAssignment_4_0_1(), "rule__Port__VariableNameAssignment_4_0_1");
			builder.put(grammarAccess.getPortAccess().getDirectionAssignment_4_1_1(), "rule__Port__DirectionAssignment_4_1_1");
			builder.put(grammarAccess.getPortAccess().getNatureAssignment_4_2_1(), "rule__Port__NatureAssignment_4_2_1");
			builder.put(grammarAccess.getPortAccess().getTypeAssignment_4_3_1(), "rule__Port__TypeAssignment_4_3_1");
			builder.put(grammarAccess.getPortAccess().getRTDAssignment_4_4_1(), "rule__Port__RTDAssignment_4_4_1");
			builder.put(grammarAccess.getPortAccess().getInitValueAssignment_4_5_1(), "rule__Port__InitValueAssignment_4_5_1");
			builder.put(grammarAccess.getPortAccess().getColorAssignment_4_6_1(), "rule__Port__ColorAssignment_4_6_1");
			builder.put(grammarAccess.getPortAccess().getMonitoredAssignment_4_7_1(), "rule__Port__MonitoredAssignment_4_7_1");
			builder.put(grammarAccess.getPortAccess().getTemporalreferenceAssignment_4_8_1(), "rule__Port__TemporalreferenceAssignment_4_8_1");
			builder.put(grammarAccess.getPortAccess().getIoeventsAssignment_4_9_2(), "rule__Port__IoeventsAssignment_4_9_2");
			builder.put(grammarAccess.getPortAccess().getIoeventsAssignment_4_9_3_1(), "rule__Port__IoeventsAssignment_4_9_3_1");
			builder.put(grammarAccess.getPortAccess().getPropertiesAssignment_4_10_2(), "rule__Port__PropertiesAssignment_4_10_2");
			builder.put(grammarAccess.getPortAccess().getPropertiesAssignment_4_10_3_1(), "rule__Port__PropertiesAssignment_4_10_3_1");
			builder.put(grammarAccess.getModelTemporalReferenceAccess().getNameAssignment_2(), "rule__ModelTemporalReference__NameAssignment_2");
			builder.put(grammarAccess.getModelTemporalReferenceAccess().getReferenceAssignment_4_1(), "rule__ModelTemporalReference__ReferenceAssignment_4_1");
			builder.put(grammarAccess.getUpdatedAccess().getNameAssignment_2(), "rule__Updated__NameAssignment_2");
			builder.put(grammarAccess.getReadyToReadAccess().getNameAssignment_2(), "rule__ReadyToRead__NameAssignment_2");
			builder.put(grammarAccess.getTriggeredAccess().getNameAssignment_2(), "rule__Triggered__NameAssignment_2");
			builder.put(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), "rule__Interface__UnorderedGroup_3");
			builder.put(grammarAccess.getPortAccess().getUnorderedGroup_4(), "rule__Port__UnorderedGroup_4");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private MBILANGGrammarAccess grammarAccess;

	@Override
	protected InternalMBILANGParser createParser() {
		InternalMBILANGParser result = new InternalMBILANGParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public MBILANGGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(MBILANGGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
