/*
 * generated by Xtext 2.14.0
 */
package fr.inria.glose.mbilang.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import fr.inria.glose.mbilang.Interface
import javax.xml.parsers.DocumentBuilderFactory
import java.util.zip.ZipFile
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.xtext.RuleCall
import fr.inria.glose.mbilang.Port
import fr.inria.glose.mbilang.PortDirection

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class MBILANGProposalProvider extends AbstractMBILANGProposalProvider {
	
	/*
	 * 		Port gen {
			direction OUTPUT
			nature piecewiseConstant
			type Boolean
			monitored true
			ioevents {
				Updated updated
			}
		},
		Port value {
			direction INPUT
			nature piecewiseConstant
			type Real
		},
		Port upper_bound {
			direction INPUT
			nature piecewiseConstant
			type Real
			initValue "70.0"
		},
		Port sample {
			direction INPUT
			nature piecewiseConstant
			type Integer
			initValue "5000"
		}
	 */
	
	override void completeInterface_Ports(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		 // call implementation of superclass
		  super.completeInterface_Ports(model, assignment, context, acceptor)
		 if (!model.eResource.allContents.filter(Interface).filter[i | i.FMUPath.isNullOrEmpty].empty) {
		 	// If any FMU path is specified, then return nothing
		 	// acceptor.accept(createCompletionProposal("FMUPath not defined", context))
		 	return
		 } else {
		 	var interfaceModel = model.eResource.allContents.filter(Interface).head		 			
		 
  			val file = ResourcesPlugin.workspace.root.getFile(new Path(model.eResource.URI.toPlatformString(true)))
			val fmuZip = file.rawLocation.removeLastSegments(1) + "/" + interfaceModel.FMUPath
		 	val zipFile = new ZipFile(fmuZip);
		 	val entries = zipFile.entries()
		 	
		 	while (entries.hasMoreElements) {
		 		val entry = entries.nextElement();
		 		if (entry.name.compareTo("modelDescription.xml")==0) {
		 			val inputStream = zipFile.getInputStream(entry)		 			
		         	val dbFactory = DocumentBuilderFactory.newInstance();
		         	val dBuilder = dbFactory.newDocumentBuilder();
		         	val doc = dBuilder.parse(inputStream);
		         	doc.getDocumentElement().normalize();
		         	val nList = doc.getElementsByTagName("ScalarVariable");
		         	var res = ""
		         	for (var i = 0; i < nList.length; i++) {
		         		val nNode = nList.item(i);
		         		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               				val eElement = nNode as Element;
               				var typeNode = "Real"
               				if (eElement.getElementsByTagName("Real").length > 0) {
               					typeNode = "Real"
               				}
               				else if (eElement.getElementsByTagName("Boolean").length > 0) {
               					typeNode = "Boolean"
               				}
               				else if (eElement.getElementsByTagName("Integer").length > 0) {
               					typeNode = "Integer"
               				}
               				
               				val direction = eElement.getAttribute("causality").toUpperCase
               				if (direction.compareTo("INPUT") == 0 || direction.compareTo("OUTPUT") == 0) {
               				res = res +
               				'''
							Port "«eElement.getAttribute("name")»" {
								direction «direction»
								nature continuous
								type «typeNode»
							},
               				'''	
               				}               				
               			}
		         	}
		         	acceptor.accept(createCompletionProposal(res.substring(0, res.lastIndexOf(",")), context))
		 		}
		 	}
		 	zipFile.close	 	
		 }
		 
	}
}
