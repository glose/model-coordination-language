package fr.inria.glose.mbilang.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.glose.mbilang.services.MBILANGGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMBILANGParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Interface'", "'{'", "'model'", "'FMUPath'", "'GemocExecutablePath'", "'ports'", "','", "'}'", "'temporalreferences'", "'properties'", "'Port'", "'variableName'", "'direction'", "'nature'", "'type'", "'RTD'", "'initValue'", "'plotterColor'", "'monitored'", "'temporalreference'", "'ioevents'", "'ModelTemporalReference'", "'reference'", "'Updated'", "'ReadyToRead'", "'Triggered'", "'ZeroCrossingDetection'", "'Extrapolation'", "'Interpolation'", "'DiscontinuityLocator'", "'HistoryProvider'", "'Rollback'", "'SuperDenseTime'", "'DeltaStepSize'", "'true'", "'false'", "'constant'", "'transient'", "'piecewiseConstant'", "'piecewiseContinuous'", "'continuous'", "'Real'", "'Integer'", "'Boolean'", "'String'", "'Enumeration'", "'INPUT'", "'OUTPUT'", "'time'", "'distance'", "'angle'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMBILANGParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMBILANGParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMBILANGParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMBILANG.g"; }



     	private MBILANGGrammarAccess grammarAccess;

        public InternalMBILANGParser(TokenStream input, MBILANGGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Interface";
       	}

       	@Override
       	protected MBILANGGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleInterface"
    // InternalMBILANG.g:65:1: entryRuleInterface returns [EObject current=null] : iv_ruleInterface= ruleInterface EOF ;
    public final EObject entryRuleInterface() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterface = null;


        try {
            // InternalMBILANG.g:65:50: (iv_ruleInterface= ruleInterface EOF )
            // InternalMBILANG.g:66:2: iv_ruleInterface= ruleInterface EOF
            {
             newCompositeNode(grammarAccess.getInterfaceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterface=ruleInterface();

            state._fsp--;

             current =iv_ruleInterface; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterface"


    // $ANTLR start "ruleInterface"
    // InternalMBILANG.g:72:1: ruleInterface returns [EObject current=null] : (otherlv_0= 'Interface' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) ) otherlv_28= '}' ) ;
    public final EObject ruleInterface() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_FMUPath_7_0 = null;

        AntlrDatatypeRuleToken lv_GemocExecutablePath_9_0 = null;

        EObject lv_ports_12_0 = null;

        EObject lv_ports_14_0 = null;

        EObject lv_temporalreferences_18_0 = null;

        EObject lv_temporalreferences_20_0 = null;

        EObject lv_properties_24_0 = null;

        EObject lv_properties_26_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:78:2: ( (otherlv_0= 'Interface' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) ) otherlv_28= '}' ) )
            // InternalMBILANG.g:79:2: (otherlv_0= 'Interface' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) ) otherlv_28= '}' )
            {
            // InternalMBILANG.g:79:2: (otherlv_0= 'Interface' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) ) otherlv_28= '}' )
            // InternalMBILANG.g:80:3: otherlv_0= 'Interface' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) ) otherlv_28= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getInterfaceAccess().getInterfaceKeyword_0());
            		
            // InternalMBILANG.g:84:3: ( (lv_name_1_0= ruleEString ) )
            // InternalMBILANG.g:85:4: (lv_name_1_0= ruleEString )
            {
            // InternalMBILANG.g:85:4: (lv_name_1_0= ruleEString )
            // InternalMBILANG.g:86:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getInterfaceAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInterfaceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMBILANG.g:107:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) ) )
            // InternalMBILANG.g:108:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) )
            {
            // InternalMBILANG.g:108:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?) )
            // InternalMBILANG.g:109:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?)
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            				
            // InternalMBILANG.g:112:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?)
            // InternalMBILANG.g:113:6: ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+ {...}?
            {
            // InternalMBILANG.g:113:6: ( ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=7;
                int LA4_0 = input.LA(1);

                if ( LA4_0 == 13 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 0) ) {
                    alt4=1;
                }
                else if ( LA4_0 == 14 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 1) ) {
                    alt4=2;
                }
                else if ( LA4_0 == 15 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 2) ) {
                    alt4=3;
                }
                else if ( LA4_0 == 16 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 3) ) {
                    alt4=4;
                }
                else if ( LA4_0 == 19 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 4) ) {
                    alt4=5;
                }
                else if ( LA4_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 5) ) {
                    alt4=6;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMBILANG.g:114:4: ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:114:4: ({...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:115:5: {...}? => ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalMBILANG.g:115:106: ( ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:116:6: ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 0);
            	    					
            	    // InternalMBILANG.g:119:9: ({...}? => (otherlv_4= 'model' ( ( ruleEString ) ) ) )
            	    // InternalMBILANG.g:119:10: {...}? => (otherlv_4= 'model' ( ( ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:119:19: (otherlv_4= 'model' ( ( ruleEString ) ) )
            	    // InternalMBILANG.g:119:20: otherlv_4= 'model' ( ( ruleEString ) )
            	    {
            	    otherlv_4=(Token)match(input,13,FOLLOW_3); 

            	    									newLeafNode(otherlv_4, grammarAccess.getInterfaceAccess().getModelKeyword_3_0_0());
            	    								
            	    // InternalMBILANG.g:123:9: ( ( ruleEString ) )
            	    // InternalMBILANG.g:124:10: ( ruleEString )
            	    {
            	    // InternalMBILANG.g:124:10: ( ruleEString )
            	    // InternalMBILANG.g:125:11: ruleEString
            	    {

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getInterfaceRule());
            	    											}
            	    										

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getModelEObjectCrossReference_3_0_1_0());
            	    										
            	    pushFollow(FOLLOW_6);
            	    ruleEString();

            	    state._fsp--;


            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMBILANG.g:145:4: ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:145:4: ({...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:146:5: {...}? => ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalMBILANG.g:146:106: ( ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:147:6: ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 1);
            	    					
            	    // InternalMBILANG.g:150:9: ({...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:150:10: {...}? => (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:150:19: (otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) ) )
            	    // InternalMBILANG.g:150:20: otherlv_6= 'FMUPath' ( (lv_FMUPath_7_0= ruleEString ) )
            	    {
            	    otherlv_6=(Token)match(input,14,FOLLOW_3); 

            	    									newLeafNode(otherlv_6, grammarAccess.getInterfaceAccess().getFMUPathKeyword_3_1_0());
            	    								
            	    // InternalMBILANG.g:154:9: ( (lv_FMUPath_7_0= ruleEString ) )
            	    // InternalMBILANG.g:155:10: (lv_FMUPath_7_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:155:10: (lv_FMUPath_7_0= ruleEString )
            	    // InternalMBILANG.g:156:11: lv_FMUPath_7_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getFMUPathEStringParserRuleCall_3_1_1_0());
            	    										
            	    pushFollow(FOLLOW_6);
            	    lv_FMUPath_7_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    											}
            	    											set(
            	    												current,
            	    												"FMUPath",
            	    												lv_FMUPath_7_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalMBILANG.g:179:4: ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:179:4: ({...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:180:5: {...}? => ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalMBILANG.g:180:106: ( ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:181:6: ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 2);
            	    					
            	    // InternalMBILANG.g:184:9: ({...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:184:10: {...}? => (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:184:19: (otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) ) )
            	    // InternalMBILANG.g:184:20: otherlv_8= 'GemocExecutablePath' ( (lv_GemocExecutablePath_9_0= ruleEString ) )
            	    {
            	    otherlv_8=(Token)match(input,15,FOLLOW_3); 

            	    									newLeafNode(otherlv_8, grammarAccess.getInterfaceAccess().getGemocExecutablePathKeyword_3_2_0());
            	    								
            	    // InternalMBILANG.g:188:9: ( (lv_GemocExecutablePath_9_0= ruleEString ) )
            	    // InternalMBILANG.g:189:10: (lv_GemocExecutablePath_9_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:189:10: (lv_GemocExecutablePath_9_0= ruleEString )
            	    // InternalMBILANG.g:190:11: lv_GemocExecutablePath_9_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getGemocExecutablePathEStringParserRuleCall_3_2_1_0());
            	    										
            	    pushFollow(FOLLOW_6);
            	    lv_GemocExecutablePath_9_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    											}
            	    											set(
            	    												current,
            	    												"GemocExecutablePath",
            	    												lv_GemocExecutablePath_9_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalMBILANG.g:213:4: ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) )
            	    {
            	    // InternalMBILANG.g:213:4: ({...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) ) )
            	    // InternalMBILANG.g:214:5: {...}? => ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 3)");
            	    }
            	    // InternalMBILANG.g:214:106: ( ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) ) )
            	    // InternalMBILANG.g:215:6: ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 3);
            	    					
            	    // InternalMBILANG.g:218:9: ({...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' ) )
            	    // InternalMBILANG.g:218:10: {...}? => (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:218:19: (otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}' )
            	    // InternalMBILANG.g:218:20: otherlv_10= 'ports' otherlv_11= '{' ( (lv_ports_12_0= rulePort ) ) (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )* otherlv_15= '}'
            	    {
            	    otherlv_10=(Token)match(input,16,FOLLOW_4); 

            	    									newLeafNode(otherlv_10, grammarAccess.getInterfaceAccess().getPortsKeyword_3_3_0());
            	    								
            	    otherlv_11=(Token)match(input,12,FOLLOW_7); 

            	    									newLeafNode(otherlv_11, grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3_3_1());
            	    								
            	    // InternalMBILANG.g:226:9: ( (lv_ports_12_0= rulePort ) )
            	    // InternalMBILANG.g:227:10: (lv_ports_12_0= rulePort )
            	    {
            	    // InternalMBILANG.g:227:10: (lv_ports_12_0= rulePort )
            	    // InternalMBILANG.g:228:11: lv_ports_12_0= rulePort
            	    {

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getPortsPortParserRuleCall_3_3_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_ports_12_0=rulePort();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    											}
            	    											add(
            	    												current,
            	    												"ports",
            	    												lv_ports_12_0,
            	    												"fr.inria.glose.mbilang.MBILANG.Port");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalMBILANG.g:245:9: (otherlv_13= ',' ( (lv_ports_14_0= rulePort ) ) )*
            	    loop1:
            	    do {
            	        int alt1=2;
            	        int LA1_0 = input.LA(1);

            	        if ( (LA1_0==17) ) {
            	            alt1=1;
            	        }


            	        switch (alt1) {
            	    	case 1 :
            	    	    // InternalMBILANG.g:246:10: otherlv_13= ',' ( (lv_ports_14_0= rulePort ) )
            	    	    {
            	    	    otherlv_13=(Token)match(input,17,FOLLOW_7); 

            	    	    										newLeafNode(otherlv_13, grammarAccess.getInterfaceAccess().getCommaKeyword_3_3_3_0());
            	    	    									
            	    	    // InternalMBILANG.g:250:10: ( (lv_ports_14_0= rulePort ) )
            	    	    // InternalMBILANG.g:251:11: (lv_ports_14_0= rulePort )
            	    	    {
            	    	    // InternalMBILANG.g:251:11: (lv_ports_14_0= rulePort )
            	    	    // InternalMBILANG.g:252:12: lv_ports_14_0= rulePort
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getInterfaceAccess().getPortsPortParserRuleCall_3_3_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_ports_14_0=rulePort();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"ports",
            	    	    													lv_ports_14_0,
            	    	    													"fr.inria.glose.mbilang.MBILANG.Port");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop1;
            	        }
            	    } while (true);

            	    otherlv_15=(Token)match(input,18,FOLLOW_6); 

            	    									newLeafNode(otherlv_15, grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_3_3_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalMBILANG.g:280:4: ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) )
            	    {
            	    // InternalMBILANG.g:280:4: ({...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) ) )
            	    // InternalMBILANG.g:281:5: {...}? => ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 4)");
            	    }
            	    // InternalMBILANG.g:281:106: ( ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) ) )
            	    // InternalMBILANG.g:282:6: ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 4);
            	    					
            	    // InternalMBILANG.g:285:9: ({...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' ) )
            	    // InternalMBILANG.g:285:10: {...}? => (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:285:19: (otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}' )
            	    // InternalMBILANG.g:285:20: otherlv_16= 'temporalreferences' otherlv_17= '{' ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) ) (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )* otherlv_21= '}'
            	    {
            	    otherlv_16=(Token)match(input,19,FOLLOW_4); 

            	    									newLeafNode(otherlv_16, grammarAccess.getInterfaceAccess().getTemporalreferencesKeyword_3_4_0());
            	    								
            	    otherlv_17=(Token)match(input,12,FOLLOW_9); 

            	    									newLeafNode(otherlv_17, grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3_4_1());
            	    								
            	    // InternalMBILANG.g:293:9: ( (lv_temporalreferences_18_0= ruleModelTemporalReference ) )
            	    // InternalMBILANG.g:294:10: (lv_temporalreferences_18_0= ruleModelTemporalReference )
            	    {
            	    // InternalMBILANG.g:294:10: (lv_temporalreferences_18_0= ruleModelTemporalReference )
            	    // InternalMBILANG.g:295:11: lv_temporalreferences_18_0= ruleModelTemporalReference
            	    {

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getTemporalreferencesModelTemporalReferenceParserRuleCall_3_4_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_temporalreferences_18_0=ruleModelTemporalReference();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    											}
            	    											add(
            	    												current,
            	    												"temporalreferences",
            	    												lv_temporalreferences_18_0,
            	    												"fr.inria.glose.mbilang.MBILANG.ModelTemporalReference");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalMBILANG.g:312:9: (otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) ) )*
            	    loop2:
            	    do {
            	        int alt2=2;
            	        int LA2_0 = input.LA(1);

            	        if ( (LA2_0==17) ) {
            	            alt2=1;
            	        }


            	        switch (alt2) {
            	    	case 1 :
            	    	    // InternalMBILANG.g:313:10: otherlv_19= ',' ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) )
            	    	    {
            	    	    otherlv_19=(Token)match(input,17,FOLLOW_9); 

            	    	    										newLeafNode(otherlv_19, grammarAccess.getInterfaceAccess().getCommaKeyword_3_4_3_0());
            	    	    									
            	    	    // InternalMBILANG.g:317:10: ( (lv_temporalreferences_20_0= ruleModelTemporalReference ) )
            	    	    // InternalMBILANG.g:318:11: (lv_temporalreferences_20_0= ruleModelTemporalReference )
            	    	    {
            	    	    // InternalMBILANG.g:318:11: (lv_temporalreferences_20_0= ruleModelTemporalReference )
            	    	    // InternalMBILANG.g:319:12: lv_temporalreferences_20_0= ruleModelTemporalReference
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getInterfaceAccess().getTemporalreferencesModelTemporalReferenceParserRuleCall_3_4_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_temporalreferences_20_0=ruleModelTemporalReference();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"temporalreferences",
            	    	    													lv_temporalreferences_20_0,
            	    	    													"fr.inria.glose.mbilang.MBILANG.ModelTemporalReference");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop2;
            	        }
            	    } while (true);

            	    otherlv_21=(Token)match(input,18,FOLLOW_6); 

            	    									newLeafNode(otherlv_21, grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_3_4_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalMBILANG.g:347:4: ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) )
            	    {
            	    // InternalMBILANG.g:347:4: ({...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) ) )
            	    // InternalMBILANG.g:348:5: {...}? => ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 5) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canSelect(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 5)");
            	    }
            	    // InternalMBILANG.g:348:106: ( ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) ) )
            	    // InternalMBILANG.g:349:6: ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getInterfaceAccess().getUnorderedGroup_3(), 5);
            	    					
            	    // InternalMBILANG.g:352:9: ({...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' ) )
            	    // InternalMBILANG.g:352:10: {...}? => (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleInterface", "true");
            	    }
            	    // InternalMBILANG.g:352:19: (otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}' )
            	    // InternalMBILANG.g:352:20: otherlv_22= 'properties' otherlv_23= '{' ( (lv_properties_24_0= ruleModelProperty ) ) (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )* otherlv_27= '}'
            	    {
            	    otherlv_22=(Token)match(input,20,FOLLOW_4); 

            	    									newLeafNode(otherlv_22, grammarAccess.getInterfaceAccess().getPropertiesKeyword_3_5_0());
            	    								
            	    otherlv_23=(Token)match(input,12,FOLLOW_10); 

            	    									newLeafNode(otherlv_23, grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3_5_1());
            	    								
            	    // InternalMBILANG.g:360:9: ( (lv_properties_24_0= ruleModelProperty ) )
            	    // InternalMBILANG.g:361:10: (lv_properties_24_0= ruleModelProperty )
            	    {
            	    // InternalMBILANG.g:361:10: (lv_properties_24_0= ruleModelProperty )
            	    // InternalMBILANG.g:362:11: lv_properties_24_0= ruleModelProperty
            	    {

            	    											newCompositeNode(grammarAccess.getInterfaceAccess().getPropertiesModelPropertyParserRuleCall_3_5_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_properties_24_0=ruleModelProperty();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    											}
            	    											add(
            	    												current,
            	    												"properties",
            	    												lv_properties_24_0,
            	    												"fr.inria.glose.mbilang.MBILANG.ModelProperty");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalMBILANG.g:379:9: (otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) ) )*
            	    loop3:
            	    do {
            	        int alt3=2;
            	        int LA3_0 = input.LA(1);

            	        if ( (LA3_0==17) ) {
            	            alt3=1;
            	        }


            	        switch (alt3) {
            	    	case 1 :
            	    	    // InternalMBILANG.g:380:10: otherlv_25= ',' ( (lv_properties_26_0= ruleModelProperty ) )
            	    	    {
            	    	    otherlv_25=(Token)match(input,17,FOLLOW_10); 

            	    	    										newLeafNode(otherlv_25, grammarAccess.getInterfaceAccess().getCommaKeyword_3_5_3_0());
            	    	    									
            	    	    // InternalMBILANG.g:384:10: ( (lv_properties_26_0= ruleModelProperty ) )
            	    	    // InternalMBILANG.g:385:11: (lv_properties_26_0= ruleModelProperty )
            	    	    {
            	    	    // InternalMBILANG.g:385:11: (lv_properties_26_0= ruleModelProperty )
            	    	    // InternalMBILANG.g:386:12: lv_properties_26_0= ruleModelProperty
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getInterfaceAccess().getPropertiesModelPropertyParserRuleCall_3_5_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_properties_26_0=ruleModelProperty();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getInterfaceRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"properties",
            	    	    													lv_properties_26_0,
            	    	    													"fr.inria.glose.mbilang.MBILANG.ModelProperty");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop3;
            	        }
            	    } while (true);

            	    otherlv_27=(Token)match(input,18,FOLLOW_6); 

            	    									newLeafNode(otherlv_27, grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_3_5_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            if ( ! getUnorderedGroupHelper().canLeave(grammarAccess.getInterfaceAccess().getUnorderedGroup_3()) ) {
                throw new FailedPredicateException(input, "ruleInterface", "getUnorderedGroupHelper().canLeave(grammarAccess.getInterfaceAccess().getUnorderedGroup_3())");
            }

            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getInterfaceAccess().getUnorderedGroup_3());
            				

            }

            otherlv_28=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_28, grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterface"


    // $ANTLR start "entryRuleModelProperty"
    // InternalMBILANG.g:430:1: entryRuleModelProperty returns [EObject current=null] : iv_ruleModelProperty= ruleModelProperty EOF ;
    public final EObject entryRuleModelProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModelProperty = null;


        try {
            // InternalMBILANG.g:430:54: (iv_ruleModelProperty= ruleModelProperty EOF )
            // InternalMBILANG.g:431:2: iv_ruleModelProperty= ruleModelProperty EOF
            {
             newCompositeNode(grammarAccess.getModelPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModelProperty=ruleModelProperty();

            state._fsp--;

             current =iv_ruleModelProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModelProperty"


    // $ANTLR start "ruleModelProperty"
    // InternalMBILANG.g:437:1: ruleModelProperty returns [EObject current=null] : (this_Rollback_0= ruleRollback | this_SuperDenseTime_1= ruleSuperDenseTime | this_DeltaStepSize_2= ruleDeltaStepSize ) ;
    public final EObject ruleModelProperty() throws RecognitionException {
        EObject current = null;

        EObject this_Rollback_0 = null;

        EObject this_SuperDenseTime_1 = null;

        EObject this_DeltaStepSize_2 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:443:2: ( (this_Rollback_0= ruleRollback | this_SuperDenseTime_1= ruleSuperDenseTime | this_DeltaStepSize_2= ruleDeltaStepSize ) )
            // InternalMBILANG.g:444:2: (this_Rollback_0= ruleRollback | this_SuperDenseTime_1= ruleSuperDenseTime | this_DeltaStepSize_2= ruleDeltaStepSize )
            {
            // InternalMBILANG.g:444:2: (this_Rollback_0= ruleRollback | this_SuperDenseTime_1= ruleSuperDenseTime | this_DeltaStepSize_2= ruleDeltaStepSize )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt5=1;
                }
                break;
            case 43:
                {
                alt5=2;
                }
                break;
            case 44:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalMBILANG.g:445:3: this_Rollback_0= ruleRollback
                    {

                    			newCompositeNode(grammarAccess.getModelPropertyAccess().getRollbackParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Rollback_0=ruleRollback();

                    state._fsp--;


                    			current = this_Rollback_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:454:3: this_SuperDenseTime_1= ruleSuperDenseTime
                    {

                    			newCompositeNode(grammarAccess.getModelPropertyAccess().getSuperDenseTimeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SuperDenseTime_1=ruleSuperDenseTime();

                    state._fsp--;


                    			current = this_SuperDenseTime_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:463:3: this_DeltaStepSize_2= ruleDeltaStepSize
                    {

                    			newCompositeNode(grammarAccess.getModelPropertyAccess().getDeltaStepSizeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_DeltaStepSize_2=ruleDeltaStepSize();

                    state._fsp--;


                    			current = this_DeltaStepSize_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModelProperty"


    // $ANTLR start "entryRuleIOEvent"
    // InternalMBILANG.g:475:1: entryRuleIOEvent returns [EObject current=null] : iv_ruleIOEvent= ruleIOEvent EOF ;
    public final EObject entryRuleIOEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIOEvent = null;


        try {
            // InternalMBILANG.g:475:48: (iv_ruleIOEvent= ruleIOEvent EOF )
            // InternalMBILANG.g:476:2: iv_ruleIOEvent= ruleIOEvent EOF
            {
             newCompositeNode(grammarAccess.getIOEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIOEvent=ruleIOEvent();

            state._fsp--;

             current =iv_ruleIOEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIOEvent"


    // $ANTLR start "ruleIOEvent"
    // InternalMBILANG.g:482:1: ruleIOEvent returns [EObject current=null] : (this_Updated_0= ruleUpdated | this_ReadyToRead_1= ruleReadyToRead | this_Triggered_2= ruleTriggered ) ;
    public final EObject ruleIOEvent() throws RecognitionException {
        EObject current = null;

        EObject this_Updated_0 = null;

        EObject this_ReadyToRead_1 = null;

        EObject this_Triggered_2 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:488:2: ( (this_Updated_0= ruleUpdated | this_ReadyToRead_1= ruleReadyToRead | this_Triggered_2= ruleTriggered ) )
            // InternalMBILANG.g:489:2: (this_Updated_0= ruleUpdated | this_ReadyToRead_1= ruleReadyToRead | this_Triggered_2= ruleTriggered )
            {
            // InternalMBILANG.g:489:2: (this_Updated_0= ruleUpdated | this_ReadyToRead_1= ruleReadyToRead | this_Triggered_2= ruleTriggered )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 34:
                {
                alt6=1;
                }
                break;
            case 35:
                {
                alt6=2;
                }
                break;
            case 36:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalMBILANG.g:490:3: this_Updated_0= ruleUpdated
                    {

                    			newCompositeNode(grammarAccess.getIOEventAccess().getUpdatedParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Updated_0=ruleUpdated();

                    state._fsp--;


                    			current = this_Updated_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:499:3: this_ReadyToRead_1= ruleReadyToRead
                    {

                    			newCompositeNode(grammarAccess.getIOEventAccess().getReadyToReadParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReadyToRead_1=ruleReadyToRead();

                    state._fsp--;


                    			current = this_ReadyToRead_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:508:3: this_Triggered_2= ruleTriggered
                    {

                    			newCompositeNode(grammarAccess.getIOEventAccess().getTriggeredParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Triggered_2=ruleTriggered();

                    state._fsp--;


                    			current = this_Triggered_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIOEvent"


    // $ANTLR start "entryRuleEString"
    // InternalMBILANG.g:520:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalMBILANG.g:520:47: (iv_ruleEString= ruleEString EOF )
            // InternalMBILANG.g:521:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMBILANG.g:527:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:533:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalMBILANG.g:534:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalMBILANG.g:534:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMBILANG.g:535:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:543:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRulePortProperty"
    // InternalMBILANG.g:554:1: entryRulePortProperty returns [EObject current=null] : iv_rulePortProperty= rulePortProperty EOF ;
    public final EObject entryRulePortProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePortProperty = null;


        try {
            // InternalMBILANG.g:554:53: (iv_rulePortProperty= rulePortProperty EOF )
            // InternalMBILANG.g:555:2: iv_rulePortProperty= rulePortProperty EOF
            {
             newCompositeNode(grammarAccess.getPortPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePortProperty=rulePortProperty();

            state._fsp--;

             current =iv_rulePortProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePortProperty"


    // $ANTLR start "rulePortProperty"
    // InternalMBILANG.g:561:1: rulePortProperty returns [EObject current=null] : (this_ZeroCrossingDetection_0= ruleZeroCrossingDetection | this_Extrapolation_1= ruleExtrapolation | this_Interpolation_2= ruleInterpolation | this_DiscontinuityLocator_3= ruleDiscontinuityLocator | this_HistoryProvider_4= ruleHistoryProvider ) ;
    public final EObject rulePortProperty() throws RecognitionException {
        EObject current = null;

        EObject this_ZeroCrossingDetection_0 = null;

        EObject this_Extrapolation_1 = null;

        EObject this_Interpolation_2 = null;

        EObject this_DiscontinuityLocator_3 = null;

        EObject this_HistoryProvider_4 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:567:2: ( (this_ZeroCrossingDetection_0= ruleZeroCrossingDetection | this_Extrapolation_1= ruleExtrapolation | this_Interpolation_2= ruleInterpolation | this_DiscontinuityLocator_3= ruleDiscontinuityLocator | this_HistoryProvider_4= ruleHistoryProvider ) )
            // InternalMBILANG.g:568:2: (this_ZeroCrossingDetection_0= ruleZeroCrossingDetection | this_Extrapolation_1= ruleExtrapolation | this_Interpolation_2= ruleInterpolation | this_DiscontinuityLocator_3= ruleDiscontinuityLocator | this_HistoryProvider_4= ruleHistoryProvider )
            {
            // InternalMBILANG.g:568:2: (this_ZeroCrossingDetection_0= ruleZeroCrossingDetection | this_Extrapolation_1= ruleExtrapolation | this_Interpolation_2= ruleInterpolation | this_DiscontinuityLocator_3= ruleDiscontinuityLocator | this_HistoryProvider_4= ruleHistoryProvider )
            int alt8=5;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt8=1;
                }
                break;
            case 38:
                {
                alt8=2;
                }
                break;
            case 39:
                {
                alt8=3;
                }
                break;
            case 40:
                {
                alt8=4;
                }
                break;
            case 41:
                {
                alt8=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalMBILANG.g:569:3: this_ZeroCrossingDetection_0= ruleZeroCrossingDetection
                    {

                    			newCompositeNode(grammarAccess.getPortPropertyAccess().getZeroCrossingDetectionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ZeroCrossingDetection_0=ruleZeroCrossingDetection();

                    state._fsp--;


                    			current = this_ZeroCrossingDetection_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:578:3: this_Extrapolation_1= ruleExtrapolation
                    {

                    			newCompositeNode(grammarAccess.getPortPropertyAccess().getExtrapolationParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Extrapolation_1=ruleExtrapolation();

                    state._fsp--;


                    			current = this_Extrapolation_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:587:3: this_Interpolation_2= ruleInterpolation
                    {

                    			newCompositeNode(grammarAccess.getPortPropertyAccess().getInterpolationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Interpolation_2=ruleInterpolation();

                    state._fsp--;


                    			current = this_Interpolation_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalMBILANG.g:596:3: this_DiscontinuityLocator_3= ruleDiscontinuityLocator
                    {

                    			newCompositeNode(grammarAccess.getPortPropertyAccess().getDiscontinuityLocatorParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_DiscontinuityLocator_3=ruleDiscontinuityLocator();

                    state._fsp--;


                    			current = this_DiscontinuityLocator_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalMBILANG.g:605:3: this_HistoryProvider_4= ruleHistoryProvider
                    {

                    			newCompositeNode(grammarAccess.getPortPropertyAccess().getHistoryProviderParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_HistoryProvider_4=ruleHistoryProvider();

                    state._fsp--;


                    			current = this_HistoryProvider_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePortProperty"


    // $ANTLR start "entryRulePort"
    // InternalMBILANG.g:617:1: entryRulePort returns [EObject current=null] : iv_rulePort= rulePort EOF ;
    public final EObject entryRulePort() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePort = null;


        try {
            // InternalMBILANG.g:617:45: (iv_rulePort= rulePort EOF )
            // InternalMBILANG.g:618:2: iv_rulePort= rulePort EOF
            {
             newCompositeNode(grammarAccess.getPortRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePort=rulePort();

            state._fsp--;

             current =iv_rulePort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePort"


    // $ANTLR start "rulePort"
    // InternalMBILANG.g:624:1: rulePort returns [EObject current=null] : ( () otherlv_1= 'Port' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) ) otherlv_35= '}' ) ;
    public final EObject rulePort() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_variableName_6_0 = null;

        Enumerator lv_direction_8_0 = null;

        Enumerator lv_nature_10_0 = null;

        Enumerator lv_type_12_0 = null;

        AntlrDatatypeRuleToken lv_RTD_14_0 = null;

        AntlrDatatypeRuleToken lv_initValue_16_0 = null;

        AntlrDatatypeRuleToken lv_color_18_0 = null;

        AntlrDatatypeRuleToken lv_monitored_20_0 = null;

        EObject lv_ioevents_25_0 = null;

        EObject lv_ioevents_27_0 = null;

        EObject lv_properties_31_0 = null;

        EObject lv_properties_33_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:630:2: ( ( () otherlv_1= 'Port' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) ) otherlv_35= '}' ) )
            // InternalMBILANG.g:631:2: ( () otherlv_1= 'Port' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) ) otherlv_35= '}' )
            {
            // InternalMBILANG.g:631:2: ( () otherlv_1= 'Port' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) ) otherlv_35= '}' )
            // InternalMBILANG.g:632:3: () otherlv_1= 'Port' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) ) otherlv_35= '}'
            {
            // InternalMBILANG.g:632:3: ()
            // InternalMBILANG.g:633:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPortAccess().getPortAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPortAccess().getPortKeyword_1());
            		
            // InternalMBILANG.g:643:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMBILANG.g:644:4: (lv_name_2_0= ruleEString )
            {
            // InternalMBILANG.g:644:4: (lv_name_2_0= ruleEString )
            // InternalMBILANG.g:645:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPortAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPortRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getPortAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMBILANG.g:666:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) ) )
            // InternalMBILANG.g:667:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) )
            {
            // InternalMBILANG.g:667:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* ) )
            // InternalMBILANG.g:668:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getPortAccess().getUnorderedGroup_4());
            				
            // InternalMBILANG.g:671:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )* )
            // InternalMBILANG.g:672:6: ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )*
            {
            // InternalMBILANG.g:672:6: ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )*
            loop11:
            do {
                int alt11=12;
                alt11 = dfa11.predict(input);
                switch (alt11) {
            	case 1 :
            	    // InternalMBILANG.g:673:4: ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:673:4: ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:674:5: {...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalMBILANG.g:674:101: ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:675:6: ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalMBILANG.g:678:9: ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:678:10: {...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:678:19: (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) )
            	    // InternalMBILANG.g:678:20: otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) )
            	    {
            	    otherlv_5=(Token)match(input,22,FOLLOW_3); 

            	    									newLeafNode(otherlv_5, grammarAccess.getPortAccess().getVariableNameKeyword_4_0_0());
            	    								
            	    // InternalMBILANG.g:682:9: ( (lv_variableName_6_0= ruleEString ) )
            	    // InternalMBILANG.g:683:10: (lv_variableName_6_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:683:10: (lv_variableName_6_0= ruleEString )
            	    // InternalMBILANG.g:684:11: lv_variableName_6_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getVariableNameEStringParserRuleCall_4_0_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_variableName_6_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"variableName",
            	    												lv_variableName_6_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMBILANG.g:707:4: ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:707:4: ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) )
            	    // InternalMBILANG.g:708:5: {...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalMBILANG.g:708:101: ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) )
            	    // InternalMBILANG.g:709:6: ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalMBILANG.g:712:9: ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) )
            	    // InternalMBILANG.g:712:10: {...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:712:19: (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) )
            	    // InternalMBILANG.g:712:20: otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) )
            	    {
            	    otherlv_7=(Token)match(input,23,FOLLOW_12); 

            	    									newLeafNode(otherlv_7, grammarAccess.getPortAccess().getDirectionKeyword_4_1_0());
            	    								
            	    // InternalMBILANG.g:716:9: ( (lv_direction_8_0= rulePortDirection ) )
            	    // InternalMBILANG.g:717:10: (lv_direction_8_0= rulePortDirection )
            	    {
            	    // InternalMBILANG.g:717:10: (lv_direction_8_0= rulePortDirection )
            	    // InternalMBILANG.g:718:11: lv_direction_8_0= rulePortDirection
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getDirectionPortDirectionEnumRuleCall_4_1_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_direction_8_0=rulePortDirection();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"direction",
            	    												lv_direction_8_0,
            	    												"fr.inria.glose.mbilang.MBILANG.PortDirection");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalMBILANG.g:741:4: ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:741:4: ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) )
            	    // InternalMBILANG.g:742:5: {...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 2) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 2)");
            	    }
            	    // InternalMBILANG.g:742:101: ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) )
            	    // InternalMBILANG.g:743:6: ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 2);
            	    					
            	    // InternalMBILANG.g:746:9: ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) )
            	    // InternalMBILANG.g:746:10: {...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:746:19: (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) )
            	    // InternalMBILANG.g:746:20: otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) )
            	    {
            	    otherlv_9=(Token)match(input,24,FOLLOW_13); 

            	    									newLeafNode(otherlv_9, grammarAccess.getPortAccess().getNatureKeyword_4_2_0());
            	    								
            	    // InternalMBILANG.g:750:9: ( (lv_nature_10_0= ruleDataNature ) )
            	    // InternalMBILANG.g:751:10: (lv_nature_10_0= ruleDataNature )
            	    {
            	    // InternalMBILANG.g:751:10: (lv_nature_10_0= ruleDataNature )
            	    // InternalMBILANG.g:752:11: lv_nature_10_0= ruleDataNature
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getNatureDataNatureEnumRuleCall_4_2_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_nature_10_0=ruleDataNature();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"nature",
            	    												lv_nature_10_0,
            	    												"fr.inria.glose.mbilang.MBILANG.DataNature");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalMBILANG.g:775:4: ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:775:4: ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) )
            	    // InternalMBILANG.g:776:5: {...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 3) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 3)");
            	    }
            	    // InternalMBILANG.g:776:101: ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) )
            	    // InternalMBILANG.g:777:6: ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 3);
            	    					
            	    // InternalMBILANG.g:780:9: ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) )
            	    // InternalMBILANG.g:780:10: {...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:780:19: (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) )
            	    // InternalMBILANG.g:780:20: otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) )
            	    {
            	    otherlv_11=(Token)match(input,25,FOLLOW_14); 

            	    									newLeafNode(otherlv_11, grammarAccess.getPortAccess().getTypeKeyword_4_3_0());
            	    								
            	    // InternalMBILANG.g:784:9: ( (lv_type_12_0= ruleDataType ) )
            	    // InternalMBILANG.g:785:10: (lv_type_12_0= ruleDataType )
            	    {
            	    // InternalMBILANG.g:785:10: (lv_type_12_0= ruleDataType )
            	    // InternalMBILANG.g:786:11: lv_type_12_0= ruleDataType
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getTypeDataTypeEnumRuleCall_4_3_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_type_12_0=ruleDataType();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"type",
            	    												lv_type_12_0,
            	    												"fr.inria.glose.mbilang.MBILANG.DataType");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalMBILANG.g:809:4: ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:809:4: ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:810:5: {...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 4) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 4)");
            	    }
            	    // InternalMBILANG.g:810:101: ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:811:6: ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 4);
            	    					
            	    // InternalMBILANG.g:814:9: ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:814:10: {...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:814:19: (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) )
            	    // InternalMBILANG.g:814:20: otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) )
            	    {
            	    otherlv_13=(Token)match(input,26,FOLLOW_3); 

            	    									newLeafNode(otherlv_13, grammarAccess.getPortAccess().getRTDKeyword_4_4_0());
            	    								
            	    // InternalMBILANG.g:818:9: ( (lv_RTD_14_0= ruleEString ) )
            	    // InternalMBILANG.g:819:10: (lv_RTD_14_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:819:10: (lv_RTD_14_0= ruleEString )
            	    // InternalMBILANG.g:820:11: lv_RTD_14_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getRTDEStringParserRuleCall_4_4_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_RTD_14_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"RTD",
            	    												lv_RTD_14_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 6 :
            	    // InternalMBILANG.g:843:4: ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:843:4: ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:844:5: {...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 5) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 5)");
            	    }
            	    // InternalMBILANG.g:844:101: ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:845:6: ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 5);
            	    					
            	    // InternalMBILANG.g:848:9: ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:848:10: {...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:848:19: (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) )
            	    // InternalMBILANG.g:848:20: otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) )
            	    {
            	    otherlv_15=(Token)match(input,27,FOLLOW_3); 

            	    									newLeafNode(otherlv_15, grammarAccess.getPortAccess().getInitValueKeyword_4_5_0());
            	    								
            	    // InternalMBILANG.g:852:9: ( (lv_initValue_16_0= ruleEString ) )
            	    // InternalMBILANG.g:853:10: (lv_initValue_16_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:853:10: (lv_initValue_16_0= ruleEString )
            	    // InternalMBILANG.g:854:11: lv_initValue_16_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getInitValueEStringParserRuleCall_4_5_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_initValue_16_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"initValue",
            	    												lv_initValue_16_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 7 :
            	    // InternalMBILANG.g:877:4: ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:877:4: ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:878:5: {...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 6) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 6)");
            	    }
            	    // InternalMBILANG.g:878:101: ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:879:6: ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 6);
            	    					
            	    // InternalMBILANG.g:882:9: ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) )
            	    // InternalMBILANG.g:882:10: {...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:882:19: (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) )
            	    // InternalMBILANG.g:882:20: otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) )
            	    {
            	    otherlv_17=(Token)match(input,28,FOLLOW_3); 

            	    									newLeafNode(otherlv_17, grammarAccess.getPortAccess().getPlotterColorKeyword_4_6_0());
            	    								
            	    // InternalMBILANG.g:886:9: ( (lv_color_18_0= ruleEString ) )
            	    // InternalMBILANG.g:887:10: (lv_color_18_0= ruleEString )
            	    {
            	    // InternalMBILANG.g:887:10: (lv_color_18_0= ruleEString )
            	    // InternalMBILANG.g:888:11: lv_color_18_0= ruleEString
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getColorEStringParserRuleCall_4_6_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_color_18_0=ruleEString();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"color",
            	    												lv_color_18_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EString");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 8 :
            	    // InternalMBILANG.g:911:4: ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:911:4: ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) )
            	    // InternalMBILANG.g:912:5: {...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 7) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 7)");
            	    }
            	    // InternalMBILANG.g:912:101: ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) )
            	    // InternalMBILANG.g:913:6: ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 7);
            	    					
            	    // InternalMBILANG.g:916:9: ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) )
            	    // InternalMBILANG.g:916:10: {...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:916:19: (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) )
            	    // InternalMBILANG.g:916:20: otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) )
            	    {
            	    otherlv_19=(Token)match(input,29,FOLLOW_15); 

            	    									newLeafNode(otherlv_19, grammarAccess.getPortAccess().getMonitoredKeyword_4_7_0());
            	    								
            	    // InternalMBILANG.g:920:9: ( (lv_monitored_20_0= ruleEBoolean ) )
            	    // InternalMBILANG.g:921:10: (lv_monitored_20_0= ruleEBoolean )
            	    {
            	    // InternalMBILANG.g:921:10: (lv_monitored_20_0= ruleEBoolean )
            	    // InternalMBILANG.g:922:11: lv_monitored_20_0= ruleEBoolean
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getMonitoredEBooleanParserRuleCall_4_7_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    lv_monitored_20_0=ruleEBoolean();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											set(
            	    												current,
            	    												"monitored",
            	    												lv_monitored_20_0,
            	    												"fr.inria.glose.mbilang.MBILANG.EBoolean");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 9 :
            	    // InternalMBILANG.g:945:4: ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) )
            	    {
            	    // InternalMBILANG.g:945:4: ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) )
            	    // InternalMBILANG.g:946:5: {...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 8) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 8)");
            	    }
            	    // InternalMBILANG.g:946:101: ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) )
            	    // InternalMBILANG.g:947:6: ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 8);
            	    					
            	    // InternalMBILANG.g:950:9: ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) )
            	    // InternalMBILANG.g:950:10: {...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:950:19: (otherlv_21= 'temporalreference' ( ( ruleEString ) ) )
            	    // InternalMBILANG.g:950:20: otherlv_21= 'temporalreference' ( ( ruleEString ) )
            	    {
            	    otherlv_21=(Token)match(input,30,FOLLOW_3); 

            	    									newLeafNode(otherlv_21, grammarAccess.getPortAccess().getTemporalreferenceKeyword_4_8_0());
            	    								
            	    // InternalMBILANG.g:954:9: ( ( ruleEString ) )
            	    // InternalMBILANG.g:955:10: ( ruleEString )
            	    {
            	    // InternalMBILANG.g:955:10: ( ruleEString )
            	    // InternalMBILANG.g:956:11: ruleEString
            	    {

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getPortRule());
            	    											}
            	    										

            	    											newCompositeNode(grammarAccess.getPortAccess().getTemporalreferenceModelTemporalReferenceCrossReference_4_8_1_0());
            	    										
            	    pushFollow(FOLLOW_11);
            	    ruleEString();

            	    state._fsp--;


            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 10 :
            	    // InternalMBILANG.g:976:4: ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) )
            	    {
            	    // InternalMBILANG.g:976:4: ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) )
            	    // InternalMBILANG.g:977:5: {...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 9) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 9)");
            	    }
            	    // InternalMBILANG.g:977:101: ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) )
            	    // InternalMBILANG.g:978:6: ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 9);
            	    					
            	    // InternalMBILANG.g:981:9: ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) )
            	    // InternalMBILANG.g:981:10: {...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:981:19: (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' )
            	    // InternalMBILANG.g:981:20: otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}'
            	    {
            	    otherlv_23=(Token)match(input,31,FOLLOW_4); 

            	    									newLeafNode(otherlv_23, grammarAccess.getPortAccess().getIoeventsKeyword_4_9_0());
            	    								
            	    otherlv_24=(Token)match(input,12,FOLLOW_16); 

            	    									newLeafNode(otherlv_24, grammarAccess.getPortAccess().getLeftCurlyBracketKeyword_4_9_1());
            	    								
            	    // InternalMBILANG.g:989:9: ( (lv_ioevents_25_0= ruleIOEvent ) )
            	    // InternalMBILANG.g:990:10: (lv_ioevents_25_0= ruleIOEvent )
            	    {
            	    // InternalMBILANG.g:990:10: (lv_ioevents_25_0= ruleIOEvent )
            	    // InternalMBILANG.g:991:11: lv_ioevents_25_0= ruleIOEvent
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getIoeventsIOEventParserRuleCall_4_9_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_ioevents_25_0=ruleIOEvent();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											add(
            	    												current,
            	    												"ioevents",
            	    												lv_ioevents_25_0,
            	    												"fr.inria.glose.mbilang.MBILANG.IOEvent");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalMBILANG.g:1008:9: (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )*
            	    loop9:
            	    do {
            	        int alt9=2;
            	        int LA9_0 = input.LA(1);

            	        if ( (LA9_0==17) ) {
            	            alt9=1;
            	        }


            	        switch (alt9) {
            	    	case 1 :
            	    	    // InternalMBILANG.g:1009:10: otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) )
            	    	    {
            	    	    otherlv_26=(Token)match(input,17,FOLLOW_16); 

            	    	    										newLeafNode(otherlv_26, grammarAccess.getPortAccess().getCommaKeyword_4_9_3_0());
            	    	    									
            	    	    // InternalMBILANG.g:1013:10: ( (lv_ioevents_27_0= ruleIOEvent ) )
            	    	    // InternalMBILANG.g:1014:11: (lv_ioevents_27_0= ruleIOEvent )
            	    	    {
            	    	    // InternalMBILANG.g:1014:11: (lv_ioevents_27_0= ruleIOEvent )
            	    	    // InternalMBILANG.g:1015:12: lv_ioevents_27_0= ruleIOEvent
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getPortAccess().getIoeventsIOEventParserRuleCall_4_9_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_ioevents_27_0=ruleIOEvent();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getPortRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"ioevents",
            	    	    													lv_ioevents_27_0,
            	    	    													"fr.inria.glose.mbilang.MBILANG.IOEvent");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop9;
            	        }
            	    } while (true);

            	    otherlv_28=(Token)match(input,18,FOLLOW_11); 

            	    									newLeafNode(otherlv_28, grammarAccess.getPortAccess().getRightCurlyBracketKeyword_4_9_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 11 :
            	    // InternalMBILANG.g:1043:4: ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) )
            	    {
            	    // InternalMBILANG.g:1043:4: ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) )
            	    // InternalMBILANG.g:1044:5: {...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 10) ) {
            	        throw new FailedPredicateException(input, "rulePort", "getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 10)");
            	    }
            	    // InternalMBILANG.g:1044:102: ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) )
            	    // InternalMBILANG.g:1045:6: ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getPortAccess().getUnorderedGroup_4(), 10);
            	    					
            	    // InternalMBILANG.g:1048:9: ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) )
            	    // InternalMBILANG.g:1048:10: {...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "rulePort", "true");
            	    }
            	    // InternalMBILANG.g:1048:19: (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' )
            	    // InternalMBILANG.g:1048:20: otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}'
            	    {
            	    otherlv_29=(Token)match(input,20,FOLLOW_4); 

            	    									newLeafNode(otherlv_29, grammarAccess.getPortAccess().getPropertiesKeyword_4_10_0());
            	    								
            	    otherlv_30=(Token)match(input,12,FOLLOW_17); 

            	    									newLeafNode(otherlv_30, grammarAccess.getPortAccess().getLeftCurlyBracketKeyword_4_10_1());
            	    								
            	    // InternalMBILANG.g:1056:9: ( (lv_properties_31_0= rulePortProperty ) )
            	    // InternalMBILANG.g:1057:10: (lv_properties_31_0= rulePortProperty )
            	    {
            	    // InternalMBILANG.g:1057:10: (lv_properties_31_0= rulePortProperty )
            	    // InternalMBILANG.g:1058:11: lv_properties_31_0= rulePortProperty
            	    {

            	    											newCompositeNode(grammarAccess.getPortAccess().getPropertiesPortPropertyParserRuleCall_4_10_2_0());
            	    										
            	    pushFollow(FOLLOW_8);
            	    lv_properties_31_0=rulePortProperty();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getPortRule());
            	    											}
            	    											add(
            	    												current,
            	    												"properties",
            	    												lv_properties_31_0,
            	    												"fr.inria.glose.mbilang.MBILANG.PortProperty");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }

            	    // InternalMBILANG.g:1075:9: (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )*
            	    loop10:
            	    do {
            	        int alt10=2;
            	        int LA10_0 = input.LA(1);

            	        if ( (LA10_0==17) ) {
            	            alt10=1;
            	        }


            	        switch (alt10) {
            	    	case 1 :
            	    	    // InternalMBILANG.g:1076:10: otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) )
            	    	    {
            	    	    otherlv_32=(Token)match(input,17,FOLLOW_17); 

            	    	    										newLeafNode(otherlv_32, grammarAccess.getPortAccess().getCommaKeyword_4_10_3_0());
            	    	    									
            	    	    // InternalMBILANG.g:1080:10: ( (lv_properties_33_0= rulePortProperty ) )
            	    	    // InternalMBILANG.g:1081:11: (lv_properties_33_0= rulePortProperty )
            	    	    {
            	    	    // InternalMBILANG.g:1081:11: (lv_properties_33_0= rulePortProperty )
            	    	    // InternalMBILANG.g:1082:12: lv_properties_33_0= rulePortProperty
            	    	    {

            	    	    												newCompositeNode(grammarAccess.getPortAccess().getPropertiesPortPropertyParserRuleCall_4_10_3_1_0());
            	    	    											
            	    	    pushFollow(FOLLOW_8);
            	    	    lv_properties_33_0=rulePortProperty();

            	    	    state._fsp--;


            	    	    												if (current==null) {
            	    	    													current = createModelElementForParent(grammarAccess.getPortRule());
            	    	    												}
            	    	    												add(
            	    	    													current,
            	    	    													"properties",
            	    	    													lv_properties_33_0,
            	    	    													"fr.inria.glose.mbilang.MBILANG.PortProperty");
            	    	    												afterParserOrEnumRuleCall();
            	    	    											

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop10;
            	        }
            	    } while (true);

            	    otherlv_34=(Token)match(input,18,FOLLOW_11); 

            	    									newLeafNode(otherlv_34, grammarAccess.getPortAccess().getRightCurlyBracketKeyword_4_10_4());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getPortAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getPortAccess().getUnorderedGroup_4());
            				

            }

            otherlv_35=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_35, grammarAccess.getPortAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePort"


    // $ANTLR start "entryRuleModelTemporalReference"
    // InternalMBILANG.g:1125:1: entryRuleModelTemporalReference returns [EObject current=null] : iv_ruleModelTemporalReference= ruleModelTemporalReference EOF ;
    public final EObject entryRuleModelTemporalReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModelTemporalReference = null;


        try {
            // InternalMBILANG.g:1125:63: (iv_ruleModelTemporalReference= ruleModelTemporalReference EOF )
            // InternalMBILANG.g:1126:2: iv_ruleModelTemporalReference= ruleModelTemporalReference EOF
            {
             newCompositeNode(grammarAccess.getModelTemporalReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModelTemporalReference=ruleModelTemporalReference();

            state._fsp--;

             current =iv_ruleModelTemporalReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModelTemporalReference"


    // $ANTLR start "ruleModelTemporalReference"
    // InternalMBILANG.g:1132:1: ruleModelTemporalReference returns [EObject current=null] : ( () otherlv_1= 'ModelTemporalReference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleModelTemporalReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        Enumerator lv_reference_5_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:1138:2: ( ( () otherlv_1= 'ModelTemporalReference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )? otherlv_6= '}' ) )
            // InternalMBILANG.g:1139:2: ( () otherlv_1= 'ModelTemporalReference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )? otherlv_6= '}' )
            {
            // InternalMBILANG.g:1139:2: ( () otherlv_1= 'ModelTemporalReference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )? otherlv_6= '}' )
            // InternalMBILANG.g:1140:3: () otherlv_1= 'ModelTemporalReference' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )? otherlv_6= '}'
            {
            // InternalMBILANG.g:1140:3: ()
            // InternalMBILANG.g:1141:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getModelTemporalReferenceAccess().getModelTemporalReferenceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getModelTemporalReferenceAccess().getModelTemporalReferenceKeyword_1());
            		
            // InternalMBILANG.g:1151:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMBILANG.g:1152:4: (lv_name_2_0= ruleEString )
            {
            // InternalMBILANG.g:1152:4: (lv_name_2_0= ruleEString )
            // InternalMBILANG.g:1153:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getModelTemporalReferenceAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelTemporalReferenceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_18); 

            			newLeafNode(otherlv_3, grammarAccess.getModelTemporalReferenceAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMBILANG.g:1174:3: (otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==33) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalMBILANG.g:1175:4: otherlv_4= 'reference' ( (lv_reference_5_0= ruleTemporalReference ) )
                    {
                    otherlv_4=(Token)match(input,33,FOLLOW_19); 

                    				newLeafNode(otherlv_4, grammarAccess.getModelTemporalReferenceAccess().getReferenceKeyword_4_0());
                    			
                    // InternalMBILANG.g:1179:4: ( (lv_reference_5_0= ruleTemporalReference ) )
                    // InternalMBILANG.g:1180:5: (lv_reference_5_0= ruleTemporalReference )
                    {
                    // InternalMBILANG.g:1180:5: (lv_reference_5_0= ruleTemporalReference )
                    // InternalMBILANG.g:1181:6: lv_reference_5_0= ruleTemporalReference
                    {

                    						newCompositeNode(grammarAccess.getModelTemporalReferenceAccess().getReferenceTemporalReferenceEnumRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_reference_5_0=ruleTemporalReference();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getModelTemporalReferenceRule());
                    						}
                    						set(
                    							current,
                    							"reference",
                    							lv_reference_5_0,
                    							"fr.inria.glose.mbilang.MBILANG.TemporalReference");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getModelTemporalReferenceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModelTemporalReference"


    // $ANTLR start "entryRuleUpdated"
    // InternalMBILANG.g:1207:1: entryRuleUpdated returns [EObject current=null] : iv_ruleUpdated= ruleUpdated EOF ;
    public final EObject entryRuleUpdated() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUpdated = null;


        try {
            // InternalMBILANG.g:1207:48: (iv_ruleUpdated= ruleUpdated EOF )
            // InternalMBILANG.g:1208:2: iv_ruleUpdated= ruleUpdated EOF
            {
             newCompositeNode(grammarAccess.getUpdatedRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUpdated=ruleUpdated();

            state._fsp--;

             current =iv_ruleUpdated; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUpdated"


    // $ANTLR start "ruleUpdated"
    // InternalMBILANG.g:1214:1: ruleUpdated returns [EObject current=null] : ( () otherlv_1= 'Updated' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleUpdated() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:1220:2: ( ( () otherlv_1= 'Updated' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalMBILANG.g:1221:2: ( () otherlv_1= 'Updated' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalMBILANG.g:1221:2: ( () otherlv_1= 'Updated' ( (lv_name_2_0= ruleEString ) ) )
            // InternalMBILANG.g:1222:3: () otherlv_1= 'Updated' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalMBILANG.g:1222:3: ()
            // InternalMBILANG.g:1223:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUpdatedAccess().getUpdatedAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getUpdatedAccess().getUpdatedKeyword_1());
            		
            // InternalMBILANG.g:1233:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMBILANG.g:1234:4: (lv_name_2_0= ruleEString )
            {
            // InternalMBILANG.g:1234:4: (lv_name_2_0= ruleEString )
            // InternalMBILANG.g:1235:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getUpdatedAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUpdatedRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUpdated"


    // $ANTLR start "entryRuleReadyToRead"
    // InternalMBILANG.g:1256:1: entryRuleReadyToRead returns [EObject current=null] : iv_ruleReadyToRead= ruleReadyToRead EOF ;
    public final EObject entryRuleReadyToRead() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReadyToRead = null;


        try {
            // InternalMBILANG.g:1256:52: (iv_ruleReadyToRead= ruleReadyToRead EOF )
            // InternalMBILANG.g:1257:2: iv_ruleReadyToRead= ruleReadyToRead EOF
            {
             newCompositeNode(grammarAccess.getReadyToReadRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReadyToRead=ruleReadyToRead();

            state._fsp--;

             current =iv_ruleReadyToRead; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReadyToRead"


    // $ANTLR start "ruleReadyToRead"
    // InternalMBILANG.g:1263:1: ruleReadyToRead returns [EObject current=null] : ( () otherlv_1= 'ReadyToRead' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleReadyToRead() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:1269:2: ( ( () otherlv_1= 'ReadyToRead' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalMBILANG.g:1270:2: ( () otherlv_1= 'ReadyToRead' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalMBILANG.g:1270:2: ( () otherlv_1= 'ReadyToRead' ( (lv_name_2_0= ruleEString ) ) )
            // InternalMBILANG.g:1271:3: () otherlv_1= 'ReadyToRead' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalMBILANG.g:1271:3: ()
            // InternalMBILANG.g:1272:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getReadyToReadAccess().getReadyToReadAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,35,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getReadyToReadAccess().getReadyToReadKeyword_1());
            		
            // InternalMBILANG.g:1282:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMBILANG.g:1283:4: (lv_name_2_0= ruleEString )
            {
            // InternalMBILANG.g:1283:4: (lv_name_2_0= ruleEString )
            // InternalMBILANG.g:1284:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getReadyToReadAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReadyToReadRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReadyToRead"


    // $ANTLR start "entryRuleTriggered"
    // InternalMBILANG.g:1305:1: entryRuleTriggered returns [EObject current=null] : iv_ruleTriggered= ruleTriggered EOF ;
    public final EObject entryRuleTriggered() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTriggered = null;


        try {
            // InternalMBILANG.g:1305:50: (iv_ruleTriggered= ruleTriggered EOF )
            // InternalMBILANG.g:1306:2: iv_ruleTriggered= ruleTriggered EOF
            {
             newCompositeNode(grammarAccess.getTriggeredRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTriggered=ruleTriggered();

            state._fsp--;

             current =iv_ruleTriggered; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTriggered"


    // $ANTLR start "ruleTriggered"
    // InternalMBILANG.g:1312:1: ruleTriggered returns [EObject current=null] : ( () otherlv_1= 'Triggered' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleTriggered() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalMBILANG.g:1318:2: ( ( () otherlv_1= 'Triggered' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalMBILANG.g:1319:2: ( () otherlv_1= 'Triggered' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalMBILANG.g:1319:2: ( () otherlv_1= 'Triggered' ( (lv_name_2_0= ruleEString ) ) )
            // InternalMBILANG.g:1320:3: () otherlv_1= 'Triggered' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalMBILANG.g:1320:3: ()
            // InternalMBILANG.g:1321:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTriggeredAccess().getTriggeredAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,36,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTriggeredAccess().getTriggeredKeyword_1());
            		
            // InternalMBILANG.g:1331:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMBILANG.g:1332:4: (lv_name_2_0= ruleEString )
            {
            // InternalMBILANG.g:1332:4: (lv_name_2_0= ruleEString )
            // InternalMBILANG.g:1333:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTriggeredAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTriggeredRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.inria.glose.mbilang.MBILANG.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTriggered"


    // $ANTLR start "entryRuleZeroCrossingDetection"
    // InternalMBILANG.g:1354:1: entryRuleZeroCrossingDetection returns [EObject current=null] : iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF ;
    public final EObject entryRuleZeroCrossingDetection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleZeroCrossingDetection = null;


        try {
            // InternalMBILANG.g:1354:62: (iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF )
            // InternalMBILANG.g:1355:2: iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF
            {
             newCompositeNode(grammarAccess.getZeroCrossingDetectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleZeroCrossingDetection=ruleZeroCrossingDetection();

            state._fsp--;

             current =iv_ruleZeroCrossingDetection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZeroCrossingDetection"


    // $ANTLR start "ruleZeroCrossingDetection"
    // InternalMBILANG.g:1361:1: ruleZeroCrossingDetection returns [EObject current=null] : ( () otherlv_1= 'ZeroCrossingDetection' ) ;
    public final EObject ruleZeroCrossingDetection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1367:2: ( ( () otherlv_1= 'ZeroCrossingDetection' ) )
            // InternalMBILANG.g:1368:2: ( () otherlv_1= 'ZeroCrossingDetection' )
            {
            // InternalMBILANG.g:1368:2: ( () otherlv_1= 'ZeroCrossingDetection' )
            // InternalMBILANG.g:1369:3: () otherlv_1= 'ZeroCrossingDetection'
            {
            // InternalMBILANG.g:1369:3: ()
            // InternalMBILANG.g:1370:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,37,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZeroCrossingDetection"


    // $ANTLR start "entryRuleExtrapolation"
    // InternalMBILANG.g:1384:1: entryRuleExtrapolation returns [EObject current=null] : iv_ruleExtrapolation= ruleExtrapolation EOF ;
    public final EObject entryRuleExtrapolation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtrapolation = null;


        try {
            // InternalMBILANG.g:1384:54: (iv_ruleExtrapolation= ruleExtrapolation EOF )
            // InternalMBILANG.g:1385:2: iv_ruleExtrapolation= ruleExtrapolation EOF
            {
             newCompositeNode(grammarAccess.getExtrapolationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExtrapolation=ruleExtrapolation();

            state._fsp--;

             current =iv_ruleExtrapolation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtrapolation"


    // $ANTLR start "ruleExtrapolation"
    // InternalMBILANG.g:1391:1: ruleExtrapolation returns [EObject current=null] : ( () otherlv_1= 'Extrapolation' ) ;
    public final EObject ruleExtrapolation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1397:2: ( ( () otherlv_1= 'Extrapolation' ) )
            // InternalMBILANG.g:1398:2: ( () otherlv_1= 'Extrapolation' )
            {
            // InternalMBILANG.g:1398:2: ( () otherlv_1= 'Extrapolation' )
            // InternalMBILANG.g:1399:3: () otherlv_1= 'Extrapolation'
            {
            // InternalMBILANG.g:1399:3: ()
            // InternalMBILANG.g:1400:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getExtrapolationAccess().getExtrapolationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,38,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getExtrapolationAccess().getExtrapolationKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtrapolation"


    // $ANTLR start "entryRuleInterpolation"
    // InternalMBILANG.g:1414:1: entryRuleInterpolation returns [EObject current=null] : iv_ruleInterpolation= ruleInterpolation EOF ;
    public final EObject entryRuleInterpolation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterpolation = null;


        try {
            // InternalMBILANG.g:1414:54: (iv_ruleInterpolation= ruleInterpolation EOF )
            // InternalMBILANG.g:1415:2: iv_ruleInterpolation= ruleInterpolation EOF
            {
             newCompositeNode(grammarAccess.getInterpolationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterpolation=ruleInterpolation();

            state._fsp--;

             current =iv_ruleInterpolation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterpolation"


    // $ANTLR start "ruleInterpolation"
    // InternalMBILANG.g:1421:1: ruleInterpolation returns [EObject current=null] : ( () otherlv_1= 'Interpolation' ) ;
    public final EObject ruleInterpolation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1427:2: ( ( () otherlv_1= 'Interpolation' ) )
            // InternalMBILANG.g:1428:2: ( () otherlv_1= 'Interpolation' )
            {
            // InternalMBILANG.g:1428:2: ( () otherlv_1= 'Interpolation' )
            // InternalMBILANG.g:1429:3: () otherlv_1= 'Interpolation'
            {
            // InternalMBILANG.g:1429:3: ()
            // InternalMBILANG.g:1430:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInterpolationAccess().getInterpolationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getInterpolationAccess().getInterpolationKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterpolation"


    // $ANTLR start "entryRuleDiscontinuityLocator"
    // InternalMBILANG.g:1444:1: entryRuleDiscontinuityLocator returns [EObject current=null] : iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF ;
    public final EObject entryRuleDiscontinuityLocator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDiscontinuityLocator = null;


        try {
            // InternalMBILANG.g:1444:61: (iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF )
            // InternalMBILANG.g:1445:2: iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF
            {
             newCompositeNode(grammarAccess.getDiscontinuityLocatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDiscontinuityLocator=ruleDiscontinuityLocator();

            state._fsp--;

             current =iv_ruleDiscontinuityLocator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDiscontinuityLocator"


    // $ANTLR start "ruleDiscontinuityLocator"
    // InternalMBILANG.g:1451:1: ruleDiscontinuityLocator returns [EObject current=null] : ( () otherlv_1= 'DiscontinuityLocator' ) ;
    public final EObject ruleDiscontinuityLocator() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1457:2: ( ( () otherlv_1= 'DiscontinuityLocator' ) )
            // InternalMBILANG.g:1458:2: ( () otherlv_1= 'DiscontinuityLocator' )
            {
            // InternalMBILANG.g:1458:2: ( () otherlv_1= 'DiscontinuityLocator' )
            // InternalMBILANG.g:1459:3: () otherlv_1= 'DiscontinuityLocator'
            {
            // InternalMBILANG.g:1459:3: ()
            // InternalMBILANG.g:1460:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,40,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDiscontinuityLocator"


    // $ANTLR start "entryRuleHistoryProvider"
    // InternalMBILANG.g:1474:1: entryRuleHistoryProvider returns [EObject current=null] : iv_ruleHistoryProvider= ruleHistoryProvider EOF ;
    public final EObject entryRuleHistoryProvider() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHistoryProvider = null;


        try {
            // InternalMBILANG.g:1474:56: (iv_ruleHistoryProvider= ruleHistoryProvider EOF )
            // InternalMBILANG.g:1475:2: iv_ruleHistoryProvider= ruleHistoryProvider EOF
            {
             newCompositeNode(grammarAccess.getHistoryProviderRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHistoryProvider=ruleHistoryProvider();

            state._fsp--;

             current =iv_ruleHistoryProvider; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHistoryProvider"


    // $ANTLR start "ruleHistoryProvider"
    // InternalMBILANG.g:1481:1: ruleHistoryProvider returns [EObject current=null] : ( () otherlv_1= 'HistoryProvider' ) ;
    public final EObject ruleHistoryProvider() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1487:2: ( ( () otherlv_1= 'HistoryProvider' ) )
            // InternalMBILANG.g:1488:2: ( () otherlv_1= 'HistoryProvider' )
            {
            // InternalMBILANG.g:1488:2: ( () otherlv_1= 'HistoryProvider' )
            // InternalMBILANG.g:1489:3: () otherlv_1= 'HistoryProvider'
            {
            // InternalMBILANG.g:1489:3: ()
            // InternalMBILANG.g:1490:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getHistoryProviderAccess().getHistoryProviderAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,41,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getHistoryProviderAccess().getHistoryProviderKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHistoryProvider"


    // $ANTLR start "entryRuleRollback"
    // InternalMBILANG.g:1504:1: entryRuleRollback returns [EObject current=null] : iv_ruleRollback= ruleRollback EOF ;
    public final EObject entryRuleRollback() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRollback = null;


        try {
            // InternalMBILANG.g:1504:49: (iv_ruleRollback= ruleRollback EOF )
            // InternalMBILANG.g:1505:2: iv_ruleRollback= ruleRollback EOF
            {
             newCompositeNode(grammarAccess.getRollbackRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRollback=ruleRollback();

            state._fsp--;

             current =iv_ruleRollback; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRollback"


    // $ANTLR start "ruleRollback"
    // InternalMBILANG.g:1511:1: ruleRollback returns [EObject current=null] : ( () otherlv_1= 'Rollback' ) ;
    public final EObject ruleRollback() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1517:2: ( ( () otherlv_1= 'Rollback' ) )
            // InternalMBILANG.g:1518:2: ( () otherlv_1= 'Rollback' )
            {
            // InternalMBILANG.g:1518:2: ( () otherlv_1= 'Rollback' )
            // InternalMBILANG.g:1519:3: () otherlv_1= 'Rollback'
            {
            // InternalMBILANG.g:1519:3: ()
            // InternalMBILANG.g:1520:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRollbackAccess().getRollbackAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getRollbackAccess().getRollbackKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRollback"


    // $ANTLR start "entryRuleSuperDenseTime"
    // InternalMBILANG.g:1534:1: entryRuleSuperDenseTime returns [EObject current=null] : iv_ruleSuperDenseTime= ruleSuperDenseTime EOF ;
    public final EObject entryRuleSuperDenseTime() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSuperDenseTime = null;


        try {
            // InternalMBILANG.g:1534:55: (iv_ruleSuperDenseTime= ruleSuperDenseTime EOF )
            // InternalMBILANG.g:1535:2: iv_ruleSuperDenseTime= ruleSuperDenseTime EOF
            {
             newCompositeNode(grammarAccess.getSuperDenseTimeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSuperDenseTime=ruleSuperDenseTime();

            state._fsp--;

             current =iv_ruleSuperDenseTime; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSuperDenseTime"


    // $ANTLR start "ruleSuperDenseTime"
    // InternalMBILANG.g:1541:1: ruleSuperDenseTime returns [EObject current=null] : ( () otherlv_1= 'SuperDenseTime' ) ;
    public final EObject ruleSuperDenseTime() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1547:2: ( ( () otherlv_1= 'SuperDenseTime' ) )
            // InternalMBILANG.g:1548:2: ( () otherlv_1= 'SuperDenseTime' )
            {
            // InternalMBILANG.g:1548:2: ( () otherlv_1= 'SuperDenseTime' )
            // InternalMBILANG.g:1549:3: () otherlv_1= 'SuperDenseTime'
            {
            // InternalMBILANG.g:1549:3: ()
            // InternalMBILANG.g:1550:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSuperDenseTimeAccess().getSuperDenseTimeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,43,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getSuperDenseTimeAccess().getSuperDenseTimeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSuperDenseTime"


    // $ANTLR start "entryRuleDeltaStepSize"
    // InternalMBILANG.g:1564:1: entryRuleDeltaStepSize returns [EObject current=null] : iv_ruleDeltaStepSize= ruleDeltaStepSize EOF ;
    public final EObject entryRuleDeltaStepSize() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeltaStepSize = null;


        try {
            // InternalMBILANG.g:1564:54: (iv_ruleDeltaStepSize= ruleDeltaStepSize EOF )
            // InternalMBILANG.g:1565:2: iv_ruleDeltaStepSize= ruleDeltaStepSize EOF
            {
             newCompositeNode(grammarAccess.getDeltaStepSizeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeltaStepSize=ruleDeltaStepSize();

            state._fsp--;

             current =iv_ruleDeltaStepSize; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeltaStepSize"


    // $ANTLR start "ruleDeltaStepSize"
    // InternalMBILANG.g:1571:1: ruleDeltaStepSize returns [EObject current=null] : ( () otherlv_1= 'DeltaStepSize' ) ;
    public final EObject ruleDeltaStepSize() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1577:2: ( ( () otherlv_1= 'DeltaStepSize' ) )
            // InternalMBILANG.g:1578:2: ( () otherlv_1= 'DeltaStepSize' )
            {
            // InternalMBILANG.g:1578:2: ( () otherlv_1= 'DeltaStepSize' )
            // InternalMBILANG.g:1579:3: () otherlv_1= 'DeltaStepSize'
            {
            // InternalMBILANG.g:1579:3: ()
            // InternalMBILANG.g:1580:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDeltaStepSizeAccess().getDeltaStepSizeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,44,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDeltaStepSizeAccess().getDeltaStepSizeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeltaStepSize"


    // $ANTLR start "entryRuleEBoolean"
    // InternalMBILANG.g:1594:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalMBILANG.g:1594:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalMBILANG.g:1595:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
             newCompositeNode(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;

             current =iv_ruleEBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalMBILANG.g:1601:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1607:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalMBILANG.g:1608:2: (kw= 'true' | kw= 'false' )
            {
            // InternalMBILANG.g:1608:2: (kw= 'true' | kw= 'false' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==45) ) {
                alt13=1;
            }
            else if ( (LA13_0==46) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalMBILANG.g:1609:3: kw= 'true'
                    {
                    kw=(Token)match(input,45,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:1615:3: kw= 'false'
                    {
                    kw=(Token)match(input,46,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "ruleDataNature"
    // InternalMBILANG.g:1624:1: ruleDataNature returns [Enumerator current=null] : ( (enumLiteral_0= 'constant' ) | (enumLiteral_1= 'transient' ) | (enumLiteral_2= 'piecewiseConstant' ) | (enumLiteral_3= 'piecewiseContinuous' ) | (enumLiteral_4= 'continuous' ) ) ;
    public final Enumerator ruleDataNature() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1630:2: ( ( (enumLiteral_0= 'constant' ) | (enumLiteral_1= 'transient' ) | (enumLiteral_2= 'piecewiseConstant' ) | (enumLiteral_3= 'piecewiseContinuous' ) | (enumLiteral_4= 'continuous' ) ) )
            // InternalMBILANG.g:1631:2: ( (enumLiteral_0= 'constant' ) | (enumLiteral_1= 'transient' ) | (enumLiteral_2= 'piecewiseConstant' ) | (enumLiteral_3= 'piecewiseContinuous' ) | (enumLiteral_4= 'continuous' ) )
            {
            // InternalMBILANG.g:1631:2: ( (enumLiteral_0= 'constant' ) | (enumLiteral_1= 'transient' ) | (enumLiteral_2= 'piecewiseConstant' ) | (enumLiteral_3= 'piecewiseContinuous' ) | (enumLiteral_4= 'continuous' ) )
            int alt14=5;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt14=1;
                }
                break;
            case 48:
                {
                alt14=2;
                }
                break;
            case 49:
                {
                alt14=3;
                }
                break;
            case 50:
                {
                alt14=4;
                }
                break;
            case 51:
                {
                alt14=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalMBILANG.g:1632:3: (enumLiteral_0= 'constant' )
                    {
                    // InternalMBILANG.g:1632:3: (enumLiteral_0= 'constant' )
                    // InternalMBILANG.g:1633:4: enumLiteral_0= 'constant'
                    {
                    enumLiteral_0=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getDataNatureAccess().getConstantEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDataNatureAccess().getConstantEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:1640:3: (enumLiteral_1= 'transient' )
                    {
                    // InternalMBILANG.g:1640:3: (enumLiteral_1= 'transient' )
                    // InternalMBILANG.g:1641:4: enumLiteral_1= 'transient'
                    {
                    enumLiteral_1=(Token)match(input,48,FOLLOW_2); 

                    				current = grammarAccess.getDataNatureAccess().getSpuriousEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDataNatureAccess().getSpuriousEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:1648:3: (enumLiteral_2= 'piecewiseConstant' )
                    {
                    // InternalMBILANG.g:1648:3: (enumLiteral_2= 'piecewiseConstant' )
                    // InternalMBILANG.g:1649:4: enumLiteral_2= 'piecewiseConstant'
                    {
                    enumLiteral_2=(Token)match(input,49,FOLLOW_2); 

                    				current = grammarAccess.getDataNatureAccess().getPiecewiseConstantEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDataNatureAccess().getPiecewiseConstantEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMBILANG.g:1656:3: (enumLiteral_3= 'piecewiseContinuous' )
                    {
                    // InternalMBILANG.g:1656:3: (enumLiteral_3= 'piecewiseContinuous' )
                    // InternalMBILANG.g:1657:4: enumLiteral_3= 'piecewiseContinuous'
                    {
                    enumLiteral_3=(Token)match(input,50,FOLLOW_2); 

                    				current = grammarAccess.getDataNatureAccess().getPiecewiseContinuousEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDataNatureAccess().getPiecewiseContinuousEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMBILANG.g:1664:3: (enumLiteral_4= 'continuous' )
                    {
                    // InternalMBILANG.g:1664:3: (enumLiteral_4= 'continuous' )
                    // InternalMBILANG.g:1665:4: enumLiteral_4= 'continuous'
                    {
                    enumLiteral_4=(Token)match(input,51,FOLLOW_2); 

                    				current = grammarAccess.getDataNatureAccess().getContinuousEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getDataNatureAccess().getContinuousEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataNature"


    // $ANTLR start "ruleDataType"
    // InternalMBILANG.g:1675:1: ruleDataType returns [Enumerator current=null] : ( (enumLiteral_0= 'Real' ) | (enumLiteral_1= 'Integer' ) | (enumLiteral_2= 'Boolean' ) | (enumLiteral_3= 'String' ) | (enumLiteral_4= 'Enumeration' ) ) ;
    public final Enumerator ruleDataType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1681:2: ( ( (enumLiteral_0= 'Real' ) | (enumLiteral_1= 'Integer' ) | (enumLiteral_2= 'Boolean' ) | (enumLiteral_3= 'String' ) | (enumLiteral_4= 'Enumeration' ) ) )
            // InternalMBILANG.g:1682:2: ( (enumLiteral_0= 'Real' ) | (enumLiteral_1= 'Integer' ) | (enumLiteral_2= 'Boolean' ) | (enumLiteral_3= 'String' ) | (enumLiteral_4= 'Enumeration' ) )
            {
            // InternalMBILANG.g:1682:2: ( (enumLiteral_0= 'Real' ) | (enumLiteral_1= 'Integer' ) | (enumLiteral_2= 'Boolean' ) | (enumLiteral_3= 'String' ) | (enumLiteral_4= 'Enumeration' ) )
            int alt15=5;
            switch ( input.LA(1) ) {
            case 52:
                {
                alt15=1;
                }
                break;
            case 53:
                {
                alt15=2;
                }
                break;
            case 54:
                {
                alt15=3;
                }
                break;
            case 55:
                {
                alt15=4;
                }
                break;
            case 56:
                {
                alt15=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalMBILANG.g:1683:3: (enumLiteral_0= 'Real' )
                    {
                    // InternalMBILANG.g:1683:3: (enumLiteral_0= 'Real' )
                    // InternalMBILANG.g:1684:4: enumLiteral_0= 'Real'
                    {
                    enumLiteral_0=(Token)match(input,52,FOLLOW_2); 

                    				current = grammarAccess.getDataTypeAccess().getRealEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDataTypeAccess().getRealEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:1691:3: (enumLiteral_1= 'Integer' )
                    {
                    // InternalMBILANG.g:1691:3: (enumLiteral_1= 'Integer' )
                    // InternalMBILANG.g:1692:4: enumLiteral_1= 'Integer'
                    {
                    enumLiteral_1=(Token)match(input,53,FOLLOW_2); 

                    				current = grammarAccess.getDataTypeAccess().getIntegerEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDataTypeAccess().getIntegerEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:1699:3: (enumLiteral_2= 'Boolean' )
                    {
                    // InternalMBILANG.g:1699:3: (enumLiteral_2= 'Boolean' )
                    // InternalMBILANG.g:1700:4: enumLiteral_2= 'Boolean'
                    {
                    enumLiteral_2=(Token)match(input,54,FOLLOW_2); 

                    				current = grammarAccess.getDataTypeAccess().getBooleanEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDataTypeAccess().getBooleanEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalMBILANG.g:1707:3: (enumLiteral_3= 'String' )
                    {
                    // InternalMBILANG.g:1707:3: (enumLiteral_3= 'String' )
                    // InternalMBILANG.g:1708:4: enumLiteral_3= 'String'
                    {
                    enumLiteral_3=(Token)match(input,55,FOLLOW_2); 

                    				current = grammarAccess.getDataTypeAccess().getStringEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDataTypeAccess().getStringEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalMBILANG.g:1715:3: (enumLiteral_4= 'Enumeration' )
                    {
                    // InternalMBILANG.g:1715:3: (enumLiteral_4= 'Enumeration' )
                    // InternalMBILANG.g:1716:4: enumLiteral_4= 'Enumeration'
                    {
                    enumLiteral_4=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getDataTypeAccess().getEnumerationEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getDataTypeAccess().getEnumerationEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "rulePortDirection"
    // InternalMBILANG.g:1726:1: rulePortDirection returns [Enumerator current=null] : ( (enumLiteral_0= 'INPUT' ) | (enumLiteral_1= 'OUTPUT' ) ) ;
    public final Enumerator rulePortDirection() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1732:2: ( ( (enumLiteral_0= 'INPUT' ) | (enumLiteral_1= 'OUTPUT' ) ) )
            // InternalMBILANG.g:1733:2: ( (enumLiteral_0= 'INPUT' ) | (enumLiteral_1= 'OUTPUT' ) )
            {
            // InternalMBILANG.g:1733:2: ( (enumLiteral_0= 'INPUT' ) | (enumLiteral_1= 'OUTPUT' ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==57) ) {
                alt16=1;
            }
            else if ( (LA16_0==58) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalMBILANG.g:1734:3: (enumLiteral_0= 'INPUT' )
                    {
                    // InternalMBILANG.g:1734:3: (enumLiteral_0= 'INPUT' )
                    // InternalMBILANG.g:1735:4: enumLiteral_0= 'INPUT'
                    {
                    enumLiteral_0=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getPortDirectionAccess().getINPUTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getPortDirectionAccess().getINPUTEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:1742:3: (enumLiteral_1= 'OUTPUT' )
                    {
                    // InternalMBILANG.g:1742:3: (enumLiteral_1= 'OUTPUT' )
                    // InternalMBILANG.g:1743:4: enumLiteral_1= 'OUTPUT'
                    {
                    enumLiteral_1=(Token)match(input,58,FOLLOW_2); 

                    				current = grammarAccess.getPortDirectionAccess().getOUTPUTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getPortDirectionAccess().getOUTPUTEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePortDirection"


    // $ANTLR start "ruleTemporalReference"
    // InternalMBILANG.g:1753:1: ruleTemporalReference returns [Enumerator current=null] : ( (enumLiteral_0= 'time' ) | (enumLiteral_1= 'distance' ) | (enumLiteral_2= 'angle' ) ) ;
    public final Enumerator ruleTemporalReference() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalMBILANG.g:1759:2: ( ( (enumLiteral_0= 'time' ) | (enumLiteral_1= 'distance' ) | (enumLiteral_2= 'angle' ) ) )
            // InternalMBILANG.g:1760:2: ( (enumLiteral_0= 'time' ) | (enumLiteral_1= 'distance' ) | (enumLiteral_2= 'angle' ) )
            {
            // InternalMBILANG.g:1760:2: ( (enumLiteral_0= 'time' ) | (enumLiteral_1= 'distance' ) | (enumLiteral_2= 'angle' ) )
            int alt17=3;
            switch ( input.LA(1) ) {
            case 59:
                {
                alt17=1;
                }
                break;
            case 60:
                {
                alt17=2;
                }
                break;
            case 61:
                {
                alt17=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalMBILANG.g:1761:3: (enumLiteral_0= 'time' )
                    {
                    // InternalMBILANG.g:1761:3: (enumLiteral_0= 'time' )
                    // InternalMBILANG.g:1762:4: enumLiteral_0= 'time'
                    {
                    enumLiteral_0=(Token)match(input,59,FOLLOW_2); 

                    				current = grammarAccess.getTemporalReferenceAccess().getTimeEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTemporalReferenceAccess().getTimeEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalMBILANG.g:1769:3: (enumLiteral_1= 'distance' )
                    {
                    // InternalMBILANG.g:1769:3: (enumLiteral_1= 'distance' )
                    // InternalMBILANG.g:1770:4: enumLiteral_1= 'distance'
                    {
                    enumLiteral_1=(Token)match(input,60,FOLLOW_2); 

                    				current = grammarAccess.getTemporalReferenceAccess().getDistanceEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTemporalReferenceAccess().getDistanceEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalMBILANG.g:1777:3: (enumLiteral_2= 'angle' )
                    {
                    // InternalMBILANG.g:1777:3: (enumLiteral_2= 'angle' )
                    // InternalMBILANG.g:1778:4: enumLiteral_2= 'angle'
                    {
                    enumLiteral_2=(Token)match(input,61,FOLLOW_2); 

                    				current = grammarAccess.getTemporalReferenceAccess().getAngleEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTemporalReferenceAccess().getAngleEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTemporalReference"

    // Delegated rules


    protected DFA11 dfa11 = new DFA11(this);
    static final String dfa_1s = "\15\uffff";
    static final String dfa_2s = "\1\22\14\uffff";
    static final String dfa_3s = "\1\37\14\uffff";
    static final String dfa_4s = "\1\uffff\1\14\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13";
    static final String dfa_5s = "\1\0\14\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\1\uffff\1\14\1\uffff\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA11 extends DFA {

        public DFA11(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 11;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "()* loopback of 672:6: ( ({...}? => ( ({...}? => (otherlv_5= 'variableName' ( (lv_variableName_6_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'direction' ( (lv_direction_8_0= rulePortDirection ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_9= 'nature' ( (lv_nature_10_0= ruleDataNature ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_11= 'type' ( (lv_type_12_0= ruleDataType ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'RTD' ( (lv_RTD_14_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_15= 'initValue' ( (lv_initValue_16_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_17= 'plotterColor' ( (lv_color_18_0= ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_19= 'monitored' ( (lv_monitored_20_0= ruleEBoolean ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_21= 'temporalreference' ( ( ruleEString ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_23= 'ioevents' otherlv_24= '{' ( (lv_ioevents_25_0= ruleIOEvent ) ) (otherlv_26= ',' ( (lv_ioevents_27_0= ruleIOEvent ) ) )* otherlv_28= '}' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_29= 'properties' otherlv_30= '{' ( (lv_properties_31_0= rulePortProperty ) ) (otherlv_32= ',' ( (lv_properties_33_0= rulePortProperty ) ) )* otherlv_34= '}' ) ) ) ) )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA11_0 = input.LA(1);

                         
                        int index11_0 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA11_0==18) ) {s = 1;}

                        else if ( LA11_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 0) ) {s = 2;}

                        else if ( LA11_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 1) ) {s = 3;}

                        else if ( LA11_0 == 24 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 2) ) {s = 4;}

                        else if ( LA11_0 == 25 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 3) ) {s = 5;}

                        else if ( LA11_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 4) ) {s = 6;}

                        else if ( LA11_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 5) ) {s = 7;}

                        else if ( LA11_0 == 28 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 6) ) {s = 8;}

                        else if ( LA11_0 == 29 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 7) ) {s = 9;}

                        else if ( LA11_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 8) ) {s = 10;}

                        else if ( LA11_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 9) ) {s = 11;}

                        else if ( LA11_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getPortAccess().getUnorderedGroup_4(), 10) ) {s = 12;}

                         
                        input.seek(index11_0);
                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 11, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000019E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000001DE000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00001C0000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000FFD40000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0600000000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x000F800000000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x01F0000000000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000600000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000001C00000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000003E000000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000200040000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x3800000000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000040000L});

}
