/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.PortProperty;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class PortPropertyImpl extends MinimalEObjectImpl.Container implements PortProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MbilangPackage.Literals.PORT_PROPERTY;
	}

} //PortPropertyImpl
