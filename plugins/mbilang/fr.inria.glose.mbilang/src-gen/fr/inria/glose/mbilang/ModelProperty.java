/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getModelProperty()
 * @model abstract="true"
 * @generated
 */
public interface ModelProperty extends EObject {
} // ModelProperty
