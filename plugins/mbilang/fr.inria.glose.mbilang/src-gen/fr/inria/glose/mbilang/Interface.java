/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getTemporalreferences <em>Temporalreferences</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getProperties <em>Properties</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getModel <em>Model</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getFMUPath <em>FMU Path</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Interface#getGemocExecutablePath <em>Gemoc Executable Path</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mbilang.Port}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' containment reference list.
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_Ports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Port> getPorts();

	/**
	 * Returns the value of the '<em><b>Temporalreferences</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mbilang.ModelTemporalReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temporalreferences</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporalreferences</em>' containment reference list.
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_Temporalreferences()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ModelTemporalReference> getTemporalreferences();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mbilang.ModelProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<ModelProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model</em>' reference.
	 * @see #setModel(EObject)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_Model()
	 * @model
	 * @generated
	 */
	EObject getModel();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Interface#getModel <em>Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' reference.
	 * @see #getModel()
	 * @generated
	 */
	void setModel(EObject value);

	/**
	 * Returns the value of the '<em><b>FMU Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FMU Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FMU Path</em>' attribute.
	 * @see #setFMUPath(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_FMUPath()
	 * @model
	 * @generated
	 */
	String getFMUPath();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Interface#getFMUPath <em>FMU Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FMU Path</em>' attribute.
	 * @see #getFMUPath()
	 * @generated
	 */
	void setFMUPath(String value);

	/**
	 * Returns the value of the '<em><b>Gemoc Executable Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gemoc Executable Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gemoc Executable Path</em>' attribute.
	 * @see #setGemocExecutablePath(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getInterface_GemocExecutablePath()
	 * @model
	 * @generated
	 */
	String getGemocExecutablePath();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Interface#getGemocExecutablePath <em>Gemoc Executable Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Gemoc Executable Path</em>' attribute.
	 * @see #getGemocExecutablePath()
	 * @generated
	 */
	void setGemocExecutablePath(String value);

} // Interface
