/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.Triggered;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triggered</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TriggeredImpl extends IOEventImpl implements Triggered {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriggeredImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MbilangPackage.Literals.TRIGGERED;
	}

} //TriggeredImpl
