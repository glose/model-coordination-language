/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.Port#getDirection <em>Direction</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getIoevents <em>Ioevents</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getTemporalreference <em>Temporalreference</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getNature <em>Nature</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getType <em>Type</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getProperties <em>Properties</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getRTD <em>RTD</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#isMonitored <em>Monitored</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getInitValue <em>Init Value</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getVariableName <em>Variable Name</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.Port#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getPort()
 * @model
 * @generated
 */
public interface Port extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.glose.mbilang.PortDirection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see fr.inria.glose.mbilang.PortDirection
	 * @see #setDirection(PortDirection)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Direction()
	 * @model
	 * @generated
	 */
	PortDirection getDirection();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see fr.inria.glose.mbilang.PortDirection
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(PortDirection value);

	/**
	 * Returns the value of the '<em><b>Ioevents</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mbilang.IOEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ioevents</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ioevents</em>' containment reference list.
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Ioevents()
	 * @model containment="true" upper="2"
	 * @generated
	 */
	EList<IOEvent> getIoevents();

	/**
	 * Returns the value of the '<em><b>Temporalreference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temporalreference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporalreference</em>' reference.
	 * @see #setTemporalreference(ModelTemporalReference)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Temporalreference()
	 * @model
	 * @generated
	 */
	ModelTemporalReference getTemporalreference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getTemporalreference <em>Temporalreference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temporalreference</em>' reference.
	 * @see #getTemporalreference()
	 * @generated
	 */
	void setTemporalreference(ModelTemporalReference value);

	/**
	 * Returns the value of the '<em><b>Nature</b></em>' attribute.
	 * The default value is <code>"continuous"</code>.
	 * The literals are from the enumeration {@link fr.inria.glose.mbilang.DataNature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature</em>' attribute.
	 * @see fr.inria.glose.mbilang.DataNature
	 * @see #setNature(DataNature)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Nature()
	 * @model default="continuous"
	 * @generated
	 */
	DataNature getNature();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getNature <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' attribute.
	 * @see fr.inria.glose.mbilang.DataNature
	 * @see #getNature()
	 * @generated
	 */
	void setNature(DataNature value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.glose.mbilang.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see fr.inria.glose.mbilang.DataType
	 * @see #setType(DataType)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Type()
	 * @model
	 * @generated
	 */
	DataType getType();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see fr.inria.glose.mbilang.DataType
	 * @see #getType()
	 * @generated
	 */
	void setType(DataType value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mbilang.PortProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>RTD</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RTD</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RTD</em>' attribute.
	 * @see #setRTD(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_RTD()
	 * @model
	 * @generated
	 */
	String getRTD();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getRTD <em>RTD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RTD</em>' attribute.
	 * @see #getRTD()
	 * @generated
	 */
	void setRTD(String value);

	/**
	 * Returns the value of the '<em><b>Monitored</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monitored</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitored</em>' attribute.
	 * @see #setMonitored(boolean)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Monitored()
	 * @model default="false"
	 * @generated
	 */
	boolean isMonitored();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#isMonitored <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Monitored</em>' attribute.
	 * @see #isMonitored()
	 * @generated
	 */
	void setMonitored(boolean value);

	/**
	 * Returns the value of the '<em><b>Init Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Value</em>' attribute.
	 * @see #setInitValue(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_InitValue()
	 * @model
	 * @generated
	 */
	String getInitValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getInitValue <em>Init Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Value</em>' attribute.
	 * @see #getInitValue()
	 * @generated
	 */
	void setInitValue(String value);

	/**
	 * Returns the value of the '<em><b>Variable Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Name</em>' attribute.
	 * @see #setVariableName(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_VariableName()
	 * @model default=""
	 * @generated
	 */
	String getVariableName();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getVariableName <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable Name</em>' attribute.
	 * @see #getVariableName()
	 * @generated
	 */
	void setVariableName(String value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getPort_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.Port#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

} // Port
