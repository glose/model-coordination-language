/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.mbilang.MbilangPackage
 * @generated
 */
public interface MbilangFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MbilangFactory eINSTANCE = fr.inria.glose.mbilang.impl.MbilangFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interface</em>'.
	 * @generated
	 */
	Interface createInterface();

	/**
	 * Returns a new object of class '<em>Model Temporal Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Temporal Reference</em>'.
	 * @generated
	 */
	ModelTemporalReference createModelTemporalReference();

	/**
	 * Returns a new object of class '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port</em>'.
	 * @generated
	 */
	Port createPort();

	/**
	 * Returns a new object of class '<em>Updated</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Updated</em>'.
	 * @generated
	 */
	Updated createUpdated();

	/**
	 * Returns a new object of class '<em>Ready To Read</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ready To Read</em>'.
	 * @generated
	 */
	ReadyToRead createReadyToRead();

	/**
	 * Returns a new object of class '<em>Rollback</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rollback</em>'.
	 * @generated
	 */
	Rollback createRollback();

	/**
	 * Returns a new object of class '<em>Super Dense Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Super Dense Time</em>'.
	 * @generated
	 */
	SuperDenseTime createSuperDenseTime();

	/**
	 * Returns a new object of class '<em>Delta Step Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delta Step Size</em>'.
	 * @generated
	 */
	DeltaStepSize createDeltaStepSize();

	/**
	 * Returns a new object of class '<em>Zero Crossing Detection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Zero Crossing Detection</em>'.
	 * @generated
	 */
	ZeroCrossingDetection createZeroCrossingDetection();

	/**
	 * Returns a new object of class '<em>Extrapolation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extrapolation</em>'.
	 * @generated
	 */
	Extrapolation createExtrapolation();

	/**
	 * Returns a new object of class '<em>Interpolation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interpolation</em>'.
	 * @generated
	 */
	Interpolation createInterpolation();

	/**
	 * Returns a new object of class '<em>Discontinuity Locator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discontinuity Locator</em>'.
	 * @generated
	 */
	DiscontinuityLocator createDiscontinuityLocator();

	/**
	 * Returns a new object of class '<em>History Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History Provider</em>'.
	 * @generated
	 */
	HistoryProvider createHistoryProvider();

	/**
	 * Returns a new object of class '<em>Triggered</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triggered</em>'.
	 * @generated
	 */
	Triggered createTriggered();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MbilangPackage getMbilangPackage();

} //MbilangFactory
