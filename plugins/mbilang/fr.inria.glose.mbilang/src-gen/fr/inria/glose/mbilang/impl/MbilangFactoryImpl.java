/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MbilangFactoryImpl extends EFactoryImpl implements MbilangFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MbilangFactory init() {
		try {
			MbilangFactory theMbilangFactory = (MbilangFactory) EPackage.Registry.INSTANCE
					.getEFactory(MbilangPackage.eNS_URI);
			if (theMbilangFactory != null) {
				return theMbilangFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MbilangFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MbilangFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case MbilangPackage.INTERFACE:
			return createInterface();
		case MbilangPackage.MODEL_TEMPORAL_REFERENCE:
			return createModelTemporalReference();
		case MbilangPackage.PORT:
			return createPort();
		case MbilangPackage.UPDATED:
			return createUpdated();
		case MbilangPackage.READY_TO_READ:
			return createReadyToRead();
		case MbilangPackage.ROLLBACK:
			return createRollback();
		case MbilangPackage.SUPER_DENSE_TIME:
			return createSuperDenseTime();
		case MbilangPackage.DELTA_STEP_SIZE:
			return createDeltaStepSize();
		case MbilangPackage.ZERO_CROSSING_DETECTION:
			return createZeroCrossingDetection();
		case MbilangPackage.EXTRAPOLATION:
			return createExtrapolation();
		case MbilangPackage.INTERPOLATION:
			return createInterpolation();
		case MbilangPackage.DISCONTINUITY_LOCATOR:
			return createDiscontinuityLocator();
		case MbilangPackage.HISTORY_PROVIDER:
			return createHistoryProvider();
		case MbilangPackage.TRIGGERED:
			return createTriggered();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case MbilangPackage.DATA_NATURE:
			return createDataNatureFromString(eDataType, initialValue);
		case MbilangPackage.TEMPORAL_REFERENCE:
			return createTemporalReferenceFromString(eDataType, initialValue);
		case MbilangPackage.DATA_TYPE:
			return createDataTypeFromString(eDataType, initialValue);
		case MbilangPackage.PORT_DIRECTION:
			return createPortDirectionFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case MbilangPackage.DATA_NATURE:
			return convertDataNatureToString(eDataType, instanceValue);
		case MbilangPackage.TEMPORAL_REFERENCE:
			return convertTemporalReferenceToString(eDataType, instanceValue);
		case MbilangPackage.DATA_TYPE:
			return convertDataTypeToString(eDataType, instanceValue);
		case MbilangPackage.PORT_DIRECTION:
			return convertPortDirectionToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference createModelTemporalReference() {
		ModelTemporalReferenceImpl modelTemporalReference = new ModelTemporalReferenceImpl();
		return modelTemporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port createPort() {
		PortImpl port = new PortImpl();
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Updated createUpdated() {
		UpdatedImpl updated = new UpdatedImpl();
		return updated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadyToRead createReadyToRead() {
		ReadyToReadImpl readyToRead = new ReadyToReadImpl();
		return readyToRead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rollback createRollback() {
		RollbackImpl rollback = new RollbackImpl();
		return rollback;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuperDenseTime createSuperDenseTime() {
		SuperDenseTimeImpl superDenseTime = new SuperDenseTimeImpl();
		return superDenseTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaStepSize createDeltaStepSize() {
		DeltaStepSizeImpl deltaStepSize = new DeltaStepSizeImpl();
		return deltaStepSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ZeroCrossingDetection createZeroCrossingDetection() {
		ZeroCrossingDetectionImpl zeroCrossingDetection = new ZeroCrossingDetectionImpl();
		return zeroCrossingDetection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Extrapolation createExtrapolation() {
		ExtrapolationImpl extrapolation = new ExtrapolationImpl();
		return extrapolation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interpolation createInterpolation() {
		InterpolationImpl interpolation = new InterpolationImpl();
		return interpolation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscontinuityLocator createDiscontinuityLocator() {
		DiscontinuityLocatorImpl discontinuityLocator = new DiscontinuityLocatorImpl();
		return discontinuityLocator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryProvider createHistoryProvider() {
		HistoryProviderImpl historyProvider = new HistoryProviderImpl();
		return historyProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Triggered createTriggered() {
		TriggeredImpl triggered = new TriggeredImpl();
		return triggered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataNature createDataNatureFromString(EDataType eDataType, String initialValue) {
		DataNature result = DataNature.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataNatureToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemporalReference createTemporalReferenceFromString(EDataType eDataType, String initialValue) {
		TemporalReference result = TemporalReference.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTemporalReferenceToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType createDataTypeFromString(EDataType eDataType, String initialValue) {
		DataType result = DataType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDirection createPortDirectionFromString(EDataType eDataType, String initialValue) {
		PortDirection result = PortDirection.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MbilangPackage getMbilangPackage() {
		return (MbilangPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MbilangPackage getPackage() {
		return MbilangPackage.eINSTANCE;
	}

} //MbilangFactoryImpl
