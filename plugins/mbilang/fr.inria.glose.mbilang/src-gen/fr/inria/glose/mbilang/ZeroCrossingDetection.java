/**
 */
package fr.inria.glose.mbilang;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Zero Crossing Detection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.ZeroCrossingDetection#isState <em>State</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getZeroCrossingDetection()
 * @model
 * @generated
 */
public interface ZeroCrossingDetection extends PortProperty {
	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see #setState(boolean)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getZeroCrossingDetection_State()
	 * @model
	 * @generated
	 */
	boolean isState();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.ZeroCrossingDetection#isState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see #isState()
	 * @generated
	 */
	void setState(boolean value);

} // ZeroCrossingDetection
