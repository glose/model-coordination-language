/**
 */
package fr.inria.glose.mbilang;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ready To Read</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getReadyToRead()
 * @model
 * @generated
 */
public interface ReadyToRead extends IOEvent {
} // ReadyToRead
