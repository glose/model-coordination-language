/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IO Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.IOEvent#getMSE <em>MSE</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getIOEvent()
 * @model abstract="true"
 * @generated
 */
public interface IOEvent extends NamedElement {
	/**
	 * Returns the value of the '<em><b>MSE</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MSE</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MSE</em>' reference.
	 * @see #setMSE(EObject)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getIOEvent_MSE()
	 * @model
	 * @generated
	 */
	EObject getMSE();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.IOEvent#getMSE <em>MSE</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MSE</em>' reference.
	 * @see #getMSE()
	 * @generated
	 */
	void setMSE(EObject value);

} // IOEvent
