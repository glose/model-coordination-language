/**
 */
package fr.inria.glose.mbilang.util;

import fr.inria.glose.mbilang.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.mbilang.MbilangPackage
 * @generated
 */
public class MbilangSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MbilangPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MbilangSwitch() {
		if (modelPackage == null) {
			modelPackage = MbilangPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case MbilangPackage.INTERFACE: {
			Interface interface_ = (Interface) theEObject;
			T result = caseInterface(interface_);
			if (result == null)
				result = caseNamedElement(interface_);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.MODEL_TEMPORAL_REFERENCE: {
			ModelTemporalReference modelTemporalReference = (ModelTemporalReference) theEObject;
			T result = caseModelTemporalReference(modelTemporalReference);
			if (result == null)
				result = caseNamedElement(modelTemporalReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.PORT: {
			Port port = (Port) theEObject;
			T result = casePort(port);
			if (result == null)
				result = caseNamedElement(port);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.IO_EVENT: {
			IOEvent ioEvent = (IOEvent) theEObject;
			T result = caseIOEvent(ioEvent);
			if (result == null)
				result = caseNamedElement(ioEvent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.UPDATED: {
			Updated updated = (Updated) theEObject;
			T result = caseUpdated(updated);
			if (result == null)
				result = caseIOEvent(updated);
			if (result == null)
				result = caseNamedElement(updated);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.READY_TO_READ: {
			ReadyToRead readyToRead = (ReadyToRead) theEObject;
			T result = caseReadyToRead(readyToRead);
			if (result == null)
				result = caseIOEvent(readyToRead);
			if (result == null)
				result = caseNamedElement(readyToRead);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.MODEL_PROPERTY: {
			ModelProperty modelProperty = (ModelProperty) theEObject;
			T result = caseModelProperty(modelProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.NAMED_ELEMENT: {
			NamedElement namedElement = (NamedElement) theEObject;
			T result = caseNamedElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.ROLLBACK: {
			Rollback rollback = (Rollback) theEObject;
			T result = caseRollback(rollback);
			if (result == null)
				result = caseModelProperty(rollback);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.SUPER_DENSE_TIME: {
			SuperDenseTime superDenseTime = (SuperDenseTime) theEObject;
			T result = caseSuperDenseTime(superDenseTime);
			if (result == null)
				result = caseModelProperty(superDenseTime);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.DELTA_STEP_SIZE: {
			DeltaStepSize deltaStepSize = (DeltaStepSize) theEObject;
			T result = caseDeltaStepSize(deltaStepSize);
			if (result == null)
				result = caseModelProperty(deltaStepSize);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.PORT_PROPERTY: {
			PortProperty portProperty = (PortProperty) theEObject;
			T result = casePortProperty(portProperty);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.ZERO_CROSSING_DETECTION: {
			ZeroCrossingDetection zeroCrossingDetection = (ZeroCrossingDetection) theEObject;
			T result = caseZeroCrossingDetection(zeroCrossingDetection);
			if (result == null)
				result = casePortProperty(zeroCrossingDetection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.EXTRAPOLATION: {
			Extrapolation extrapolation = (Extrapolation) theEObject;
			T result = caseExtrapolation(extrapolation);
			if (result == null)
				result = casePortProperty(extrapolation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.INTERPOLATION: {
			Interpolation interpolation = (Interpolation) theEObject;
			T result = caseInterpolation(interpolation);
			if (result == null)
				result = casePortProperty(interpolation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.DISCONTINUITY_LOCATOR: {
			DiscontinuityLocator discontinuityLocator = (DiscontinuityLocator) theEObject;
			T result = caseDiscontinuityLocator(discontinuityLocator);
			if (result == null)
				result = casePortProperty(discontinuityLocator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.HISTORY_PROVIDER: {
			HistoryProvider historyProvider = (HistoryProvider) theEObject;
			T result = caseHistoryProvider(historyProvider);
			if (result == null)
				result = casePortProperty(historyProvider);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MbilangPackage.TRIGGERED: {
			Triggered triggered = (Triggered) theEObject;
			T result = caseTriggered(triggered);
			if (result == null)
				result = caseIOEvent(triggered);
			if (result == null)
				result = caseNamedElement(triggered);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterface(Interface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Temporal Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Temporal Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelTemporalReference(ModelTemporalReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IO Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IO Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIOEvent(IOEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Updated</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Updated</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdated(Updated object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ready To Read</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ready To Read</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadyToRead(ReadyToRead object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelProperty(ModelProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rollback</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rollback</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRollback(Rollback object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Super Dense Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Super Dense Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSuperDenseTime(SuperDenseTime object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delta Step Size</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delta Step Size</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeltaStepSize(DeltaStepSize object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortProperty(PortProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Zero Crossing Detection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Zero Crossing Detection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseZeroCrossingDetection(ZeroCrossingDetection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extrapolation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extrapolation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtrapolation(Extrapolation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interpolation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interpolation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterpolation(Interpolation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discontinuity Locator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discontinuity Locator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscontinuityLocator(DiscontinuityLocator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoryProvider(HistoryProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggered</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggered</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTriggered(Triggered object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MbilangSwitch
