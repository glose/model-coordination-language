/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.glose.mbilang.MbilangFactory
 * @model kind="package"
 * @generated
 */
public interface MbilangPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mbilang";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.inria.fr/glose/mbilang";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mbilang";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MbilangPackage eINSTANCE = fr.inria.glose.mbilang.impl.MbilangPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.NamedElementImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.InterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.InterfaceImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getInterface()
	 * @generated
	 */
	int INTERFACE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__PORTS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Temporalreferences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__TEMPORALREFERENCES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__PROPERTIES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__MODEL = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>FMU Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__FMU_PATH = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Gemoc Executable Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__GEMOC_EXECUTABLE_PATH = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.ModelTemporalReferenceImpl <em>Model Temporal Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.ModelTemporalReferenceImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getModelTemporalReference()
	 * @generated
	 */
	int MODEL_TEMPORAL_REFERENCE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_TEMPORAL_REFERENCE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_TEMPORAL_REFERENCE__REFERENCE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model Temporal Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_TEMPORAL_REFERENCE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Model Temporal Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_TEMPORAL_REFERENCE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.PortImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DIRECTION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ioevents</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__IOEVENTS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Temporalreference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__TEMPORALREFERENCE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NATURE = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PROPERTIES = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>RTD</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__RTD = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Monitored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__MONITORED = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Init Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__INIT_VALUE = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__VARIABLE_NAME = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__COLOR = NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The number of operations of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.IOEventImpl <em>IO Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.IOEventImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getIOEvent()
	 * @generated
	 */
	int IO_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IO_EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>MSE</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IO_EVENT__MSE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IO Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IO_EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>IO Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IO_EVENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.UpdatedImpl <em>Updated</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.UpdatedImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getUpdated()
	 * @generated
	 */
	int UPDATED = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED__NAME = IO_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>MSE</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED__MSE = IO_EVENT__MSE;

	/**
	 * The number of structural features of the '<em>Updated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED_FEATURE_COUNT = IO_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Updated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED_OPERATION_COUNT = IO_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.ReadyToReadImpl <em>Ready To Read</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.ReadyToReadImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getReadyToRead()
	 * @generated
	 */
	int READY_TO_READ = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ__NAME = IO_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>MSE</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ__MSE = IO_EVENT__MSE;

	/**
	 * The number of structural features of the '<em>Ready To Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ_FEATURE_COUNT = IO_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ready To Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ_OPERATION_COUNT = IO_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.ModelPropertyImpl <em>Model Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.ModelPropertyImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getModelProperty()
	 * @generated
	 */
	int MODEL_PROPERTY = 6;

	/**
	 * The number of structural features of the '<em>Model Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_PROPERTY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Model Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.RollbackImpl <em>Rollback</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.RollbackImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getRollback()
	 * @generated
	 */
	int ROLLBACK = 8;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLLBACK__STATE = MODEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Rollback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLLBACK_FEATURE_COUNT = MODEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Rollback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLLBACK_OPERATION_COUNT = MODEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.SuperDenseTimeImpl <em>Super Dense Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.SuperDenseTimeImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getSuperDenseTime()
	 * @generated
	 */
	int SUPER_DENSE_TIME = 9;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_DENSE_TIME__STATE = MODEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Super Dense Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_DENSE_TIME_FEATURE_COUNT = MODEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Super Dense Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPER_DENSE_TIME_OPERATION_COUNT = MODEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.DeltaStepSizeImpl <em>Delta Step Size</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.DeltaStepSizeImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDeltaStepSize()
	 * @generated
	 */
	int DELTA_STEP_SIZE = 10;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELTA_STEP_SIZE__STATE = MODEL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Delta Step Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELTA_STEP_SIZE_FEATURE_COUNT = MODEL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Delta Step Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELTA_STEP_SIZE_OPERATION_COUNT = MODEL_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.PortPropertyImpl <em>Port Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.PortPropertyImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPortProperty()
	 * @generated
	 */
	int PORT_PROPERTY = 11;

	/**
	 * The number of structural features of the '<em>Port Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PROPERTY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Port Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PROPERTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.ZeroCrossingDetectionImpl <em>Zero Crossing Detection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.ZeroCrossingDetectionImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getZeroCrossingDetection()
	 * @generated
	 */
	int ZERO_CROSSING_DETECTION = 12;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZERO_CROSSING_DETECTION__STATE = PORT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Zero Crossing Detection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZERO_CROSSING_DETECTION_FEATURE_COUNT = PORT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Zero Crossing Detection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ZERO_CROSSING_DETECTION_OPERATION_COUNT = PORT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.ExtrapolationImpl <em>Extrapolation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.ExtrapolationImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getExtrapolation()
	 * @generated
	 */
	int EXTRAPOLATION = 13;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAPOLATION__STATE = PORT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Extrapolation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAPOLATION_FEATURE_COUNT = PORT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Extrapolation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRAPOLATION_OPERATION_COUNT = PORT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.InterpolationImpl <em>Interpolation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.InterpolationImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getInterpolation()
	 * @generated
	 */
	int INTERPOLATION = 14;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPOLATION__STATE = PORT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interpolation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPOLATION_FEATURE_COUNT = PORT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Interpolation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERPOLATION_OPERATION_COUNT = PORT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.DiscontinuityLocatorImpl <em>Discontinuity Locator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.DiscontinuityLocatorImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDiscontinuityLocator()
	 * @generated
	 */
	int DISCONTINUITY_LOCATOR = 15;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONTINUITY_LOCATOR__STATE = PORT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Discontinuity Locator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONTINUITY_LOCATOR_FEATURE_COUNT = PORT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Discontinuity Locator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCONTINUITY_LOCATOR_OPERATION_COUNT = PORT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.HistoryProviderImpl <em>History Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.HistoryProviderImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getHistoryProvider()
	 * @generated
	 */
	int HISTORY_PROVIDER = 16;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_PROVIDER__STATE = PORT_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>History Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_PROVIDER_FEATURE_COUNT = PORT_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>History Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_PROVIDER_OPERATION_COUNT = PORT_PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.impl.TriggeredImpl <em>Triggered</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.impl.TriggeredImpl
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getTriggered()
	 * @generated
	 */
	int TRIGGERED = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERED__NAME = IO_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>MSE</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERED__MSE = IO_EVENT__MSE;

	/**
	 * The number of structural features of the '<em>Triggered</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERED_FEATURE_COUNT = IO_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Triggered</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERED_OPERATION_COUNT = IO_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.DataNature <em>Data Nature</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.DataNature
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDataNature()
	 * @generated
	 */
	int DATA_NATURE = 18;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.TemporalReference <em>Temporal Reference</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.TemporalReference
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getTemporalReference()
	 * @generated
	 */
	int TEMPORAL_REFERENCE = 19;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.DataType <em>Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.DataType
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 20;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mbilang.PortDirection <em>Port Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mbilang.PortDirection
	 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPortDirection()
	 * @generated
	 */
	int PORT_DIRECTION = 21;

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see fr.inria.glose.mbilang.Interface
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mbilang.Interface#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ports</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getPorts()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Ports();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mbilang.Interface#getTemporalreferences <em>Temporalreferences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Temporalreferences</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getTemporalreferences()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Temporalreferences();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mbilang.Interface#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getProperties()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Properties();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mbilang.Interface#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getModel()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Model();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Interface#getFMUPath <em>FMU Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>FMU Path</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getFMUPath()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_FMUPath();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Interface#getGemocExecutablePath <em>Gemoc Executable Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gemoc Executable Path</em>'.
	 * @see fr.inria.glose.mbilang.Interface#getGemocExecutablePath()
	 * @see #getInterface()
	 * @generated
	 */
	EAttribute getInterface_GemocExecutablePath();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.ModelTemporalReference <em>Model Temporal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Temporal Reference</em>'.
	 * @see fr.inria.glose.mbilang.ModelTemporalReference
	 * @generated
	 */
	EClass getModelTemporalReference();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.ModelTemporalReference#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference</em>'.
	 * @see fr.inria.glose.mbilang.ModelTemporalReference#getReference()
	 * @see #getModelTemporalReference()
	 * @generated
	 */
	EAttribute getModelTemporalReference_Reference();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see fr.inria.glose.mbilang.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see fr.inria.glose.mbilang.Port#getDirection()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Direction();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mbilang.Port#getIoevents <em>Ioevents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ioevents</em>'.
	 * @see fr.inria.glose.mbilang.Port#getIoevents()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_Ioevents();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mbilang.Port#getTemporalreference <em>Temporalreference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Temporalreference</em>'.
	 * @see fr.inria.glose.mbilang.Port#getTemporalreference()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_Temporalreference();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature</em>'.
	 * @see fr.inria.glose.mbilang.Port#getNature()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Nature();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.inria.glose.mbilang.Port#getType()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mbilang.Port#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see fr.inria.glose.mbilang.Port#getProperties()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_Properties();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getRTD <em>RTD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>RTD</em>'.
	 * @see fr.inria.glose.mbilang.Port#getRTD()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_RTD();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#isMonitored <em>Monitored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Monitored</em>'.
	 * @see fr.inria.glose.mbilang.Port#isMonitored()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Monitored();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getInitValue <em>Init Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Init Value</em>'.
	 * @see fr.inria.glose.mbilang.Port#getInitValue()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_InitValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getVariableName <em>Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable Name</em>'.
	 * @see fr.inria.glose.mbilang.Port#getVariableName()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_VariableName();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Port#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see fr.inria.glose.mbilang.Port#getColor()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Color();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.IOEvent <em>IO Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IO Event</em>'.
	 * @see fr.inria.glose.mbilang.IOEvent
	 * @generated
	 */
	EClass getIOEvent();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mbilang.IOEvent#getMSE <em>MSE</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>MSE</em>'.
	 * @see fr.inria.glose.mbilang.IOEvent#getMSE()
	 * @see #getIOEvent()
	 * @generated
	 */
	EReference getIOEvent_MSE();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Updated <em>Updated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Updated</em>'.
	 * @see fr.inria.glose.mbilang.Updated
	 * @generated
	 */
	EClass getUpdated();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.ReadyToRead <em>Ready To Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ready To Read</em>'.
	 * @see fr.inria.glose.mbilang.ReadyToRead
	 * @generated
	 */
	EClass getReadyToRead();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.ModelProperty <em>Model Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Property</em>'.
	 * @see fr.inria.glose.mbilang.ModelProperty
	 * @generated
	 */
	EClass getModelProperty();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.glose.mbilang.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.glose.mbilang.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Rollback <em>Rollback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rollback</em>'.
	 * @see fr.inria.glose.mbilang.Rollback
	 * @generated
	 */
	EClass getRollback();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Rollback#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.Rollback#isState()
	 * @see #getRollback()
	 * @generated
	 */
	EAttribute getRollback_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.SuperDenseTime <em>Super Dense Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Super Dense Time</em>'.
	 * @see fr.inria.glose.mbilang.SuperDenseTime
	 * @generated
	 */
	EClass getSuperDenseTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.SuperDenseTime#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.SuperDenseTime#isState()
	 * @see #getSuperDenseTime()
	 * @generated
	 */
	EAttribute getSuperDenseTime_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.DeltaStepSize <em>Delta Step Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delta Step Size</em>'.
	 * @see fr.inria.glose.mbilang.DeltaStepSize
	 * @generated
	 */
	EClass getDeltaStepSize();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.DeltaStepSize#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.DeltaStepSize#isState()
	 * @see #getDeltaStepSize()
	 * @generated
	 */
	EAttribute getDeltaStepSize_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.PortProperty <em>Port Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Property</em>'.
	 * @see fr.inria.glose.mbilang.PortProperty
	 * @generated
	 */
	EClass getPortProperty();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.ZeroCrossingDetection <em>Zero Crossing Detection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Zero Crossing Detection</em>'.
	 * @see fr.inria.glose.mbilang.ZeroCrossingDetection
	 * @generated
	 */
	EClass getZeroCrossingDetection();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.ZeroCrossingDetection#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.ZeroCrossingDetection#isState()
	 * @see #getZeroCrossingDetection()
	 * @generated
	 */
	EAttribute getZeroCrossingDetection_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Extrapolation <em>Extrapolation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extrapolation</em>'.
	 * @see fr.inria.glose.mbilang.Extrapolation
	 * @generated
	 */
	EClass getExtrapolation();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Extrapolation#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.Extrapolation#isState()
	 * @see #getExtrapolation()
	 * @generated
	 */
	EAttribute getExtrapolation_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Interpolation <em>Interpolation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interpolation</em>'.
	 * @see fr.inria.glose.mbilang.Interpolation
	 * @generated
	 */
	EClass getInterpolation();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.Interpolation#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.Interpolation#isState()
	 * @see #getInterpolation()
	 * @generated
	 */
	EAttribute getInterpolation_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.DiscontinuityLocator <em>Discontinuity Locator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discontinuity Locator</em>'.
	 * @see fr.inria.glose.mbilang.DiscontinuityLocator
	 * @generated
	 */
	EClass getDiscontinuityLocator();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.DiscontinuityLocator#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.DiscontinuityLocator#isState()
	 * @see #getDiscontinuityLocator()
	 * @generated
	 */
	EAttribute getDiscontinuityLocator_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.HistoryProvider <em>History Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Provider</em>'.
	 * @see fr.inria.glose.mbilang.HistoryProvider
	 * @generated
	 */
	EClass getHistoryProvider();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mbilang.HistoryProvider#isState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see fr.inria.glose.mbilang.HistoryProvider#isState()
	 * @see #getHistoryProvider()
	 * @generated
	 */
	EAttribute getHistoryProvider_State();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mbilang.Triggered <em>Triggered</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triggered</em>'.
	 * @see fr.inria.glose.mbilang.Triggered
	 * @generated
	 */
	EClass getTriggered();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.mbilang.DataNature <em>Data Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Nature</em>'.
	 * @see fr.inria.glose.mbilang.DataNature
	 * @generated
	 */
	EEnum getDataNature();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.mbilang.TemporalReference <em>Temporal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Temporal Reference</em>'.
	 * @see fr.inria.glose.mbilang.TemporalReference
	 * @generated
	 */
	EEnum getTemporalReference();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.mbilang.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type</em>'.
	 * @see fr.inria.glose.mbilang.DataType
	 * @generated
	 */
	EEnum getDataType();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.mbilang.PortDirection <em>Port Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Port Direction</em>'.
	 * @see fr.inria.glose.mbilang.PortDirection
	 * @generated
	 */
	EEnum getPortDirection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MbilangFactory getMbilangFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.InterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.InterfaceImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getInterface()
		 * @generated
		 */
		EClass INTERFACE = eINSTANCE.getInterface();

		/**
		 * The meta object literal for the '<em><b>Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__PORTS = eINSTANCE.getInterface_Ports();

		/**
		 * The meta object literal for the '<em><b>Temporalreferences</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__TEMPORALREFERENCES = eINSTANCE.getInterface_Temporalreferences();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__PROPERTIES = eINSTANCE.getInterface_Properties();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__MODEL = eINSTANCE.getInterface_Model();

		/**
		 * The meta object literal for the '<em><b>FMU Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__FMU_PATH = eINSTANCE.getInterface_FMUPath();

		/**
		 * The meta object literal for the '<em><b>Gemoc Executable Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE__GEMOC_EXECUTABLE_PATH = eINSTANCE.getInterface_GemocExecutablePath();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.ModelTemporalReferenceImpl <em>Model Temporal Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.ModelTemporalReferenceImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getModelTemporalReference()
		 * @generated
		 */
		EClass MODEL_TEMPORAL_REFERENCE = eINSTANCE.getModelTemporalReference();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_TEMPORAL_REFERENCE__REFERENCE = eINSTANCE.getModelTemporalReference_Reference();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.PortImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__DIRECTION = eINSTANCE.getPort_Direction();

		/**
		 * The meta object literal for the '<em><b>Ioevents</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__IOEVENTS = eINSTANCE.getPort_Ioevents();

		/**
		 * The meta object literal for the '<em><b>Temporalreference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__TEMPORALREFERENCE = eINSTANCE.getPort_Temporalreference();

		/**
		 * The meta object literal for the '<em><b>Nature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__NATURE = eINSTANCE.getPort_Nature();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__TYPE = eINSTANCE.getPort_Type();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__PROPERTIES = eINSTANCE.getPort_Properties();

		/**
		 * The meta object literal for the '<em><b>RTD</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__RTD = eINSTANCE.getPort_RTD();

		/**
		 * The meta object literal for the '<em><b>Monitored</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__MONITORED = eINSTANCE.getPort_Monitored();

		/**
		 * The meta object literal for the '<em><b>Init Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__INIT_VALUE = eINSTANCE.getPort_InitValue();

		/**
		 * The meta object literal for the '<em><b>Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__VARIABLE_NAME = eINSTANCE.getPort_VariableName();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__COLOR = eINSTANCE.getPort_Color();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.IOEventImpl <em>IO Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.IOEventImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getIOEvent()
		 * @generated
		 */
		EClass IO_EVENT = eINSTANCE.getIOEvent();

		/**
		 * The meta object literal for the '<em><b>MSE</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IO_EVENT__MSE = eINSTANCE.getIOEvent_MSE();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.UpdatedImpl <em>Updated</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.UpdatedImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getUpdated()
		 * @generated
		 */
		EClass UPDATED = eINSTANCE.getUpdated();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.ReadyToReadImpl <em>Ready To Read</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.ReadyToReadImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getReadyToRead()
		 * @generated
		 */
		EClass READY_TO_READ = eINSTANCE.getReadyToRead();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.ModelPropertyImpl <em>Model Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.ModelPropertyImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getModelProperty()
		 * @generated
		 */
		EClass MODEL_PROPERTY = eINSTANCE.getModelProperty();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.NamedElementImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.RollbackImpl <em>Rollback</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.RollbackImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getRollback()
		 * @generated
		 */
		EClass ROLLBACK = eINSTANCE.getRollback();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLLBACK__STATE = eINSTANCE.getRollback_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.SuperDenseTimeImpl <em>Super Dense Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.SuperDenseTimeImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getSuperDenseTime()
		 * @generated
		 */
		EClass SUPER_DENSE_TIME = eINSTANCE.getSuperDenseTime();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPER_DENSE_TIME__STATE = eINSTANCE.getSuperDenseTime_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.DeltaStepSizeImpl <em>Delta Step Size</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.DeltaStepSizeImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDeltaStepSize()
		 * @generated
		 */
		EClass DELTA_STEP_SIZE = eINSTANCE.getDeltaStepSize();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELTA_STEP_SIZE__STATE = eINSTANCE.getDeltaStepSize_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.PortPropertyImpl <em>Port Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.PortPropertyImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPortProperty()
		 * @generated
		 */
		EClass PORT_PROPERTY = eINSTANCE.getPortProperty();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.ZeroCrossingDetectionImpl <em>Zero Crossing Detection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.ZeroCrossingDetectionImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getZeroCrossingDetection()
		 * @generated
		 */
		EClass ZERO_CROSSING_DETECTION = eINSTANCE.getZeroCrossingDetection();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ZERO_CROSSING_DETECTION__STATE = eINSTANCE.getZeroCrossingDetection_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.ExtrapolationImpl <em>Extrapolation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.ExtrapolationImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getExtrapolation()
		 * @generated
		 */
		EClass EXTRAPOLATION = eINSTANCE.getExtrapolation();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRAPOLATION__STATE = eINSTANCE.getExtrapolation_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.InterpolationImpl <em>Interpolation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.InterpolationImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getInterpolation()
		 * @generated
		 */
		EClass INTERPOLATION = eINSTANCE.getInterpolation();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERPOLATION__STATE = eINSTANCE.getInterpolation_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.DiscontinuityLocatorImpl <em>Discontinuity Locator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.DiscontinuityLocatorImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDiscontinuityLocator()
		 * @generated
		 */
		EClass DISCONTINUITY_LOCATOR = eINSTANCE.getDiscontinuityLocator();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISCONTINUITY_LOCATOR__STATE = eINSTANCE.getDiscontinuityLocator_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.HistoryProviderImpl <em>History Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.HistoryProviderImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getHistoryProvider()
		 * @generated
		 */
		EClass HISTORY_PROVIDER = eINSTANCE.getHistoryProvider();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_PROVIDER__STATE = eINSTANCE.getHistoryProvider_State();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.impl.TriggeredImpl <em>Triggered</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.impl.TriggeredImpl
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getTriggered()
		 * @generated
		 */
		EClass TRIGGERED = eINSTANCE.getTriggered();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.DataNature <em>Data Nature</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.DataNature
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDataNature()
		 * @generated
		 */
		EEnum DATA_NATURE = eINSTANCE.getDataNature();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.TemporalReference <em>Temporal Reference</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.TemporalReference
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getTemporalReference()
		 * @generated
		 */
		EEnum TEMPORAL_REFERENCE = eINSTANCE.getTemporalReference();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.DataType <em>Data Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.DataType
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getDataType()
		 * @generated
		 */
		EEnum DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mbilang.PortDirection <em>Port Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mbilang.PortDirection
		 * @see fr.inria.glose.mbilang.impl.MbilangPackageImpl#getPortDirection()
		 * @generated
		 */
		EEnum PORT_DIRECTION = eINSTANCE.getPortDirection();

	}

} //MbilangPackage
