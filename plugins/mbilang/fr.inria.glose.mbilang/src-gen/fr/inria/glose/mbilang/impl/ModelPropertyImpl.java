/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.ModelProperty;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ModelPropertyImpl extends MinimalEObjectImpl.Container implements ModelProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MbilangPackage.Literals.MODEL_PROPERTY;
	}

} //ModelPropertyImpl
