/**
 */
package fr.inria.glose.mbilang;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triggered</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getTriggered()
 * @model
 * @generated
 */
public interface Triggered extends IOEvent {
} // Triggered
