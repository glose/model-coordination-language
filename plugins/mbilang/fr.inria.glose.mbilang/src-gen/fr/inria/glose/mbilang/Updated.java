/**
 */
package fr.inria.glose.mbilang;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Updated</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getUpdated()
 * @model
 * @generated
 */
public interface Updated extends IOEvent {
} // Updated
