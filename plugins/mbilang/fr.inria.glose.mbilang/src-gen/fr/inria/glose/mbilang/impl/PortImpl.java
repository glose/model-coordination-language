/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.DataNature;
import fr.inria.glose.mbilang.DataType;
import fr.inria.glose.mbilang.IOEvent;
import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.ModelTemporalReference;
import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mbilang.PortDirection;
import fr.inria.glose.mbilang.PortProperty;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getDirection <em>Direction</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getIoevents <em>Ioevents</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getTemporalreference <em>Temporalreference</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getNature <em>Nature</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getRTD <em>RTD</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#isMonitored <em>Monitored</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getInitValue <em>Init Value</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getVariableName <em>Variable Name</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.PortImpl#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortImpl extends NamedElementImpl implements Port {
	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final PortDirection DIRECTION_EDEFAULT = PortDirection.INPUT;

	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected PortDirection direction = DIRECTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIoevents() <em>Ioevents</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIoevents()
	 * @generated
	 * @ordered
	 */
	protected EList<IOEvent> ioevents;

	/**
	 * The cached value of the '{@link #getTemporalreference() <em>Temporalreference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalreference()
	 * @generated
	 * @ordered
	 */
	protected ModelTemporalReference temporalreference;

	/**
	 * The default value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected static final DataNature NATURE_EDEFAULT = DataNature.CONTINUOUS;

	/**
	 * The cached value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected DataNature nature = NATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final DataType TYPE_EDEFAULT = DataType.REAL;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DataType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PortProperty> properties;

	/**
	 * The default value of the '{@link #getRTD() <em>RTD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTD()
	 * @generated
	 * @ordered
	 */
	protected static final String RTD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRTD() <em>RTD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTD()
	 * @generated
	 * @ordered
	 */
	protected String rtd = RTD_EDEFAULT;

	/**
	 * The default value of the '{@link #isMonitored() <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MONITORED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMonitored() <em>Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected boolean monitored = MONITORED_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitValue() <em>Init Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitValue()
	 * @generated
	 * @ordered
	 */
	protected static final String INIT_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitValue() <em>Init Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitValue()
	 * @generated
	 * @ordered
	 */
	protected String initValue = INIT_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVariableName() <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableName()
	 * @generated
	 * @ordered
	 */
	protected static final String VARIABLE_NAME_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getVariableName() <em>Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableName()
	 * @generated
	 * @ordered
	 */
	protected String variableName = VARIABLE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MbilangPackage.Literals.PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDirection getDirection() {
		return direction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirection(PortDirection newDirection) {
		PortDirection oldDirection = direction;
		direction = newDirection == null ? DIRECTION_EDEFAULT : newDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__DIRECTION, oldDirection,
					direction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IOEvent> getIoevents() {
		if (ioevents == null) {
			ioevents = new EObjectContainmentEList<IOEvent>(IOEvent.class, this, MbilangPackage.PORT__IOEVENTS);
		}
		return ioevents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference getTemporalreference() {
		if (temporalreference != null && temporalreference.eIsProxy()) {
			InternalEObject oldTemporalreference = (InternalEObject) temporalreference;
			temporalreference = (ModelTemporalReference) eResolveProxy(oldTemporalreference);
			if (temporalreference != oldTemporalreference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MbilangPackage.PORT__TEMPORALREFERENCE,
							oldTemporalreference, temporalreference));
			}
		}
		return temporalreference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference basicGetTemporalreference() {
		return temporalreference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemporalreference(ModelTemporalReference newTemporalreference) {
		ModelTemporalReference oldTemporalreference = temporalreference;
		temporalreference = newTemporalreference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__TEMPORALREFERENCE,
					oldTemporalreference, temporalreference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataNature getNature() {
		return nature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNature(DataNature newNature) {
		DataNature oldNature = nature;
		nature = newNature == null ? NATURE_EDEFAULT : newNature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__NATURE, oldNature, nature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DataType newType) {
		DataType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<PortProperty>(PortProperty.class, this,
					MbilangPackage.PORT__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRTD() {
		return rtd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRTD(String newRTD) {
		String oldRTD = rtd;
		rtd = newRTD;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__RTD, oldRTD, rtd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMonitored() {
		return monitored;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitored(boolean newMonitored) {
		boolean oldMonitored = monitored;
		monitored = newMonitored;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__MONITORED, oldMonitored,
					monitored));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInitValue() {
		return initValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitValue(String newInitValue) {
		String oldInitValue = initValue;
		initValue = newInitValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__INIT_VALUE, oldInitValue,
					initValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableName(String newVariableName) {
		String oldVariableName = variableName;
		variableName = newVariableName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__VARIABLE_NAME, oldVariableName,
					variableName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.PORT__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MbilangPackage.PORT__IOEVENTS:
			return ((InternalEList<?>) getIoevents()).basicRemove(otherEnd, msgs);
		case MbilangPackage.PORT__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MbilangPackage.PORT__DIRECTION:
			return getDirection();
		case MbilangPackage.PORT__IOEVENTS:
			return getIoevents();
		case MbilangPackage.PORT__TEMPORALREFERENCE:
			if (resolve)
				return getTemporalreference();
			return basicGetTemporalreference();
		case MbilangPackage.PORT__NATURE:
			return getNature();
		case MbilangPackage.PORT__TYPE:
			return getType();
		case MbilangPackage.PORT__PROPERTIES:
			return getProperties();
		case MbilangPackage.PORT__RTD:
			return getRTD();
		case MbilangPackage.PORT__MONITORED:
			return isMonitored();
		case MbilangPackage.PORT__INIT_VALUE:
			return getInitValue();
		case MbilangPackage.PORT__VARIABLE_NAME:
			return getVariableName();
		case MbilangPackage.PORT__COLOR:
			return getColor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MbilangPackage.PORT__DIRECTION:
			setDirection((PortDirection) newValue);
			return;
		case MbilangPackage.PORT__IOEVENTS:
			getIoevents().clear();
			getIoevents().addAll((Collection<? extends IOEvent>) newValue);
			return;
		case MbilangPackage.PORT__TEMPORALREFERENCE:
			setTemporalreference((ModelTemporalReference) newValue);
			return;
		case MbilangPackage.PORT__NATURE:
			setNature((DataNature) newValue);
			return;
		case MbilangPackage.PORT__TYPE:
			setType((DataType) newValue);
			return;
		case MbilangPackage.PORT__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends PortProperty>) newValue);
			return;
		case MbilangPackage.PORT__RTD:
			setRTD((String) newValue);
			return;
		case MbilangPackage.PORT__MONITORED:
			setMonitored((Boolean) newValue);
			return;
		case MbilangPackage.PORT__INIT_VALUE:
			setInitValue((String) newValue);
			return;
		case MbilangPackage.PORT__VARIABLE_NAME:
			setVariableName((String) newValue);
			return;
		case MbilangPackage.PORT__COLOR:
			setColor((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MbilangPackage.PORT__DIRECTION:
			setDirection(DIRECTION_EDEFAULT);
			return;
		case MbilangPackage.PORT__IOEVENTS:
			getIoevents().clear();
			return;
		case MbilangPackage.PORT__TEMPORALREFERENCE:
			setTemporalreference((ModelTemporalReference) null);
			return;
		case MbilangPackage.PORT__NATURE:
			setNature(NATURE_EDEFAULT);
			return;
		case MbilangPackage.PORT__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case MbilangPackage.PORT__PROPERTIES:
			getProperties().clear();
			return;
		case MbilangPackage.PORT__RTD:
			setRTD(RTD_EDEFAULT);
			return;
		case MbilangPackage.PORT__MONITORED:
			setMonitored(MONITORED_EDEFAULT);
			return;
		case MbilangPackage.PORT__INIT_VALUE:
			setInitValue(INIT_VALUE_EDEFAULT);
			return;
		case MbilangPackage.PORT__VARIABLE_NAME:
			setVariableName(VARIABLE_NAME_EDEFAULT);
			return;
		case MbilangPackage.PORT__COLOR:
			setColor(COLOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MbilangPackage.PORT__DIRECTION:
			return direction != DIRECTION_EDEFAULT;
		case MbilangPackage.PORT__IOEVENTS:
			return ioevents != null && !ioevents.isEmpty();
		case MbilangPackage.PORT__TEMPORALREFERENCE:
			return temporalreference != null;
		case MbilangPackage.PORT__NATURE:
			return nature != NATURE_EDEFAULT;
		case MbilangPackage.PORT__TYPE:
			return type != TYPE_EDEFAULT;
		case MbilangPackage.PORT__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case MbilangPackage.PORT__RTD:
			return RTD_EDEFAULT == null ? rtd != null : !RTD_EDEFAULT.equals(rtd);
		case MbilangPackage.PORT__MONITORED:
			return monitored != MONITORED_EDEFAULT;
		case MbilangPackage.PORT__INIT_VALUE:
			return INIT_VALUE_EDEFAULT == null ? initValue != null : !INIT_VALUE_EDEFAULT.equals(initValue);
		case MbilangPackage.PORT__VARIABLE_NAME:
			return VARIABLE_NAME_EDEFAULT == null ? variableName != null : !VARIABLE_NAME_EDEFAULT.equals(variableName);
		case MbilangPackage.PORT__COLOR:
			return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (direction: ");
		result.append(direction);
		result.append(", nature: ");
		result.append(nature);
		result.append(", type: ");
		result.append(type);
		result.append(", RTD: ");
		result.append(rtd);
		result.append(", monitored: ");
		result.append(monitored);
		result.append(", initValue: ");
		result.append(initValue);
		result.append(", variableName: ");
		result.append(variableName);
		result.append(", color: ");
		result.append(color);
		result.append(')');
		return result.toString();
	}

} //PortImpl
