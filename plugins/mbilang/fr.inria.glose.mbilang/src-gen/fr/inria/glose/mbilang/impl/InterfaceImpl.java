/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.Interface;
import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.ModelProperty;
import fr.inria.glose.mbilang.ModelTemporalReference;
import fr.inria.glose.mbilang.Port;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getTemporalreferences <em>Temporalreferences</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getModel <em>Model</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getFMUPath <em>FMU Path</em>}</li>
 *   <li>{@link fr.inria.glose.mbilang.impl.InterfaceImpl#getGemocExecutablePath <em>Gemoc Executable Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceImpl extends NamedElementImpl implements Interface {
	/**
	 * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<Port> ports;

	/**
	 * The cached value of the '{@link #getTemporalreferences() <em>Temporalreferences</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalreferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelTemporalReference> temporalreferences;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelProperty> properties;

	/**
	 * The cached value of the '{@link #getModel() <em>Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel()
	 * @generated
	 * @ordered
	 */
	protected EObject model;

	/**
	 * The default value of the '{@link #getFMUPath() <em>FMU Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFMUPath()
	 * @generated
	 * @ordered
	 */
	protected static final String FMU_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFMUPath() <em>FMU Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFMUPath()
	 * @generated
	 * @ordered
	 */
	protected String fmuPath = FMU_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getGemocExecutablePath() <em>Gemoc Executable Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGemocExecutablePath()
	 * @generated
	 * @ordered
	 */
	protected static final String GEMOC_EXECUTABLE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGemocExecutablePath() <em>Gemoc Executable Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGemocExecutablePath()
	 * @generated
	 * @ordered
	 */
	protected String gemocExecutablePath = GEMOC_EXECUTABLE_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MbilangPackage.Literals.INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Port> getPorts() {
		if (ports == null) {
			ports = new EObjectContainmentEList<Port>(Port.class, this, MbilangPackage.INTERFACE__PORTS);
		}
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelTemporalReference> getTemporalreferences() {
		if (temporalreferences == null) {
			temporalreferences = new EObjectContainmentEList<ModelTemporalReference>(ModelTemporalReference.class, this,
					MbilangPackage.INTERFACE__TEMPORALREFERENCES);
		}
		return temporalreferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelProperty> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<ModelProperty>(ModelProperty.class, this,
					MbilangPackage.INTERFACE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getModel() {
		if (model != null && model.eIsProxy()) {
			InternalEObject oldModel = (InternalEObject) model;
			model = eResolveProxy(oldModel);
			if (model != oldModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MbilangPackage.INTERFACE__MODEL, oldModel,
							model));
			}
		}
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetModel() {
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel(EObject newModel) {
		EObject oldModel = model;
		model = newModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.INTERFACE__MODEL, oldModel, model));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFMUPath() {
		return fmuPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFMUPath(String newFMUPath) {
		String oldFMUPath = fmuPath;
		fmuPath = newFMUPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.INTERFACE__FMU_PATH, oldFMUPath,
					fmuPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGemocExecutablePath() {
		return gemocExecutablePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGemocExecutablePath(String newGemocExecutablePath) {
		String oldGemocExecutablePath = gemocExecutablePath;
		gemocExecutablePath = newGemocExecutablePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MbilangPackage.INTERFACE__GEMOC_EXECUTABLE_PATH,
					oldGemocExecutablePath, gemocExecutablePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MbilangPackage.INTERFACE__PORTS:
			return ((InternalEList<?>) getPorts()).basicRemove(otherEnd, msgs);
		case MbilangPackage.INTERFACE__TEMPORALREFERENCES:
			return ((InternalEList<?>) getTemporalreferences()).basicRemove(otherEnd, msgs);
		case MbilangPackage.INTERFACE__PROPERTIES:
			return ((InternalEList<?>) getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MbilangPackage.INTERFACE__PORTS:
			return getPorts();
		case MbilangPackage.INTERFACE__TEMPORALREFERENCES:
			return getTemporalreferences();
		case MbilangPackage.INTERFACE__PROPERTIES:
			return getProperties();
		case MbilangPackage.INTERFACE__MODEL:
			if (resolve)
				return getModel();
			return basicGetModel();
		case MbilangPackage.INTERFACE__FMU_PATH:
			return getFMUPath();
		case MbilangPackage.INTERFACE__GEMOC_EXECUTABLE_PATH:
			return getGemocExecutablePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MbilangPackage.INTERFACE__PORTS:
			getPorts().clear();
			getPorts().addAll((Collection<? extends Port>) newValue);
			return;
		case MbilangPackage.INTERFACE__TEMPORALREFERENCES:
			getTemporalreferences().clear();
			getTemporalreferences().addAll((Collection<? extends ModelTemporalReference>) newValue);
			return;
		case MbilangPackage.INTERFACE__PROPERTIES:
			getProperties().clear();
			getProperties().addAll((Collection<? extends ModelProperty>) newValue);
			return;
		case MbilangPackage.INTERFACE__MODEL:
			setModel((EObject) newValue);
			return;
		case MbilangPackage.INTERFACE__FMU_PATH:
			setFMUPath((String) newValue);
			return;
		case MbilangPackage.INTERFACE__GEMOC_EXECUTABLE_PATH:
			setGemocExecutablePath((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MbilangPackage.INTERFACE__PORTS:
			getPorts().clear();
			return;
		case MbilangPackage.INTERFACE__TEMPORALREFERENCES:
			getTemporalreferences().clear();
			return;
		case MbilangPackage.INTERFACE__PROPERTIES:
			getProperties().clear();
			return;
		case MbilangPackage.INTERFACE__MODEL:
			setModel((EObject) null);
			return;
		case MbilangPackage.INTERFACE__FMU_PATH:
			setFMUPath(FMU_PATH_EDEFAULT);
			return;
		case MbilangPackage.INTERFACE__GEMOC_EXECUTABLE_PATH:
			setGemocExecutablePath(GEMOC_EXECUTABLE_PATH_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MbilangPackage.INTERFACE__PORTS:
			return ports != null && !ports.isEmpty();
		case MbilangPackage.INTERFACE__TEMPORALREFERENCES:
			return temporalreferences != null && !temporalreferences.isEmpty();
		case MbilangPackage.INTERFACE__PROPERTIES:
			return properties != null && !properties.isEmpty();
		case MbilangPackage.INTERFACE__MODEL:
			return model != null;
		case MbilangPackage.INTERFACE__FMU_PATH:
			return FMU_PATH_EDEFAULT == null ? fmuPath != null : !FMU_PATH_EDEFAULT.equals(fmuPath);
		case MbilangPackage.INTERFACE__GEMOC_EXECUTABLE_PATH:
			return GEMOC_EXECUTABLE_PATH_EDEFAULT == null ? gemocExecutablePath != null
					: !GEMOC_EXECUTABLE_PATH_EDEFAULT.equals(gemocExecutablePath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (FMUPath: ");
		result.append(fmuPath);
		result.append(", GemocExecutablePath: ");
		result.append(gemocExecutablePath);
		result.append(')');
		return result.toString();
	}

} //InterfaceImpl
