/**
 */
package fr.inria.glose.mbilang.impl;

import fr.inria.glose.mbilang.DataNature;
import fr.inria.glose.mbilang.DataType;
import fr.inria.glose.mbilang.DeltaStepSize;
import fr.inria.glose.mbilang.DiscontinuityLocator;
import fr.inria.glose.mbilang.Extrapolation;
import fr.inria.glose.mbilang.HistoryProvider;
import fr.inria.glose.mbilang.IOEvent;
import fr.inria.glose.mbilang.Interface;
import fr.inria.glose.mbilang.Interpolation;
import fr.inria.glose.mbilang.MbilangFactory;
import fr.inria.glose.mbilang.MbilangPackage;
import fr.inria.glose.mbilang.ModelProperty;
import fr.inria.glose.mbilang.ModelTemporalReference;
import fr.inria.glose.mbilang.NamedElement;
import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mbilang.PortDirection;
import fr.inria.glose.mbilang.PortProperty;
import fr.inria.glose.mbilang.ReadyToRead;
import fr.inria.glose.mbilang.Rollback;
import fr.inria.glose.mbilang.SuperDenseTime;
import fr.inria.glose.mbilang.TemporalReference;
import fr.inria.glose.mbilang.Triggered;
import fr.inria.glose.mbilang.Updated;
import fr.inria.glose.mbilang.ZeroCrossingDetection;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MbilangPackageImpl extends EPackageImpl implements MbilangPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelTemporalReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ioEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass updatedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readyToReadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rollbackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass superDenseTimeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deltaStepSizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass zeroCrossingDetectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extrapolationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interpolationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass discontinuityLocatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historyProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass triggeredEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataNatureEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum temporalReferenceEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portDirectionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.inria.glose.mbilang.MbilangPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MbilangPackageImpl() {
		super(eNS_URI, MbilangFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link MbilangPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MbilangPackage init() {
		if (isInited)
			return (MbilangPackage) EPackage.Registry.INSTANCE.getEPackage(MbilangPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredMbilangPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		MbilangPackageImpl theMbilangPackage = registeredMbilangPackage instanceof MbilangPackageImpl
				? (MbilangPackageImpl) registeredMbilangPackage
				: new MbilangPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theMbilangPackage.createPackageContents();

		// Initialize created meta-data
		theMbilangPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMbilangPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MbilangPackage.eNS_URI, theMbilangPackage);
		return theMbilangPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterface() {
		return interfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterface_Ports() {
		return (EReference) interfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterface_Temporalreferences() {
		return (EReference) interfaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterface_Properties() {
		return (EReference) interfaceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterface_Model() {
		return (EReference) interfaceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterface_FMUPath() {
		return (EAttribute) interfaceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterface_GemocExecutablePath() {
		return (EAttribute) interfaceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelTemporalReference() {
		return modelTemporalReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelTemporalReference_Reference() {
		return (EAttribute) modelTemporalReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort() {
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Direction() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Ioevents() {
		return (EReference) portEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Temporalreference() {
		return (EReference) portEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Nature() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Type() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPort_Properties() {
		return (EReference) portEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_RTD() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Monitored() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_InitValue() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_VariableName() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Color() {
		return (EAttribute) portEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIOEvent() {
		return ioEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIOEvent_MSE() {
		return (EReference) ioEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUpdated() {
		return updatedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReadyToRead() {
		return readyToReadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelProperty() {
		return modelPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute) namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRollback() {
		return rollbackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRollback_State() {
		return (EAttribute) rollbackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSuperDenseTime() {
		return superDenseTimeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSuperDenseTime_State() {
		return (EAttribute) superDenseTimeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeltaStepSize() {
		return deltaStepSizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeltaStepSize_State() {
		return (EAttribute) deltaStepSizeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortProperty() {
		return portPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getZeroCrossingDetection() {
		return zeroCrossingDetectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getZeroCrossingDetection_State() {
		return (EAttribute) zeroCrossingDetectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtrapolation() {
		return extrapolationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtrapolation_State() {
		return (EAttribute) extrapolationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterpolation() {
		return interpolationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterpolation_State() {
		return (EAttribute) interpolationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiscontinuityLocator() {
		return discontinuityLocatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiscontinuityLocator_State() {
		return (EAttribute) discontinuityLocatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoryProvider() {
		return historyProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryProvider_State() {
		return (EAttribute) historyProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTriggered() {
		return triggeredEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataNature() {
		return dataNatureEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTemporalReference() {
		return temporalReferenceEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataType() {
		return dataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortDirection() {
		return portDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MbilangFactory getMbilangFactory() {
		return (MbilangFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		interfaceEClass = createEClass(INTERFACE);
		createEReference(interfaceEClass, INTERFACE__PORTS);
		createEReference(interfaceEClass, INTERFACE__TEMPORALREFERENCES);
		createEReference(interfaceEClass, INTERFACE__PROPERTIES);
		createEReference(interfaceEClass, INTERFACE__MODEL);
		createEAttribute(interfaceEClass, INTERFACE__FMU_PATH);
		createEAttribute(interfaceEClass, INTERFACE__GEMOC_EXECUTABLE_PATH);

		modelTemporalReferenceEClass = createEClass(MODEL_TEMPORAL_REFERENCE);
		createEAttribute(modelTemporalReferenceEClass, MODEL_TEMPORAL_REFERENCE__REFERENCE);

		portEClass = createEClass(PORT);
		createEAttribute(portEClass, PORT__DIRECTION);
		createEReference(portEClass, PORT__IOEVENTS);
		createEReference(portEClass, PORT__TEMPORALREFERENCE);
		createEAttribute(portEClass, PORT__NATURE);
		createEAttribute(portEClass, PORT__TYPE);
		createEReference(portEClass, PORT__PROPERTIES);
		createEAttribute(portEClass, PORT__RTD);
		createEAttribute(portEClass, PORT__MONITORED);
		createEAttribute(portEClass, PORT__INIT_VALUE);
		createEAttribute(portEClass, PORT__VARIABLE_NAME);
		createEAttribute(portEClass, PORT__COLOR);

		ioEventEClass = createEClass(IO_EVENT);
		createEReference(ioEventEClass, IO_EVENT__MSE);

		updatedEClass = createEClass(UPDATED);

		readyToReadEClass = createEClass(READY_TO_READ);

		modelPropertyEClass = createEClass(MODEL_PROPERTY);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		rollbackEClass = createEClass(ROLLBACK);
		createEAttribute(rollbackEClass, ROLLBACK__STATE);

		superDenseTimeEClass = createEClass(SUPER_DENSE_TIME);
		createEAttribute(superDenseTimeEClass, SUPER_DENSE_TIME__STATE);

		deltaStepSizeEClass = createEClass(DELTA_STEP_SIZE);
		createEAttribute(deltaStepSizeEClass, DELTA_STEP_SIZE__STATE);

		portPropertyEClass = createEClass(PORT_PROPERTY);

		zeroCrossingDetectionEClass = createEClass(ZERO_CROSSING_DETECTION);
		createEAttribute(zeroCrossingDetectionEClass, ZERO_CROSSING_DETECTION__STATE);

		extrapolationEClass = createEClass(EXTRAPOLATION);
		createEAttribute(extrapolationEClass, EXTRAPOLATION__STATE);

		interpolationEClass = createEClass(INTERPOLATION);
		createEAttribute(interpolationEClass, INTERPOLATION__STATE);

		discontinuityLocatorEClass = createEClass(DISCONTINUITY_LOCATOR);
		createEAttribute(discontinuityLocatorEClass, DISCONTINUITY_LOCATOR__STATE);

		historyProviderEClass = createEClass(HISTORY_PROVIDER);
		createEAttribute(historyProviderEClass, HISTORY_PROVIDER__STATE);

		triggeredEClass = createEClass(TRIGGERED);

		// Create enums
		dataNatureEEnum = createEEnum(DATA_NATURE);
		temporalReferenceEEnum = createEEnum(TEMPORAL_REFERENCE);
		dataTypeEEnum = createEEnum(DATA_TYPE);
		portDirectionEEnum = createEEnum(PORT_DIRECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		interfaceEClass.getESuperTypes().add(this.getNamedElement());
		modelTemporalReferenceEClass.getESuperTypes().add(this.getNamedElement());
		portEClass.getESuperTypes().add(this.getNamedElement());
		ioEventEClass.getESuperTypes().add(this.getNamedElement());
		updatedEClass.getESuperTypes().add(this.getIOEvent());
		readyToReadEClass.getESuperTypes().add(this.getIOEvent());
		rollbackEClass.getESuperTypes().add(this.getModelProperty());
		superDenseTimeEClass.getESuperTypes().add(this.getModelProperty());
		deltaStepSizeEClass.getESuperTypes().add(this.getModelProperty());
		zeroCrossingDetectionEClass.getESuperTypes().add(this.getPortProperty());
		extrapolationEClass.getESuperTypes().add(this.getPortProperty());
		interpolationEClass.getESuperTypes().add(this.getPortProperty());
		discontinuityLocatorEClass.getESuperTypes().add(this.getPortProperty());
		historyProviderEClass.getESuperTypes().add(this.getPortProperty());
		triggeredEClass.getESuperTypes().add(this.getIOEvent());

		// Initialize classes, features, and operations; add parameters
		initEClass(interfaceEClass, Interface.class, "Interface", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterface_Ports(), this.getPort(), null, "ports", null, 0, -1, Interface.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getInterface_Temporalreferences(), this.getModelTemporalReference(), null, "temporalreferences",
				null, 1, -1, Interface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterface_Properties(), this.getModelProperty(), null, "properties", null, 0, -1,
				Interface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInterface_Model(), ecorePackage.getEObject(), null, "model", null, 0, 1, Interface.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterface_FMUPath(), ecorePackage.getEString(), "FMUPath", null, 0, 1, Interface.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterface_GemocExecutablePath(), ecorePackage.getEString(), "GemocExecutablePath", null, 0, 1,
				Interface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(modelTemporalReferenceEClass, ModelTemporalReference.class, "ModelTemporalReference", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModelTemporalReference_Reference(), this.getTemporalReference(), "reference", null, 0, 1,
				ModelTemporalReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPort_Direction(), this.getPortDirection(), "direction", null, 0, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPort_Ioevents(), this.getIOEvent(), null, "ioevents", null, 0, 2, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getPort_Temporalreference(), this.getModelTemporalReference(), null, "temporalreference", null,
				0, 1, Port.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Nature(), this.getDataNature(), "nature", "continuous", 0, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Type(), this.getDataType(), "type", null, 0, 1, Port.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPort_Properties(), this.getPortProperty(), null, "properties", null, 0, -1, Port.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_RTD(), ecorePackage.getEString(), "RTD", null, 0, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Monitored(), ecorePackage.getEBoolean(), "monitored", "false", 0, 1, Port.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_InitValue(), ecorePackage.getEString(), "initValue", null, 0, 1, Port.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_VariableName(), ecorePackage.getEString(), "variableName", "", 0, 1, Port.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPort_Color(), ecorePackage.getEString(), "color", null, 0, 1, Port.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ioEventEClass, IOEvent.class, "IOEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getIOEvent_MSE(), ecorePackage.getEObject(), null, "MSE", null, 0, 1, IOEvent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(updatedEClass, Updated.class, "Updated", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(readyToReadEClass, ReadyToRead.class, "ReadyToRead", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(modelPropertyEClass, ModelProperty.class, "ModelProperty", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rollbackEClass, Rollback.class, "Rollback", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRollback_State(), ecorePackage.getEBoolean(), "state", null, 0, 1, Rollback.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(superDenseTimeEClass, SuperDenseTime.class, "SuperDenseTime", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSuperDenseTime_State(), ecorePackage.getEBoolean(), "state", null, 0, 1, SuperDenseTime.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deltaStepSizeEClass, DeltaStepSize.class, "DeltaStepSize", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeltaStepSize_State(), ecorePackage.getEBoolean(), "state", null, 0, 1, DeltaStepSize.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portPropertyEClass, PortProperty.class, "PortProperty", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(zeroCrossingDetectionEClass, ZeroCrossingDetection.class, "ZeroCrossingDetection", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getZeroCrossingDetection_State(), ecorePackage.getEBoolean(), "state", null, 0, 1,
				ZeroCrossingDetection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(extrapolationEClass, Extrapolation.class, "Extrapolation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtrapolation_State(), ecorePackage.getEBoolean(), "state", null, 0, 1, Extrapolation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interpolationEClass, Interpolation.class, "Interpolation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInterpolation_State(), ecorePackage.getEBoolean(), "state", null, 0, 1, Interpolation.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(discontinuityLocatorEClass, DiscontinuityLocator.class, "DiscontinuityLocator", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDiscontinuityLocator_State(), ecorePackage.getEBoolean(), "state", null, 0, 1,
				DiscontinuityLocator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(historyProviderEClass, HistoryProvider.class, "HistoryProvider", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHistoryProvider_State(), ecorePackage.getEBoolean(), "state", null, 0, 1,
				HistoryProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(triggeredEClass, Triggered.class, "Triggered", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(dataNatureEEnum, DataNature.class, "DataNature");
		addEEnumLiteral(dataNatureEEnum, DataNature.CONSTANT);
		addEEnumLiteral(dataNatureEEnum, DataNature.SPURIOUS);
		addEEnumLiteral(dataNatureEEnum, DataNature.PIECEWISE_CONSTANT);
		addEEnumLiteral(dataNatureEEnum, DataNature.PIECEWISE_CONTINUOUS);
		addEEnumLiteral(dataNatureEEnum, DataNature.CONTINUOUS);

		initEEnum(temporalReferenceEEnum, TemporalReference.class, "TemporalReference");
		addEEnumLiteral(temporalReferenceEEnum, TemporalReference.TIME);
		addEEnumLiteral(temporalReferenceEEnum, TemporalReference.DISTANCE);
		addEEnumLiteral(temporalReferenceEEnum, TemporalReference.ANGLE);

		initEEnum(dataTypeEEnum, DataType.class, "DataType");
		addEEnumLiteral(dataTypeEEnum, DataType.REAL);
		addEEnumLiteral(dataTypeEEnum, DataType.INTEGER);
		addEEnumLiteral(dataTypeEEnum, DataType.BOOLEAN);
		addEEnumLiteral(dataTypeEEnum, DataType.STRING);
		addEEnumLiteral(dataTypeEEnum, DataType.ENUMERATION);

		initEEnum(portDirectionEEnum, PortDirection.class, "PortDirection");
		addEEnumLiteral(portDirectionEEnum, PortDirection.INPUT);
		addEEnumLiteral(portDirectionEEnum, PortDirection.OUTPUT);
		addEEnumLiteral(portDirectionEEnum, PortDirection.VARYING);

		// Create resource
		createResource(eNS_URI);
	}

} //MbilangPackageImpl
