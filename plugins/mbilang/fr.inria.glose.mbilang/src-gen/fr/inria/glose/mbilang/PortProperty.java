/**
 */
package fr.inria.glose.mbilang;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getPortProperty()
 * @model abstract="true"
 * @generated
 */
public interface PortProperty extends EObject {
} // PortProperty
