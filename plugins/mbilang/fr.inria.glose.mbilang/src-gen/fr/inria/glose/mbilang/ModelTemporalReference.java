/**
 */
package fr.inria.glose.mbilang;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Temporal Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mbilang.ModelTemporalReference#getReference <em>Reference</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mbilang.MbilangPackage#getModelTemporalReference()
 * @model
 * @generated
 */
public interface ModelTemporalReference extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.glose.mbilang.TemporalReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' attribute.
	 * @see fr.inria.glose.mbilang.TemporalReference
	 * @see #setReference(TemporalReference)
	 * @see fr.inria.glose.mbilang.MbilangPackage#getModelTemporalReference_Reference()
	 * @model
	 * @generated
	 */
	TemporalReference getReference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mbilang.ModelTemporalReference#getReference <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' attribute.
	 * @see fr.inria.glose.mbilang.TemporalReference
	 * @see #getReference()
	 * @generated
	 */
	void setReference(TemporalReference value);

} // ModelTemporalReference
