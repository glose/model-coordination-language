document.addEventListener('DOMContentLoaded',function(){

        var cy = window.cy = cytoscape({
          container: document.getElementById('cy'),
					autounselectify: false,
					boxSelectionEnabled: false,

			style: cytoscape.stylesheet()
		    .selector('node').css({
		        'background-color': '#ad1a66',
		        'width' : '8',
		        'height' : '8'
		    }).selector('edge').css({
		    	'curve-style': 'bezier',
		    	'width': 1,
		        'line-color': '#ad1a66',
		    }).selector('edge.directedtrue').css({
		    	'target-arrow-shape': 'triangle-backcurve',
		    	'arrow-scale': '0.5',
		    	'width': 1,
		        'line-color': '#ad1a66',
		        'target-arrow-color': '#ad1a66',
		    }).selector(':selected').css({
		    	label: 'data(label)',
		    	'width': 4,
		    	'color': '#00BFFF',
		    	'background-color':'#00BFFF',
		    	'target-arrow-color': '#00BFFF',
		    	'line-color':'#00BFFF',
		    }).selector('.highlighted').css({
		    	label: 'data(label)',
		    	'color': '#FF0000',
		    	'width': 4,
		    	'background-color':'#FF0000',
		    	'target-arrow-color': '#FF0000',
		    	'line-color':'#FF0000',
		    })
		    
		    });
        
        
        
        document.getElementById("layoutCoseColaButton").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'cola',
              animate: true, // whether to show the layout as it's running
  			  refresh: 2, // number of ticks per frame; higher is faster but more jerky
  			  maxSimulationTime: 40000, // max length in ms to run the layout
			  ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
			  fit: true, // on every layout reposition of nodes, fit the viewport
			  padding: 30, // padding around the simulation
			  boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
			  nodeDimensionsIncludeLabels: undefined, // whether labels should be included in determining the space used by a node (default true)
			
			  // layout event callbacks
			  ready: function(){}, // on layoutready
			  stop: function(){}, // on layoutstop
			
			  // positioning options
			  randomize: false, // use random node positions at beginning of layout
			  avoidOverlap: true, // if true, prevents overlap of node bounding boxes
			  handleDisconnected: true, // if true, avoids disconnected components from overlapping
			  nodeSpacing: function( node ){ return 10; }, // extra spacing around nodes
			  flow: undefined, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
			  alignment: undefined, // relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }
			
			  // different methods of specifying edge length
			  // each can be a constant numerical value or a function like `function( edge ){ return 2; }`
			  edgeLength: undefined, // sets edge length directly in simulation
			  edgeSymDiffLength: undefined, // symmetric diff edge length in simulation
			  edgeJaccardLength: undefined, // jaccard edge length in simulation
			
			  // iterations of cola algorithm; uses default values on undefined
			  unconstrIter: undefined, // unconstrained initial layout iterations
			  userConstIter: undefined, // initial layout iterations with user-specified constraints
			  allConstIter: undefined, // initial layout iterations with all constraints including non-overlap
			
			  // infinite layout options
			  infinite: false // overrides all other options for a forces-all-the-time mode
          });

          layout.run();
        });
        document.getElementById("layoutCoseButton").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'cose',
            animate:true,
            randomize: false
          });

          layout.run();
        });
        document.getElementById("layoutEulerButton").addEventListener("click", function(){
		var layout = cy.layout({
		  name: 'euler',
		  timeStep: 40,
		  refresh: 30,
		  animate: true,
		  animationDuration: undefined,
		  animationEasing: undefined,
		  maxIterations: 10000,
		  maxSimulationTime: 40000,
		  randomize: false
		});

          layout.run();
        });
        document.getElementById("randomize").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'random'
          });
          layout.run();
        });
        document.getElementById("layoutConcentricButton").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'concentric'
          });
          layout.run();
        });
        document.getElementById("layoutCircleButton").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'circle'
          });
          layout.run();
        });
        document.getElementById("layoutBreadthFirstButton").addEventListener("click", function(){
          var layout = cy.layout({
            name: 'breadthfirst'
          });

          layout.run();
        });
      });        

function searcheIndex(node){
	var temp = cy.elements('#'+node);
	if (temp.length == 0) return -1;
	return temp[0].data('index');
}

function searcheName(index){
	var temp = cy.elements("node[index="+index+"]");
	if (temp.length == 0) return "null";
	return temp[0].data('id');
}

// requestAnim shim layer by Paul Irish
window.requestAnimationFrame = (function(){
  return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function(/* function */ callback, /* DOMElement */ element){
              window.setTimeout(callback, 1000 / 60);
          };
})();

function runLayout(){
var layout = cy.layout({
  name: 'concentric'
});
	
	layout.run();
	return 0// ready-start;
}