/*******************************************************************************
 * Copyright (c) 2017 I3S laboratory, INRIA and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S laboratory and INRIA Kairos - initial API and implementation
 *******************************************************************************/
package fr.inria.kairos.timesquare.grph.viewer;

import java.util.ArrayList;
import java.util.Iterator;

import fr.inria.aoste.utils.grph.FindAllCycles;
import grph.Grph;
import grph.in_memory.InMemoryGrph;
import grph.io.EdgeListReader;
import grph.io.GraphBuildException;
import grph.io.ParseException;
import grph.path.Path;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntArraySet;
import it.unimi.dsi.fastutil.ints.IntSet;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import toools.text.TextUtilities;

public class GrphViewControler{
	public GrphView view;
	public Grph model;

	
	public GrphViewControler(GrphView v) {
		view = v;
		model = new InMemoryGrph(); //default grph
//		model.dgrid(10,20);
	}
	
	public void addNode(int v) {
				String name = "n"+v;
				view.addNode(name, v);
				return;
	}
	
	public void addEdge(int edge, String label, int source, int target, boolean directed) {
				String id = "e"+edge;
				view.addEdge(id, label, "n"+source,"n"+target, directed,edge);
				return;
	}
	

	
	public void doSelectElementInView() {
		view.selectElements(selectedElements);
		
	}
	private ArrayList<String> selectedElements = new ArrayList<String>();
	private FindAllCycles cycleProvider = null;
	private Iterator<IntArrayList> cycleIterator = null;
	private boolean hasNext;
	private int initialState;
	public ArrayList<IntSet> allInitPath = new ArrayList<IntSet>();
	private IntArrayList lastCycleNodes = new IntArrayList();
	private IntSet lastPath = new IntArraySet();
	private Grph g;
	
	public void addEdgeToSelectedElements(int e) {
		selectedElements.add("e"+e);
	}
	public void addNodeToSelectedElements(int n) {
		selectedElements.add("n"+n);
	}

	
	
public void addGrph(int initialState) {
		
		Worker<Void> loadWorker = view.internalView.webEngine.getLoadWorker();
		if(loadWorker.getState() == State.FAILED) {
			throw new RuntimeException("Web engine load worker in failed state");
		}
		while (loadWorker.getState() != State.SUCCEEDED){
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("sleeping...");
		}
		
		if(model == null) {
			return;
		}
		
		view.clearAll();
		
		for(Integer ic : model.getVertices()){
			this.addNode(ic);
		}
		
		for(Integer ic : model.getEdges()){
			this.addEdge(ic, model.getEdgeLabelProperty().getValueAsString(ic), model.getDirectedSimpleEdgeTail(ic), model.getDirectedSimpleEdgeHead(ic), true);
		}
		view.internalView.autosize();
		view.layoutRun();
		
		this.initialState = initialState;
//		EdgeListReader reader = new EdgeListReader();
//		reader.setCreateDirectedEdges(true);
//		g =null;
//		try {
//			g = reader.readGraph(model.toEdgeList());
//		} catch (ParseException | GraphBuildException e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("begin G:");
//		System.out.println(g.toEdgeList());
//		System.out.println("end G");
//		System.out.println("begin G:");
//		System.out.println(model.toEdgeList());
//		System.out.println("end G");
		cycleProvider = new FindAllCycles(model, model.getVertices(),false);
		
		cycleIterator = cycleProvider.iterator();
		hasNext = cycleIterator.hasNext();
		

		
	}

	public void highlightNextSchedule() {
		selectedElements.clear();
		view.unselectAll();
		unsetClassForVertexAndEdges(new IntArrayList(lastPath), "highlighted");
		lastPath.clear();
		
//		while (allInitPath.isEmpty()) {
			if(cycleIterator == null) {
				return;
			}
			if (!hasNext) {
				return;
			}			
			lastCycleNodes.clear();
//			do {
				lastCycleNodes = cycleIterator.next();
				hasNext = cycleIterator.hasNext() && lastCycleNodes.size() > 0;
				System.out.println("cycle is "+lastCycleNodes);
//				if (!hasNext) {
//					return;
//				}
//			}while (lastCycleNodes.size() == 0);
//			
			allInitPath  = computeAllInitializationPath();
//		}
		highlightVertexAndEdges(lastCycleNodes);
		doSelectElementInView();

		lastPath = allInitPath.get(0);
		allInitPath.remove(0);
		setClassForVertexAndEdges(new IntArrayList(lastPath), "highlighted");
		
		
}
	
	public void highlightNextDifferentSchedule(int n) {
		selectedElements.clear();
		view.unselectAll();
		unsetClassForVertexAndEdges(new IntArrayList(lastPath), "highlighted");
		lastPath.clear();
		
//		while (allInitPath.isEmpty()) {
			if(cycleIterator == null) {
				return;
			}
			if (!hasNext) {
				return;
			}
			
			IntArrayList newCycleNodes = new IntArrayList();
			do{
//			do {
//				System.out.println("skipped cycle is "+newCycleNodes);
				newCycleNodes.clear();
				newCycleNodes = cycleIterator.next();
				hasNext = cycleIterator.hasNext() && newCycleNodes.size() > 0;
//				if (!hasNext) {
//					return;
//				}
//			}while (lastCycleNodes.size() == 0);
//			
			}while(hasNext && 
					toools.text.TextUtilities.computeLevenshteinDistance(newCycleNodes.toString(), lastCycleNodes.toString()) < (lastCycleNodes.size()-n > 0 ? lastCycleNodes.size()-n : 1) 
					);			
			
			System.out.println("old cycle: "+lastCycleNodes+"\n kept cycle is "+newCycleNodes);
			lastCycleNodes.clear();
			lastCycleNodes = newCycleNodes;
			allInitPath  = computeAllInitializationPath();
//		}
		highlightVertexAndEdges(lastCycleNodes);
		doSelectElementInView();

		lastPath = allInitPath.get(0);
		allInitPath.remove(0);
		setClassForVertexAndEdges(new IntArrayList(lastPath), "highlighted");
		
		
}

	private ArrayList<IntSet> computeAllInitializationPath() {
		ArrayList<IntSet> allInitPath = new ArrayList<IntSet>();
			int n = lastCycleNodes.getInt(0);
			Path p = model.getShortestPath(initialState, n);
			if (p != null) {
				IntSet pset = p.toVertexSet();
//				for (int v: pset) {
//					if ((v != n) && lastCycleNodes.contains(v)) {
//						pset.remove(v);
//					}
//				}
				pset.add(n);
//				if (!allInitPath.contains(pset)){
					allInitPath.add(pset);
//				}
			}
//		}
		return allInitPath;
	}

	
	public void setClassForVertexAndEdges(IntArrayList nodes, String classs) {
		int previous = -1;
		int first = -1;
		for (int n : nodes) {
			view.addClass("n"+n, classs);
			if (previous == -1) {
				previous = n;
				first = n;
				continue;
			}
			IntSet edges = model.getEdgesConnecting(previous, n);
			for (int e : edges) {
				view.addClass("e"+e, classs);;
			}
			previous = n;
	
	
		}
		IntSet edges = model.getEdgesConnecting(previous, first);
		
		for (int e : edges) {
			view.addClass("e"+e, classs);
		}
	}
	
	public void unsetClassForVertexAndEdges(IntArrayList nodes, String classs) {
		int previous = -1;
		int first = -1;
		for (int n : nodes) {
			view.removeClass("n"+n, classs);
			if (previous == -1) {
				previous = n;
				first = n;
				continue;
			}
			IntSet edges = model.getEdgesConnecting(previous, n);
			for (int e : edges) {
				view.removeClass("e"+e, classs);;
			}
			previous = n;
	
	
		}
		if (previous != -1 && first != -1) {
			IntSet edges = model.getEdgesConnecting(previous, first);
			for (int e : edges) {
				view.removeClass("e"+e, classs);
			}
		}
	}
	
	private void highlightVertexAndEdges(IntArrayList nodes) {
		int previous = -1;
		int first = -1;
		for (int n : nodes) {
			addNodeToSelectedElements(n);
			if (previous == -1) {
				previous = n;
				first = n;
				continue;
			}
			IntSet edges = model.getEdgesConnecting(previous, n);
			for (int e : edges) {
				addEdgeToSelectedElements(e);
			}
			previous = n;
	
	
		}
		IntSet edges = model.getEdgesConnecting(previous, first);
		
		for (int e : edges) {
			addEdgeToSelectedElements(e);
		}
	}

	public void clearAllInitPath() {
		allInitPath.clear();
	}



}
