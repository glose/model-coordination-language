package fr.inria.glose.mcl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.inria.glose.mcl.services.MCLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalMCLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_DOUBLE", "RULE_BOOLEAN", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'and'", "'xor'", "'or'", "'!'", "'not'", "'.'", "'->'", "'='", "'<>'", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'~'", "'Connector'", "'when'", "'do'", "'('", "'from'", "'to'", "')'", "'sync'", "'load'", "'event'", "'on'", "'occurs'", "'every'", "'value'", "'has'", "'been'", "'Updated'", "'is'", "'ReadyToRead'", "'ZeroCrossingDetection'", "'Extrapolation'", "'Interpolation'", "'DiscontinuityLocator'", "'HistoryProvider'", "'tolerance'", "'import'", "'#'", "'if'", "'then'", "'else'", "'endif'", "'state'"
    };
    public static final int T__50=50;
    public static final int RULE_BOOLEAN=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=7;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMCLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMCLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMCLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMCL.g"; }


    	private MCLGrammarAccess grammarAccess;

    	public void setGrammarAccess(MCLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleMCSpecification"
    // InternalMCL.g:54:1: entryRuleMCSpecification : ruleMCSpecification EOF ;
    public final void entryRuleMCSpecification() throws RecognitionException {
        try {
            // InternalMCL.g:55:1: ( ruleMCSpecification EOF )
            // InternalMCL.g:56:1: ruleMCSpecification EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleMCSpecification();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMCSpecification"


    // $ANTLR start "ruleMCSpecification"
    // InternalMCL.g:63:1: ruleMCSpecification : ( ( rule__MCSpecification__Group__0 ) ) ;
    public final void ruleMCSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:67:2: ( ( ( rule__MCSpecification__Group__0 ) ) )
            // InternalMCL.g:68:2: ( ( rule__MCSpecification__Group__0 ) )
            {
            // InternalMCL.g:68:2: ( ( rule__MCSpecification__Group__0 ) )
            // InternalMCL.g:69:3: ( rule__MCSpecification__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getGroup()); 
            }
            // InternalMCL.g:70:3: ( rule__MCSpecification__Group__0 )
            // InternalMCL.g:70:4: rule__MCSpecification__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMCSpecification"


    // $ANTLR start "entryRuleSynchronizationConstraint"
    // InternalMCL.g:79:1: entryRuleSynchronizationConstraint : ruleSynchronizationConstraint EOF ;
    public final void entryRuleSynchronizationConstraint() throws RecognitionException {
        try {
            // InternalMCL.g:80:1: ( ruleSynchronizationConstraint EOF )
            // InternalMCL.g:81:1: ruleSynchronizationConstraint EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSynchronizationConstraintRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleSynchronizationConstraint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSynchronizationConstraintRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSynchronizationConstraint"


    // $ANTLR start "ruleSynchronizationConstraint"
    // InternalMCL.g:88:1: ruleSynchronizationConstraint : ( ruleSimpleSync ) ;
    public final void ruleSynchronizationConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:92:2: ( ( ruleSimpleSync ) )
            // InternalMCL.g:93:2: ( ruleSimpleSync )
            {
            // InternalMCL.g:93:2: ( ruleSimpleSync )
            // InternalMCL.g:94:3: ruleSimpleSync
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSynchronizationConstraintAccess().getSimpleSyncParserRuleCall()); 
            }
            pushFollow(FOLLOW_2);
            ruleSimpleSync();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSynchronizationConstraintAccess().getSimpleSyncParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSynchronizationConstraint"


    // $ANTLR start "entryRuleExpOperation"
    // InternalMCL.g:104:1: entryRuleExpOperation : ruleExpOperation EOF ;
    public final void entryRuleExpOperation() throws RecognitionException {
        try {
            // InternalMCL.g:105:1: ( ruleExpOperation EOF )
            // InternalMCL.g:106:1: ruleExpOperation EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpOperationRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleExpOperation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpOperationRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpOperation"


    // $ANTLR start "ruleExpOperation"
    // InternalMCL.g:113:1: ruleExpOperation : ( ( rule__ExpOperation__Alternatives ) ) ;
    public final void ruleExpOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:117:2: ( ( ( rule__ExpOperation__Alternatives ) ) )
            // InternalMCL.g:118:2: ( ( rule__ExpOperation__Alternatives ) )
            {
            // InternalMCL.g:118:2: ( ( rule__ExpOperation__Alternatives ) )
            // InternalMCL.g:119:3: ( rule__ExpOperation__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpOperationAccess().getAlternatives()); 
            }
            // InternalMCL.g:120:3: ( rule__ExpOperation__Alternatives )
            // InternalMCL.g:120:4: rule__ExpOperation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ExpOperation__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpOperationAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpOperation"


    // $ANTLR start "entryRuleConnector"
    // InternalMCL.g:129:1: entryRuleConnector : ruleConnector EOF ;
    public final void entryRuleConnector() throws RecognitionException {
        try {
            // InternalMCL.g:130:1: ( ruleConnector EOF )
            // InternalMCL.g:131:1: ruleConnector EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleConnector();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConnector"


    // $ANTLR start "ruleConnector"
    // InternalMCL.g:138:1: ruleConnector : ( ( rule__Connector__Group__0 ) ) ;
    public final void ruleConnector() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:142:2: ( ( ( rule__Connector__Group__0 ) ) )
            // InternalMCL.g:143:2: ( ( rule__Connector__Group__0 ) )
            {
            // InternalMCL.g:143:2: ( ( rule__Connector__Group__0 ) )
            // InternalMCL.g:144:3: ( rule__Connector__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getGroup()); 
            }
            // InternalMCL.g:145:3: ( rule__Connector__Group__0 )
            // InternalMCL.g:145:4: rule__Connector__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Connector__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConnector"


    // $ANTLR start "entryRuleImportInterfaceStatement"
    // InternalMCL.g:154:1: entryRuleImportInterfaceStatement : ruleImportInterfaceStatement EOF ;
    public final void entryRuleImportInterfaceStatement() throws RecognitionException {
        try {
            // InternalMCL.g:155:1: ( ruleImportInterfaceStatement EOF )
            // InternalMCL.g:156:1: ruleImportInterfaceStatement EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportInterfaceStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleImportInterfaceStatement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportInterfaceStatementRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportInterfaceStatement"


    // $ANTLR start "ruleImportInterfaceStatement"
    // InternalMCL.g:163:1: ruleImportInterfaceStatement : ( ( rule__ImportInterfaceStatement__Group__0 ) ) ;
    public final void ruleImportInterfaceStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:167:2: ( ( ( rule__ImportInterfaceStatement__Group__0 ) ) )
            // InternalMCL.g:168:2: ( ( rule__ImportInterfaceStatement__Group__0 ) )
            {
            // InternalMCL.g:168:2: ( ( rule__ImportInterfaceStatement__Group__0 ) )
            // InternalMCL.g:169:3: ( rule__ImportInterfaceStatement__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportInterfaceStatementAccess().getGroup()); 
            }
            // InternalMCL.g:170:3: ( rule__ImportInterfaceStatement__Group__0 )
            // InternalMCL.g:170:4: rule__ImportInterfaceStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportInterfaceStatement__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportInterfaceStatementAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportInterfaceStatement"


    // $ANTLR start "entryRuleEString"
    // InternalMCL.g:179:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalMCL.g:180:1: ( ruleEString EOF )
            // InternalMCL.g:181:1: ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEStringRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEStringRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMCL.g:188:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:192:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalMCL.g:193:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalMCL.g:193:2: ( ( rule__EString__Alternatives ) )
            // InternalMCL.g:194:3: ( rule__EString__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEStringAccess().getAlternatives()); 
            }
            // InternalMCL.g:195:3: ( rule__EString__Alternatives )
            // InternalMCL.g:195:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEStringAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAbstractInteraction"
    // InternalMCL.g:204:1: entryRuleAbstractInteraction : ruleAbstractInteraction EOF ;
    public final void entryRuleAbstractInteraction() throws RecognitionException {
        try {
            // InternalMCL.g:205:1: ( ruleAbstractInteraction EOF )
            // InternalMCL.g:206:1: ruleAbstractInteraction EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractInteractionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAbstractInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractInteractionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractInteraction"


    // $ANTLR start "ruleAbstractInteraction"
    // InternalMCL.g:213:1: ruleAbstractInteraction : ( ruleAssignmentExpression ) ;
    public final void ruleAbstractInteraction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:217:2: ( ( ruleAssignmentExpression ) )
            // InternalMCL.g:218:2: ( ruleAssignmentExpression )
            {
            // InternalMCL.g:218:2: ( ruleAssignmentExpression )
            // InternalMCL.g:219:3: ruleAssignmentExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAbstractInteractionAccess().getAssignmentExpressionParserRuleCall()); 
            }
            pushFollow(FOLLOW_2);
            ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAbstractInteractionAccess().getAssignmentExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractInteraction"


    // $ANTLR start "entryRuleThresholdExpression"
    // InternalMCL.g:229:1: entryRuleThresholdExpression : ruleThresholdExpression EOF ;
    public final void entryRuleThresholdExpression() throws RecognitionException {
        try {
            // InternalMCL.g:230:1: ( ruleThresholdExpression EOF )
            // InternalMCL.g:231:1: ruleThresholdExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleThresholdExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThresholdExpression"


    // $ANTLR start "ruleThresholdExpression"
    // InternalMCL.g:238:1: ruleThresholdExpression : ( ( rule__ThresholdExpression__Group__0 ) ) ;
    public final void ruleThresholdExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:242:2: ( ( ( rule__ThresholdExpression__Group__0 ) ) )
            // InternalMCL.g:243:2: ( ( rule__ThresholdExpression__Group__0 ) )
            {
            // InternalMCL.g:243:2: ( ( rule__ThresholdExpression__Group__0 ) )
            // InternalMCL.g:244:3: ( rule__ThresholdExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:245:3: ( rule__ThresholdExpression__Group__0 )
            // InternalMCL.g:245:4: rule__ThresholdExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThresholdExpression"


    // $ANTLR start "entryRuleDisjoint"
    // InternalMCL.g:254:1: entryRuleDisjoint : ruleDisjoint EOF ;
    public final void entryRuleDisjoint() throws RecognitionException {
        try {
            // InternalMCL.g:255:1: ( ruleDisjoint EOF )
            // InternalMCL.g:256:1: ruleDisjoint EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDisjoint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisjoint"


    // $ANTLR start "ruleDisjoint"
    // InternalMCL.g:263:1: ruleDisjoint : ( ( rule__Disjoint__Group__0 ) ) ;
    public final void ruleDisjoint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:267:2: ( ( ( rule__Disjoint__Group__0 ) ) )
            // InternalMCL.g:268:2: ( ( rule__Disjoint__Group__0 ) )
            {
            // InternalMCL.g:268:2: ( ( rule__Disjoint__Group__0 ) )
            // InternalMCL.g:269:3: ( rule__Disjoint__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getGroup()); 
            }
            // InternalMCL.g:270:3: ( rule__Disjoint__Group__0 )
            // InternalMCL.g:270:4: rule__Disjoint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisjoint"


    // $ANTLR start "entryRuleUnion"
    // InternalMCL.g:279:1: entryRuleUnion : ruleUnion EOF ;
    public final void entryRuleUnion() throws RecognitionException {
        try {
            // InternalMCL.g:280:1: ( ruleUnion EOF )
            // InternalMCL.g:281:1: ruleUnion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleUnion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnion"


    // $ANTLR start "ruleUnion"
    // InternalMCL.g:288:1: ruleUnion : ( ( rule__Union__Group__0 ) ) ;
    public final void ruleUnion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:292:2: ( ( ( rule__Union__Group__0 ) ) )
            // InternalMCL.g:293:2: ( ( rule__Union__Group__0 ) )
            {
            // InternalMCL.g:293:2: ( ( rule__Union__Group__0 ) )
            // InternalMCL.g:294:3: ( rule__Union__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getGroup()); 
            }
            // InternalMCL.g:295:3: ( rule__Union__Group__0 )
            // InternalMCL.g:295:4: rule__Union__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Union__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnion"


    // $ANTLR start "entryRuleTriggeringCondition"
    // InternalMCL.g:304:1: entryRuleTriggeringCondition : ruleTriggeringCondition EOF ;
    public final void entryRuleTriggeringCondition() throws RecognitionException {
        try {
            // InternalMCL.g:305:1: ( ruleTriggeringCondition EOF )
            // InternalMCL.g:306:1: ruleTriggeringCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTriggeringConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTriggeringConditionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTriggeringCondition"


    // $ANTLR start "ruleTriggeringCondition"
    // InternalMCL.g:313:1: ruleTriggeringCondition : ( ( rule__TriggeringCondition__Alternatives ) ) ;
    public final void ruleTriggeringCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:317:2: ( ( ( rule__TriggeringCondition__Alternatives ) ) )
            // InternalMCL.g:318:2: ( ( rule__TriggeringCondition__Alternatives ) )
            {
            // InternalMCL.g:318:2: ( ( rule__TriggeringCondition__Alternatives ) )
            // InternalMCL.g:319:3: ( rule__TriggeringCondition__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTriggeringConditionAccess().getAlternatives()); 
            }
            // InternalMCL.g:320:3: ( rule__TriggeringCondition__Alternatives )
            // InternalMCL.g:320:4: rule__TriggeringCondition__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TriggeringCondition__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTriggeringConditionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTriggeringCondition"


    // $ANTLR start "entryRuleEventTrigger"
    // InternalMCL.g:329:1: entryRuleEventTrigger : ruleEventTrigger EOF ;
    public final void entryRuleEventTrigger() throws RecognitionException {
        try {
            // InternalMCL.g:330:1: ( ruleEventTrigger EOF )
            // InternalMCL.g:331:1: ruleEventTrigger EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleEventTrigger();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEventTrigger"


    // $ANTLR start "ruleEventTrigger"
    // InternalMCL.g:338:1: ruleEventTrigger : ( ( rule__EventTrigger__Group__0 ) ) ;
    public final void ruleEventTrigger() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:342:2: ( ( ( rule__EventTrigger__Group__0 ) ) )
            // InternalMCL.g:343:2: ( ( rule__EventTrigger__Group__0 ) )
            {
            // InternalMCL.g:343:2: ( ( rule__EventTrigger__Group__0 ) )
            // InternalMCL.g:344:3: ( rule__EventTrigger__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getGroup()); 
            }
            // InternalMCL.g:345:3: ( rule__EventTrigger__Group__0 )
            // InternalMCL.g:345:4: rule__EventTrigger__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EventTrigger__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEventTrigger"


    // $ANTLR start "entryRulePeriodicExpression"
    // InternalMCL.g:354:1: entryRulePeriodicExpression : rulePeriodicExpression EOF ;
    public final void entryRulePeriodicExpression() throws RecognitionException {
        try {
            // InternalMCL.g:355:1: ( rulePeriodicExpression EOF )
            // InternalMCL.g:356:1: rulePeriodicExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            rulePeriodicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePeriodicExpression"


    // $ANTLR start "rulePeriodicExpression"
    // InternalMCL.g:363:1: rulePeriodicExpression : ( ( rule__PeriodicExpression__Group__0 ) ) ;
    public final void rulePeriodicExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:367:2: ( ( ( rule__PeriodicExpression__Group__0 ) ) )
            // InternalMCL.g:368:2: ( ( rule__PeriodicExpression__Group__0 ) )
            {
            // InternalMCL.g:368:2: ( ( rule__PeriodicExpression__Group__0 ) )
            // InternalMCL.g:369:3: ( rule__PeriodicExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:370:3: ( rule__PeriodicExpression__Group__0 )
            // InternalMCL.g:370:4: rule__PeriodicExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePeriodicExpression"


    // $ANTLR start "entryRuleVariable"
    // InternalMCL.g:379:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalMCL.g:380:1: ( ruleVariable EOF )
            // InternalMCL.g:381:1: ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalMCL.g:388:1: ruleVariable : ( ( rule__Variable__Group__0 ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:392:2: ( ( ( rule__Variable__Group__0 ) ) )
            // InternalMCL.g:393:2: ( ( rule__Variable__Group__0 ) )
            {
            // InternalMCL.g:393:2: ( ( rule__Variable__Group__0 ) )
            // InternalMCL.g:394:3: ( rule__Variable__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getGroup()); 
            }
            // InternalMCL.g:395:3: ( rule__Variable__Group__0 )
            // InternalMCL.g:395:4: rule__Variable__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleUpdated"
    // InternalMCL.g:404:1: entryRuleUpdated : ruleUpdated EOF ;
    public final void entryRuleUpdated() throws RecognitionException {
        try {
            // InternalMCL.g:405:1: ( ruleUpdated EOF )
            // InternalMCL.g:406:1: ruleUpdated EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleUpdated();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUpdated"


    // $ANTLR start "ruleUpdated"
    // InternalMCL.g:413:1: ruleUpdated : ( ( rule__Updated__Group__0 ) ) ;
    public final void ruleUpdated() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:417:2: ( ( ( rule__Updated__Group__0 ) ) )
            // InternalMCL.g:418:2: ( ( rule__Updated__Group__0 ) )
            {
            // InternalMCL.g:418:2: ( ( rule__Updated__Group__0 ) )
            // InternalMCL.g:419:3: ( rule__Updated__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getGroup()); 
            }
            // InternalMCL.g:420:3: ( rule__Updated__Group__0 )
            // InternalMCL.g:420:4: rule__Updated__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Updated__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUpdated"


    // $ANTLR start "entryRuleReadyToRead"
    // InternalMCL.g:429:1: entryRuleReadyToRead : ruleReadyToRead EOF ;
    public final void entryRuleReadyToRead() throws RecognitionException {
        try {
            // InternalMCL.g:430:1: ( ruleReadyToRead EOF )
            // InternalMCL.g:431:1: ruleReadyToRead EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleReadyToRead();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReadyToRead"


    // $ANTLR start "ruleReadyToRead"
    // InternalMCL.g:438:1: ruleReadyToRead : ( ( rule__ReadyToRead__Group__0 ) ) ;
    public final void ruleReadyToRead() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:442:2: ( ( ( rule__ReadyToRead__Group__0 ) ) )
            // InternalMCL.g:443:2: ( ( rule__ReadyToRead__Group__0 ) )
            {
            // InternalMCL.g:443:2: ( ( rule__ReadyToRead__Group__0 ) )
            // InternalMCL.g:444:3: ( rule__ReadyToRead__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getGroup()); 
            }
            // InternalMCL.g:445:3: ( rule__ReadyToRead__Group__0 )
            // InternalMCL.g:445:4: rule__ReadyToRead__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReadyToRead"


    // $ANTLR start "entryRuleZeroCrossingDetection"
    // InternalMCL.g:454:1: entryRuleZeroCrossingDetection : ruleZeroCrossingDetection EOF ;
    public final void entryRuleZeroCrossingDetection() throws RecognitionException {
        try {
            // InternalMCL.g:455:1: ( ruleZeroCrossingDetection EOF )
            // InternalMCL.g:456:1: ruleZeroCrossingDetection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleZeroCrossingDetection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleZeroCrossingDetection"


    // $ANTLR start "ruleZeroCrossingDetection"
    // InternalMCL.g:463:1: ruleZeroCrossingDetection : ( ( rule__ZeroCrossingDetection__Group__0 ) ) ;
    public final void ruleZeroCrossingDetection() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:467:2: ( ( ( rule__ZeroCrossingDetection__Group__0 ) ) )
            // InternalMCL.g:468:2: ( ( rule__ZeroCrossingDetection__Group__0 ) )
            {
            // InternalMCL.g:468:2: ( ( rule__ZeroCrossingDetection__Group__0 ) )
            // InternalMCL.g:469:3: ( rule__ZeroCrossingDetection__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getGroup()); 
            }
            // InternalMCL.g:470:3: ( rule__ZeroCrossingDetection__Group__0 )
            // InternalMCL.g:470:4: rule__ZeroCrossingDetection__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ZeroCrossingDetection__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleZeroCrossingDetection"


    // $ANTLR start "entryRuleExtrapolation"
    // InternalMCL.g:479:1: entryRuleExtrapolation : ruleExtrapolation EOF ;
    public final void entryRuleExtrapolation() throws RecognitionException {
        try {
            // InternalMCL.g:480:1: ( ruleExtrapolation EOF )
            // InternalMCL.g:481:1: ruleExtrapolation EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleExtrapolation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExtrapolation"


    // $ANTLR start "ruleExtrapolation"
    // InternalMCL.g:488:1: ruleExtrapolation : ( ( rule__Extrapolation__Group__0 ) ) ;
    public final void ruleExtrapolation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:492:2: ( ( ( rule__Extrapolation__Group__0 ) ) )
            // InternalMCL.g:493:2: ( ( rule__Extrapolation__Group__0 ) )
            {
            // InternalMCL.g:493:2: ( ( rule__Extrapolation__Group__0 ) )
            // InternalMCL.g:494:3: ( rule__Extrapolation__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getGroup()); 
            }
            // InternalMCL.g:495:3: ( rule__Extrapolation__Group__0 )
            // InternalMCL.g:495:4: rule__Extrapolation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Extrapolation__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExtrapolation"


    // $ANTLR start "entryRuleInterpolation"
    // InternalMCL.g:504:1: entryRuleInterpolation : ruleInterpolation EOF ;
    public final void entryRuleInterpolation() throws RecognitionException {
        try {
            // InternalMCL.g:505:1: ( ruleInterpolation EOF )
            // InternalMCL.g:506:1: ruleInterpolation EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleInterpolation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterpolation"


    // $ANTLR start "ruleInterpolation"
    // InternalMCL.g:513:1: ruleInterpolation : ( ( rule__Interpolation__Group__0 ) ) ;
    public final void ruleInterpolation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:517:2: ( ( ( rule__Interpolation__Group__0 ) ) )
            // InternalMCL.g:518:2: ( ( rule__Interpolation__Group__0 ) )
            {
            // InternalMCL.g:518:2: ( ( rule__Interpolation__Group__0 ) )
            // InternalMCL.g:519:3: ( rule__Interpolation__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getGroup()); 
            }
            // InternalMCL.g:520:3: ( rule__Interpolation__Group__0 )
            // InternalMCL.g:520:4: rule__Interpolation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interpolation__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterpolation"


    // $ANTLR start "entryRuleDiscontinuityLocator"
    // InternalMCL.g:529:1: entryRuleDiscontinuityLocator : ruleDiscontinuityLocator EOF ;
    public final void entryRuleDiscontinuityLocator() throws RecognitionException {
        try {
            // InternalMCL.g:530:1: ( ruleDiscontinuityLocator EOF )
            // InternalMCL.g:531:1: ruleDiscontinuityLocator EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleDiscontinuityLocator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDiscontinuityLocator"


    // $ANTLR start "ruleDiscontinuityLocator"
    // InternalMCL.g:538:1: ruleDiscontinuityLocator : ( ( rule__DiscontinuityLocator__Group__0 ) ) ;
    public final void ruleDiscontinuityLocator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:542:2: ( ( ( rule__DiscontinuityLocator__Group__0 ) ) )
            // InternalMCL.g:543:2: ( ( rule__DiscontinuityLocator__Group__0 ) )
            {
            // InternalMCL.g:543:2: ( ( rule__DiscontinuityLocator__Group__0 ) )
            // InternalMCL.g:544:3: ( rule__DiscontinuityLocator__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getGroup()); 
            }
            // InternalMCL.g:545:3: ( rule__DiscontinuityLocator__Group__0 )
            // InternalMCL.g:545:4: rule__DiscontinuityLocator__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DiscontinuityLocator__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDiscontinuityLocator"


    // $ANTLR start "entryRuleHistoryProvider"
    // InternalMCL.g:554:1: entryRuleHistoryProvider : ruleHistoryProvider EOF ;
    public final void entryRuleHistoryProvider() throws RecognitionException {
        try {
            // InternalMCL.g:555:1: ( ruleHistoryProvider EOF )
            // InternalMCL.g:556:1: ruleHistoryProvider EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleHistoryProvider();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHistoryProvider"


    // $ANTLR start "ruleHistoryProvider"
    // InternalMCL.g:563:1: ruleHistoryProvider : ( ( rule__HistoryProvider__Group__0 ) ) ;
    public final void ruleHistoryProvider() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:567:2: ( ( ( rule__HistoryProvider__Group__0 ) ) )
            // InternalMCL.g:568:2: ( ( rule__HistoryProvider__Group__0 ) )
            {
            // InternalMCL.g:568:2: ( ( rule__HistoryProvider__Group__0 ) )
            // InternalMCL.g:569:3: ( rule__HistoryProvider__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getGroup()); 
            }
            // InternalMCL.g:570:3: ( rule__HistoryProvider__Group__0 )
            // InternalMCL.g:570:4: rule__HistoryProvider__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HistoryProvider__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHistoryProvider"


    // $ANTLR start "entryRuleGreater"
    // InternalMCL.g:579:1: entryRuleGreater : ruleGreater EOF ;
    public final void entryRuleGreater() throws RecognitionException {
        try {
            // InternalMCL.g:580:1: ( ruleGreater EOF )
            // InternalMCL.g:581:1: ruleGreater EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGreaterRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGreater();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGreaterRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGreater"


    // $ANTLR start "ruleGreater"
    // InternalMCL.g:588:1: ruleGreater : ( ( rule__Greater__Group__0 ) ) ;
    public final void ruleGreater() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:592:2: ( ( ( rule__Greater__Group__0 ) ) )
            // InternalMCL.g:593:2: ( ( rule__Greater__Group__0 ) )
            {
            // InternalMCL.g:593:2: ( ( rule__Greater__Group__0 ) )
            // InternalMCL.g:594:3: ( rule__Greater__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGreaterAccess().getGroup()); 
            }
            // InternalMCL.g:595:3: ( rule__Greater__Group__0 )
            // InternalMCL.g:595:4: rule__Greater__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Greater__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGreaterAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGreater"


    // $ANTLR start "entryRuleLesser"
    // InternalMCL.g:604:1: entryRuleLesser : ruleLesser EOF ;
    public final void entryRuleLesser() throws RecognitionException {
        try {
            // InternalMCL.g:605:1: ( ruleLesser EOF )
            // InternalMCL.g:606:1: ruleLesser EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLesserRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleLesser();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLesserRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLesser"


    // $ANTLR start "ruleLesser"
    // InternalMCL.g:613:1: ruleLesser : ( ( rule__Lesser__Group__0 ) ) ;
    public final void ruleLesser() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:617:2: ( ( ( rule__Lesser__Group__0 ) ) )
            // InternalMCL.g:618:2: ( ( rule__Lesser__Group__0 ) )
            {
            // InternalMCL.g:618:2: ( ( rule__Lesser__Group__0 ) )
            // InternalMCL.g:619:3: ( rule__Lesser__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLesserAccess().getGroup()); 
            }
            // InternalMCL.g:620:3: ( rule__Lesser__Group__0 )
            // InternalMCL.g:620:4: rule__Lesser__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Lesser__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLesserAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLesser"


    // $ANTLR start "entryRuleAssignmentExpression"
    // InternalMCL.g:629:1: entryRuleAssignmentExpression : ruleAssignmentExpression EOF ;
    public final void entryRuleAssignmentExpression() throws RecognitionException {
        try {
            // InternalMCL.g:630:1: ( ruleAssignmentExpression EOF )
            // InternalMCL.g:631:1: ruleAssignmentExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssignmentExpression"


    // $ANTLR start "ruleAssignmentExpression"
    // InternalMCL.g:638:1: ruleAssignmentExpression : ( ruleAssignment ) ;
    public final void ruleAssignmentExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:642:2: ( ( ruleAssignment ) )
            // InternalMCL.g:643:2: ( ruleAssignment )
            {
            // InternalMCL.g:643:2: ( ruleAssignment )
            // InternalMCL.g:644:3: ruleAssignment
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentExpressionAccess().getAssignmentParserRuleCall()); 
            }
            pushFollow(FOLLOW_2);
            ruleAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentExpressionAccess().getAssignmentParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignmentExpression"


    // $ANTLR start "entryRuleOtherExpression"
    // InternalMCL.g:654:1: entryRuleOtherExpression : ruleOtherExpression EOF ;
    public final void entryRuleOtherExpression() throws RecognitionException {
        try {
            // InternalMCL.g:655:1: ( ruleOtherExpression EOF )
            // InternalMCL.g:656:1: ruleOtherExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOtherExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleOtherExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOtherExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOtherExpression"


    // $ANTLR start "ruleOtherExpression"
    // InternalMCL.g:663:1: ruleOtherExpression : ( ( rule__OtherExpression__Alternatives ) ) ;
    public final void ruleOtherExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:667:2: ( ( ( rule__OtherExpression__Alternatives ) ) )
            // InternalMCL.g:668:2: ( ( rule__OtherExpression__Alternatives ) )
            {
            // InternalMCL.g:668:2: ( ( rule__OtherExpression__Alternatives ) )
            // InternalMCL.g:669:3: ( rule__OtherExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOtherExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:670:3: ( rule__OtherExpression__Alternatives )
            // InternalMCL.g:670:4: rule__OtherExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OtherExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOtherExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOtherExpression"


    // $ANTLR start "entryRuleBinaryExpression"
    // InternalMCL.g:679:1: entryRuleBinaryExpression : ruleBinaryExpression EOF ;
    public final void entryRuleBinaryExpression() throws RecognitionException {
        try {
            // InternalMCL.g:680:1: ( ruleBinaryExpression EOF )
            // InternalMCL.g:681:1: ruleBinaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBinaryExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryExpression"


    // $ANTLR start "ruleBinaryExpression"
    // InternalMCL.g:688:1: ruleBinaryExpression : ( ( rule__BinaryExpression__Alternatives ) ) ;
    public final void ruleBinaryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:692:2: ( ( ( rule__BinaryExpression__Alternatives ) ) )
            // InternalMCL.g:693:2: ( ( rule__BinaryExpression__Alternatives ) )
            {
            // InternalMCL.g:693:2: ( ( rule__BinaryExpression__Alternatives ) )
            // InternalMCL.g:694:3: ( rule__BinaryExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:695:3: ( rule__BinaryExpression__Alternatives )
            // InternalMCL.g:695:4: rule__BinaryExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryExpression"


    // $ANTLR start "entryRuleUnaryMinus"
    // InternalMCL.g:704:1: entryRuleUnaryMinus : ruleUnaryMinus EOF ;
    public final void entryRuleUnaryMinus() throws RecognitionException {
        try {
            // InternalMCL.g:705:1: ( ruleUnaryMinus EOF )
            // InternalMCL.g:706:1: ruleUnaryMinus EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnaryMinusRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleUnaryMinus();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnaryMinusRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnaryMinus"


    // $ANTLR start "ruleUnaryMinus"
    // InternalMCL.g:713:1: ruleUnaryMinus : ( ( rule__UnaryMinus__Group__0 ) ) ;
    public final void ruleUnaryMinus() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:717:2: ( ( ( rule__UnaryMinus__Group__0 ) ) )
            // InternalMCL.g:718:2: ( ( rule__UnaryMinus__Group__0 ) )
            {
            // InternalMCL.g:718:2: ( ( rule__UnaryMinus__Group__0 ) )
            // InternalMCL.g:719:3: ( rule__UnaryMinus__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnaryMinusAccess().getGroup()); 
            }
            // InternalMCL.g:720:3: ( rule__UnaryMinus__Group__0 )
            // InternalMCL.g:720:4: rule__UnaryMinus__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UnaryMinus__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnaryMinusAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryMinus"


    // $ANTLR start "entryRuleNot"
    // InternalMCL.g:729:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // InternalMCL.g:730:1: ( ruleNot EOF )
            // InternalMCL.g:731:1: ruleNot EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleNot();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMCL.g:738:1: ruleNot : ( ( rule__Not__Group__0 ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:742:2: ( ( ( rule__Not__Group__0 ) ) )
            // InternalMCL.g:743:2: ( ( rule__Not__Group__0 ) )
            {
            // InternalMCL.g:743:2: ( ( rule__Not__Group__0 ) )
            // InternalMCL.g:744:3: ( rule__Not__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getGroup()); 
            }
            // InternalMCL.g:745:3: ( rule__Not__Group__0 )
            // InternalMCL.g:745:4: rule__Not__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleBinaryMinus"
    // InternalMCL.g:754:1: entryRuleBinaryMinus : ruleBinaryMinus EOF ;
    public final void entryRuleBinaryMinus() throws RecognitionException {
        try {
            // InternalMCL.g:755:1: ( ruleBinaryMinus EOF )
            // InternalMCL.g:756:1: ruleBinaryMinus EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBinaryMinus();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryMinus"


    // $ANTLR start "ruleBinaryMinus"
    // InternalMCL.g:763:1: ruleBinaryMinus : ( ( rule__BinaryMinus__Group__0 ) ) ;
    public final void ruleBinaryMinus() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:767:2: ( ( ( rule__BinaryMinus__Group__0 ) ) )
            // InternalMCL.g:768:2: ( ( rule__BinaryMinus__Group__0 ) )
            {
            // InternalMCL.g:768:2: ( ( rule__BinaryMinus__Group__0 ) )
            // InternalMCL.g:769:3: ( rule__BinaryMinus__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getGroup()); 
            }
            // InternalMCL.g:770:3: ( rule__BinaryMinus__Group__0 )
            // InternalMCL.g:770:4: rule__BinaryMinus__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryMinus"


    // $ANTLR start "entryRuleBinaryPlus"
    // InternalMCL.g:779:1: entryRuleBinaryPlus : ruleBinaryPlus EOF ;
    public final void entryRuleBinaryPlus() throws RecognitionException {
        try {
            // InternalMCL.g:780:1: ( ruleBinaryPlus EOF )
            // InternalMCL.g:781:1: ruleBinaryPlus EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleBinaryPlus();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryPlus"


    // $ANTLR start "ruleBinaryPlus"
    // InternalMCL.g:788:1: ruleBinaryPlus : ( ( rule__BinaryPlus__Group__0 ) ) ;
    public final void ruleBinaryPlus() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:792:2: ( ( ( rule__BinaryPlus__Group__0 ) ) )
            // InternalMCL.g:793:2: ( ( rule__BinaryPlus__Group__0 ) )
            {
            // InternalMCL.g:793:2: ( ( rule__BinaryPlus__Group__0 ) )
            // InternalMCL.g:794:3: ( rule__BinaryPlus__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getGroup()); 
            }
            // InternalMCL.g:795:3: ( rule__BinaryPlus__Group__0 )
            // InternalMCL.g:795:4: rule__BinaryPlus__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryPlus"


    // $ANTLR start "entryRuleAssignment"
    // InternalMCL.g:804:1: entryRuleAssignment : ruleAssignment EOF ;
    public final void entryRuleAssignment() throws RecognitionException {
        try {
            // InternalMCL.g:805:1: ( ruleAssignment EOF )
            // InternalMCL.g:806:1: ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalMCL.g:813:1: ruleAssignment : ( ( rule__Assignment__Group__0 ) ) ;
    public final void ruleAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:817:2: ( ( ( rule__Assignment__Group__0 ) ) )
            // InternalMCL.g:818:2: ( ( rule__Assignment__Group__0 ) )
            {
            // InternalMCL.g:818:2: ( ( rule__Assignment__Group__0 ) )
            // InternalMCL.g:819:3: ( rule__Assignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getGroup()); 
            }
            // InternalMCL.g:820:3: ( rule__Assignment__Group__0 )
            // InternalMCL.g:820:4: rule__Assignment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRulePortRef"
    // InternalMCL.g:829:1: entryRulePortRef : rulePortRef EOF ;
    public final void entryRulePortRef() throws RecognitionException {
        try {
            // InternalMCL.g:830:1: ( rulePortRef EOF )
            // InternalMCL.g:831:1: rulePortRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPortRefRule()); 
            }
            pushFollow(FOLLOW_1);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPortRefRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePortRef"


    // $ANTLR start "rulePortRef"
    // InternalMCL.g:838:1: rulePortRef : ( ( rule__PortRef__RefPortAssignment ) ) ;
    public final void rulePortRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:842:2: ( ( ( rule__PortRef__RefPortAssignment ) ) )
            // InternalMCL.g:843:2: ( ( rule__PortRef__RefPortAssignment ) )
            {
            // InternalMCL.g:843:2: ( ( rule__PortRef__RefPortAssignment ) )
            // InternalMCL.g:844:3: ( rule__PortRef__RefPortAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPortRefAccess().getRefPortAssignment()); 
            }
            // InternalMCL.g:845:3: ( rule__PortRef__RefPortAssignment )
            // InternalMCL.g:845:4: rule__PortRef__RefPortAssignment
            {
            pushFollow(FOLLOW_2);
            rule__PortRef__RefPortAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPortRefAccess().getRefPortAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePortRef"


    // $ANTLR start "entryRuleSimpleSync"
    // InternalMCL.g:854:1: entryRuleSimpleSync : ruleSimpleSync EOF ;
    public final void entryRuleSimpleSync() throws RecognitionException {
        try {
            // InternalMCL.g:855:1: ( ruleSimpleSync EOF )
            // InternalMCL.g:856:1: ruleSimpleSync EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleSimpleSync();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleSync"


    // $ANTLR start "ruleSimpleSync"
    // InternalMCL.g:863:1: ruleSimpleSync : ( ( rule__SimpleSync__Group__0 ) ) ;
    public final void ruleSimpleSync() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:867:2: ( ( ( rule__SimpleSync__Group__0 ) ) )
            // InternalMCL.g:868:2: ( ( rule__SimpleSync__Group__0 ) )
            {
            // InternalMCL.g:868:2: ( ( rule__SimpleSync__Group__0 ) )
            // InternalMCL.g:869:3: ( rule__SimpleSync__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getGroup()); 
            }
            // InternalMCL.g:870:3: ( rule__SimpleSync__Group__0 )
            // InternalMCL.g:870:4: rule__SimpleSync__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleSync"


    // $ANTLR start "entryRuleGImportStatement"
    // InternalMCL.g:879:1: entryRuleGImportStatement : ruleGImportStatement EOF ;
    public final void entryRuleGImportStatement() throws RecognitionException {
        try {
            // InternalMCL.g:880:1: ( ruleGImportStatement EOF )
            // InternalMCL.g:881:1: ruleGImportStatement EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGImportStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGImportStatement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGImportStatementRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGImportStatement"


    // $ANTLR start "ruleGImportStatement"
    // InternalMCL.g:888:1: ruleGImportStatement : ( ( rule__GImportStatement__Group__0 ) ) ;
    public final void ruleGImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:892:2: ( ( ( rule__GImportStatement__Group__0 ) ) )
            // InternalMCL.g:893:2: ( ( rule__GImportStatement__Group__0 ) )
            {
            // InternalMCL.g:893:2: ( ( rule__GImportStatement__Group__0 ) )
            // InternalMCL.g:894:3: ( rule__GImportStatement__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGImportStatementAccess().getGroup()); 
            }
            // InternalMCL.g:895:3: ( rule__GImportStatement__Group__0 )
            // InternalMCL.g:895:4: rule__GImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GImportStatement__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGImportStatementAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGImportStatement"


    // $ANTLR start "entryRuleGExpression"
    // InternalMCL.g:904:1: entryRuleGExpression : ruleGExpression EOF ;
    public final void entryRuleGExpression() throws RecognitionException {
        try {
            // InternalMCL.g:905:1: ( ruleGExpression EOF )
            // InternalMCL.g:906:1: ruleGExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGExpression"


    // $ANTLR start "ruleGExpression"
    // InternalMCL.g:913:1: ruleGExpression : ( ruleGOrExpression ) ;
    public final void ruleGExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:917:2: ( ( ruleGOrExpression ) )
            // InternalMCL.g:918:2: ( ruleGOrExpression )
            {
            // InternalMCL.g:918:2: ( ruleGOrExpression )
            // InternalMCL.g:919:3: ruleGOrExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGExpressionAccess().getGOrExpressionParserRuleCall()); 
            }
            pushFollow(FOLLOW_2);
            ruleGOrExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGExpressionAccess().getGOrExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGExpression"


    // $ANTLR start "entryRuleGOrExpression"
    // InternalMCL.g:929:1: entryRuleGOrExpression : ruleGOrExpression EOF ;
    public final void entryRuleGOrExpression() throws RecognitionException {
        try {
            // InternalMCL.g:930:1: ( ruleGOrExpression EOF )
            // InternalMCL.g:931:1: ruleGOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGOrExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGOrExpression"


    // $ANTLR start "ruleGOrExpression"
    // InternalMCL.g:938:1: ruleGOrExpression : ( ( rule__GOrExpression__Group__0 ) ) ;
    public final void ruleGOrExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:942:2: ( ( ( rule__GOrExpression__Group__0 ) ) )
            // InternalMCL.g:943:2: ( ( rule__GOrExpression__Group__0 ) )
            {
            // InternalMCL.g:943:2: ( ( rule__GOrExpression__Group__0 ) )
            // InternalMCL.g:944:3: ( rule__GOrExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:945:3: ( rule__GOrExpression__Group__0 )
            // InternalMCL.g:945:4: rule__GOrExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGOrExpression"


    // $ANTLR start "entryRuleGXorExpression"
    // InternalMCL.g:954:1: entryRuleGXorExpression : ruleGXorExpression EOF ;
    public final void entryRuleGXorExpression() throws RecognitionException {
        try {
            // InternalMCL.g:955:1: ( ruleGXorExpression EOF )
            // InternalMCL.g:956:1: ruleGXorExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGXorExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGXorExpression"


    // $ANTLR start "ruleGXorExpression"
    // InternalMCL.g:963:1: ruleGXorExpression : ( ( rule__GXorExpression__Group__0 ) ) ;
    public final void ruleGXorExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:967:2: ( ( ( rule__GXorExpression__Group__0 ) ) )
            // InternalMCL.g:968:2: ( ( rule__GXorExpression__Group__0 ) )
            {
            // InternalMCL.g:968:2: ( ( rule__GXorExpression__Group__0 ) )
            // InternalMCL.g:969:3: ( rule__GXorExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:970:3: ( rule__GXorExpression__Group__0 )
            // InternalMCL.g:970:4: rule__GXorExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGXorExpression"


    // $ANTLR start "entryRuleGAndExpression"
    // InternalMCL.g:979:1: entryRuleGAndExpression : ruleGAndExpression EOF ;
    public final void entryRuleGAndExpression() throws RecognitionException {
        try {
            // InternalMCL.g:980:1: ( ruleGAndExpression EOF )
            // InternalMCL.g:981:1: ruleGAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGAndExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGAndExpression"


    // $ANTLR start "ruleGAndExpression"
    // InternalMCL.g:988:1: ruleGAndExpression : ( ( rule__GAndExpression__Group__0 ) ) ;
    public final void ruleGAndExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:992:2: ( ( ( rule__GAndExpression__Group__0 ) ) )
            // InternalMCL.g:993:2: ( ( rule__GAndExpression__Group__0 ) )
            {
            // InternalMCL.g:993:2: ( ( rule__GAndExpression__Group__0 ) )
            // InternalMCL.g:994:3: ( rule__GAndExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:995:3: ( rule__GAndExpression__Group__0 )
            // InternalMCL.g:995:4: rule__GAndExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGAndExpression"


    // $ANTLR start "entryRuleGEqualityExpression"
    // InternalMCL.g:1004:1: entryRuleGEqualityExpression : ruleGEqualityExpression EOF ;
    public final void entryRuleGEqualityExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1005:1: ( ruleGEqualityExpression EOF )
            // InternalMCL.g:1006:1: ruleGEqualityExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGEqualityExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGEqualityExpression"


    // $ANTLR start "ruleGEqualityExpression"
    // InternalMCL.g:1013:1: ruleGEqualityExpression : ( ( rule__GEqualityExpression__Group__0 ) ) ;
    public final void ruleGEqualityExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1017:2: ( ( ( rule__GEqualityExpression__Group__0 ) ) )
            // InternalMCL.g:1018:2: ( ( rule__GEqualityExpression__Group__0 ) )
            {
            // InternalMCL.g:1018:2: ( ( rule__GEqualityExpression__Group__0 ) )
            // InternalMCL.g:1019:3: ( rule__GEqualityExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1020:3: ( rule__GEqualityExpression__Group__0 )
            // InternalMCL.g:1020:4: rule__GEqualityExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGEqualityExpression"


    // $ANTLR start "entryRuleGRelationExpression"
    // InternalMCL.g:1029:1: entryRuleGRelationExpression : ruleGRelationExpression EOF ;
    public final void entryRuleGRelationExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1030:1: ( ruleGRelationExpression EOF )
            // InternalMCL.g:1031:1: ruleGRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGRelationExpression"


    // $ANTLR start "ruleGRelationExpression"
    // InternalMCL.g:1038:1: ruleGRelationExpression : ( ( rule__GRelationExpression__Group__0 ) ) ;
    public final void ruleGRelationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1042:2: ( ( ( rule__GRelationExpression__Group__0 ) ) )
            // InternalMCL.g:1043:2: ( ( rule__GRelationExpression__Group__0 ) )
            {
            // InternalMCL.g:1043:2: ( ( rule__GRelationExpression__Group__0 ) )
            // InternalMCL.g:1044:3: ( rule__GRelationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1045:3: ( rule__GRelationExpression__Group__0 )
            // InternalMCL.g:1045:4: rule__GRelationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGRelationExpression"


    // $ANTLR start "entryRuleGAdditionExpression"
    // InternalMCL.g:1054:1: entryRuleGAdditionExpression : ruleGAdditionExpression EOF ;
    public final void entryRuleGAdditionExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1055:1: ( ruleGAdditionExpression EOF )
            // InternalMCL.g:1056:1: ruleGAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGAdditionExpression"


    // $ANTLR start "ruleGAdditionExpression"
    // InternalMCL.g:1063:1: ruleGAdditionExpression : ( ( rule__GAdditionExpression__Group__0 ) ) ;
    public final void ruleGAdditionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1067:2: ( ( ( rule__GAdditionExpression__Group__0 ) ) )
            // InternalMCL.g:1068:2: ( ( rule__GAdditionExpression__Group__0 ) )
            {
            // InternalMCL.g:1068:2: ( ( rule__GAdditionExpression__Group__0 ) )
            // InternalMCL.g:1069:3: ( rule__GAdditionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1070:3: ( rule__GAdditionExpression__Group__0 )
            // InternalMCL.g:1070:4: rule__GAdditionExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGAdditionExpression"


    // $ANTLR start "entryRuleGMultiplicationExpression"
    // InternalMCL.g:1079:1: entryRuleGMultiplicationExpression : ruleGMultiplicationExpression EOF ;
    public final void entryRuleGMultiplicationExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1080:1: ( ruleGMultiplicationExpression EOF )
            // InternalMCL.g:1081:1: ruleGMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGMultiplicationExpression"


    // $ANTLR start "ruleGMultiplicationExpression"
    // InternalMCL.g:1088:1: ruleGMultiplicationExpression : ( ( rule__GMultiplicationExpression__Group__0 ) ) ;
    public final void ruleGMultiplicationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1092:2: ( ( ( rule__GMultiplicationExpression__Group__0 ) ) )
            // InternalMCL.g:1093:2: ( ( rule__GMultiplicationExpression__Group__0 ) )
            {
            // InternalMCL.g:1093:2: ( ( rule__GMultiplicationExpression__Group__0 ) )
            // InternalMCL.g:1094:3: ( rule__GMultiplicationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1095:3: ( rule__GMultiplicationExpression__Group__0 )
            // InternalMCL.g:1095:4: rule__GMultiplicationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGMultiplicationExpression"


    // $ANTLR start "entryRuleGNegationExpression"
    // InternalMCL.g:1104:1: entryRuleGNegationExpression : ruleGNegationExpression EOF ;
    public final void entryRuleGNegationExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1105:1: ( ruleGNegationExpression EOF )
            // InternalMCL.g:1106:1: ruleGNegationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGNegationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGNegationExpression"


    // $ANTLR start "ruleGNegationExpression"
    // InternalMCL.g:1113:1: ruleGNegationExpression : ( ( rule__GNegationExpression__Alternatives ) ) ;
    public final void ruleGNegationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1117:2: ( ( ( rule__GNegationExpression__Alternatives ) ) )
            // InternalMCL.g:1118:2: ( ( rule__GNegationExpression__Alternatives ) )
            {
            // InternalMCL.g:1118:2: ( ( rule__GNegationExpression__Alternatives ) )
            // InternalMCL.g:1119:3: ( rule__GNegationExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:1120:3: ( rule__GNegationExpression__Alternatives )
            // InternalMCL.g:1120:4: rule__GNegationExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGNegationExpression"


    // $ANTLR start "entryRuleGNavigationExpression"
    // InternalMCL.g:1129:1: entryRuleGNavigationExpression : ruleGNavigationExpression EOF ;
    public final void entryRuleGNavigationExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1130:1: ( ruleGNavigationExpression EOF )
            // InternalMCL.g:1131:1: ruleGNavigationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGNavigationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGNavigationExpression"


    // $ANTLR start "ruleGNavigationExpression"
    // InternalMCL.g:1138:1: ruleGNavigationExpression : ( ( rule__GNavigationExpression__Group__0 ) ) ;
    public final void ruleGNavigationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1142:2: ( ( ( rule__GNavigationExpression__Group__0 ) ) )
            // InternalMCL.g:1143:2: ( ( rule__GNavigationExpression__Group__0 ) )
            {
            // InternalMCL.g:1143:2: ( ( rule__GNavigationExpression__Group__0 ) )
            // InternalMCL.g:1144:3: ( rule__GNavigationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1145:3: ( rule__GNavigationExpression__Group__0 )
            // InternalMCL.g:1145:4: rule__GNavigationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGNavigationExpression"


    // $ANTLR start "entryRuleGReferenceExpression"
    // InternalMCL.g:1154:1: entryRuleGReferenceExpression : ruleGReferenceExpression EOF ;
    public final void entryRuleGReferenceExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1155:1: ( ruleGReferenceExpression EOF )
            // InternalMCL.g:1156:1: ruleGReferenceExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGReferenceExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGReferenceExpression"


    // $ANTLR start "ruleGReferenceExpression"
    // InternalMCL.g:1163:1: ruleGReferenceExpression : ( ( rule__GReferenceExpression__Alternatives ) ) ;
    public final void ruleGReferenceExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1167:2: ( ( ( rule__GReferenceExpression__Alternatives ) ) )
            // InternalMCL.g:1168:2: ( ( rule__GReferenceExpression__Alternatives ) )
            {
            // InternalMCL.g:1168:2: ( ( rule__GReferenceExpression__Alternatives ) )
            // InternalMCL.g:1169:3: ( rule__GReferenceExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:1170:3: ( rule__GReferenceExpression__Alternatives )
            // InternalMCL.g:1170:4: rule__GReferenceExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GReferenceExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGReferenceExpression"


    // $ANTLR start "entryRuleGPrimaryExpression"
    // InternalMCL.g:1179:1: entryRuleGPrimaryExpression : ruleGPrimaryExpression EOF ;
    public final void entryRuleGPrimaryExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1180:1: ( ruleGPrimaryExpression EOF )
            // InternalMCL.g:1181:1: ruleGPrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGPrimaryExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGPrimaryExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGPrimaryExpression"


    // $ANTLR start "ruleGPrimaryExpression"
    // InternalMCL.g:1188:1: ruleGPrimaryExpression : ( ( rule__GPrimaryExpression__Alternatives ) ) ;
    public final void ruleGPrimaryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1192:2: ( ( ( rule__GPrimaryExpression__Alternatives ) ) )
            // InternalMCL.g:1193:2: ( ( rule__GPrimaryExpression__Alternatives ) )
            {
            // InternalMCL.g:1193:2: ( ( rule__GPrimaryExpression__Alternatives ) )
            // InternalMCL.g:1194:3: ( rule__GPrimaryExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGPrimaryExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:1195:3: ( rule__GPrimaryExpression__Alternatives )
            // InternalMCL.g:1195:4: rule__GPrimaryExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GPrimaryExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGPrimaryExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGPrimaryExpression"


    // $ANTLR start "entryRuleGStringExpression"
    // InternalMCL.g:1204:1: entryRuleGStringExpression : ruleGStringExpression EOF ;
    public final void entryRuleGStringExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1205:1: ( ruleGStringExpression EOF )
            // InternalMCL.g:1206:1: ruleGStringExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGStringExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGStringExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGStringExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGStringExpression"


    // $ANTLR start "ruleGStringExpression"
    // InternalMCL.g:1213:1: ruleGStringExpression : ( ( rule__GStringExpression__Group__0 ) ) ;
    public final void ruleGStringExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1217:2: ( ( ( rule__GStringExpression__Group__0 ) ) )
            // InternalMCL.g:1218:2: ( ( rule__GStringExpression__Group__0 ) )
            {
            // InternalMCL.g:1218:2: ( ( rule__GStringExpression__Group__0 ) )
            // InternalMCL.g:1219:3: ( rule__GStringExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGStringExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1220:3: ( rule__GStringExpression__Group__0 )
            // InternalMCL.g:1220:4: rule__GStringExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GStringExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGStringExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGStringExpression"


    // $ANTLR start "entryRuleGBooleanExpression"
    // InternalMCL.g:1229:1: entryRuleGBooleanExpression : ruleGBooleanExpression EOF ;
    public final void entryRuleGBooleanExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1230:1: ( ruleGBooleanExpression EOF )
            // InternalMCL.g:1231:1: ruleGBooleanExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBooleanExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGBooleanExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBooleanExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGBooleanExpression"


    // $ANTLR start "ruleGBooleanExpression"
    // InternalMCL.g:1238:1: ruleGBooleanExpression : ( ( rule__GBooleanExpression__Group__0 ) ) ;
    public final void ruleGBooleanExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1242:2: ( ( ( rule__GBooleanExpression__Group__0 ) ) )
            // InternalMCL.g:1243:2: ( ( rule__GBooleanExpression__Group__0 ) )
            {
            // InternalMCL.g:1243:2: ( ( rule__GBooleanExpression__Group__0 ) )
            // InternalMCL.g:1244:3: ( rule__GBooleanExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBooleanExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1245:3: ( rule__GBooleanExpression__Group__0 )
            // InternalMCL.g:1245:4: rule__GBooleanExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GBooleanExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBooleanExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGBooleanExpression"


    // $ANTLR start "entryRuleGNumericExpression"
    // InternalMCL.g:1254:1: entryRuleGNumericExpression : ruleGNumericExpression EOF ;
    public final void entryRuleGNumericExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1255:1: ( ruleGNumericExpression EOF )
            // InternalMCL.g:1256:1: ruleGNumericExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNumericExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGNumericExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNumericExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGNumericExpression"


    // $ANTLR start "ruleGNumericExpression"
    // InternalMCL.g:1263:1: ruleGNumericExpression : ( ( rule__GNumericExpression__Alternatives ) ) ;
    public final void ruleGNumericExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1267:2: ( ( ( rule__GNumericExpression__Alternatives ) ) )
            // InternalMCL.g:1268:2: ( ( rule__GNumericExpression__Alternatives ) )
            {
            // InternalMCL.g:1268:2: ( ( rule__GNumericExpression__Alternatives ) )
            // InternalMCL.g:1269:3: ( rule__GNumericExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNumericExpressionAccess().getAlternatives()); 
            }
            // InternalMCL.g:1270:3: ( rule__GNumericExpression__Alternatives )
            // InternalMCL.g:1270:4: rule__GNumericExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GNumericExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNumericExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGNumericExpression"


    // $ANTLR start "entryRuleGIntegerExpression"
    // InternalMCL.g:1279:1: entryRuleGIntegerExpression : ruleGIntegerExpression EOF ;
    public final void entryRuleGIntegerExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1280:1: ( ruleGIntegerExpression EOF )
            // InternalMCL.g:1281:1: ruleGIntegerExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIntegerExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGIntegerExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIntegerExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGIntegerExpression"


    // $ANTLR start "ruleGIntegerExpression"
    // InternalMCL.g:1288:1: ruleGIntegerExpression : ( ( rule__GIntegerExpression__Group__0 ) ) ;
    public final void ruleGIntegerExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1292:2: ( ( ( rule__GIntegerExpression__Group__0 ) ) )
            // InternalMCL.g:1293:2: ( ( rule__GIntegerExpression__Group__0 ) )
            {
            // InternalMCL.g:1293:2: ( ( rule__GIntegerExpression__Group__0 ) )
            // InternalMCL.g:1294:3: ( rule__GIntegerExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIntegerExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1295:3: ( rule__GIntegerExpression__Group__0 )
            // InternalMCL.g:1295:4: rule__GIntegerExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GIntegerExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIntegerExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGIntegerExpression"


    // $ANTLR start "entryRuleGDoubleExpression"
    // InternalMCL.g:1304:1: entryRuleGDoubleExpression : ruleGDoubleExpression EOF ;
    public final void entryRuleGDoubleExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1305:1: ( ruleGDoubleExpression EOF )
            // InternalMCL.g:1306:1: ruleGDoubleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGDoubleExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGDoubleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGDoubleExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGDoubleExpression"


    // $ANTLR start "ruleGDoubleExpression"
    // InternalMCL.g:1313:1: ruleGDoubleExpression : ( ( rule__GDoubleExpression__Group__0 ) ) ;
    public final void ruleGDoubleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1317:2: ( ( ( rule__GDoubleExpression__Group__0 ) ) )
            // InternalMCL.g:1318:2: ( ( rule__GDoubleExpression__Group__0 ) )
            {
            // InternalMCL.g:1318:2: ( ( rule__GDoubleExpression__Group__0 ) )
            // InternalMCL.g:1319:3: ( rule__GDoubleExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGDoubleExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1320:3: ( rule__GDoubleExpression__Group__0 )
            // InternalMCL.g:1320:4: rule__GDoubleExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GDoubleExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGDoubleExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGDoubleExpression"


    // $ANTLR start "entryRuleGEnumLiteralExpression"
    // InternalMCL.g:1329:1: entryRuleGEnumLiteralExpression : ruleGEnumLiteralExpression EOF ;
    public final void entryRuleGEnumLiteralExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1330:1: ( ruleGEnumLiteralExpression EOF )
            // InternalMCL.g:1331:1: ruleGEnumLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGEnumLiteralExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGEnumLiteralExpression"


    // $ANTLR start "ruleGEnumLiteralExpression"
    // InternalMCL.g:1338:1: ruleGEnumLiteralExpression : ( ( rule__GEnumLiteralExpression__Group__0 ) ) ;
    public final void ruleGEnumLiteralExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1342:2: ( ( ( rule__GEnumLiteralExpression__Group__0 ) ) )
            // InternalMCL.g:1343:2: ( ( rule__GEnumLiteralExpression__Group__0 ) )
            {
            // InternalMCL.g:1343:2: ( ( rule__GEnumLiteralExpression__Group__0 ) )
            // InternalMCL.g:1344:3: ( rule__GEnumLiteralExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1345:3: ( rule__GEnumLiteralExpression__Group__0 )
            // InternalMCL.g:1345:4: rule__GEnumLiteralExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GEnumLiteralExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGEnumLiteralExpression"


    // $ANTLR start "entryRuleGIfExpression"
    // InternalMCL.g:1354:1: entryRuleGIfExpression : ruleGIfExpression EOF ;
    public final void entryRuleGIfExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1355:1: ( ruleGIfExpression EOF )
            // InternalMCL.g:1356:1: ruleGIfExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGIfExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGIfExpression"


    // $ANTLR start "ruleGIfExpression"
    // InternalMCL.g:1363:1: ruleGIfExpression : ( ( rule__GIfExpression__Group__0 ) ) ;
    public final void ruleGIfExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1367:2: ( ( ( rule__GIfExpression__Group__0 ) ) )
            // InternalMCL.g:1368:2: ( ( rule__GIfExpression__Group__0 ) )
            {
            // InternalMCL.g:1368:2: ( ( rule__GIfExpression__Group__0 ) )
            // InternalMCL.g:1369:3: ( rule__GIfExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1370:3: ( rule__GIfExpression__Group__0 )
            // InternalMCL.g:1370:4: rule__GIfExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGIfExpression"


    // $ANTLR start "entryRuleGBraceExpression"
    // InternalMCL.g:1379:1: entryRuleGBraceExpression : ruleGBraceExpression EOF ;
    public final void entryRuleGBraceExpression() throws RecognitionException {
        try {
            // InternalMCL.g:1380:1: ( ruleGBraceExpression EOF )
            // InternalMCL.g:1381:1: ruleGBraceExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleGBraceExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGBraceExpression"


    // $ANTLR start "ruleGBraceExpression"
    // InternalMCL.g:1388:1: ruleGBraceExpression : ( ( rule__GBraceExpression__Group__0 ) ) ;
    public final void ruleGBraceExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1392:2: ( ( ( rule__GBraceExpression__Group__0 ) ) )
            // InternalMCL.g:1393:2: ( ( rule__GBraceExpression__Group__0 ) )
            {
            // InternalMCL.g:1393:2: ( ( rule__GBraceExpression__Group__0 ) )
            // InternalMCL.g:1394:3: ( rule__GBraceExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getGroup()); 
            }
            // InternalMCL.g:1395:3: ( rule__GBraceExpression__Group__0 )
            // InternalMCL.g:1395:4: rule__GBraceExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGBraceExpression"


    // $ANTLR start "entryRuleNavigationOperator"
    // InternalMCL.g:1404:1: entryRuleNavigationOperator : ruleNavigationOperator EOF ;
    public final void entryRuleNavigationOperator() throws RecognitionException {
        try {
            // InternalMCL.g:1405:1: ( ruleNavigationOperator EOF )
            // InternalMCL.g:1406:1: ruleNavigationOperator EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNavigationOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleNavigationOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNavigationOperatorRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNavigationOperator"


    // $ANTLR start "ruleNavigationOperator"
    // InternalMCL.g:1413:1: ruleNavigationOperator : ( ( rule__NavigationOperator__Alternatives ) ) ;
    public final void ruleNavigationOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1417:2: ( ( ( rule__NavigationOperator__Alternatives ) ) )
            // InternalMCL.g:1418:2: ( ( rule__NavigationOperator__Alternatives ) )
            {
            // InternalMCL.g:1418:2: ( ( rule__NavigationOperator__Alternatives ) )
            // InternalMCL.g:1419:3: ( rule__NavigationOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNavigationOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1420:3: ( rule__NavigationOperator__Alternatives )
            // InternalMCL.g:1420:4: rule__NavigationOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__NavigationOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNavigationOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNavigationOperator"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalMCL.g:1429:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalMCL.g:1430:1: ( ruleQualifiedName EOF )
            // InternalMCL.g:1431:1: ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameRule()); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalMCL.g:1438:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1442:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalMCL.g:1443:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalMCL.g:1443:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalMCL.g:1444:3: ( rule__QualifiedName__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            }
            // InternalMCL.g:1445:3: ( rule__QualifiedName__Group__0 )
            // InternalMCL.g:1445:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleGAndOperator"
    // InternalMCL.g:1454:1: ruleGAndOperator : ( ( 'and' ) ) ;
    public final void ruleGAndOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1458:1: ( ( ( 'and' ) ) )
            // InternalMCL.g:1459:2: ( ( 'and' ) )
            {
            // InternalMCL.g:1459:2: ( ( 'and' ) )
            // InternalMCL.g:1460:3: ( 'and' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndOperatorAccess().getANDEnumLiteralDeclaration()); 
            }
            // InternalMCL.g:1461:3: ( 'and' )
            // InternalMCL.g:1461:4: 'and'
            {
            match(input,13,FOLLOW_2); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndOperatorAccess().getANDEnumLiteralDeclaration()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGAndOperator"


    // $ANTLR start "ruleGXorOperator"
    // InternalMCL.g:1470:1: ruleGXorOperator : ( ( 'xor' ) ) ;
    public final void ruleGXorOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1474:1: ( ( ( 'xor' ) ) )
            // InternalMCL.g:1475:2: ( ( 'xor' ) )
            {
            // InternalMCL.g:1475:2: ( ( 'xor' ) )
            // InternalMCL.g:1476:3: ( 'xor' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorOperatorAccess().getXOREnumLiteralDeclaration()); 
            }
            // InternalMCL.g:1477:3: ( 'xor' )
            // InternalMCL.g:1477:4: 'xor'
            {
            match(input,14,FOLLOW_2); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorOperatorAccess().getXOREnumLiteralDeclaration()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGXorOperator"


    // $ANTLR start "ruleGOrOperator"
    // InternalMCL.g:1486:1: ruleGOrOperator : ( ( 'or' ) ) ;
    public final void ruleGOrOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1490:1: ( ( ( 'or' ) ) )
            // InternalMCL.g:1491:2: ( ( 'or' ) )
            {
            // InternalMCL.g:1491:2: ( ( 'or' ) )
            // InternalMCL.g:1492:3: ( 'or' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrOperatorAccess().getOREnumLiteralDeclaration()); 
            }
            // InternalMCL.g:1493:3: ( 'or' )
            // InternalMCL.g:1493:4: 'or'
            {
            match(input,15,FOLLOW_2); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrOperatorAccess().getOREnumLiteralDeclaration()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGOrOperator"


    // $ANTLR start "ruleGEqualityOperator"
    // InternalMCL.g:1502:1: ruleGEqualityOperator : ( ( rule__GEqualityOperator__Alternatives ) ) ;
    public final void ruleGEqualityOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1506:1: ( ( ( rule__GEqualityOperator__Alternatives ) ) )
            // InternalMCL.g:1507:2: ( ( rule__GEqualityOperator__Alternatives ) )
            {
            // InternalMCL.g:1507:2: ( ( rule__GEqualityOperator__Alternatives ) )
            // InternalMCL.g:1508:3: ( rule__GEqualityOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1509:3: ( rule__GEqualityOperator__Alternatives )
            // InternalMCL.g:1509:4: rule__GEqualityOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGEqualityOperator"


    // $ANTLR start "ruleGRelationOperator"
    // InternalMCL.g:1518:1: ruleGRelationOperator : ( ( rule__GRelationOperator__Alternatives ) ) ;
    public final void ruleGRelationOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1522:1: ( ( ( rule__GRelationOperator__Alternatives ) ) )
            // InternalMCL.g:1523:2: ( ( rule__GRelationOperator__Alternatives ) )
            {
            // InternalMCL.g:1523:2: ( ( rule__GRelationOperator__Alternatives ) )
            // InternalMCL.g:1524:3: ( rule__GRelationOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1525:3: ( rule__GRelationOperator__Alternatives )
            // InternalMCL.g:1525:4: rule__GRelationOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GRelationOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGRelationOperator"


    // $ANTLR start "ruleGAdditionOperator"
    // InternalMCL.g:1534:1: ruleGAdditionOperator : ( ( rule__GAdditionOperator__Alternatives ) ) ;
    public final void ruleGAdditionOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1538:1: ( ( ( rule__GAdditionOperator__Alternatives ) ) )
            // InternalMCL.g:1539:2: ( ( rule__GAdditionOperator__Alternatives ) )
            {
            // InternalMCL.g:1539:2: ( ( rule__GAdditionOperator__Alternatives ) )
            // InternalMCL.g:1540:3: ( rule__GAdditionOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1541:3: ( rule__GAdditionOperator__Alternatives )
            // InternalMCL.g:1541:4: rule__GAdditionOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGAdditionOperator"


    // $ANTLR start "ruleGMultiplicationOperator"
    // InternalMCL.g:1550:1: ruleGMultiplicationOperator : ( ( rule__GMultiplicationOperator__Alternatives ) ) ;
    public final void ruleGMultiplicationOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1554:1: ( ( ( rule__GMultiplicationOperator__Alternatives ) ) )
            // InternalMCL.g:1555:2: ( ( rule__GMultiplicationOperator__Alternatives ) )
            {
            // InternalMCL.g:1555:2: ( ( rule__GMultiplicationOperator__Alternatives ) )
            // InternalMCL.g:1556:3: ( rule__GMultiplicationOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1557:3: ( rule__GMultiplicationOperator__Alternatives )
            // InternalMCL.g:1557:4: rule__GMultiplicationOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGMultiplicationOperator"


    // $ANTLR start "ruleGNegationOperator"
    // InternalMCL.g:1566:1: ruleGNegationOperator : ( ( rule__GNegationOperator__Alternatives ) ) ;
    public final void ruleGNegationOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1570:1: ( ( ( rule__GNegationOperator__Alternatives ) ) )
            // InternalMCL.g:1571:2: ( ( rule__GNegationOperator__Alternatives ) )
            {
            // InternalMCL.g:1571:2: ( ( rule__GNegationOperator__Alternatives ) )
            // InternalMCL.g:1572:3: ( rule__GNegationOperator__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationOperatorAccess().getAlternatives()); 
            }
            // InternalMCL.g:1573:3: ( rule__GNegationOperator__Alternatives )
            // InternalMCL.g:1573:4: rule__GNegationOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GNegationOperator__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationOperatorAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGNegationOperator"


    // $ANTLR start "rule__ExpOperation__Alternatives"
    // InternalMCL.g:1581:1: rule__ExpOperation__Alternatives : ( ( ruleGreater ) | ( ruleLesser ) );
    public final void rule__ExpOperation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1585:1: ( ( ruleGreater ) | ( ruleLesser ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==23) ) {
                alt1=1;
            }
            else if ( (LA1_0==22) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMCL.g:1586:2: ( ruleGreater )
                    {
                    // InternalMCL.g:1586:2: ( ruleGreater )
                    // InternalMCL.g:1587:3: ruleGreater
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpOperationAccess().getGreaterParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGreater();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpOperationAccess().getGreaterParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1592:2: ( ruleLesser )
                    {
                    // InternalMCL.g:1592:2: ( ruleLesser )
                    // InternalMCL.g:1593:3: ruleLesser
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpOperationAccess().getLesserParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleLesser();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpOperationAccess().getLesserParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpOperation__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalMCL.g:1602:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1606:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMCL.g:1607:2: ( RULE_STRING )
                    {
                    // InternalMCL.g:1607:2: ( RULE_STRING )
                    // InternalMCL.g:1608:3: RULE_STRING
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }
                    match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1613:2: ( RULE_ID )
                    {
                    // InternalMCL.g:1613:2: ( RULE_ID )
                    // InternalMCL.g:1614:3: RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    }
                    match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__TriggeringCondition__Alternatives"
    // InternalMCL.g:1623:1: rule__TriggeringCondition__Alternatives : ( ( rulePeriodicExpression ) | ( ruleThresholdExpression ) | ( ruleEventTrigger ) | ( ruleUpdated ) | ( ruleReadyToRead ) | ( ( rule__TriggeringCondition__Group_5__0 ) ) );
    public final void rule__TriggeringCondition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1627:1: ( ( rulePeriodicExpression ) | ( ruleThresholdExpression ) | ( ruleEventTrigger ) | ( ruleUpdated ) | ( ruleReadyToRead ) | ( ( rule__TriggeringCondition__Group_5__0 ) ) )
            int alt3=6;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // InternalMCL.g:1628:2: ( rulePeriodicExpression )
                    {
                    // InternalMCL.g:1628:2: ( rulePeriodicExpression )
                    // InternalMCL.g:1629:3: rulePeriodicExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getPeriodicExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    rulePeriodicExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getPeriodicExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1634:2: ( ruleThresholdExpression )
                    {
                    // InternalMCL.g:1634:2: ( ruleThresholdExpression )
                    // InternalMCL.g:1635:3: ruleThresholdExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getThresholdExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleThresholdExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getThresholdExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalMCL.g:1640:2: ( ruleEventTrigger )
                    {
                    // InternalMCL.g:1640:2: ( ruleEventTrigger )
                    // InternalMCL.g:1641:3: ruleEventTrigger
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getEventTriggerParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleEventTrigger();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getEventTriggerParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalMCL.g:1646:2: ( ruleUpdated )
                    {
                    // InternalMCL.g:1646:2: ( ruleUpdated )
                    // InternalMCL.g:1647:3: ruleUpdated
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getUpdatedParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleUpdated();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getUpdatedParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalMCL.g:1652:2: ( ruleReadyToRead )
                    {
                    // InternalMCL.g:1652:2: ( ruleReadyToRead )
                    // InternalMCL.g:1653:3: ruleReadyToRead
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getReadyToReadParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleReadyToRead();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getReadyToReadParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalMCL.g:1658:2: ( ( rule__TriggeringCondition__Group_5__0 ) )
                    {
                    // InternalMCL.g:1658:2: ( ( rule__TriggeringCondition__Group_5__0 ) )
                    // InternalMCL.g:1659:3: ( rule__TriggeringCondition__Group_5__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getTriggeringConditionAccess().getGroup_5()); 
                    }
                    // InternalMCL.g:1660:3: ( rule__TriggeringCondition__Group_5__0 )
                    // InternalMCL.g:1660:4: rule__TriggeringCondition__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TriggeringCondition__Group_5__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getTriggeringConditionAccess().getGroup_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Alternatives"


    // $ANTLR start "rule__OtherExpression__Alternatives"
    // InternalMCL.g:1668:1: rule__OtherExpression__Alternatives : ( ( rulePortRef ) | ( ruleUnaryMinus ) | ( ruleNot ) | ( ruleBinaryExpression ) );
    public final void rule__OtherExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1672:1: ( ( rulePortRef ) | ( ruleUnaryMinus ) | ( ruleNot ) | ( ruleBinaryExpression ) )
            int alt4=4;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalMCL.g:1673:2: ( rulePortRef )
                    {
                    // InternalMCL.g:1673:2: ( rulePortRef )
                    // InternalMCL.g:1674:3: rulePortRef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOtherExpressionAccess().getPortRefParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    rulePortRef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOtherExpressionAccess().getPortRefParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1679:2: ( ruleUnaryMinus )
                    {
                    // InternalMCL.g:1679:2: ( ruleUnaryMinus )
                    // InternalMCL.g:1680:3: ruleUnaryMinus
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOtherExpressionAccess().getUnaryMinusParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleUnaryMinus();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOtherExpressionAccess().getUnaryMinusParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalMCL.g:1685:2: ( ruleNot )
                    {
                    // InternalMCL.g:1685:2: ( ruleNot )
                    // InternalMCL.g:1686:3: ruleNot
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOtherExpressionAccess().getNotParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleNot();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOtherExpressionAccess().getNotParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalMCL.g:1691:2: ( ruleBinaryExpression )
                    {
                    // InternalMCL.g:1691:2: ( ruleBinaryExpression )
                    // InternalMCL.g:1692:3: ruleBinaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getOtherExpressionAccess().getBinaryExpressionParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleBinaryExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getOtherExpressionAccess().getBinaryExpressionParserRuleCall_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherExpression__Alternatives"


    // $ANTLR start "rule__BinaryExpression__Alternatives"
    // InternalMCL.g:1701:1: rule__BinaryExpression__Alternatives : ( ( ruleBinaryMinus ) | ( ruleBinaryPlus ) );
    public final void rule__BinaryExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1705:1: ( ( ruleBinaryMinus ) | ( ruleBinaryPlus ) )
            int alt5=2;
            alt5 = dfa5.predict(input);
            switch (alt5) {
                case 1 :
                    // InternalMCL.g:1706:2: ( ruleBinaryMinus )
                    {
                    // InternalMCL.g:1706:2: ( ruleBinaryMinus )
                    // InternalMCL.g:1707:3: ruleBinaryMinus
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryExpressionAccess().getBinaryMinusParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleBinaryMinus();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryExpressionAccess().getBinaryMinusParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1712:2: ( ruleBinaryPlus )
                    {
                    // InternalMCL.g:1712:2: ( ruleBinaryPlus )
                    // InternalMCL.g:1713:3: ruleBinaryPlus
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryExpressionAccess().getBinaryPlusParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleBinaryPlus();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryExpressionAccess().getBinaryPlusParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Alternatives"


    // $ANTLR start "rule__Not__Alternatives_0"
    // InternalMCL.g:1722:1: rule__Not__Alternatives_0 : ( ( '!' ) | ( 'not' ) );
    public final void rule__Not__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1726:1: ( ( '!' ) | ( 'not' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            else if ( (LA6_0==17) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalMCL.g:1727:2: ( '!' )
                    {
                    // InternalMCL.g:1727:2: ( '!' )
                    // InternalMCL.g:1728:3: '!'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNotAccess().getExclamationMarkKeyword_0_0()); 
                    }
                    match(input,16,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNotAccess().getExclamationMarkKeyword_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1733:2: ( 'not' )
                    {
                    // InternalMCL.g:1733:2: ( 'not' )
                    // InternalMCL.g:1734:3: 'not'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNotAccess().getNotKeyword_0_1()); 
                    }
                    match(input,17,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNotAccess().getNotKeyword_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Alternatives_0"


    // $ANTLR start "rule__GNegationExpression__Alternatives"
    // InternalMCL.g:1743:1: rule__GNegationExpression__Alternatives : ( ( ruleGNavigationExpression ) | ( ( rule__GNegationExpression__Group_1__0 ) ) );
    public final void rule__GNegationExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1747:1: ( ( ruleGNavigationExpression ) | ( ( rule__GNegationExpression__Group_1__0 ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=RULE_STRING && LA7_0<=RULE_BOOLEAN)||LA7_0==34||(LA7_0>=57 && LA7_0<=58)) ) {
                alt7=1;
            }
            else if ( (LA7_0==17||LA7_0==30) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMCL.g:1748:2: ( ruleGNavigationExpression )
                    {
                    // InternalMCL.g:1748:2: ( ruleGNavigationExpression )
                    // InternalMCL.g:1749:3: ruleGNavigationExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNegationExpressionAccess().getGNavigationExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGNavigationExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNegationExpressionAccess().getGNavigationExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1754:2: ( ( rule__GNegationExpression__Group_1__0 ) )
                    {
                    // InternalMCL.g:1754:2: ( ( rule__GNegationExpression__Group_1__0 ) )
                    // InternalMCL.g:1755:3: ( rule__GNegationExpression__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNegationExpressionAccess().getGroup_1()); 
                    }
                    // InternalMCL.g:1756:3: ( rule__GNegationExpression__Group_1__0 )
                    // InternalMCL.g:1756:4: rule__GNegationExpression__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GNegationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNegationExpressionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Alternatives"


    // $ANTLR start "rule__GReferenceExpression__Alternatives"
    // InternalMCL.g:1764:1: rule__GReferenceExpression__Alternatives : ( ( ruleGPrimaryExpression ) | ( ( rule__GReferenceExpression__Group_1__0 ) ) );
    public final void rule__GReferenceExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1768:1: ( ( ruleGPrimaryExpression ) | ( ( rule__GReferenceExpression__Group_1__0 ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING||(LA8_0>=RULE_INT && LA8_0<=RULE_BOOLEAN)||LA8_0==34||(LA8_0>=57 && LA8_0<=58)) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalMCL.g:1769:2: ( ruleGPrimaryExpression )
                    {
                    // InternalMCL.g:1769:2: ( ruleGPrimaryExpression )
                    // InternalMCL.g:1770:3: ruleGPrimaryExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGReferenceExpressionAccess().getGPrimaryExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGPrimaryExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGReferenceExpressionAccess().getGPrimaryExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1775:2: ( ( rule__GReferenceExpression__Group_1__0 ) )
                    {
                    // InternalMCL.g:1775:2: ( ( rule__GReferenceExpression__Group_1__0 ) )
                    // InternalMCL.g:1776:3: ( rule__GReferenceExpression__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGReferenceExpressionAccess().getGroup_1()); 
                    }
                    // InternalMCL.g:1777:3: ( rule__GReferenceExpression__Group_1__0 )
                    // InternalMCL.g:1777:4: rule__GReferenceExpression__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GReferenceExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGReferenceExpressionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__Alternatives"


    // $ANTLR start "rule__GPrimaryExpression__Alternatives"
    // InternalMCL.g:1785:1: rule__GPrimaryExpression__Alternatives : ( ( ruleGStringExpression ) | ( ruleGBooleanExpression ) | ( ruleGNumericExpression ) | ( ruleGEnumLiteralExpression ) | ( ruleGIfExpression ) | ( ruleGBraceExpression ) );
    public final void rule__GPrimaryExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1789:1: ( ( ruleGStringExpression ) | ( ruleGBooleanExpression ) | ( ruleGNumericExpression ) | ( ruleGEnumLiteralExpression ) | ( ruleGIfExpression ) | ( ruleGBraceExpression ) )
            int alt9=6;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt9=1;
                }
                break;
            case RULE_BOOLEAN:
                {
                alt9=2;
                }
                break;
            case RULE_INT:
            case RULE_DOUBLE:
                {
                alt9=3;
                }
                break;
            case 57:
                {
                alt9=4;
                }
                break;
            case 58:
                {
                alt9=5;
                }
                break;
            case 34:
                {
                alt9=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalMCL.g:1790:2: ( ruleGStringExpression )
                    {
                    // InternalMCL.g:1790:2: ( ruleGStringExpression )
                    // InternalMCL.g:1791:3: ruleGStringExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGStringExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGStringExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGStringExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1796:2: ( ruleGBooleanExpression )
                    {
                    // InternalMCL.g:1796:2: ( ruleGBooleanExpression )
                    // InternalMCL.g:1797:3: ruleGBooleanExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGBooleanExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGBooleanExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGBooleanExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalMCL.g:1802:2: ( ruleGNumericExpression )
                    {
                    // InternalMCL.g:1802:2: ( ruleGNumericExpression )
                    // InternalMCL.g:1803:3: ruleGNumericExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGNumericExpressionParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGNumericExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGNumericExpressionParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalMCL.g:1808:2: ( ruleGEnumLiteralExpression )
                    {
                    // InternalMCL.g:1808:2: ( ruleGEnumLiteralExpression )
                    // InternalMCL.g:1809:3: ruleGEnumLiteralExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGEnumLiteralExpressionParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGEnumLiteralExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGEnumLiteralExpressionParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalMCL.g:1814:2: ( ruleGIfExpression )
                    {
                    // InternalMCL.g:1814:2: ( ruleGIfExpression )
                    // InternalMCL.g:1815:3: ruleGIfExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGIfExpressionParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGIfExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGIfExpressionParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalMCL.g:1820:2: ( ruleGBraceExpression )
                    {
                    // InternalMCL.g:1820:2: ( ruleGBraceExpression )
                    // InternalMCL.g:1821:3: ruleGBraceExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGPrimaryExpressionAccess().getGBraceExpressionParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGBraceExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGPrimaryExpressionAccess().getGBraceExpressionParserRuleCall_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GPrimaryExpression__Alternatives"


    // $ANTLR start "rule__GNumericExpression__Alternatives"
    // InternalMCL.g:1830:1: rule__GNumericExpression__Alternatives : ( ( ruleGIntegerExpression ) | ( ruleGDoubleExpression ) );
    public final void rule__GNumericExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1834:1: ( ( ruleGIntegerExpression ) | ( ruleGDoubleExpression ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_INT) ) {
                alt10=1;
            }
            else if ( (LA10_0==RULE_DOUBLE) ) {
                alt10=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalMCL.g:1835:2: ( ruleGIntegerExpression )
                    {
                    // InternalMCL.g:1835:2: ( ruleGIntegerExpression )
                    // InternalMCL.g:1836:3: ruleGIntegerExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNumericExpressionAccess().getGIntegerExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGIntegerExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNumericExpressionAccess().getGIntegerExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1841:2: ( ruleGDoubleExpression )
                    {
                    // InternalMCL.g:1841:2: ( ruleGDoubleExpression )
                    // InternalMCL.g:1842:3: ruleGDoubleExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNumericExpressionAccess().getGDoubleExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_2);
                    ruleGDoubleExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNumericExpressionAccess().getGDoubleExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNumericExpression__Alternatives"


    // $ANTLR start "rule__NavigationOperator__Alternatives"
    // InternalMCL.g:1851:1: rule__NavigationOperator__Alternatives : ( ( '.' ) | ( '->' ) );
    public final void rule__NavigationOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1855:1: ( ( '.' ) | ( '->' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==18) ) {
                alt11=1;
            }
            else if ( (LA11_0==19) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalMCL.g:1856:2: ( '.' )
                    {
                    // InternalMCL.g:1856:2: ( '.' )
                    // InternalMCL.g:1857:3: '.'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNavigationOperatorAccess().getFullStopKeyword_0()); 
                    }
                    match(input,18,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNavigationOperatorAccess().getFullStopKeyword_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1862:2: ( '->' )
                    {
                    // InternalMCL.g:1862:2: ( '->' )
                    // InternalMCL.g:1863:3: '->'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNavigationOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
                    }
                    match(input,19,FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNavigationOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NavigationOperator__Alternatives"


    // $ANTLR start "rule__GEqualityOperator__Alternatives"
    // InternalMCL.g:1872:1: rule__GEqualityOperator__Alternatives : ( ( ( '=' ) ) | ( ( '<>' ) ) );
    public final void rule__GEqualityOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1876:1: ( ( ( '=' ) ) | ( ( '<>' ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==20) ) {
                alt12=1;
            }
            else if ( (LA12_0==21) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalMCL.g:1877:2: ( ( '=' ) )
                    {
                    // InternalMCL.g:1877:2: ( ( '=' ) )
                    // InternalMCL.g:1878:3: ( '=' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGEqualityOperatorAccess().getEQUALEnumLiteralDeclaration_0()); 
                    }
                    // InternalMCL.g:1879:3: ( '=' )
                    // InternalMCL.g:1879:4: '='
                    {
                    match(input,20,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGEqualityOperatorAccess().getEQUALEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1883:2: ( ( '<>' ) )
                    {
                    // InternalMCL.g:1883:2: ( ( '<>' ) )
                    // InternalMCL.g:1884:3: ( '<>' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGEqualityOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1()); 
                    }
                    // InternalMCL.g:1885:3: ( '<>' )
                    // InternalMCL.g:1885:4: '<>'
                    {
                    match(input,21,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGEqualityOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityOperator__Alternatives"


    // $ANTLR start "rule__GRelationOperator__Alternatives"
    // InternalMCL.g:1893:1: rule__GRelationOperator__Alternatives : ( ( ( '<' ) ) | ( ( '>' ) ) | ( ( '<=' ) ) | ( ( '>=' ) ) );
    public final void rule__GRelationOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1897:1: ( ( ( '<' ) ) | ( ( '>' ) ) | ( ( '<=' ) ) | ( ( '>=' ) ) )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt13=1;
                }
                break;
            case 23:
                {
                alt13=2;
                }
                break;
            case 24:
                {
                alt13=3;
                }
                break;
            case 25:
                {
                alt13=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalMCL.g:1898:2: ( ( '<' ) )
                    {
                    // InternalMCL.g:1898:2: ( ( '<' ) )
                    // InternalMCL.g:1899:3: ( '<' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGRelationOperatorAccess().getLESSEnumLiteralDeclaration_0()); 
                    }
                    // InternalMCL.g:1900:3: ( '<' )
                    // InternalMCL.g:1900:4: '<'
                    {
                    match(input,22,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGRelationOperatorAccess().getLESSEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1904:2: ( ( '>' ) )
                    {
                    // InternalMCL.g:1904:2: ( ( '>' ) )
                    // InternalMCL.g:1905:3: ( '>' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGRelationOperatorAccess().getGREATEREnumLiteralDeclaration_1()); 
                    }
                    // InternalMCL.g:1906:3: ( '>' )
                    // InternalMCL.g:1906:4: '>'
                    {
                    match(input,23,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGRelationOperatorAccess().getGREATEREnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalMCL.g:1910:2: ( ( '<=' ) )
                    {
                    // InternalMCL.g:1910:2: ( ( '<=' ) )
                    // InternalMCL.g:1911:3: ( '<=' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGRelationOperatorAccess().getLESSEQUALEnumLiteralDeclaration_2()); 
                    }
                    // InternalMCL.g:1912:3: ( '<=' )
                    // InternalMCL.g:1912:4: '<='
                    {
                    match(input,24,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGRelationOperatorAccess().getLESSEQUALEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalMCL.g:1916:2: ( ( '>=' ) )
                    {
                    // InternalMCL.g:1916:2: ( ( '>=' ) )
                    // InternalMCL.g:1917:3: ( '>=' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGRelationOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3()); 
                    }
                    // InternalMCL.g:1918:3: ( '>=' )
                    // InternalMCL.g:1918:4: '>='
                    {
                    match(input,25,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGRelationOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationOperator__Alternatives"


    // $ANTLR start "rule__GAdditionOperator__Alternatives"
    // InternalMCL.g:1926:1: rule__GAdditionOperator__Alternatives : ( ( ( '+' ) ) | ( ( '-' ) ) );
    public final void rule__GAdditionOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1930:1: ( ( ( '+' ) ) | ( ( '-' ) ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==26) ) {
                alt14=1;
            }
            else if ( (LA14_0==27) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalMCL.g:1931:2: ( ( '+' ) )
                    {
                    // InternalMCL.g:1931:2: ( ( '+' ) )
                    // InternalMCL.g:1932:3: ( '+' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGAdditionOperatorAccess().getADDITIONEnumLiteralDeclaration_0()); 
                    }
                    // InternalMCL.g:1933:3: ( '+' )
                    // InternalMCL.g:1933:4: '+'
                    {
                    match(input,26,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGAdditionOperatorAccess().getADDITIONEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1937:2: ( ( '-' ) )
                    {
                    // InternalMCL.g:1937:2: ( ( '-' ) )
                    // InternalMCL.g:1938:3: ( '-' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGAdditionOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1()); 
                    }
                    // InternalMCL.g:1939:3: ( '-' )
                    // InternalMCL.g:1939:4: '-'
                    {
                    match(input,27,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGAdditionOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionOperator__Alternatives"


    // $ANTLR start "rule__GMultiplicationOperator__Alternatives"
    // InternalMCL.g:1947:1: rule__GMultiplicationOperator__Alternatives : ( ( ( '*' ) ) | ( ( '/' ) ) );
    public final void rule__GMultiplicationOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1951:1: ( ( ( '*' ) ) | ( ( '/' ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==28) ) {
                alt15=1;
            }
            else if ( (LA15_0==29) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalMCL.g:1952:2: ( ( '*' ) )
                    {
                    // InternalMCL.g:1952:2: ( ( '*' ) )
                    // InternalMCL.g:1953:3: ( '*' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGMultiplicationOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0()); 
                    }
                    // InternalMCL.g:1954:3: ( '*' )
                    // InternalMCL.g:1954:4: '*'
                    {
                    match(input,28,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGMultiplicationOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1958:2: ( ( '/' ) )
                    {
                    // InternalMCL.g:1958:2: ( ( '/' ) )
                    // InternalMCL.g:1959:3: ( '/' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGMultiplicationOperatorAccess().getDIVISIONEnumLiteralDeclaration_1()); 
                    }
                    // InternalMCL.g:1960:3: ( '/' )
                    // InternalMCL.g:1960:4: '/'
                    {
                    match(input,29,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGMultiplicationOperatorAccess().getDIVISIONEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationOperator__Alternatives"


    // $ANTLR start "rule__GNegationOperator__Alternatives"
    // InternalMCL.g:1968:1: rule__GNegationOperator__Alternatives : ( ( ( 'not' ) ) | ( ( '~' ) ) );
    public final void rule__GNegationOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1972:1: ( ( ( 'not' ) ) | ( ( '~' ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==17) ) {
                alt16=1;
            }
            else if ( (LA16_0==30) ) {
                alt16=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalMCL.g:1973:2: ( ( 'not' ) )
                    {
                    // InternalMCL.g:1973:2: ( ( 'not' ) )
                    // InternalMCL.g:1974:3: ( 'not' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNegationOperatorAccess().getNEGATIONEnumLiteralDeclaration_0()); 
                    }
                    // InternalMCL.g:1975:3: ( 'not' )
                    // InternalMCL.g:1975:4: 'not'
                    {
                    match(input,17,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNegationOperatorAccess().getNEGATIONEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:1979:2: ( ( '~' ) )
                    {
                    // InternalMCL.g:1979:2: ( ( '~' ) )
                    // InternalMCL.g:1980:3: ( '~' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getGNegationOperatorAccess().getMINUSEnumLiteralDeclaration_1()); 
                    }
                    // InternalMCL.g:1981:3: ( '~' )
                    // InternalMCL.g:1981:4: '~'
                    {
                    match(input,30,FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getGNegationOperatorAccess().getMINUSEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationOperator__Alternatives"


    // $ANTLR start "rule__MCSpecification__Group__0"
    // InternalMCL.g:1989:1: rule__MCSpecification__Group__0 : rule__MCSpecification__Group__0__Impl rule__MCSpecification__Group__1 ;
    public final void rule__MCSpecification__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:1993:1: ( rule__MCSpecification__Group__0__Impl rule__MCSpecification__Group__1 )
            // InternalMCL.g:1994:2: rule__MCSpecification__Group__0__Impl rule__MCSpecification__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__MCSpecification__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__0"


    // $ANTLR start "rule__MCSpecification__Group__0__Impl"
    // InternalMCL.g:2001:1: rule__MCSpecification__Group__0__Impl : ( () ) ;
    public final void rule__MCSpecification__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2005:1: ( ( () ) )
            // InternalMCL.g:2006:1: ( () )
            {
            // InternalMCL.g:2006:1: ( () )
            // InternalMCL.g:2007:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getMCSpecificationAction_0()); 
            }
            // InternalMCL.g:2008:2: ()
            // InternalMCL.g:2008:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getMCSpecificationAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__0__Impl"


    // $ANTLR start "rule__MCSpecification__Group__1"
    // InternalMCL.g:2016:1: rule__MCSpecification__Group__1 : rule__MCSpecification__Group__1__Impl rule__MCSpecification__Group__2 ;
    public final void rule__MCSpecification__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2020:1: ( rule__MCSpecification__Group__1__Impl rule__MCSpecification__Group__2 )
            // InternalMCL.g:2021:2: rule__MCSpecification__Group__1__Impl rule__MCSpecification__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__MCSpecification__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__1"


    // $ANTLR start "rule__MCSpecification__Group__1__Impl"
    // InternalMCL.g:2028:1: rule__MCSpecification__Group__1__Impl : ( ( rule__MCSpecification__Group_1__0 )? ) ;
    public final void rule__MCSpecification__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2032:1: ( ( ( rule__MCSpecification__Group_1__0 )? ) )
            // InternalMCL.g:2033:1: ( ( rule__MCSpecification__Group_1__0 )? )
            {
            // InternalMCL.g:2033:1: ( ( rule__MCSpecification__Group_1__0 )? )
            // InternalMCL.g:2034:2: ( rule__MCSpecification__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getGroup_1()); 
            }
            // InternalMCL.g:2035:2: ( rule__MCSpecification__Group_1__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==39) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalMCL.g:2035:3: rule__MCSpecification__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__MCSpecification__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__1__Impl"


    // $ANTLR start "rule__MCSpecification__Group__2"
    // InternalMCL.g:2043:1: rule__MCSpecification__Group__2 : rule__MCSpecification__Group__2__Impl ;
    public final void rule__MCSpecification__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2047:1: ( rule__MCSpecification__Group__2__Impl )
            // InternalMCL.g:2048:2: rule__MCSpecification__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__2"


    // $ANTLR start "rule__MCSpecification__Group__2__Impl"
    // InternalMCL.g:2054:1: rule__MCSpecification__Group__2__Impl : ( ( rule__MCSpecification__Group_2__0 )? ) ;
    public final void rule__MCSpecification__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2058:1: ( ( ( rule__MCSpecification__Group_2__0 )? ) )
            // InternalMCL.g:2059:1: ( ( rule__MCSpecification__Group_2__0 )? )
            {
            // InternalMCL.g:2059:1: ( ( rule__MCSpecification__Group_2__0 )? )
            // InternalMCL.g:2060:2: ( rule__MCSpecification__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getGroup_2()); 
            }
            // InternalMCL.g:2061:2: ( rule__MCSpecification__Group_2__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==31) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalMCL.g:2061:3: rule__MCSpecification__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__MCSpecification__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group__2__Impl"


    // $ANTLR start "rule__MCSpecification__Group_1__0"
    // InternalMCL.g:2070:1: rule__MCSpecification__Group_1__0 : rule__MCSpecification__Group_1__0__Impl rule__MCSpecification__Group_1__1 ;
    public final void rule__MCSpecification__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2074:1: ( rule__MCSpecification__Group_1__0__Impl rule__MCSpecification__Group_1__1 )
            // InternalMCL.g:2075:2: rule__MCSpecification__Group_1__0__Impl rule__MCSpecification__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__MCSpecification__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_1__0"


    // $ANTLR start "rule__MCSpecification__Group_1__0__Impl"
    // InternalMCL.g:2082:1: rule__MCSpecification__Group_1__0__Impl : ( ( rule__MCSpecification__ImportedMBIAssignment_1_0 ) ) ;
    public final void rule__MCSpecification__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2086:1: ( ( ( rule__MCSpecification__ImportedMBIAssignment_1_0 ) ) )
            // InternalMCL.g:2087:1: ( ( rule__MCSpecification__ImportedMBIAssignment_1_0 ) )
            {
            // InternalMCL.g:2087:1: ( ( rule__MCSpecification__ImportedMBIAssignment_1_0 ) )
            // InternalMCL.g:2088:2: ( rule__MCSpecification__ImportedMBIAssignment_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getImportedMBIAssignment_1_0()); 
            }
            // InternalMCL.g:2089:2: ( rule__MCSpecification__ImportedMBIAssignment_1_0 )
            // InternalMCL.g:2089:3: rule__MCSpecification__ImportedMBIAssignment_1_0
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__ImportedMBIAssignment_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getImportedMBIAssignment_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_1__0__Impl"


    // $ANTLR start "rule__MCSpecification__Group_1__1"
    // InternalMCL.g:2097:1: rule__MCSpecification__Group_1__1 : rule__MCSpecification__Group_1__1__Impl ;
    public final void rule__MCSpecification__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2101:1: ( rule__MCSpecification__Group_1__1__Impl )
            // InternalMCL.g:2102:2: rule__MCSpecification__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_1__1"


    // $ANTLR start "rule__MCSpecification__Group_1__1__Impl"
    // InternalMCL.g:2108:1: rule__MCSpecification__Group_1__1__Impl : ( ( rule__MCSpecification__ImportedMBIAssignment_1_1 )* ) ;
    public final void rule__MCSpecification__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2112:1: ( ( ( rule__MCSpecification__ImportedMBIAssignment_1_1 )* ) )
            // InternalMCL.g:2113:1: ( ( rule__MCSpecification__ImportedMBIAssignment_1_1 )* )
            {
            // InternalMCL.g:2113:1: ( ( rule__MCSpecification__ImportedMBIAssignment_1_1 )* )
            // InternalMCL.g:2114:2: ( rule__MCSpecification__ImportedMBIAssignment_1_1 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getImportedMBIAssignment_1_1()); 
            }
            // InternalMCL.g:2115:2: ( rule__MCSpecification__ImportedMBIAssignment_1_1 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==39) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalMCL.g:2115:3: rule__MCSpecification__ImportedMBIAssignment_1_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__MCSpecification__ImportedMBIAssignment_1_1();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getImportedMBIAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_1__1__Impl"


    // $ANTLR start "rule__MCSpecification__Group_2__0"
    // InternalMCL.g:2124:1: rule__MCSpecification__Group_2__0 : rule__MCSpecification__Group_2__0__Impl rule__MCSpecification__Group_2__1 ;
    public final void rule__MCSpecification__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2128:1: ( rule__MCSpecification__Group_2__0__Impl rule__MCSpecification__Group_2__1 )
            // InternalMCL.g:2129:2: rule__MCSpecification__Group_2__0__Impl rule__MCSpecification__Group_2__1
            {
            pushFollow(FOLLOW_6);
            rule__MCSpecification__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_2__0"


    // $ANTLR start "rule__MCSpecification__Group_2__0__Impl"
    // InternalMCL.g:2136:1: rule__MCSpecification__Group_2__0__Impl : ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 ) ) ;
    public final void rule__MCSpecification__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2140:1: ( ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 ) ) )
            // InternalMCL.g:2141:1: ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 ) )
            {
            // InternalMCL.g:2141:1: ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 ) )
            // InternalMCL.g:2142:2: ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsAssignment_2_0()); 
            }
            // InternalMCL.g:2143:2: ( rule__MCSpecification__OwnedConnectorsAssignment_2_0 )
            // InternalMCL.g:2143:3: rule__MCSpecification__OwnedConnectorsAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__OwnedConnectorsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_2__0__Impl"


    // $ANTLR start "rule__MCSpecification__Group_2__1"
    // InternalMCL.g:2151:1: rule__MCSpecification__Group_2__1 : rule__MCSpecification__Group_2__1__Impl ;
    public final void rule__MCSpecification__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2155:1: ( rule__MCSpecification__Group_2__1__Impl )
            // InternalMCL.g:2156:2: rule__MCSpecification__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MCSpecification__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_2__1"


    // $ANTLR start "rule__MCSpecification__Group_2__1__Impl"
    // InternalMCL.g:2162:1: rule__MCSpecification__Group_2__1__Impl : ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )* ) ;
    public final void rule__MCSpecification__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2166:1: ( ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )* ) )
            // InternalMCL.g:2167:1: ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )* )
            {
            // InternalMCL.g:2167:1: ( ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )* )
            // InternalMCL.g:2168:2: ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsAssignment_2_1()); 
            }
            // InternalMCL.g:2169:2: ( rule__MCSpecification__OwnedConnectorsAssignment_2_1 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==31) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalMCL.g:2169:3: rule__MCSpecification__OwnedConnectorsAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__MCSpecification__OwnedConnectorsAssignment_2_1();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__Group_2__1__Impl"


    // $ANTLR start "rule__Connector__Group__0"
    // InternalMCL.g:2178:1: rule__Connector__Group__0 : rule__Connector__Group__0__Impl rule__Connector__Group__1 ;
    public final void rule__Connector__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2182:1: ( rule__Connector__Group__0__Impl rule__Connector__Group__1 )
            // InternalMCL.g:2183:2: rule__Connector__Group__0__Impl rule__Connector__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Connector__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__0"


    // $ANTLR start "rule__Connector__Group__0__Impl"
    // InternalMCL.g:2190:1: rule__Connector__Group__0__Impl : ( 'Connector' ) ;
    public final void rule__Connector__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2194:1: ( ( 'Connector' ) )
            // InternalMCL.g:2195:1: ( 'Connector' )
            {
            // InternalMCL.g:2195:1: ( 'Connector' )
            // InternalMCL.g:2196:2: 'Connector'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getConnectorKeyword_0()); 
            }
            match(input,31,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getConnectorKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__0__Impl"


    // $ANTLR start "rule__Connector__Group__1"
    // InternalMCL.g:2205:1: rule__Connector__Group__1 : rule__Connector__Group__1__Impl rule__Connector__Group__2 ;
    public final void rule__Connector__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2209:1: ( rule__Connector__Group__1__Impl rule__Connector__Group__2 )
            // InternalMCL.g:2210:2: rule__Connector__Group__1__Impl rule__Connector__Group__2
            {
            pushFollow(FOLLOW_9);
            rule__Connector__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__1"


    // $ANTLR start "rule__Connector__Group__1__Impl"
    // InternalMCL.g:2217:1: rule__Connector__Group__1__Impl : ( ( rule__Connector__NameAssignment_1 ) ) ;
    public final void rule__Connector__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2221:1: ( ( ( rule__Connector__NameAssignment_1 ) ) )
            // InternalMCL.g:2222:1: ( ( rule__Connector__NameAssignment_1 ) )
            {
            // InternalMCL.g:2222:1: ( ( rule__Connector__NameAssignment_1 ) )
            // InternalMCL.g:2223:2: ( rule__Connector__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getNameAssignment_1()); 
            }
            // InternalMCL.g:2224:2: ( rule__Connector__NameAssignment_1 )
            // InternalMCL.g:2224:3: rule__Connector__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Connector__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__1__Impl"


    // $ANTLR start "rule__Connector__Group__2"
    // InternalMCL.g:2232:1: rule__Connector__Group__2 : rule__Connector__Group__2__Impl rule__Connector__Group__3 ;
    public final void rule__Connector__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2236:1: ( rule__Connector__Group__2__Impl rule__Connector__Group__3 )
            // InternalMCL.g:2237:2: rule__Connector__Group__2__Impl rule__Connector__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__Connector__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__2"


    // $ANTLR start "rule__Connector__Group__2__Impl"
    // InternalMCL.g:2244:1: rule__Connector__Group__2__Impl : ( ( rule__Connector__Group_2__0 )? ) ;
    public final void rule__Connector__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2248:1: ( ( ( rule__Connector__Group_2__0 )? ) )
            // InternalMCL.g:2249:1: ( ( rule__Connector__Group_2__0 )? )
            {
            // InternalMCL.g:2249:1: ( ( rule__Connector__Group_2__0 )? )
            // InternalMCL.g:2250:2: ( rule__Connector__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getGroup_2()); 
            }
            // InternalMCL.g:2251:2: ( rule__Connector__Group_2__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==34) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalMCL.g:2251:3: rule__Connector__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Connector__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__2__Impl"


    // $ANTLR start "rule__Connector__Group__3"
    // InternalMCL.g:2259:1: rule__Connector__Group__3 : rule__Connector__Group__3__Impl rule__Connector__Group__4 ;
    public final void rule__Connector__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2263:1: ( rule__Connector__Group__3__Impl rule__Connector__Group__4 )
            // InternalMCL.g:2264:2: rule__Connector__Group__3__Impl rule__Connector__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__Connector__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__3"


    // $ANTLR start "rule__Connector__Group__3__Impl"
    // InternalMCL.g:2271:1: rule__Connector__Group__3__Impl : ( 'when' ) ;
    public final void rule__Connector__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2275:1: ( ( 'when' ) )
            // InternalMCL.g:2276:1: ( 'when' )
            {
            // InternalMCL.g:2276:1: ( 'when' )
            // InternalMCL.g:2277:2: 'when'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getWhenKeyword_3()); 
            }
            match(input,32,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getWhenKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__3__Impl"


    // $ANTLR start "rule__Connector__Group__4"
    // InternalMCL.g:2286:1: rule__Connector__Group__4 : rule__Connector__Group__4__Impl rule__Connector__Group__5 ;
    public final void rule__Connector__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2290:1: ( rule__Connector__Group__4__Impl rule__Connector__Group__5 )
            // InternalMCL.g:2291:2: rule__Connector__Group__4__Impl rule__Connector__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__Connector__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__4"


    // $ANTLR start "rule__Connector__Group__4__Impl"
    // InternalMCL.g:2298:1: rule__Connector__Group__4__Impl : ( ( rule__Connector__TriggerAssignment_4 ) ) ;
    public final void rule__Connector__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2302:1: ( ( ( rule__Connector__TriggerAssignment_4 ) ) )
            // InternalMCL.g:2303:1: ( ( rule__Connector__TriggerAssignment_4 ) )
            {
            // InternalMCL.g:2303:1: ( ( rule__Connector__TriggerAssignment_4 ) )
            // InternalMCL.g:2304:2: ( rule__Connector__TriggerAssignment_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getTriggerAssignment_4()); 
            }
            // InternalMCL.g:2305:2: ( rule__Connector__TriggerAssignment_4 )
            // InternalMCL.g:2305:3: rule__Connector__TriggerAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Connector__TriggerAssignment_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getTriggerAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__4__Impl"


    // $ANTLR start "rule__Connector__Group__5"
    // InternalMCL.g:2313:1: rule__Connector__Group__5 : rule__Connector__Group__5__Impl rule__Connector__Group__6 ;
    public final void rule__Connector__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2317:1: ( rule__Connector__Group__5__Impl rule__Connector__Group__6 )
            // InternalMCL.g:2318:2: rule__Connector__Group__5__Impl rule__Connector__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__Connector__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__5"


    // $ANTLR start "rule__Connector__Group__5__Impl"
    // InternalMCL.g:2325:1: rule__Connector__Group__5__Impl : ( ( rule__Connector__Group_5__0 )? ) ;
    public final void rule__Connector__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2329:1: ( ( ( rule__Connector__Group_5__0 )? ) )
            // InternalMCL.g:2330:1: ( ( rule__Connector__Group_5__0 )? )
            {
            // InternalMCL.g:2330:1: ( ( rule__Connector__Group_5__0 )? )
            // InternalMCL.g:2331:2: ( rule__Connector__Group_5__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getGroup_5()); 
            }
            // InternalMCL.g:2332:2: ( rule__Connector__Group_5__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==38) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalMCL.g:2332:3: rule__Connector__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Connector__Group_5__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getGroup_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__5__Impl"


    // $ANTLR start "rule__Connector__Group__6"
    // InternalMCL.g:2340:1: rule__Connector__Group__6 : rule__Connector__Group__6__Impl rule__Connector__Group__7 ;
    public final void rule__Connector__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2344:1: ( rule__Connector__Group__6__Impl rule__Connector__Group__7 )
            // InternalMCL.g:2345:2: rule__Connector__Group__6__Impl rule__Connector__Group__7
            {
            pushFollow(FOLLOW_12);
            rule__Connector__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__6"


    // $ANTLR start "rule__Connector__Group__6__Impl"
    // InternalMCL.g:2352:1: rule__Connector__Group__6__Impl : ( 'do' ) ;
    public final void rule__Connector__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2356:1: ( ( 'do' ) )
            // InternalMCL.g:2357:1: ( 'do' )
            {
            // InternalMCL.g:2357:1: ( 'do' )
            // InternalMCL.g:2358:2: 'do'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getDoKeyword_6()); 
            }
            match(input,33,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getDoKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__6__Impl"


    // $ANTLR start "rule__Connector__Group__7"
    // InternalMCL.g:2367:1: rule__Connector__Group__7 : rule__Connector__Group__7__Impl ;
    public final void rule__Connector__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2371:1: ( rule__Connector__Group__7__Impl )
            // InternalMCL.g:2372:2: rule__Connector__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Connector__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__7"


    // $ANTLR start "rule__Connector__Group__7__Impl"
    // InternalMCL.g:2378:1: rule__Connector__Group__7__Impl : ( ( rule__Connector__InteractionstatementAssignment_7 ) ) ;
    public final void rule__Connector__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2382:1: ( ( ( rule__Connector__InteractionstatementAssignment_7 ) ) )
            // InternalMCL.g:2383:1: ( ( rule__Connector__InteractionstatementAssignment_7 ) )
            {
            // InternalMCL.g:2383:1: ( ( rule__Connector__InteractionstatementAssignment_7 ) )
            // InternalMCL.g:2384:2: ( rule__Connector__InteractionstatementAssignment_7 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getInteractionstatementAssignment_7()); 
            }
            // InternalMCL.g:2385:2: ( rule__Connector__InteractionstatementAssignment_7 )
            // InternalMCL.g:2385:3: rule__Connector__InteractionstatementAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Connector__InteractionstatementAssignment_7();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getInteractionstatementAssignment_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group__7__Impl"


    // $ANTLR start "rule__Connector__Group_2__0"
    // InternalMCL.g:2394:1: rule__Connector__Group_2__0 : rule__Connector__Group_2__0__Impl rule__Connector__Group_2__1 ;
    public final void rule__Connector__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2398:1: ( rule__Connector__Group_2__0__Impl rule__Connector__Group_2__1 )
            // InternalMCL.g:2399:2: rule__Connector__Group_2__0__Impl rule__Connector__Group_2__1
            {
            pushFollow(FOLLOW_13);
            rule__Connector__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__0"


    // $ANTLR start "rule__Connector__Group_2__0__Impl"
    // InternalMCL.g:2406:1: rule__Connector__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Connector__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2410:1: ( ( '(' ) )
            // InternalMCL.g:2411:1: ( '(' )
            {
            // InternalMCL.g:2411:1: ( '(' )
            // InternalMCL.g:2412:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getLeftParenthesisKeyword_2_0()); 
            }
            match(input,34,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getLeftParenthesisKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__0__Impl"


    // $ANTLR start "rule__Connector__Group_2__1"
    // InternalMCL.g:2421:1: rule__Connector__Group_2__1 : rule__Connector__Group_2__1__Impl rule__Connector__Group_2__2 ;
    public final void rule__Connector__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2425:1: ( rule__Connector__Group_2__1__Impl rule__Connector__Group_2__2 )
            // InternalMCL.g:2426:2: rule__Connector__Group_2__1__Impl rule__Connector__Group_2__2
            {
            pushFollow(FOLLOW_8);
            rule__Connector__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__1"


    // $ANTLR start "rule__Connector__Group_2__1__Impl"
    // InternalMCL.g:2433:1: rule__Connector__Group_2__1__Impl : ( 'from' ) ;
    public final void rule__Connector__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2437:1: ( ( 'from' ) )
            // InternalMCL.g:2438:1: ( 'from' )
            {
            // InternalMCL.g:2438:1: ( 'from' )
            // InternalMCL.g:2439:2: 'from'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getFromKeyword_2_1()); 
            }
            match(input,35,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getFromKeyword_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__1__Impl"


    // $ANTLR start "rule__Connector__Group_2__2"
    // InternalMCL.g:2448:1: rule__Connector__Group_2__2 : rule__Connector__Group_2__2__Impl rule__Connector__Group_2__3 ;
    public final void rule__Connector__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2452:1: ( rule__Connector__Group_2__2__Impl rule__Connector__Group_2__3 )
            // InternalMCL.g:2453:2: rule__Connector__Group_2__2__Impl rule__Connector__Group_2__3
            {
            pushFollow(FOLLOW_14);
            rule__Connector__Group_2__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__2"


    // $ANTLR start "rule__Connector__Group_2__2__Impl"
    // InternalMCL.g:2460:1: rule__Connector__Group_2__2__Impl : ( ( rule__Connector__FromAssignment_2_2 ) ) ;
    public final void rule__Connector__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2464:1: ( ( ( rule__Connector__FromAssignment_2_2 ) ) )
            // InternalMCL.g:2465:1: ( ( rule__Connector__FromAssignment_2_2 ) )
            {
            // InternalMCL.g:2465:1: ( ( rule__Connector__FromAssignment_2_2 ) )
            // InternalMCL.g:2466:2: ( rule__Connector__FromAssignment_2_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getFromAssignment_2_2()); 
            }
            // InternalMCL.g:2467:2: ( rule__Connector__FromAssignment_2_2 )
            // InternalMCL.g:2467:3: rule__Connector__FromAssignment_2_2
            {
            pushFollow(FOLLOW_2);
            rule__Connector__FromAssignment_2_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getFromAssignment_2_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__2__Impl"


    // $ANTLR start "rule__Connector__Group_2__3"
    // InternalMCL.g:2475:1: rule__Connector__Group_2__3 : rule__Connector__Group_2__3__Impl rule__Connector__Group_2__4 ;
    public final void rule__Connector__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2479:1: ( rule__Connector__Group_2__3__Impl rule__Connector__Group_2__4 )
            // InternalMCL.g:2480:2: rule__Connector__Group_2__3__Impl rule__Connector__Group_2__4
            {
            pushFollow(FOLLOW_8);
            rule__Connector__Group_2__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__3"


    // $ANTLR start "rule__Connector__Group_2__3__Impl"
    // InternalMCL.g:2487:1: rule__Connector__Group_2__3__Impl : ( 'to' ) ;
    public final void rule__Connector__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2491:1: ( ( 'to' ) )
            // InternalMCL.g:2492:1: ( 'to' )
            {
            // InternalMCL.g:2492:1: ( 'to' )
            // InternalMCL.g:2493:2: 'to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getToKeyword_2_3()); 
            }
            match(input,36,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getToKeyword_2_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__3__Impl"


    // $ANTLR start "rule__Connector__Group_2__4"
    // InternalMCL.g:2502:1: rule__Connector__Group_2__4 : rule__Connector__Group_2__4__Impl rule__Connector__Group_2__5 ;
    public final void rule__Connector__Group_2__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2506:1: ( rule__Connector__Group_2__4__Impl rule__Connector__Group_2__5 )
            // InternalMCL.g:2507:2: rule__Connector__Group_2__4__Impl rule__Connector__Group_2__5
            {
            pushFollow(FOLLOW_15);
            rule__Connector__Group_2__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__4"


    // $ANTLR start "rule__Connector__Group_2__4__Impl"
    // InternalMCL.g:2514:1: rule__Connector__Group_2__4__Impl : ( ( rule__Connector__ToAssignment_2_4 ) ) ;
    public final void rule__Connector__Group_2__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2518:1: ( ( ( rule__Connector__ToAssignment_2_4 ) ) )
            // InternalMCL.g:2519:1: ( ( rule__Connector__ToAssignment_2_4 ) )
            {
            // InternalMCL.g:2519:1: ( ( rule__Connector__ToAssignment_2_4 ) )
            // InternalMCL.g:2520:2: ( rule__Connector__ToAssignment_2_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getToAssignment_2_4()); 
            }
            // InternalMCL.g:2521:2: ( rule__Connector__ToAssignment_2_4 )
            // InternalMCL.g:2521:3: rule__Connector__ToAssignment_2_4
            {
            pushFollow(FOLLOW_2);
            rule__Connector__ToAssignment_2_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getToAssignment_2_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__4__Impl"


    // $ANTLR start "rule__Connector__Group_2__5"
    // InternalMCL.g:2529:1: rule__Connector__Group_2__5 : rule__Connector__Group_2__5__Impl ;
    public final void rule__Connector__Group_2__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2533:1: ( rule__Connector__Group_2__5__Impl )
            // InternalMCL.g:2534:2: rule__Connector__Group_2__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Connector__Group_2__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__5"


    // $ANTLR start "rule__Connector__Group_2__5__Impl"
    // InternalMCL.g:2540:1: rule__Connector__Group_2__5__Impl : ( ')' ) ;
    public final void rule__Connector__Group_2__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2544:1: ( ( ')' ) )
            // InternalMCL.g:2545:1: ( ')' )
            {
            // InternalMCL.g:2545:1: ( ')' )
            // InternalMCL.g:2546:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getRightParenthesisKeyword_2_5()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getRightParenthesisKeyword_2_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_2__5__Impl"


    // $ANTLR start "rule__Connector__Group_5__0"
    // InternalMCL.g:2556:1: rule__Connector__Group_5__0 : rule__Connector__Group_5__0__Impl rule__Connector__Group_5__1 ;
    public final void rule__Connector__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2560:1: ( rule__Connector__Group_5__0__Impl rule__Connector__Group_5__1 )
            // InternalMCL.g:2561:2: rule__Connector__Group_5__0__Impl rule__Connector__Group_5__1
            {
            pushFollow(FOLLOW_8);
            rule__Connector__Group_5__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Connector__Group_5__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_5__0"


    // $ANTLR start "rule__Connector__Group_5__0__Impl"
    // InternalMCL.g:2568:1: rule__Connector__Group_5__0__Impl : ( 'sync' ) ;
    public final void rule__Connector__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2572:1: ( ( 'sync' ) )
            // InternalMCL.g:2573:1: ( 'sync' )
            {
            // InternalMCL.g:2573:1: ( 'sync' )
            // InternalMCL.g:2574:2: 'sync'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getSyncKeyword_5_0()); 
            }
            match(input,38,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getSyncKeyword_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_5__0__Impl"


    // $ANTLR start "rule__Connector__Group_5__1"
    // InternalMCL.g:2583:1: rule__Connector__Group_5__1 : rule__Connector__Group_5__1__Impl ;
    public final void rule__Connector__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2587:1: ( rule__Connector__Group_5__1__Impl )
            // InternalMCL.g:2588:2: rule__Connector__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Connector__Group_5__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_5__1"


    // $ANTLR start "rule__Connector__Group_5__1__Impl"
    // InternalMCL.g:2594:1: rule__Connector__Group_5__1__Impl : ( ( rule__Connector__SynchronizationRuleAssignment_5_1 ) ) ;
    public final void rule__Connector__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2598:1: ( ( ( rule__Connector__SynchronizationRuleAssignment_5_1 ) ) )
            // InternalMCL.g:2599:1: ( ( rule__Connector__SynchronizationRuleAssignment_5_1 ) )
            {
            // InternalMCL.g:2599:1: ( ( rule__Connector__SynchronizationRuleAssignment_5_1 ) )
            // InternalMCL.g:2600:2: ( rule__Connector__SynchronizationRuleAssignment_5_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getSynchronizationRuleAssignment_5_1()); 
            }
            // InternalMCL.g:2601:2: ( rule__Connector__SynchronizationRuleAssignment_5_1 )
            // InternalMCL.g:2601:3: rule__Connector__SynchronizationRuleAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Connector__SynchronizationRuleAssignment_5_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getSynchronizationRuleAssignment_5_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__Group_5__1__Impl"


    // $ANTLR start "rule__ImportInterfaceStatement__Group__0"
    // InternalMCL.g:2610:1: rule__ImportInterfaceStatement__Group__0 : rule__ImportInterfaceStatement__Group__0__Impl rule__ImportInterfaceStatement__Group__1 ;
    public final void rule__ImportInterfaceStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2614:1: ( rule__ImportInterfaceStatement__Group__0__Impl rule__ImportInterfaceStatement__Group__1 )
            // InternalMCL.g:2615:2: rule__ImportInterfaceStatement__Group__0__Impl rule__ImportInterfaceStatement__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__ImportInterfaceStatement__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ImportInterfaceStatement__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportInterfaceStatement__Group__0"


    // $ANTLR start "rule__ImportInterfaceStatement__Group__0__Impl"
    // InternalMCL.g:2622:1: rule__ImportInterfaceStatement__Group__0__Impl : ( 'load' ) ;
    public final void rule__ImportInterfaceStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2626:1: ( ( 'load' ) )
            // InternalMCL.g:2627:1: ( 'load' )
            {
            // InternalMCL.g:2627:1: ( 'load' )
            // InternalMCL.g:2628:2: 'load'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportInterfaceStatementAccess().getLoadKeyword_0()); 
            }
            match(input,39,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportInterfaceStatementAccess().getLoadKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportInterfaceStatement__Group__0__Impl"


    // $ANTLR start "rule__ImportInterfaceStatement__Group__1"
    // InternalMCL.g:2637:1: rule__ImportInterfaceStatement__Group__1 : rule__ImportInterfaceStatement__Group__1__Impl ;
    public final void rule__ImportInterfaceStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2641:1: ( rule__ImportInterfaceStatement__Group__1__Impl )
            // InternalMCL.g:2642:2: rule__ImportInterfaceStatement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportInterfaceStatement__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportInterfaceStatement__Group__1"


    // $ANTLR start "rule__ImportInterfaceStatement__Group__1__Impl"
    // InternalMCL.g:2648:1: rule__ImportInterfaceStatement__Group__1__Impl : ( ( rule__ImportInterfaceStatement__ImportURIAssignment_1 ) ) ;
    public final void rule__ImportInterfaceStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2652:1: ( ( ( rule__ImportInterfaceStatement__ImportURIAssignment_1 ) ) )
            // InternalMCL.g:2653:1: ( ( rule__ImportInterfaceStatement__ImportURIAssignment_1 ) )
            {
            // InternalMCL.g:2653:1: ( ( rule__ImportInterfaceStatement__ImportURIAssignment_1 ) )
            // InternalMCL.g:2654:2: ( rule__ImportInterfaceStatement__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportInterfaceStatementAccess().getImportURIAssignment_1()); 
            }
            // InternalMCL.g:2655:2: ( rule__ImportInterfaceStatement__ImportURIAssignment_1 )
            // InternalMCL.g:2655:3: rule__ImportInterfaceStatement__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ImportInterfaceStatement__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportInterfaceStatementAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportInterfaceStatement__Group__1__Impl"


    // $ANTLR start "rule__ThresholdExpression__Group__0"
    // InternalMCL.g:2664:1: rule__ThresholdExpression__Group__0 : rule__ThresholdExpression__Group__0__Impl rule__ThresholdExpression__Group__1 ;
    public final void rule__ThresholdExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2668:1: ( rule__ThresholdExpression__Group__0__Impl rule__ThresholdExpression__Group__1 )
            // InternalMCL.g:2669:2: rule__ThresholdExpression__Group__0__Impl rule__ThresholdExpression__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__ThresholdExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__0"


    // $ANTLR start "rule__ThresholdExpression__Group__0__Impl"
    // InternalMCL.g:2676:1: rule__ThresholdExpression__Group__0__Impl : ( ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 ) ) ;
    public final void rule__ThresholdExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2680:1: ( ( ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 ) ) )
            // InternalMCL.g:2681:1: ( ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 ) )
            {
            // InternalMCL.g:2681:1: ( ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 ) )
            // InternalMCL.g:2682:2: ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getTriggerSourcePortAssignment_0()); 
            }
            // InternalMCL.g:2683:2: ( rule__ThresholdExpression__TriggerSourcePortAssignment_0 )
            // InternalMCL.g:2683:3: rule__ThresholdExpression__TriggerSourcePortAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__TriggerSourcePortAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getTriggerSourcePortAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__0__Impl"


    // $ANTLR start "rule__ThresholdExpression__Group__1"
    // InternalMCL.g:2691:1: rule__ThresholdExpression__Group__1 : rule__ThresholdExpression__Group__1__Impl rule__ThresholdExpression__Group__2 ;
    public final void rule__ThresholdExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2695:1: ( rule__ThresholdExpression__Group__1__Impl rule__ThresholdExpression__Group__2 )
            // InternalMCL.g:2696:2: rule__ThresholdExpression__Group__1__Impl rule__ThresholdExpression__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__ThresholdExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__1"


    // $ANTLR start "rule__ThresholdExpression__Group__1__Impl"
    // InternalMCL.g:2703:1: rule__ThresholdExpression__Group__1__Impl : ( ( rule__ThresholdExpression__SetoperationAssignment_1 ) ) ;
    public final void rule__ThresholdExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2707:1: ( ( ( rule__ThresholdExpression__SetoperationAssignment_1 ) ) )
            // InternalMCL.g:2708:1: ( ( rule__ThresholdExpression__SetoperationAssignment_1 ) )
            {
            // InternalMCL.g:2708:1: ( ( rule__ThresholdExpression__SetoperationAssignment_1 ) )
            // InternalMCL.g:2709:2: ( rule__ThresholdExpression__SetoperationAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getSetoperationAssignment_1()); 
            }
            // InternalMCL.g:2710:2: ( rule__ThresholdExpression__SetoperationAssignment_1 )
            // InternalMCL.g:2710:3: rule__ThresholdExpression__SetoperationAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__SetoperationAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getSetoperationAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__1__Impl"


    // $ANTLR start "rule__ThresholdExpression__Group__2"
    // InternalMCL.g:2718:1: rule__ThresholdExpression__Group__2 : rule__ThresholdExpression__Group__2__Impl ;
    public final void rule__ThresholdExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2722:1: ( rule__ThresholdExpression__Group__2__Impl )
            // InternalMCL.g:2723:2: rule__ThresholdExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__2"


    // $ANTLR start "rule__ThresholdExpression__Group__2__Impl"
    // InternalMCL.g:2729:1: rule__ThresholdExpression__Group__2__Impl : ( ( rule__ThresholdExpression__ValueAssignment_2 ) ) ;
    public final void rule__ThresholdExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2733:1: ( ( ( rule__ThresholdExpression__ValueAssignment_2 ) ) )
            // InternalMCL.g:2734:1: ( ( rule__ThresholdExpression__ValueAssignment_2 ) )
            {
            // InternalMCL.g:2734:1: ( ( rule__ThresholdExpression__ValueAssignment_2 ) )
            // InternalMCL.g:2735:2: ( rule__ThresholdExpression__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getValueAssignment_2()); 
            }
            // InternalMCL.g:2736:2: ( rule__ThresholdExpression__ValueAssignment_2 )
            // InternalMCL.g:2736:3: rule__ThresholdExpression__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ThresholdExpression__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__Group__2__Impl"


    // $ANTLR start "rule__Disjoint__Group__0"
    // InternalMCL.g:2745:1: rule__Disjoint__Group__0 : rule__Disjoint__Group__0__Impl rule__Disjoint__Group__1 ;
    public final void rule__Disjoint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2749:1: ( rule__Disjoint__Group__0__Impl rule__Disjoint__Group__1 )
            // InternalMCL.g:2750:2: rule__Disjoint__Group__0__Impl rule__Disjoint__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Disjoint__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group__0"


    // $ANTLR start "rule__Disjoint__Group__0__Impl"
    // InternalMCL.g:2757:1: rule__Disjoint__Group__0__Impl : ( ruleUnion ) ;
    public final void rule__Disjoint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2761:1: ( ( ruleUnion ) )
            // InternalMCL.g:2762:1: ( ruleUnion )
            {
            // InternalMCL.g:2762:1: ( ruleUnion )
            // InternalMCL.g:2763:2: ruleUnion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getUnionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleUnion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getUnionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group__0__Impl"


    // $ANTLR start "rule__Disjoint__Group__1"
    // InternalMCL.g:2772:1: rule__Disjoint__Group__1 : rule__Disjoint__Group__1__Impl ;
    public final void rule__Disjoint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2776:1: ( rule__Disjoint__Group__1__Impl )
            // InternalMCL.g:2777:2: rule__Disjoint__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group__1"


    // $ANTLR start "rule__Disjoint__Group__1__Impl"
    // InternalMCL.g:2783:1: rule__Disjoint__Group__1__Impl : ( ( rule__Disjoint__Group_1__0 )* ) ;
    public final void rule__Disjoint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2787:1: ( ( ( rule__Disjoint__Group_1__0 )* ) )
            // InternalMCL.g:2788:1: ( ( rule__Disjoint__Group_1__0 )* )
            {
            // InternalMCL.g:2788:1: ( ( rule__Disjoint__Group_1__0 )* )
            // InternalMCL.g:2789:2: ( rule__Disjoint__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getGroup_1()); 
            }
            // InternalMCL.g:2790:2: ( rule__Disjoint__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==15) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalMCL.g:2790:3: rule__Disjoint__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__Disjoint__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group__1__Impl"


    // $ANTLR start "rule__Disjoint__Group_1__0"
    // InternalMCL.g:2799:1: rule__Disjoint__Group_1__0 : rule__Disjoint__Group_1__0__Impl rule__Disjoint__Group_1__1 ;
    public final void rule__Disjoint__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2803:1: ( rule__Disjoint__Group_1__0__Impl rule__Disjoint__Group_1__1 )
            // InternalMCL.g:2804:2: rule__Disjoint__Group_1__0__Impl rule__Disjoint__Group_1__1
            {
            pushFollow(FOLLOW_19);
            rule__Disjoint__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__0"


    // $ANTLR start "rule__Disjoint__Group_1__0__Impl"
    // InternalMCL.g:2811:1: rule__Disjoint__Group_1__0__Impl : ( () ) ;
    public final void rule__Disjoint__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2815:1: ( ( () ) )
            // InternalMCL.g:2816:1: ( () )
            {
            // InternalMCL.g:2816:1: ( () )
            // InternalMCL.g:2817:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getBinaryExpOperatorLeftOpAction_1_0()); 
            }
            // InternalMCL.g:2818:2: ()
            // InternalMCL.g:2818:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getBinaryExpOperatorLeftOpAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__0__Impl"


    // $ANTLR start "rule__Disjoint__Group_1__1"
    // InternalMCL.g:2826:1: rule__Disjoint__Group_1__1 : rule__Disjoint__Group_1__1__Impl rule__Disjoint__Group_1__2 ;
    public final void rule__Disjoint__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2830:1: ( rule__Disjoint__Group_1__1__Impl rule__Disjoint__Group_1__2 )
            // InternalMCL.g:2831:2: rule__Disjoint__Group_1__1__Impl rule__Disjoint__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Disjoint__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__1"


    // $ANTLR start "rule__Disjoint__Group_1__1__Impl"
    // InternalMCL.g:2838:1: rule__Disjoint__Group_1__1__Impl : ( 'or' ) ;
    public final void rule__Disjoint__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2842:1: ( ( 'or' ) )
            // InternalMCL.g:2843:1: ( 'or' )
            {
            // InternalMCL.g:2843:1: ( 'or' )
            // InternalMCL.g:2844:2: 'or'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getOrKeyword_1_1()); 
            }
            match(input,15,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getOrKeyword_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__1__Impl"


    // $ANTLR start "rule__Disjoint__Group_1__2"
    // InternalMCL.g:2853:1: rule__Disjoint__Group_1__2 : rule__Disjoint__Group_1__2__Impl ;
    public final void rule__Disjoint__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2857:1: ( rule__Disjoint__Group_1__2__Impl )
            // InternalMCL.g:2858:2: rule__Disjoint__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Disjoint__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__2"


    // $ANTLR start "rule__Disjoint__Group_1__2__Impl"
    // InternalMCL.g:2864:1: rule__Disjoint__Group_1__2__Impl : ( ( rule__Disjoint__RightOpAssignment_1_2 ) ) ;
    public final void rule__Disjoint__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2868:1: ( ( ( rule__Disjoint__RightOpAssignment_1_2 ) ) )
            // InternalMCL.g:2869:1: ( ( rule__Disjoint__RightOpAssignment_1_2 ) )
            {
            // InternalMCL.g:2869:1: ( ( rule__Disjoint__RightOpAssignment_1_2 ) )
            // InternalMCL.g:2870:2: ( rule__Disjoint__RightOpAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getRightOpAssignment_1_2()); 
            }
            // InternalMCL.g:2871:2: ( rule__Disjoint__RightOpAssignment_1_2 )
            // InternalMCL.g:2871:3: rule__Disjoint__RightOpAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Disjoint__RightOpAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getRightOpAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__Group_1__2__Impl"


    // $ANTLR start "rule__Union__Group__0"
    // InternalMCL.g:2880:1: rule__Union__Group__0 : rule__Union__Group__0__Impl rule__Union__Group__1 ;
    public final void rule__Union__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2884:1: ( rule__Union__Group__0__Impl rule__Union__Group__1 )
            // InternalMCL.g:2885:2: rule__Union__Group__0__Impl rule__Union__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Union__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Union__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group__0"


    // $ANTLR start "rule__Union__Group__0__Impl"
    // InternalMCL.g:2892:1: rule__Union__Group__0__Impl : ( ruleTriggeringCondition ) ;
    public final void rule__Union__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2896:1: ( ( ruleTriggeringCondition ) )
            // InternalMCL.g:2897:1: ( ruleTriggeringCondition )
            {
            // InternalMCL.g:2897:1: ( ruleTriggeringCondition )
            // InternalMCL.g:2898:2: ruleTriggeringCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getTriggeringConditionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getTriggeringConditionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group__0__Impl"


    // $ANTLR start "rule__Union__Group__1"
    // InternalMCL.g:2907:1: rule__Union__Group__1 : rule__Union__Group__1__Impl ;
    public final void rule__Union__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2911:1: ( rule__Union__Group__1__Impl )
            // InternalMCL.g:2912:2: rule__Union__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Union__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group__1"


    // $ANTLR start "rule__Union__Group__1__Impl"
    // InternalMCL.g:2918:1: rule__Union__Group__1__Impl : ( ( rule__Union__Group_1__0 )* ) ;
    public final void rule__Union__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2922:1: ( ( ( rule__Union__Group_1__0 )* ) )
            // InternalMCL.g:2923:1: ( ( rule__Union__Group_1__0 )* )
            {
            // InternalMCL.g:2923:1: ( ( rule__Union__Group_1__0 )* )
            // InternalMCL.g:2924:2: ( rule__Union__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getGroup_1()); 
            }
            // InternalMCL.g:2925:2: ( rule__Union__Group_1__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==13) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalMCL.g:2925:3: rule__Union__Group_1__0
            	    {
            	    pushFollow(FOLLOW_22);
            	    rule__Union__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group__1__Impl"


    // $ANTLR start "rule__Union__Group_1__0"
    // InternalMCL.g:2934:1: rule__Union__Group_1__0 : rule__Union__Group_1__0__Impl rule__Union__Group_1__1 ;
    public final void rule__Union__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2938:1: ( rule__Union__Group_1__0__Impl rule__Union__Group_1__1 )
            // InternalMCL.g:2939:2: rule__Union__Group_1__0__Impl rule__Union__Group_1__1
            {
            pushFollow(FOLLOW_21);
            rule__Union__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Union__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__0"


    // $ANTLR start "rule__Union__Group_1__0__Impl"
    // InternalMCL.g:2946:1: rule__Union__Group_1__0__Impl : ( () ) ;
    public final void rule__Union__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2950:1: ( ( () ) )
            // InternalMCL.g:2951:1: ( () )
            {
            // InternalMCL.g:2951:1: ( () )
            // InternalMCL.g:2952:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getBinaryExpOperatorLeftOpAction_1_0()); 
            }
            // InternalMCL.g:2953:2: ()
            // InternalMCL.g:2953:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getBinaryExpOperatorLeftOpAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__0__Impl"


    // $ANTLR start "rule__Union__Group_1__1"
    // InternalMCL.g:2961:1: rule__Union__Group_1__1 : rule__Union__Group_1__1__Impl rule__Union__Group_1__2 ;
    public final void rule__Union__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2965:1: ( rule__Union__Group_1__1__Impl rule__Union__Group_1__2 )
            // InternalMCL.g:2966:2: rule__Union__Group_1__1__Impl rule__Union__Group_1__2
            {
            pushFollow(FOLLOW_10);
            rule__Union__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Union__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__1"


    // $ANTLR start "rule__Union__Group_1__1__Impl"
    // InternalMCL.g:2973:1: rule__Union__Group_1__1__Impl : ( 'and' ) ;
    public final void rule__Union__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2977:1: ( ( 'and' ) )
            // InternalMCL.g:2978:1: ( 'and' )
            {
            // InternalMCL.g:2978:1: ( 'and' )
            // InternalMCL.g:2979:2: 'and'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getAndKeyword_1_1()); 
            }
            match(input,13,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getAndKeyword_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__1__Impl"


    // $ANTLR start "rule__Union__Group_1__2"
    // InternalMCL.g:2988:1: rule__Union__Group_1__2 : rule__Union__Group_1__2__Impl ;
    public final void rule__Union__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:2992:1: ( rule__Union__Group_1__2__Impl )
            // InternalMCL.g:2993:2: rule__Union__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Union__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__2"


    // $ANTLR start "rule__Union__Group_1__2__Impl"
    // InternalMCL.g:2999:1: rule__Union__Group_1__2__Impl : ( ( rule__Union__RightOpAssignment_1_2 ) ) ;
    public final void rule__Union__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3003:1: ( ( ( rule__Union__RightOpAssignment_1_2 ) ) )
            // InternalMCL.g:3004:1: ( ( rule__Union__RightOpAssignment_1_2 ) )
            {
            // InternalMCL.g:3004:1: ( ( rule__Union__RightOpAssignment_1_2 ) )
            // InternalMCL.g:3005:2: ( rule__Union__RightOpAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getRightOpAssignment_1_2()); 
            }
            // InternalMCL.g:3006:2: ( rule__Union__RightOpAssignment_1_2 )
            // InternalMCL.g:3006:3: rule__Union__RightOpAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Union__RightOpAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getRightOpAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__Group_1__2__Impl"


    // $ANTLR start "rule__TriggeringCondition__Group_5__0"
    // InternalMCL.g:3015:1: rule__TriggeringCondition__Group_5__0 : rule__TriggeringCondition__Group_5__0__Impl rule__TriggeringCondition__Group_5__1 ;
    public final void rule__TriggeringCondition__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3019:1: ( rule__TriggeringCondition__Group_5__0__Impl rule__TriggeringCondition__Group_5__1 )
            // InternalMCL.g:3020:2: rule__TriggeringCondition__Group_5__0__Impl rule__TriggeringCondition__Group_5__1
            {
            pushFollow(FOLLOW_10);
            rule__TriggeringCondition__Group_5__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TriggeringCondition__Group_5__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__0"


    // $ANTLR start "rule__TriggeringCondition__Group_5__0__Impl"
    // InternalMCL.g:3027:1: rule__TriggeringCondition__Group_5__0__Impl : ( '(' ) ;
    public final void rule__TriggeringCondition__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3031:1: ( ( '(' ) )
            // InternalMCL.g:3032:1: ( '(' )
            {
            // InternalMCL.g:3032:1: ( '(' )
            // InternalMCL.g:3033:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTriggeringConditionAccess().getLeftParenthesisKeyword_5_0()); 
            }
            match(input,34,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTriggeringConditionAccess().getLeftParenthesisKeyword_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__0__Impl"


    // $ANTLR start "rule__TriggeringCondition__Group_5__1"
    // InternalMCL.g:3042:1: rule__TriggeringCondition__Group_5__1 : rule__TriggeringCondition__Group_5__1__Impl rule__TriggeringCondition__Group_5__2 ;
    public final void rule__TriggeringCondition__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3046:1: ( rule__TriggeringCondition__Group_5__1__Impl rule__TriggeringCondition__Group_5__2 )
            // InternalMCL.g:3047:2: rule__TriggeringCondition__Group_5__1__Impl rule__TriggeringCondition__Group_5__2
            {
            pushFollow(FOLLOW_15);
            rule__TriggeringCondition__Group_5__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__TriggeringCondition__Group_5__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__1"


    // $ANTLR start "rule__TriggeringCondition__Group_5__1__Impl"
    // InternalMCL.g:3054:1: rule__TriggeringCondition__Group_5__1__Impl : ( ruleDisjoint ) ;
    public final void rule__TriggeringCondition__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3058:1: ( ( ruleDisjoint ) )
            // InternalMCL.g:3059:1: ( ruleDisjoint )
            {
            // InternalMCL.g:3059:1: ( ruleDisjoint )
            // InternalMCL.g:3060:2: ruleDisjoint
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTriggeringConditionAccess().getDisjointParserRuleCall_5_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleDisjoint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTriggeringConditionAccess().getDisjointParserRuleCall_5_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__1__Impl"


    // $ANTLR start "rule__TriggeringCondition__Group_5__2"
    // InternalMCL.g:3069:1: rule__TriggeringCondition__Group_5__2 : rule__TriggeringCondition__Group_5__2__Impl ;
    public final void rule__TriggeringCondition__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3073:1: ( rule__TriggeringCondition__Group_5__2__Impl )
            // InternalMCL.g:3074:2: rule__TriggeringCondition__Group_5__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TriggeringCondition__Group_5__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__2"


    // $ANTLR start "rule__TriggeringCondition__Group_5__2__Impl"
    // InternalMCL.g:3080:1: rule__TriggeringCondition__Group_5__2__Impl : ( ')' ) ;
    public final void rule__TriggeringCondition__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3084:1: ( ( ')' ) )
            // InternalMCL.g:3085:1: ( ')' )
            {
            // InternalMCL.g:3085:1: ( ')' )
            // InternalMCL.g:3086:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTriggeringConditionAccess().getRightParenthesisKeyword_5_2()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTriggeringConditionAccess().getRightParenthesisKeyword_5_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriggeringCondition__Group_5__2__Impl"


    // $ANTLR start "rule__EventTrigger__Group__0"
    // InternalMCL.g:3096:1: rule__EventTrigger__Group__0 : rule__EventTrigger__Group__0__Impl rule__EventTrigger__Group__1 ;
    public final void rule__EventTrigger__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3100:1: ( rule__EventTrigger__Group__0__Impl rule__EventTrigger__Group__1 )
            // InternalMCL.g:3101:2: rule__EventTrigger__Group__0__Impl rule__EventTrigger__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__EventTrigger__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EventTrigger__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__0"


    // $ANTLR start "rule__EventTrigger__Group__0__Impl"
    // InternalMCL.g:3108:1: rule__EventTrigger__Group__0__Impl : ( 'event' ) ;
    public final void rule__EventTrigger__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3112:1: ( ( 'event' ) )
            // InternalMCL.g:3113:1: ( 'event' )
            {
            // InternalMCL.g:3113:1: ( 'event' )
            // InternalMCL.g:3114:2: 'event'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getEventKeyword_0()); 
            }
            match(input,40,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getEventKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__0__Impl"


    // $ANTLR start "rule__EventTrigger__Group__1"
    // InternalMCL.g:3123:1: rule__EventTrigger__Group__1 : rule__EventTrigger__Group__1__Impl rule__EventTrigger__Group__2 ;
    public final void rule__EventTrigger__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3127:1: ( rule__EventTrigger__Group__1__Impl rule__EventTrigger__Group__2 )
            // InternalMCL.g:3128:2: rule__EventTrigger__Group__1__Impl rule__EventTrigger__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__EventTrigger__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EventTrigger__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__1"


    // $ANTLR start "rule__EventTrigger__Group__1__Impl"
    // InternalMCL.g:3135:1: rule__EventTrigger__Group__1__Impl : ( 'on' ) ;
    public final void rule__EventTrigger__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3139:1: ( ( 'on' ) )
            // InternalMCL.g:3140:1: ( 'on' )
            {
            // InternalMCL.g:3140:1: ( 'on' )
            // InternalMCL.g:3141:2: 'on'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getOnKeyword_1()); 
            }
            match(input,41,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getOnKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__1__Impl"


    // $ANTLR start "rule__EventTrigger__Group__2"
    // InternalMCL.g:3150:1: rule__EventTrigger__Group__2 : rule__EventTrigger__Group__2__Impl rule__EventTrigger__Group__3 ;
    public final void rule__EventTrigger__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3154:1: ( rule__EventTrigger__Group__2__Impl rule__EventTrigger__Group__3 )
            // InternalMCL.g:3155:2: rule__EventTrigger__Group__2__Impl rule__EventTrigger__Group__3
            {
            pushFollow(FOLLOW_24);
            rule__EventTrigger__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__EventTrigger__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__2"


    // $ANTLR start "rule__EventTrigger__Group__2__Impl"
    // InternalMCL.g:3162:1: rule__EventTrigger__Group__2__Impl : ( ( rule__EventTrigger__PortAssignment_2 ) ) ;
    public final void rule__EventTrigger__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3166:1: ( ( ( rule__EventTrigger__PortAssignment_2 ) ) )
            // InternalMCL.g:3167:1: ( ( rule__EventTrigger__PortAssignment_2 ) )
            {
            // InternalMCL.g:3167:1: ( ( rule__EventTrigger__PortAssignment_2 ) )
            // InternalMCL.g:3168:2: ( rule__EventTrigger__PortAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getPortAssignment_2()); 
            }
            // InternalMCL.g:3169:2: ( rule__EventTrigger__PortAssignment_2 )
            // InternalMCL.g:3169:3: rule__EventTrigger__PortAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__EventTrigger__PortAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getPortAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__2__Impl"


    // $ANTLR start "rule__EventTrigger__Group__3"
    // InternalMCL.g:3177:1: rule__EventTrigger__Group__3 : rule__EventTrigger__Group__3__Impl ;
    public final void rule__EventTrigger__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3181:1: ( rule__EventTrigger__Group__3__Impl )
            // InternalMCL.g:3182:2: rule__EventTrigger__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EventTrigger__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__3"


    // $ANTLR start "rule__EventTrigger__Group__3__Impl"
    // InternalMCL.g:3188:1: rule__EventTrigger__Group__3__Impl : ( 'occurs' ) ;
    public final void rule__EventTrigger__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3192:1: ( ( 'occurs' ) )
            // InternalMCL.g:3193:1: ( 'occurs' )
            {
            // InternalMCL.g:3193:1: ( 'occurs' )
            // InternalMCL.g:3194:2: 'occurs'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getOccursKeyword_3()); 
            }
            match(input,42,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getOccursKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__Group__3__Impl"


    // $ANTLR start "rule__PeriodicExpression__Group__0"
    // InternalMCL.g:3204:1: rule__PeriodicExpression__Group__0 : rule__PeriodicExpression__Group__0__Impl rule__PeriodicExpression__Group__1 ;
    public final void rule__PeriodicExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3208:1: ( rule__PeriodicExpression__Group__0__Impl rule__PeriodicExpression__Group__1 )
            // InternalMCL.g:3209:2: rule__PeriodicExpression__Group__0__Impl rule__PeriodicExpression__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__PeriodicExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__0"


    // $ANTLR start "rule__PeriodicExpression__Group__0__Impl"
    // InternalMCL.g:3216:1: rule__PeriodicExpression__Group__0__Impl : ( 'every' ) ;
    public final void rule__PeriodicExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3220:1: ( ( 'every' ) )
            // InternalMCL.g:3221:1: ( 'every' )
            {
            // InternalMCL.g:3221:1: ( 'every' )
            // InternalMCL.g:3222:2: 'every'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getEveryKeyword_0()); 
            }
            match(input,43,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getEveryKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__0__Impl"


    // $ANTLR start "rule__PeriodicExpression__Group__1"
    // InternalMCL.g:3231:1: rule__PeriodicExpression__Group__1 : rule__PeriodicExpression__Group__1__Impl rule__PeriodicExpression__Group__2 ;
    public final void rule__PeriodicExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3235:1: ( rule__PeriodicExpression__Group__1__Impl rule__PeriodicExpression__Group__2 )
            // InternalMCL.g:3236:2: rule__PeriodicExpression__Group__1__Impl rule__PeriodicExpression__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__PeriodicExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__1"


    // $ANTLR start "rule__PeriodicExpression__Group__1__Impl"
    // InternalMCL.g:3243:1: rule__PeriodicExpression__Group__1__Impl : ( ( rule__PeriodicExpression__ValueAssignment_1 ) ) ;
    public final void rule__PeriodicExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3247:1: ( ( ( rule__PeriodicExpression__ValueAssignment_1 ) ) )
            // InternalMCL.g:3248:1: ( ( rule__PeriodicExpression__ValueAssignment_1 ) )
            {
            // InternalMCL.g:3248:1: ( ( rule__PeriodicExpression__ValueAssignment_1 ) )
            // InternalMCL.g:3249:2: ( rule__PeriodicExpression__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:3250:2: ( rule__PeriodicExpression__ValueAssignment_1 )
            // InternalMCL.g:3250:3: rule__PeriodicExpression__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__1__Impl"


    // $ANTLR start "rule__PeriodicExpression__Group__2"
    // InternalMCL.g:3258:1: rule__PeriodicExpression__Group__2 : rule__PeriodicExpression__Group__2__Impl ;
    public final void rule__PeriodicExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3262:1: ( rule__PeriodicExpression__Group__2__Impl )
            // InternalMCL.g:3263:2: rule__PeriodicExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__2"


    // $ANTLR start "rule__PeriodicExpression__Group__2__Impl"
    // InternalMCL.g:3269:1: rule__PeriodicExpression__Group__2__Impl : ( ( rule__PeriodicExpression__TemporalReferenceAssignment_2 ) ) ;
    public final void rule__PeriodicExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3273:1: ( ( ( rule__PeriodicExpression__TemporalReferenceAssignment_2 ) ) )
            // InternalMCL.g:3274:1: ( ( rule__PeriodicExpression__TemporalReferenceAssignment_2 ) )
            {
            // InternalMCL.g:3274:1: ( ( rule__PeriodicExpression__TemporalReferenceAssignment_2 ) )
            // InternalMCL.g:3275:2: ( rule__PeriodicExpression__TemporalReferenceAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceAssignment_2()); 
            }
            // InternalMCL.g:3276:2: ( rule__PeriodicExpression__TemporalReferenceAssignment_2 )
            // InternalMCL.g:3276:3: rule__PeriodicExpression__TemporalReferenceAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__PeriodicExpression__TemporalReferenceAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__Group__2__Impl"


    // $ANTLR start "rule__Variable__Group__0"
    // InternalMCL.g:3285:1: rule__Variable__Group__0 : rule__Variable__Group__0__Impl rule__Variable__Group__1 ;
    public final void rule__Variable__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3289:1: ( rule__Variable__Group__0__Impl rule__Variable__Group__1 )
            // InternalMCL.g:3290:2: rule__Variable__Group__0__Impl rule__Variable__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Variable__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Variable__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0"


    // $ANTLR start "rule__Variable__Group__0__Impl"
    // InternalMCL.g:3297:1: rule__Variable__Group__0__Impl : ( () ) ;
    public final void rule__Variable__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3301:1: ( ( () ) )
            // InternalMCL.g:3302:1: ( () )
            {
            // InternalMCL.g:3302:1: ( () )
            // InternalMCL.g:3303:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getVariableAction_0()); 
            }
            // InternalMCL.g:3304:2: ()
            // InternalMCL.g:3304:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getVariableAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0__Impl"


    // $ANTLR start "rule__Variable__Group__1"
    // InternalMCL.g:3312:1: rule__Variable__Group__1 : rule__Variable__Group__1__Impl ;
    public final void rule__Variable__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3316:1: ( rule__Variable__Group__1__Impl )
            // InternalMCL.g:3317:2: rule__Variable__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1"


    // $ANTLR start "rule__Variable__Group__1__Impl"
    // InternalMCL.g:3323:1: rule__Variable__Group__1__Impl : ( ( rule__Variable__ValueAssignment_1 ) ) ;
    public final void rule__Variable__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3327:1: ( ( ( rule__Variable__ValueAssignment_1 ) ) )
            // InternalMCL.g:3328:1: ( ( rule__Variable__ValueAssignment_1 ) )
            {
            // InternalMCL.g:3328:1: ( ( rule__Variable__ValueAssignment_1 ) )
            // InternalMCL.g:3329:2: ( rule__Variable__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:3330:2: ( rule__Variable__ValueAssignment_1 )
            // InternalMCL.g:3330:3: rule__Variable__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1__Impl"


    // $ANTLR start "rule__Updated__Group__0"
    // InternalMCL.g:3339:1: rule__Updated__Group__0 : rule__Updated__Group__0__Impl rule__Updated__Group__1 ;
    public final void rule__Updated__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3343:1: ( rule__Updated__Group__0__Impl rule__Updated__Group__1 )
            // InternalMCL.g:3344:2: rule__Updated__Group__0__Impl rule__Updated__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__Updated__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Updated__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__0"


    // $ANTLR start "rule__Updated__Group__0__Impl"
    // InternalMCL.g:3351:1: rule__Updated__Group__0__Impl : ( 'value' ) ;
    public final void rule__Updated__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3355:1: ( ( 'value' ) )
            // InternalMCL.g:3356:1: ( 'value' )
            {
            // InternalMCL.g:3356:1: ( 'value' )
            // InternalMCL.g:3357:2: 'value'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getValueKeyword_0()); 
            }
            match(input,44,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getValueKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__0__Impl"


    // $ANTLR start "rule__Updated__Group__1"
    // InternalMCL.g:3366:1: rule__Updated__Group__1 : rule__Updated__Group__1__Impl rule__Updated__Group__2 ;
    public final void rule__Updated__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3370:1: ( rule__Updated__Group__1__Impl rule__Updated__Group__2 )
            // InternalMCL.g:3371:2: rule__Updated__Group__1__Impl rule__Updated__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Updated__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Updated__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__1"


    // $ANTLR start "rule__Updated__Group__1__Impl"
    // InternalMCL.g:3378:1: rule__Updated__Group__1__Impl : ( 'on' ) ;
    public final void rule__Updated__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3382:1: ( ( 'on' ) )
            // InternalMCL.g:3383:1: ( 'on' )
            {
            // InternalMCL.g:3383:1: ( 'on' )
            // InternalMCL.g:3384:2: 'on'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getOnKeyword_1()); 
            }
            match(input,41,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getOnKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__1__Impl"


    // $ANTLR start "rule__Updated__Group__2"
    // InternalMCL.g:3393:1: rule__Updated__Group__2 : rule__Updated__Group__2__Impl rule__Updated__Group__3 ;
    public final void rule__Updated__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3397:1: ( rule__Updated__Group__2__Impl rule__Updated__Group__3 )
            // InternalMCL.g:3398:2: rule__Updated__Group__2__Impl rule__Updated__Group__3
            {
            pushFollow(FOLLOW_26);
            rule__Updated__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Updated__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__2"


    // $ANTLR start "rule__Updated__Group__2__Impl"
    // InternalMCL.g:3405:1: rule__Updated__Group__2__Impl : ( ( rule__Updated__PortAssignment_2 ) ) ;
    public final void rule__Updated__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3409:1: ( ( ( rule__Updated__PortAssignment_2 ) ) )
            // InternalMCL.g:3410:1: ( ( rule__Updated__PortAssignment_2 ) )
            {
            // InternalMCL.g:3410:1: ( ( rule__Updated__PortAssignment_2 ) )
            // InternalMCL.g:3411:2: ( rule__Updated__PortAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getPortAssignment_2()); 
            }
            // InternalMCL.g:3412:2: ( rule__Updated__PortAssignment_2 )
            // InternalMCL.g:3412:3: rule__Updated__PortAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Updated__PortAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getPortAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__2__Impl"


    // $ANTLR start "rule__Updated__Group__3"
    // InternalMCL.g:3420:1: rule__Updated__Group__3 : rule__Updated__Group__3__Impl rule__Updated__Group__4 ;
    public final void rule__Updated__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3424:1: ( rule__Updated__Group__3__Impl rule__Updated__Group__4 )
            // InternalMCL.g:3425:2: rule__Updated__Group__3__Impl rule__Updated__Group__4
            {
            pushFollow(FOLLOW_27);
            rule__Updated__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Updated__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__3"


    // $ANTLR start "rule__Updated__Group__3__Impl"
    // InternalMCL.g:3432:1: rule__Updated__Group__3__Impl : ( 'has' ) ;
    public final void rule__Updated__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3436:1: ( ( 'has' ) )
            // InternalMCL.g:3437:1: ( 'has' )
            {
            // InternalMCL.g:3437:1: ( 'has' )
            // InternalMCL.g:3438:2: 'has'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getHasKeyword_3()); 
            }
            match(input,45,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getHasKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__3__Impl"


    // $ANTLR start "rule__Updated__Group__4"
    // InternalMCL.g:3447:1: rule__Updated__Group__4 : rule__Updated__Group__4__Impl rule__Updated__Group__5 ;
    public final void rule__Updated__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3451:1: ( rule__Updated__Group__4__Impl rule__Updated__Group__5 )
            // InternalMCL.g:3452:2: rule__Updated__Group__4__Impl rule__Updated__Group__5
            {
            pushFollow(FOLLOW_28);
            rule__Updated__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Updated__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__4"


    // $ANTLR start "rule__Updated__Group__4__Impl"
    // InternalMCL.g:3459:1: rule__Updated__Group__4__Impl : ( 'been' ) ;
    public final void rule__Updated__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3463:1: ( ( 'been' ) )
            // InternalMCL.g:3464:1: ( 'been' )
            {
            // InternalMCL.g:3464:1: ( 'been' )
            // InternalMCL.g:3465:2: 'been'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getBeenKeyword_4()); 
            }
            match(input,46,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getBeenKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__4__Impl"


    // $ANTLR start "rule__Updated__Group__5"
    // InternalMCL.g:3474:1: rule__Updated__Group__5 : rule__Updated__Group__5__Impl ;
    public final void rule__Updated__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3478:1: ( rule__Updated__Group__5__Impl )
            // InternalMCL.g:3479:2: rule__Updated__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Updated__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__5"


    // $ANTLR start "rule__Updated__Group__5__Impl"
    // InternalMCL.g:3485:1: rule__Updated__Group__5__Impl : ( 'Updated' ) ;
    public final void rule__Updated__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3489:1: ( ( 'Updated' ) )
            // InternalMCL.g:3490:1: ( 'Updated' )
            {
            // InternalMCL.g:3490:1: ( 'Updated' )
            // InternalMCL.g:3491:2: 'Updated'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getUpdatedKeyword_5()); 
            }
            match(input,47,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getUpdatedKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__Group__5__Impl"


    // $ANTLR start "rule__ReadyToRead__Group__0"
    // InternalMCL.g:3501:1: rule__ReadyToRead__Group__0 : rule__ReadyToRead__Group__0__Impl rule__ReadyToRead__Group__1 ;
    public final void rule__ReadyToRead__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3505:1: ( rule__ReadyToRead__Group__0__Impl rule__ReadyToRead__Group__1 )
            // InternalMCL.g:3506:2: rule__ReadyToRead__Group__0__Impl rule__ReadyToRead__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__ReadyToRead__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__0"


    // $ANTLR start "rule__ReadyToRead__Group__0__Impl"
    // InternalMCL.g:3513:1: rule__ReadyToRead__Group__0__Impl : ( 'value' ) ;
    public final void rule__ReadyToRead__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3517:1: ( ( 'value' ) )
            // InternalMCL.g:3518:1: ( 'value' )
            {
            // InternalMCL.g:3518:1: ( 'value' )
            // InternalMCL.g:3519:2: 'value'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getValueKeyword_0()); 
            }
            match(input,44,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getValueKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__0__Impl"


    // $ANTLR start "rule__ReadyToRead__Group__1"
    // InternalMCL.g:3528:1: rule__ReadyToRead__Group__1 : rule__ReadyToRead__Group__1__Impl rule__ReadyToRead__Group__2 ;
    public final void rule__ReadyToRead__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3532:1: ( rule__ReadyToRead__Group__1__Impl rule__ReadyToRead__Group__2 )
            // InternalMCL.g:3533:2: rule__ReadyToRead__Group__1__Impl rule__ReadyToRead__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__ReadyToRead__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__1"


    // $ANTLR start "rule__ReadyToRead__Group__1__Impl"
    // InternalMCL.g:3540:1: rule__ReadyToRead__Group__1__Impl : ( 'on' ) ;
    public final void rule__ReadyToRead__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3544:1: ( ( 'on' ) )
            // InternalMCL.g:3545:1: ( 'on' )
            {
            // InternalMCL.g:3545:1: ( 'on' )
            // InternalMCL.g:3546:2: 'on'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getOnKeyword_1()); 
            }
            match(input,41,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getOnKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__1__Impl"


    // $ANTLR start "rule__ReadyToRead__Group__2"
    // InternalMCL.g:3555:1: rule__ReadyToRead__Group__2 : rule__ReadyToRead__Group__2__Impl rule__ReadyToRead__Group__3 ;
    public final void rule__ReadyToRead__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3559:1: ( rule__ReadyToRead__Group__2__Impl rule__ReadyToRead__Group__3 )
            // InternalMCL.g:3560:2: rule__ReadyToRead__Group__2__Impl rule__ReadyToRead__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__ReadyToRead__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__2"


    // $ANTLR start "rule__ReadyToRead__Group__2__Impl"
    // InternalMCL.g:3567:1: rule__ReadyToRead__Group__2__Impl : ( ( rule__ReadyToRead__PortAssignment_2 ) ) ;
    public final void rule__ReadyToRead__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3571:1: ( ( ( rule__ReadyToRead__PortAssignment_2 ) ) )
            // InternalMCL.g:3572:1: ( ( rule__ReadyToRead__PortAssignment_2 ) )
            {
            // InternalMCL.g:3572:1: ( ( rule__ReadyToRead__PortAssignment_2 ) )
            // InternalMCL.g:3573:2: ( rule__ReadyToRead__PortAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getPortAssignment_2()); 
            }
            // InternalMCL.g:3574:2: ( rule__ReadyToRead__PortAssignment_2 )
            // InternalMCL.g:3574:3: rule__ReadyToRead__PortAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__PortAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getPortAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__2__Impl"


    // $ANTLR start "rule__ReadyToRead__Group__3"
    // InternalMCL.g:3582:1: rule__ReadyToRead__Group__3 : rule__ReadyToRead__Group__3__Impl rule__ReadyToRead__Group__4 ;
    public final void rule__ReadyToRead__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3586:1: ( rule__ReadyToRead__Group__3__Impl rule__ReadyToRead__Group__4 )
            // InternalMCL.g:3587:2: rule__ReadyToRead__Group__3__Impl rule__ReadyToRead__Group__4
            {
            pushFollow(FOLLOW_30);
            rule__ReadyToRead__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__3"


    // $ANTLR start "rule__ReadyToRead__Group__3__Impl"
    // InternalMCL.g:3594:1: rule__ReadyToRead__Group__3__Impl : ( 'is' ) ;
    public final void rule__ReadyToRead__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3598:1: ( ( 'is' ) )
            // InternalMCL.g:3599:1: ( 'is' )
            {
            // InternalMCL.g:3599:1: ( 'is' )
            // InternalMCL.g:3600:2: 'is'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getIsKeyword_3()); 
            }
            match(input,48,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getIsKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__3__Impl"


    // $ANTLR start "rule__ReadyToRead__Group__4"
    // InternalMCL.g:3609:1: rule__ReadyToRead__Group__4 : rule__ReadyToRead__Group__4__Impl ;
    public final void rule__ReadyToRead__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3613:1: ( rule__ReadyToRead__Group__4__Impl )
            // InternalMCL.g:3614:2: rule__ReadyToRead__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadyToRead__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__4"


    // $ANTLR start "rule__ReadyToRead__Group__4__Impl"
    // InternalMCL.g:3620:1: rule__ReadyToRead__Group__4__Impl : ( 'ReadyToRead' ) ;
    public final void rule__ReadyToRead__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3624:1: ( ( 'ReadyToRead' ) )
            // InternalMCL.g:3625:1: ( 'ReadyToRead' )
            {
            // InternalMCL.g:3625:1: ( 'ReadyToRead' )
            // InternalMCL.g:3626:2: 'ReadyToRead'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getReadyToReadKeyword_4()); 
            }
            match(input,49,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getReadyToReadKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__Group__4__Impl"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__0"
    // InternalMCL.g:3636:1: rule__ZeroCrossingDetection__Group__0 : rule__ZeroCrossingDetection__Group__0__Impl rule__ZeroCrossingDetection__Group__1 ;
    public final void rule__ZeroCrossingDetection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3640:1: ( rule__ZeroCrossingDetection__Group__0__Impl rule__ZeroCrossingDetection__Group__1 )
            // InternalMCL.g:3641:2: rule__ZeroCrossingDetection__Group__0__Impl rule__ZeroCrossingDetection__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__ZeroCrossingDetection__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ZeroCrossingDetection__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__0"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__0__Impl"
    // InternalMCL.g:3648:1: rule__ZeroCrossingDetection__Group__0__Impl : ( () ) ;
    public final void rule__ZeroCrossingDetection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3652:1: ( ( () ) )
            // InternalMCL.g:3653:1: ( () )
            {
            // InternalMCL.g:3653:1: ( () )
            // InternalMCL.g:3654:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionAction_0()); 
            }
            // InternalMCL.g:3655:2: ()
            // InternalMCL.g:3655:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__0__Impl"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__1"
    // InternalMCL.g:3663:1: rule__ZeroCrossingDetection__Group__1 : rule__ZeroCrossingDetection__Group__1__Impl rule__ZeroCrossingDetection__Group__2 ;
    public final void rule__ZeroCrossingDetection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3667:1: ( rule__ZeroCrossingDetection__Group__1__Impl rule__ZeroCrossingDetection__Group__2 )
            // InternalMCL.g:3668:2: rule__ZeroCrossingDetection__Group__1__Impl rule__ZeroCrossingDetection__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__ZeroCrossingDetection__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__ZeroCrossingDetection__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__1"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__1__Impl"
    // InternalMCL.g:3675:1: rule__ZeroCrossingDetection__Group__1__Impl : ( ( rule__ZeroCrossingDetection__StateAssignment_1 )? ) ;
    public final void rule__ZeroCrossingDetection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3679:1: ( ( ( rule__ZeroCrossingDetection__StateAssignment_1 )? ) )
            // InternalMCL.g:3680:1: ( ( rule__ZeroCrossingDetection__StateAssignment_1 )? )
            {
            // InternalMCL.g:3680:1: ( ( rule__ZeroCrossingDetection__StateAssignment_1 )? )
            // InternalMCL.g:3681:2: ( rule__ZeroCrossingDetection__StateAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getStateAssignment_1()); 
            }
            // InternalMCL.g:3682:2: ( rule__ZeroCrossingDetection__StateAssignment_1 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==62) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalMCL.g:3682:3: rule__ZeroCrossingDetection__StateAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ZeroCrossingDetection__StateAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getStateAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__1__Impl"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__2"
    // InternalMCL.g:3690:1: rule__ZeroCrossingDetection__Group__2 : rule__ZeroCrossingDetection__Group__2__Impl ;
    public final void rule__ZeroCrossingDetection__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3694:1: ( rule__ZeroCrossingDetection__Group__2__Impl )
            // InternalMCL.g:3695:2: rule__ZeroCrossingDetection__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ZeroCrossingDetection__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__2"


    // $ANTLR start "rule__ZeroCrossingDetection__Group__2__Impl"
    // InternalMCL.g:3701:1: rule__ZeroCrossingDetection__Group__2__Impl : ( 'ZeroCrossingDetection' ) ;
    public final void rule__ZeroCrossingDetection__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3705:1: ( ( 'ZeroCrossingDetection' ) )
            // InternalMCL.g:3706:1: ( 'ZeroCrossingDetection' )
            {
            // InternalMCL.g:3706:1: ( 'ZeroCrossingDetection' )
            // InternalMCL.g:3707:2: 'ZeroCrossingDetection'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionKeyword_2()); 
            }
            match(input,50,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__Group__2__Impl"


    // $ANTLR start "rule__Extrapolation__Group__0"
    // InternalMCL.g:3717:1: rule__Extrapolation__Group__0 : rule__Extrapolation__Group__0__Impl rule__Extrapolation__Group__1 ;
    public final void rule__Extrapolation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3721:1: ( rule__Extrapolation__Group__0__Impl rule__Extrapolation__Group__1 )
            // InternalMCL.g:3722:2: rule__Extrapolation__Group__0__Impl rule__Extrapolation__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__Extrapolation__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Extrapolation__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__0"


    // $ANTLR start "rule__Extrapolation__Group__0__Impl"
    // InternalMCL.g:3729:1: rule__Extrapolation__Group__0__Impl : ( () ) ;
    public final void rule__Extrapolation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3733:1: ( ( () ) )
            // InternalMCL.g:3734:1: ( () )
            {
            // InternalMCL.g:3734:1: ( () )
            // InternalMCL.g:3735:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getExtrapolationAction_0()); 
            }
            // InternalMCL.g:3736:2: ()
            // InternalMCL.g:3736:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getExtrapolationAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__0__Impl"


    // $ANTLR start "rule__Extrapolation__Group__1"
    // InternalMCL.g:3744:1: rule__Extrapolation__Group__1 : rule__Extrapolation__Group__1__Impl rule__Extrapolation__Group__2 ;
    public final void rule__Extrapolation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3748:1: ( rule__Extrapolation__Group__1__Impl rule__Extrapolation__Group__2 )
            // InternalMCL.g:3749:2: rule__Extrapolation__Group__1__Impl rule__Extrapolation__Group__2
            {
            pushFollow(FOLLOW_32);
            rule__Extrapolation__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Extrapolation__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__1"


    // $ANTLR start "rule__Extrapolation__Group__1__Impl"
    // InternalMCL.g:3756:1: rule__Extrapolation__Group__1__Impl : ( ( rule__Extrapolation__StateAssignment_1 )? ) ;
    public final void rule__Extrapolation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3760:1: ( ( ( rule__Extrapolation__StateAssignment_1 )? ) )
            // InternalMCL.g:3761:1: ( ( rule__Extrapolation__StateAssignment_1 )? )
            {
            // InternalMCL.g:3761:1: ( ( rule__Extrapolation__StateAssignment_1 )? )
            // InternalMCL.g:3762:2: ( rule__Extrapolation__StateAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getStateAssignment_1()); 
            }
            // InternalMCL.g:3763:2: ( rule__Extrapolation__StateAssignment_1 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==62) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalMCL.g:3763:3: rule__Extrapolation__StateAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Extrapolation__StateAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getStateAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__1__Impl"


    // $ANTLR start "rule__Extrapolation__Group__2"
    // InternalMCL.g:3771:1: rule__Extrapolation__Group__2 : rule__Extrapolation__Group__2__Impl ;
    public final void rule__Extrapolation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3775:1: ( rule__Extrapolation__Group__2__Impl )
            // InternalMCL.g:3776:2: rule__Extrapolation__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Extrapolation__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__2"


    // $ANTLR start "rule__Extrapolation__Group__2__Impl"
    // InternalMCL.g:3782:1: rule__Extrapolation__Group__2__Impl : ( 'Extrapolation' ) ;
    public final void rule__Extrapolation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3786:1: ( ( 'Extrapolation' ) )
            // InternalMCL.g:3787:1: ( 'Extrapolation' )
            {
            // InternalMCL.g:3787:1: ( 'Extrapolation' )
            // InternalMCL.g:3788:2: 'Extrapolation'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getExtrapolationKeyword_2()); 
            }
            match(input,51,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getExtrapolationKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__Group__2__Impl"


    // $ANTLR start "rule__Interpolation__Group__0"
    // InternalMCL.g:3798:1: rule__Interpolation__Group__0 : rule__Interpolation__Group__0__Impl rule__Interpolation__Group__1 ;
    public final void rule__Interpolation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3802:1: ( rule__Interpolation__Group__0__Impl rule__Interpolation__Group__1 )
            // InternalMCL.g:3803:2: rule__Interpolation__Group__0__Impl rule__Interpolation__Group__1
            {
            pushFollow(FOLLOW_33);
            rule__Interpolation__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Interpolation__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__0"


    // $ANTLR start "rule__Interpolation__Group__0__Impl"
    // InternalMCL.g:3810:1: rule__Interpolation__Group__0__Impl : ( () ) ;
    public final void rule__Interpolation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3814:1: ( ( () ) )
            // InternalMCL.g:3815:1: ( () )
            {
            // InternalMCL.g:3815:1: ( () )
            // InternalMCL.g:3816:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getInterpolationAction_0()); 
            }
            // InternalMCL.g:3817:2: ()
            // InternalMCL.g:3817:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getInterpolationAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__0__Impl"


    // $ANTLR start "rule__Interpolation__Group__1"
    // InternalMCL.g:3825:1: rule__Interpolation__Group__1 : rule__Interpolation__Group__1__Impl rule__Interpolation__Group__2 ;
    public final void rule__Interpolation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3829:1: ( rule__Interpolation__Group__1__Impl rule__Interpolation__Group__2 )
            // InternalMCL.g:3830:2: rule__Interpolation__Group__1__Impl rule__Interpolation__Group__2
            {
            pushFollow(FOLLOW_33);
            rule__Interpolation__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Interpolation__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__1"


    // $ANTLR start "rule__Interpolation__Group__1__Impl"
    // InternalMCL.g:3837:1: rule__Interpolation__Group__1__Impl : ( ( rule__Interpolation__StateAssignment_1 )? ) ;
    public final void rule__Interpolation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3841:1: ( ( ( rule__Interpolation__StateAssignment_1 )? ) )
            // InternalMCL.g:3842:1: ( ( rule__Interpolation__StateAssignment_1 )? )
            {
            // InternalMCL.g:3842:1: ( ( rule__Interpolation__StateAssignment_1 )? )
            // InternalMCL.g:3843:2: ( rule__Interpolation__StateAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getStateAssignment_1()); 
            }
            // InternalMCL.g:3844:2: ( rule__Interpolation__StateAssignment_1 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==62) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalMCL.g:3844:3: rule__Interpolation__StateAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Interpolation__StateAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getStateAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__1__Impl"


    // $ANTLR start "rule__Interpolation__Group__2"
    // InternalMCL.g:3852:1: rule__Interpolation__Group__2 : rule__Interpolation__Group__2__Impl ;
    public final void rule__Interpolation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3856:1: ( rule__Interpolation__Group__2__Impl )
            // InternalMCL.g:3857:2: rule__Interpolation__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interpolation__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__2"


    // $ANTLR start "rule__Interpolation__Group__2__Impl"
    // InternalMCL.g:3863:1: rule__Interpolation__Group__2__Impl : ( 'Interpolation' ) ;
    public final void rule__Interpolation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3867:1: ( ( 'Interpolation' ) )
            // InternalMCL.g:3868:1: ( 'Interpolation' )
            {
            // InternalMCL.g:3868:1: ( 'Interpolation' )
            // InternalMCL.g:3869:2: 'Interpolation'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getInterpolationKeyword_2()); 
            }
            match(input,52,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getInterpolationKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__Group__2__Impl"


    // $ANTLR start "rule__DiscontinuityLocator__Group__0"
    // InternalMCL.g:3879:1: rule__DiscontinuityLocator__Group__0 : rule__DiscontinuityLocator__Group__0__Impl rule__DiscontinuityLocator__Group__1 ;
    public final void rule__DiscontinuityLocator__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3883:1: ( rule__DiscontinuityLocator__Group__0__Impl rule__DiscontinuityLocator__Group__1 )
            // InternalMCL.g:3884:2: rule__DiscontinuityLocator__Group__0__Impl rule__DiscontinuityLocator__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__DiscontinuityLocator__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DiscontinuityLocator__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__0"


    // $ANTLR start "rule__DiscontinuityLocator__Group__0__Impl"
    // InternalMCL.g:3891:1: rule__DiscontinuityLocator__Group__0__Impl : ( () ) ;
    public final void rule__DiscontinuityLocator__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3895:1: ( ( () ) )
            // InternalMCL.g:3896:1: ( () )
            {
            // InternalMCL.g:3896:1: ( () )
            // InternalMCL.g:3897:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorAction_0()); 
            }
            // InternalMCL.g:3898:2: ()
            // InternalMCL.g:3898:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__0__Impl"


    // $ANTLR start "rule__DiscontinuityLocator__Group__1"
    // InternalMCL.g:3906:1: rule__DiscontinuityLocator__Group__1 : rule__DiscontinuityLocator__Group__1__Impl rule__DiscontinuityLocator__Group__2 ;
    public final void rule__DiscontinuityLocator__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3910:1: ( rule__DiscontinuityLocator__Group__1__Impl rule__DiscontinuityLocator__Group__2 )
            // InternalMCL.g:3911:2: rule__DiscontinuityLocator__Group__1__Impl rule__DiscontinuityLocator__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__DiscontinuityLocator__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__DiscontinuityLocator__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__1"


    // $ANTLR start "rule__DiscontinuityLocator__Group__1__Impl"
    // InternalMCL.g:3918:1: rule__DiscontinuityLocator__Group__1__Impl : ( ( rule__DiscontinuityLocator__StateAssignment_1 )? ) ;
    public final void rule__DiscontinuityLocator__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3922:1: ( ( ( rule__DiscontinuityLocator__StateAssignment_1 )? ) )
            // InternalMCL.g:3923:1: ( ( rule__DiscontinuityLocator__StateAssignment_1 )? )
            {
            // InternalMCL.g:3923:1: ( ( rule__DiscontinuityLocator__StateAssignment_1 )? )
            // InternalMCL.g:3924:2: ( rule__DiscontinuityLocator__StateAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getStateAssignment_1()); 
            }
            // InternalMCL.g:3925:2: ( rule__DiscontinuityLocator__StateAssignment_1 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==62) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalMCL.g:3925:3: rule__DiscontinuityLocator__StateAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__DiscontinuityLocator__StateAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getStateAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__1__Impl"


    // $ANTLR start "rule__DiscontinuityLocator__Group__2"
    // InternalMCL.g:3933:1: rule__DiscontinuityLocator__Group__2 : rule__DiscontinuityLocator__Group__2__Impl ;
    public final void rule__DiscontinuityLocator__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3937:1: ( rule__DiscontinuityLocator__Group__2__Impl )
            // InternalMCL.g:3938:2: rule__DiscontinuityLocator__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DiscontinuityLocator__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__2"


    // $ANTLR start "rule__DiscontinuityLocator__Group__2__Impl"
    // InternalMCL.g:3944:1: rule__DiscontinuityLocator__Group__2__Impl : ( 'DiscontinuityLocator' ) ;
    public final void rule__DiscontinuityLocator__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3948:1: ( ( 'DiscontinuityLocator' ) )
            // InternalMCL.g:3949:1: ( 'DiscontinuityLocator' )
            {
            // InternalMCL.g:3949:1: ( 'DiscontinuityLocator' )
            // InternalMCL.g:3950:2: 'DiscontinuityLocator'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorKeyword_2()); 
            }
            match(input,53,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__Group__2__Impl"


    // $ANTLR start "rule__HistoryProvider__Group__0"
    // InternalMCL.g:3960:1: rule__HistoryProvider__Group__0 : rule__HistoryProvider__Group__0__Impl rule__HistoryProvider__Group__1 ;
    public final void rule__HistoryProvider__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3964:1: ( rule__HistoryProvider__Group__0__Impl rule__HistoryProvider__Group__1 )
            // InternalMCL.g:3965:2: rule__HistoryProvider__Group__0__Impl rule__HistoryProvider__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__HistoryProvider__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__HistoryProvider__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__0"


    // $ANTLR start "rule__HistoryProvider__Group__0__Impl"
    // InternalMCL.g:3972:1: rule__HistoryProvider__Group__0__Impl : ( () ) ;
    public final void rule__HistoryProvider__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3976:1: ( ( () ) )
            // InternalMCL.g:3977:1: ( () )
            {
            // InternalMCL.g:3977:1: ( () )
            // InternalMCL.g:3978:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getHistoryProviderAction_0()); 
            }
            // InternalMCL.g:3979:2: ()
            // InternalMCL.g:3979:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getHistoryProviderAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__0__Impl"


    // $ANTLR start "rule__HistoryProvider__Group__1"
    // InternalMCL.g:3987:1: rule__HistoryProvider__Group__1 : rule__HistoryProvider__Group__1__Impl rule__HistoryProvider__Group__2 ;
    public final void rule__HistoryProvider__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:3991:1: ( rule__HistoryProvider__Group__1__Impl rule__HistoryProvider__Group__2 )
            // InternalMCL.g:3992:2: rule__HistoryProvider__Group__1__Impl rule__HistoryProvider__Group__2
            {
            pushFollow(FOLLOW_35);
            rule__HistoryProvider__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__HistoryProvider__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__1"


    // $ANTLR start "rule__HistoryProvider__Group__1__Impl"
    // InternalMCL.g:3999:1: rule__HistoryProvider__Group__1__Impl : ( ( rule__HistoryProvider__StateAssignment_1 )? ) ;
    public final void rule__HistoryProvider__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4003:1: ( ( ( rule__HistoryProvider__StateAssignment_1 )? ) )
            // InternalMCL.g:4004:1: ( ( rule__HistoryProvider__StateAssignment_1 )? )
            {
            // InternalMCL.g:4004:1: ( ( rule__HistoryProvider__StateAssignment_1 )? )
            // InternalMCL.g:4005:2: ( rule__HistoryProvider__StateAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getStateAssignment_1()); 
            }
            // InternalMCL.g:4006:2: ( rule__HistoryProvider__StateAssignment_1 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==62) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalMCL.g:4006:3: rule__HistoryProvider__StateAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__HistoryProvider__StateAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getStateAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__1__Impl"


    // $ANTLR start "rule__HistoryProvider__Group__2"
    // InternalMCL.g:4014:1: rule__HistoryProvider__Group__2 : rule__HistoryProvider__Group__2__Impl ;
    public final void rule__HistoryProvider__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4018:1: ( rule__HistoryProvider__Group__2__Impl )
            // InternalMCL.g:4019:2: rule__HistoryProvider__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HistoryProvider__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__2"


    // $ANTLR start "rule__HistoryProvider__Group__2__Impl"
    // InternalMCL.g:4025:1: rule__HistoryProvider__Group__2__Impl : ( 'HistoryProvider' ) ;
    public final void rule__HistoryProvider__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4029:1: ( ( 'HistoryProvider' ) )
            // InternalMCL.g:4030:1: ( 'HistoryProvider' )
            {
            // InternalMCL.g:4030:1: ( 'HistoryProvider' )
            // InternalMCL.g:4031:2: 'HistoryProvider'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getHistoryProviderKeyword_2()); 
            }
            match(input,54,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getHistoryProviderKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__Group__2__Impl"


    // $ANTLR start "rule__Greater__Group__0"
    // InternalMCL.g:4041:1: rule__Greater__Group__0 : rule__Greater__Group__0__Impl rule__Greater__Group__1 ;
    public final void rule__Greater__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4045:1: ( rule__Greater__Group__0__Impl rule__Greater__Group__1 )
            // InternalMCL.g:4046:2: rule__Greater__Group__0__Impl rule__Greater__Group__1
            {
            pushFollow(FOLLOW_36);
            rule__Greater__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Greater__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greater__Group__0"


    // $ANTLR start "rule__Greater__Group__0__Impl"
    // InternalMCL.g:4053:1: rule__Greater__Group__0__Impl : ( () ) ;
    public final void rule__Greater__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4057:1: ( ( () ) )
            // InternalMCL.g:4058:1: ( () )
            {
            // InternalMCL.g:4058:1: ( () )
            // InternalMCL.g:4059:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGreaterAccess().getGreaterAction_0()); 
            }
            // InternalMCL.g:4060:2: ()
            // InternalMCL.g:4060:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGreaterAccess().getGreaterAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greater__Group__0__Impl"


    // $ANTLR start "rule__Greater__Group__1"
    // InternalMCL.g:4068:1: rule__Greater__Group__1 : rule__Greater__Group__1__Impl ;
    public final void rule__Greater__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4072:1: ( rule__Greater__Group__1__Impl )
            // InternalMCL.g:4073:2: rule__Greater__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Greater__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greater__Group__1"


    // $ANTLR start "rule__Greater__Group__1__Impl"
    // InternalMCL.g:4079:1: rule__Greater__Group__1__Impl : ( '>' ) ;
    public final void rule__Greater__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4083:1: ( ( '>' ) )
            // InternalMCL.g:4084:1: ( '>' )
            {
            // InternalMCL.g:4084:1: ( '>' )
            // InternalMCL.g:4085:2: '>'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGreaterAccess().getGreaterThanSignKeyword_1()); 
            }
            match(input,23,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGreaterAccess().getGreaterThanSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Greater__Group__1__Impl"


    // $ANTLR start "rule__Lesser__Group__0"
    // InternalMCL.g:4095:1: rule__Lesser__Group__0 : rule__Lesser__Group__0__Impl rule__Lesser__Group__1 ;
    public final void rule__Lesser__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4099:1: ( rule__Lesser__Group__0__Impl rule__Lesser__Group__1 )
            // InternalMCL.g:4100:2: rule__Lesser__Group__0__Impl rule__Lesser__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Lesser__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Lesser__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lesser__Group__0"


    // $ANTLR start "rule__Lesser__Group__0__Impl"
    // InternalMCL.g:4107:1: rule__Lesser__Group__0__Impl : ( () ) ;
    public final void rule__Lesser__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4111:1: ( ( () ) )
            // InternalMCL.g:4112:1: ( () )
            {
            // InternalMCL.g:4112:1: ( () )
            // InternalMCL.g:4113:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLesserAccess().getLesserAction_0()); 
            }
            // InternalMCL.g:4114:2: ()
            // InternalMCL.g:4114:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLesserAccess().getLesserAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lesser__Group__0__Impl"


    // $ANTLR start "rule__Lesser__Group__1"
    // InternalMCL.g:4122:1: rule__Lesser__Group__1 : rule__Lesser__Group__1__Impl ;
    public final void rule__Lesser__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4126:1: ( rule__Lesser__Group__1__Impl )
            // InternalMCL.g:4127:2: rule__Lesser__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lesser__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lesser__Group__1"


    // $ANTLR start "rule__Lesser__Group__1__Impl"
    // InternalMCL.g:4133:1: rule__Lesser__Group__1__Impl : ( '<' ) ;
    public final void rule__Lesser__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4137:1: ( ( '<' ) )
            // InternalMCL.g:4138:1: ( '<' )
            {
            // InternalMCL.g:4138:1: ( '<' )
            // InternalMCL.g:4139:2: '<'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLesserAccess().getLessThanSignKeyword_1()); 
            }
            match(input,22,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLesserAccess().getLessThanSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lesser__Group__1__Impl"


    // $ANTLR start "rule__UnaryMinus__Group__0"
    // InternalMCL.g:4149:1: rule__UnaryMinus__Group__0 : rule__UnaryMinus__Group__0__Impl rule__UnaryMinus__Group__1 ;
    public final void rule__UnaryMinus__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4153:1: ( rule__UnaryMinus__Group__0__Impl rule__UnaryMinus__Group__1 )
            // InternalMCL.g:4154:2: rule__UnaryMinus__Group__0__Impl rule__UnaryMinus__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__UnaryMinus__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__UnaryMinus__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryMinus__Group__0"


    // $ANTLR start "rule__UnaryMinus__Group__0__Impl"
    // InternalMCL.g:4161:1: rule__UnaryMinus__Group__0__Impl : ( '-' ) ;
    public final void rule__UnaryMinus__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4165:1: ( ( '-' ) )
            // InternalMCL.g:4166:1: ( '-' )
            {
            // InternalMCL.g:4166:1: ( '-' )
            // InternalMCL.g:4167:2: '-'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnaryMinusAccess().getHyphenMinusKeyword_0()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnaryMinusAccess().getHyphenMinusKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryMinus__Group__0__Impl"


    // $ANTLR start "rule__UnaryMinus__Group__1"
    // InternalMCL.g:4176:1: rule__UnaryMinus__Group__1 : rule__UnaryMinus__Group__1__Impl ;
    public final void rule__UnaryMinus__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4180:1: ( rule__UnaryMinus__Group__1__Impl )
            // InternalMCL.g:4181:2: rule__UnaryMinus__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryMinus__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryMinus__Group__1"


    // $ANTLR start "rule__UnaryMinus__Group__1__Impl"
    // InternalMCL.g:4187:1: rule__UnaryMinus__Group__1__Impl : ( ( rule__UnaryMinus__OperandAssignment_1 ) ) ;
    public final void rule__UnaryMinus__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4191:1: ( ( ( rule__UnaryMinus__OperandAssignment_1 ) ) )
            // InternalMCL.g:4192:1: ( ( rule__UnaryMinus__OperandAssignment_1 ) )
            {
            // InternalMCL.g:4192:1: ( ( rule__UnaryMinus__OperandAssignment_1 ) )
            // InternalMCL.g:4193:2: ( rule__UnaryMinus__OperandAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnaryMinusAccess().getOperandAssignment_1()); 
            }
            // InternalMCL.g:4194:2: ( rule__UnaryMinus__OperandAssignment_1 )
            // InternalMCL.g:4194:3: rule__UnaryMinus__OperandAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UnaryMinus__OperandAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnaryMinusAccess().getOperandAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryMinus__Group__1__Impl"


    // $ANTLR start "rule__Not__Group__0"
    // InternalMCL.g:4203:1: rule__Not__Group__0 : rule__Not__Group__0__Impl rule__Not__Group__1 ;
    public final void rule__Not__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4207:1: ( rule__Not__Group__0__Impl rule__Not__Group__1 )
            // InternalMCL.g:4208:2: rule__Not__Group__0__Impl rule__Not__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Not__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Not__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0"


    // $ANTLR start "rule__Not__Group__0__Impl"
    // InternalMCL.g:4215:1: rule__Not__Group__0__Impl : ( ( rule__Not__Alternatives_0 ) ) ;
    public final void rule__Not__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4219:1: ( ( ( rule__Not__Alternatives_0 ) ) )
            // InternalMCL.g:4220:1: ( ( rule__Not__Alternatives_0 ) )
            {
            // InternalMCL.g:4220:1: ( ( rule__Not__Alternatives_0 ) )
            // InternalMCL.g:4221:2: ( rule__Not__Alternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getAlternatives_0()); 
            }
            // InternalMCL.g:4222:2: ( rule__Not__Alternatives_0 )
            // InternalMCL.g:4222:3: rule__Not__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Not__Alternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0__Impl"


    // $ANTLR start "rule__Not__Group__1"
    // InternalMCL.g:4230:1: rule__Not__Group__1 : rule__Not__Group__1__Impl ;
    public final void rule__Not__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4234:1: ( rule__Not__Group__1__Impl )
            // InternalMCL.g:4235:2: rule__Not__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Not__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1"


    // $ANTLR start "rule__Not__Group__1__Impl"
    // InternalMCL.g:4241:1: rule__Not__Group__1__Impl : ( ( rule__Not__OperandAssignment_1 ) ) ;
    public final void rule__Not__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4245:1: ( ( ( rule__Not__OperandAssignment_1 ) ) )
            // InternalMCL.g:4246:1: ( ( rule__Not__OperandAssignment_1 ) )
            {
            // InternalMCL.g:4246:1: ( ( rule__Not__OperandAssignment_1 ) )
            // InternalMCL.g:4247:2: ( rule__Not__OperandAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getOperandAssignment_1()); 
            }
            // InternalMCL.g:4248:2: ( rule__Not__OperandAssignment_1 )
            // InternalMCL.g:4248:3: rule__Not__OperandAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Not__OperandAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getOperandAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1__Impl"


    // $ANTLR start "rule__BinaryMinus__Group__0"
    // InternalMCL.g:4257:1: rule__BinaryMinus__Group__0 : rule__BinaryMinus__Group__0__Impl rule__BinaryMinus__Group__1 ;
    public final void rule__BinaryMinus__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4261:1: ( rule__BinaryMinus__Group__0__Impl rule__BinaryMinus__Group__1 )
            // InternalMCL.g:4262:2: rule__BinaryMinus__Group__0__Impl rule__BinaryMinus__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__BinaryMinus__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__0"


    // $ANTLR start "rule__BinaryMinus__Group__0__Impl"
    // InternalMCL.g:4269:1: rule__BinaryMinus__Group__0__Impl : ( ( rule__BinaryMinus__LeftOperandAssignment_0 ) ) ;
    public final void rule__BinaryMinus__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4273:1: ( ( ( rule__BinaryMinus__LeftOperandAssignment_0 ) ) )
            // InternalMCL.g:4274:1: ( ( rule__BinaryMinus__LeftOperandAssignment_0 ) )
            {
            // InternalMCL.g:4274:1: ( ( rule__BinaryMinus__LeftOperandAssignment_0 ) )
            // InternalMCL.g:4275:2: ( rule__BinaryMinus__LeftOperandAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getLeftOperandAssignment_0()); 
            }
            // InternalMCL.g:4276:2: ( rule__BinaryMinus__LeftOperandAssignment_0 )
            // InternalMCL.g:4276:3: rule__BinaryMinus__LeftOperandAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__LeftOperandAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getLeftOperandAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__0__Impl"


    // $ANTLR start "rule__BinaryMinus__Group__1"
    // InternalMCL.g:4284:1: rule__BinaryMinus__Group__1 : rule__BinaryMinus__Group__1__Impl rule__BinaryMinus__Group__2 ;
    public final void rule__BinaryMinus__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4288:1: ( rule__BinaryMinus__Group__1__Impl rule__BinaryMinus__Group__2 )
            // InternalMCL.g:4289:2: rule__BinaryMinus__Group__1__Impl rule__BinaryMinus__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__BinaryMinus__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__1"


    // $ANTLR start "rule__BinaryMinus__Group__1__Impl"
    // InternalMCL.g:4296:1: rule__BinaryMinus__Group__1__Impl : ( '-' ) ;
    public final void rule__BinaryMinus__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4300:1: ( ( '-' ) )
            // InternalMCL.g:4301:1: ( '-' )
            {
            // InternalMCL.g:4301:1: ( '-' )
            // InternalMCL.g:4302:2: '-'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getHyphenMinusKeyword_1()); 
            }
            match(input,27,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getHyphenMinusKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__1__Impl"


    // $ANTLR start "rule__BinaryMinus__Group__2"
    // InternalMCL.g:4311:1: rule__BinaryMinus__Group__2 : rule__BinaryMinus__Group__2__Impl ;
    public final void rule__BinaryMinus__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4315:1: ( rule__BinaryMinus__Group__2__Impl )
            // InternalMCL.g:4316:2: rule__BinaryMinus__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__2"


    // $ANTLR start "rule__BinaryMinus__Group__2__Impl"
    // InternalMCL.g:4322:1: rule__BinaryMinus__Group__2__Impl : ( ( rule__BinaryMinus__RightOperandAssignment_2 ) ) ;
    public final void rule__BinaryMinus__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4326:1: ( ( ( rule__BinaryMinus__RightOperandAssignment_2 ) ) )
            // InternalMCL.g:4327:1: ( ( rule__BinaryMinus__RightOperandAssignment_2 ) )
            {
            // InternalMCL.g:4327:1: ( ( rule__BinaryMinus__RightOperandAssignment_2 ) )
            // InternalMCL.g:4328:2: ( rule__BinaryMinus__RightOperandAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getRightOperandAssignment_2()); 
            }
            // InternalMCL.g:4329:2: ( rule__BinaryMinus__RightOperandAssignment_2 )
            // InternalMCL.g:4329:3: rule__BinaryMinus__RightOperandAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__BinaryMinus__RightOperandAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getRightOperandAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__Group__2__Impl"


    // $ANTLR start "rule__BinaryPlus__Group__0"
    // InternalMCL.g:4338:1: rule__BinaryPlus__Group__0 : rule__BinaryPlus__Group__0__Impl rule__BinaryPlus__Group__1 ;
    public final void rule__BinaryPlus__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4342:1: ( rule__BinaryPlus__Group__0__Impl rule__BinaryPlus__Group__1 )
            // InternalMCL.g:4343:2: rule__BinaryPlus__Group__0__Impl rule__BinaryPlus__Group__1
            {
            pushFollow(FOLLOW_38);
            rule__BinaryPlus__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__0"


    // $ANTLR start "rule__BinaryPlus__Group__0__Impl"
    // InternalMCL.g:4350:1: rule__BinaryPlus__Group__0__Impl : ( ( rule__BinaryPlus__LeftOperandAssignment_0 ) ) ;
    public final void rule__BinaryPlus__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4354:1: ( ( ( rule__BinaryPlus__LeftOperandAssignment_0 ) ) )
            // InternalMCL.g:4355:1: ( ( rule__BinaryPlus__LeftOperandAssignment_0 ) )
            {
            // InternalMCL.g:4355:1: ( ( rule__BinaryPlus__LeftOperandAssignment_0 ) )
            // InternalMCL.g:4356:2: ( rule__BinaryPlus__LeftOperandAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getLeftOperandAssignment_0()); 
            }
            // InternalMCL.g:4357:2: ( rule__BinaryPlus__LeftOperandAssignment_0 )
            // InternalMCL.g:4357:3: rule__BinaryPlus__LeftOperandAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__LeftOperandAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getLeftOperandAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__0__Impl"


    // $ANTLR start "rule__BinaryPlus__Group__1"
    // InternalMCL.g:4365:1: rule__BinaryPlus__Group__1 : rule__BinaryPlus__Group__1__Impl rule__BinaryPlus__Group__2 ;
    public final void rule__BinaryPlus__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4369:1: ( rule__BinaryPlus__Group__1__Impl rule__BinaryPlus__Group__2 )
            // InternalMCL.g:4370:2: rule__BinaryPlus__Group__1__Impl rule__BinaryPlus__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__BinaryPlus__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__1"


    // $ANTLR start "rule__BinaryPlus__Group__1__Impl"
    // InternalMCL.g:4377:1: rule__BinaryPlus__Group__1__Impl : ( '+' ) ;
    public final void rule__BinaryPlus__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4381:1: ( ( '+' ) )
            // InternalMCL.g:4382:1: ( '+' )
            {
            // InternalMCL.g:4382:1: ( '+' )
            // InternalMCL.g:4383:2: '+'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getPlusSignKeyword_1()); 
            }
            match(input,26,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getPlusSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__1__Impl"


    // $ANTLR start "rule__BinaryPlus__Group__2"
    // InternalMCL.g:4392:1: rule__BinaryPlus__Group__2 : rule__BinaryPlus__Group__2__Impl ;
    public final void rule__BinaryPlus__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4396:1: ( rule__BinaryPlus__Group__2__Impl )
            // InternalMCL.g:4397:2: rule__BinaryPlus__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__2"


    // $ANTLR start "rule__BinaryPlus__Group__2__Impl"
    // InternalMCL.g:4403:1: rule__BinaryPlus__Group__2__Impl : ( ( rule__BinaryPlus__RightOperandAssignment_2 ) ) ;
    public final void rule__BinaryPlus__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4407:1: ( ( ( rule__BinaryPlus__RightOperandAssignment_2 ) ) )
            // InternalMCL.g:4408:1: ( ( rule__BinaryPlus__RightOperandAssignment_2 ) )
            {
            // InternalMCL.g:4408:1: ( ( rule__BinaryPlus__RightOperandAssignment_2 ) )
            // InternalMCL.g:4409:2: ( rule__BinaryPlus__RightOperandAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getRightOperandAssignment_2()); 
            }
            // InternalMCL.g:4410:2: ( rule__BinaryPlus__RightOperandAssignment_2 )
            // InternalMCL.g:4410:3: rule__BinaryPlus__RightOperandAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__BinaryPlus__RightOperandAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getRightOperandAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__Group__2__Impl"


    // $ANTLR start "rule__Assignment__Group__0"
    // InternalMCL.g:4419:1: rule__Assignment__Group__0 : rule__Assignment__Group__0__Impl rule__Assignment__Group__1 ;
    public final void rule__Assignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4423:1: ( rule__Assignment__Group__0__Impl rule__Assignment__Group__1 )
            // InternalMCL.g:4424:2: rule__Assignment__Group__0__Impl rule__Assignment__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Assignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0"


    // $ANTLR start "rule__Assignment__Group__0__Impl"
    // InternalMCL.g:4431:1: rule__Assignment__Group__0__Impl : ( () ) ;
    public final void rule__Assignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4435:1: ( ( () ) )
            // InternalMCL.g:4436:1: ( () )
            {
            // InternalMCL.g:4436:1: ( () )
            // InternalMCL.g:4437:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getAssignmentAction_0()); 
            }
            // InternalMCL.g:4438:2: ()
            // InternalMCL.g:4438:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getAssignmentAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0__Impl"


    // $ANTLR start "rule__Assignment__Group__1"
    // InternalMCL.g:4446:1: rule__Assignment__Group__1 : rule__Assignment__Group__1__Impl rule__Assignment__Group__2 ;
    public final void rule__Assignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4450:1: ( rule__Assignment__Group__1__Impl rule__Assignment__Group__2 )
            // InternalMCL.g:4451:2: rule__Assignment__Group__1__Impl rule__Assignment__Group__2
            {
            pushFollow(FOLLOW_39);
            rule__Assignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1"


    // $ANTLR start "rule__Assignment__Group__1__Impl"
    // InternalMCL.g:4458:1: rule__Assignment__Group__1__Impl : ( ( rule__Assignment__LeftOperandAssignment_1 ) ) ;
    public final void rule__Assignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4462:1: ( ( ( rule__Assignment__LeftOperandAssignment_1 ) ) )
            // InternalMCL.g:4463:1: ( ( rule__Assignment__LeftOperandAssignment_1 ) )
            {
            // InternalMCL.g:4463:1: ( ( rule__Assignment__LeftOperandAssignment_1 ) )
            // InternalMCL.g:4464:2: ( rule__Assignment__LeftOperandAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getLeftOperandAssignment_1()); 
            }
            // InternalMCL.g:4465:2: ( rule__Assignment__LeftOperandAssignment_1 )
            // InternalMCL.g:4465:3: rule__Assignment__LeftOperandAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__LeftOperandAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getLeftOperandAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1__Impl"


    // $ANTLR start "rule__Assignment__Group__2"
    // InternalMCL.g:4473:1: rule__Assignment__Group__2 : rule__Assignment__Group__2__Impl rule__Assignment__Group__3 ;
    public final void rule__Assignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4477:1: ( rule__Assignment__Group__2__Impl rule__Assignment__Group__3 )
            // InternalMCL.g:4478:2: rule__Assignment__Group__2__Impl rule__Assignment__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Assignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2"


    // $ANTLR start "rule__Assignment__Group__2__Impl"
    // InternalMCL.g:4485:1: rule__Assignment__Group__2__Impl : ( '->' ) ;
    public final void rule__Assignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4489:1: ( ( '->' ) )
            // InternalMCL.g:4490:1: ( '->' )
            {
            // InternalMCL.g:4490:1: ( '->' )
            // InternalMCL.g:4491:2: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            }
            match(input,19,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2__Impl"


    // $ANTLR start "rule__Assignment__Group__3"
    // InternalMCL.g:4500:1: rule__Assignment__Group__3 : rule__Assignment__Group__3__Impl ;
    public final void rule__Assignment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4504:1: ( rule__Assignment__Group__3__Impl )
            // InternalMCL.g:4505:2: rule__Assignment__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__3"


    // $ANTLR start "rule__Assignment__Group__3__Impl"
    // InternalMCL.g:4511:1: rule__Assignment__Group__3__Impl : ( ( rule__Assignment__RightOperandAssignment_3 ) ) ;
    public final void rule__Assignment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4515:1: ( ( ( rule__Assignment__RightOperandAssignment_3 ) ) )
            // InternalMCL.g:4516:1: ( ( rule__Assignment__RightOperandAssignment_3 ) )
            {
            // InternalMCL.g:4516:1: ( ( rule__Assignment__RightOperandAssignment_3 ) )
            // InternalMCL.g:4517:2: ( rule__Assignment__RightOperandAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getRightOperandAssignment_3()); 
            }
            // InternalMCL.g:4518:2: ( rule__Assignment__RightOperandAssignment_3 )
            // InternalMCL.g:4518:3: rule__Assignment__RightOperandAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__RightOperandAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getRightOperandAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__3__Impl"


    // $ANTLR start "rule__SimpleSync__Group__0"
    // InternalMCL.g:4527:1: rule__SimpleSync__Group__0 : rule__SimpleSync__Group__0__Impl rule__SimpleSync__Group__1 ;
    public final void rule__SimpleSync__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4531:1: ( rule__SimpleSync__Group__0__Impl rule__SimpleSync__Group__1 )
            // InternalMCL.g:4532:2: rule__SimpleSync__Group__0__Impl rule__SimpleSync__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__SimpleSync__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__0"


    // $ANTLR start "rule__SimpleSync__Group__0__Impl"
    // InternalMCL.g:4539:1: rule__SimpleSync__Group__0__Impl : ( ( rule__SimpleSync__RightTemporalReferenceAssignment_0 ) ) ;
    public final void rule__SimpleSync__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4543:1: ( ( ( rule__SimpleSync__RightTemporalReferenceAssignment_0 ) ) )
            // InternalMCL.g:4544:1: ( ( rule__SimpleSync__RightTemporalReferenceAssignment_0 ) )
            {
            // InternalMCL.g:4544:1: ( ( rule__SimpleSync__RightTemporalReferenceAssignment_0 ) )
            // InternalMCL.g:4545:2: ( rule__SimpleSync__RightTemporalReferenceAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceAssignment_0()); 
            }
            // InternalMCL.g:4546:2: ( rule__SimpleSync__RightTemporalReferenceAssignment_0 )
            // InternalMCL.g:4546:3: rule__SimpleSync__RightTemporalReferenceAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__RightTemporalReferenceAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__0__Impl"


    // $ANTLR start "rule__SimpleSync__Group__1"
    // InternalMCL.g:4554:1: rule__SimpleSync__Group__1 : rule__SimpleSync__Group__1__Impl rule__SimpleSync__Group__2 ;
    public final void rule__SimpleSync__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4558:1: ( rule__SimpleSync__Group__1__Impl rule__SimpleSync__Group__2 )
            // InternalMCL.g:4559:2: rule__SimpleSync__Group__1__Impl rule__SimpleSync__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__SimpleSync__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__1"


    // $ANTLR start "rule__SimpleSync__Group__1__Impl"
    // InternalMCL.g:4566:1: rule__SimpleSync__Group__1__Impl : ( '=' ) ;
    public final void rule__SimpleSync__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4570:1: ( ( '=' ) )
            // InternalMCL.g:4571:1: ( '=' )
            {
            // InternalMCL.g:4571:1: ( '=' )
            // InternalMCL.g:4572:2: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getEqualsSignKeyword_1()); 
            }
            match(input,20,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__1__Impl"


    // $ANTLR start "rule__SimpleSync__Group__2"
    // InternalMCL.g:4581:1: rule__SimpleSync__Group__2 : rule__SimpleSync__Group__2__Impl rule__SimpleSync__Group__3 ;
    public final void rule__SimpleSync__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4585:1: ( rule__SimpleSync__Group__2__Impl rule__SimpleSync__Group__3 )
            // InternalMCL.g:4586:2: rule__SimpleSync__Group__2__Impl rule__SimpleSync__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__SimpleSync__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__2"


    // $ANTLR start "rule__SimpleSync__Group__2__Impl"
    // InternalMCL.g:4593:1: rule__SimpleSync__Group__2__Impl : ( ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 ) ) ;
    public final void rule__SimpleSync__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4597:1: ( ( ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 ) ) )
            // InternalMCL.g:4598:1: ( ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 ) )
            {
            // InternalMCL.g:4598:1: ( ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 ) )
            // InternalMCL.g:4599:2: ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceAssignment_2()); 
            }
            // InternalMCL.g:4600:2: ( rule__SimpleSync__LeftTemporalReferenceAssignment_2 )
            // InternalMCL.g:4600:3: rule__SimpleSync__LeftTemporalReferenceAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__LeftTemporalReferenceAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__2__Impl"


    // $ANTLR start "rule__SimpleSync__Group__3"
    // InternalMCL.g:4608:1: rule__SimpleSync__Group__3 : rule__SimpleSync__Group__3__Impl ;
    public final void rule__SimpleSync__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4612:1: ( rule__SimpleSync__Group__3__Impl )
            // InternalMCL.g:4613:2: rule__SimpleSync__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__3"


    // $ANTLR start "rule__SimpleSync__Group__3__Impl"
    // InternalMCL.g:4619:1: rule__SimpleSync__Group__3__Impl : ( ( rule__SimpleSync__Group_3__0 )? ) ;
    public final void rule__SimpleSync__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4623:1: ( ( ( rule__SimpleSync__Group_3__0 )? ) )
            // InternalMCL.g:4624:1: ( ( rule__SimpleSync__Group_3__0 )? )
            {
            // InternalMCL.g:4624:1: ( ( rule__SimpleSync__Group_3__0 )? )
            // InternalMCL.g:4625:2: ( rule__SimpleSync__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getGroup_3()); 
            }
            // InternalMCL.g:4626:2: ( rule__SimpleSync__Group_3__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==55) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalMCL.g:4626:3: rule__SimpleSync__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__SimpleSync__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group__3__Impl"


    // $ANTLR start "rule__SimpleSync__Group_3__0"
    // InternalMCL.g:4635:1: rule__SimpleSync__Group_3__0 : rule__SimpleSync__Group_3__0__Impl rule__SimpleSync__Group_3__1 ;
    public final void rule__SimpleSync__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4639:1: ( rule__SimpleSync__Group_3__0__Impl rule__SimpleSync__Group_3__1 )
            // InternalMCL.g:4640:2: rule__SimpleSync__Group_3__0__Impl rule__SimpleSync__Group_3__1
            {
            pushFollow(FOLLOW_18);
            rule__SimpleSync__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group_3__0"


    // $ANTLR start "rule__SimpleSync__Group_3__0__Impl"
    // InternalMCL.g:4647:1: rule__SimpleSync__Group_3__0__Impl : ( 'tolerance' ) ;
    public final void rule__SimpleSync__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4651:1: ( ( 'tolerance' ) )
            // InternalMCL.g:4652:1: ( 'tolerance' )
            {
            // InternalMCL.g:4652:1: ( 'tolerance' )
            // InternalMCL.g:4653:2: 'tolerance'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getToleranceKeyword_3_0()); 
            }
            match(input,55,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getToleranceKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group_3__0__Impl"


    // $ANTLR start "rule__SimpleSync__Group_3__1"
    // InternalMCL.g:4662:1: rule__SimpleSync__Group_3__1 : rule__SimpleSync__Group_3__1__Impl ;
    public final void rule__SimpleSync__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4666:1: ( rule__SimpleSync__Group_3__1__Impl )
            // InternalMCL.g:4667:2: rule__SimpleSync__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group_3__1"


    // $ANTLR start "rule__SimpleSync__Group_3__1__Impl"
    // InternalMCL.g:4673:1: rule__SimpleSync__Group_3__1__Impl : ( ( rule__SimpleSync__ToleranceAssignment_3_1 ) ) ;
    public final void rule__SimpleSync__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4677:1: ( ( ( rule__SimpleSync__ToleranceAssignment_3_1 ) ) )
            // InternalMCL.g:4678:1: ( ( rule__SimpleSync__ToleranceAssignment_3_1 ) )
            {
            // InternalMCL.g:4678:1: ( ( rule__SimpleSync__ToleranceAssignment_3_1 ) )
            // InternalMCL.g:4679:2: ( rule__SimpleSync__ToleranceAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getToleranceAssignment_3_1()); 
            }
            // InternalMCL.g:4680:2: ( rule__SimpleSync__ToleranceAssignment_3_1 )
            // InternalMCL.g:4680:3: rule__SimpleSync__ToleranceAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSync__ToleranceAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getToleranceAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__Group_3__1__Impl"


    // $ANTLR start "rule__GImportStatement__Group__0"
    // InternalMCL.g:4689:1: rule__GImportStatement__Group__0 : rule__GImportStatement__Group__0__Impl rule__GImportStatement__Group__1 ;
    public final void rule__GImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4693:1: ( rule__GImportStatement__Group__0__Impl rule__GImportStatement__Group__1 )
            // InternalMCL.g:4694:2: rule__GImportStatement__Group__0__Impl rule__GImportStatement__Group__1
            {
            pushFollow(FOLLOW_42);
            rule__GImportStatement__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GImportStatement__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GImportStatement__Group__0"


    // $ANTLR start "rule__GImportStatement__Group__0__Impl"
    // InternalMCL.g:4701:1: rule__GImportStatement__Group__0__Impl : ( 'import' ) ;
    public final void rule__GImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4705:1: ( ( 'import' ) )
            // InternalMCL.g:4706:1: ( 'import' )
            {
            // InternalMCL.g:4706:1: ( 'import' )
            // InternalMCL.g:4707:2: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGImportStatementAccess().getImportKeyword_0()); 
            }
            match(input,56,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGImportStatementAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GImportStatement__Group__0__Impl"


    // $ANTLR start "rule__GImportStatement__Group__1"
    // InternalMCL.g:4716:1: rule__GImportStatement__Group__1 : rule__GImportStatement__Group__1__Impl ;
    public final void rule__GImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4720:1: ( rule__GImportStatement__Group__1__Impl )
            // InternalMCL.g:4721:2: rule__GImportStatement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GImportStatement__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GImportStatement__Group__1"


    // $ANTLR start "rule__GImportStatement__Group__1__Impl"
    // InternalMCL.g:4727:1: rule__GImportStatement__Group__1__Impl : ( ( rule__GImportStatement__ImportURIAssignment_1 ) ) ;
    public final void rule__GImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4731:1: ( ( ( rule__GImportStatement__ImportURIAssignment_1 ) ) )
            // InternalMCL.g:4732:1: ( ( rule__GImportStatement__ImportURIAssignment_1 ) )
            {
            // InternalMCL.g:4732:1: ( ( rule__GImportStatement__ImportURIAssignment_1 ) )
            // InternalMCL.g:4733:2: ( rule__GImportStatement__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGImportStatementAccess().getImportURIAssignment_1()); 
            }
            // InternalMCL.g:4734:2: ( rule__GImportStatement__ImportURIAssignment_1 )
            // InternalMCL.g:4734:3: rule__GImportStatement__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GImportStatement__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGImportStatementAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GImportStatement__Group__1__Impl"


    // $ANTLR start "rule__GOrExpression__Group__0"
    // InternalMCL.g:4743:1: rule__GOrExpression__Group__0 : rule__GOrExpression__Group__0__Impl rule__GOrExpression__Group__1 ;
    public final void rule__GOrExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4747:1: ( rule__GOrExpression__Group__0__Impl rule__GOrExpression__Group__1 )
            // InternalMCL.g:4748:2: rule__GOrExpression__Group__0__Impl rule__GOrExpression__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__GOrExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group__0"


    // $ANTLR start "rule__GOrExpression__Group__0__Impl"
    // InternalMCL.g:4755:1: rule__GOrExpression__Group__0__Impl : ( ruleGXorExpression ) ;
    public final void rule__GOrExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4759:1: ( ( ruleGXorExpression ) )
            // InternalMCL.g:4760:1: ( ruleGXorExpression )
            {
            // InternalMCL.g:4760:1: ( ruleGXorExpression )
            // InternalMCL.g:4761:2: ruleGXorExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getGXorExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGXorExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getGXorExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group__0__Impl"


    // $ANTLR start "rule__GOrExpression__Group__1"
    // InternalMCL.g:4770:1: rule__GOrExpression__Group__1 : rule__GOrExpression__Group__1__Impl ;
    public final void rule__GOrExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4774:1: ( rule__GOrExpression__Group__1__Impl )
            // InternalMCL.g:4775:2: rule__GOrExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group__1"


    // $ANTLR start "rule__GOrExpression__Group__1__Impl"
    // InternalMCL.g:4781:1: rule__GOrExpression__Group__1__Impl : ( ( rule__GOrExpression__Group_1__0 )* ) ;
    public final void rule__GOrExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4785:1: ( ( ( rule__GOrExpression__Group_1__0 )* ) )
            // InternalMCL.g:4786:1: ( ( rule__GOrExpression__Group_1__0 )* )
            {
            // InternalMCL.g:4786:1: ( ( rule__GOrExpression__Group_1__0 )* )
            // InternalMCL.g:4787:2: ( rule__GOrExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:4788:2: ( rule__GOrExpression__Group_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==15) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalMCL.g:4788:3: rule__GOrExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__GOrExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group__1__Impl"


    // $ANTLR start "rule__GOrExpression__Group_1__0"
    // InternalMCL.g:4797:1: rule__GOrExpression__Group_1__0 : rule__GOrExpression__Group_1__0__Impl rule__GOrExpression__Group_1__1 ;
    public final void rule__GOrExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4801:1: ( rule__GOrExpression__Group_1__0__Impl rule__GOrExpression__Group_1__1 )
            // InternalMCL.g:4802:2: rule__GOrExpression__Group_1__0__Impl rule__GOrExpression__Group_1__1
            {
            pushFollow(FOLLOW_19);
            rule__GOrExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__0"


    // $ANTLR start "rule__GOrExpression__Group_1__0__Impl"
    // InternalMCL.g:4809:1: rule__GOrExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GOrExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4813:1: ( ( () ) )
            // InternalMCL.g:4814:1: ( () )
            {
            // InternalMCL.g:4814:1: ( () )
            // InternalMCL.g:4815:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getGOrExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:4816:2: ()
            // InternalMCL.g:4816:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getGOrExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GOrExpression__Group_1__1"
    // InternalMCL.g:4824:1: rule__GOrExpression__Group_1__1 : rule__GOrExpression__Group_1__1__Impl rule__GOrExpression__Group_1__2 ;
    public final void rule__GOrExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4828:1: ( rule__GOrExpression__Group_1__1__Impl rule__GOrExpression__Group_1__2 )
            // InternalMCL.g:4829:2: rule__GOrExpression__Group_1__1__Impl rule__GOrExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GOrExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__1"


    // $ANTLR start "rule__GOrExpression__Group_1__1__Impl"
    // InternalMCL.g:4836:1: rule__GOrExpression__Group_1__1__Impl : ( ( rule__GOrExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GOrExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4840:1: ( ( ( rule__GOrExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:4841:1: ( ( rule__GOrExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:4841:1: ( ( rule__GOrExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:4842:2: ( rule__GOrExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:4843:2: ( rule__GOrExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:4843:3: rule__GOrExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GOrExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GOrExpression__Group_1__2"
    // InternalMCL.g:4851:1: rule__GOrExpression__Group_1__2 : rule__GOrExpression__Group_1__2__Impl ;
    public final void rule__GOrExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4855:1: ( rule__GOrExpression__Group_1__2__Impl )
            // InternalMCL.g:4856:2: rule__GOrExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GOrExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__2"


    // $ANTLR start "rule__GOrExpression__Group_1__2__Impl"
    // InternalMCL.g:4862:1: rule__GOrExpression__Group_1__2__Impl : ( ( rule__GOrExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GOrExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4866:1: ( ( ( rule__GOrExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:4867:1: ( ( rule__GOrExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:4867:1: ( ( rule__GOrExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:4868:2: ( rule__GOrExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:4869:2: ( rule__GOrExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:4869:3: rule__GOrExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GOrExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GXorExpression__Group__0"
    // InternalMCL.g:4878:1: rule__GXorExpression__Group__0 : rule__GXorExpression__Group__0__Impl rule__GXorExpression__Group__1 ;
    public final void rule__GXorExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4882:1: ( rule__GXorExpression__Group__0__Impl rule__GXorExpression__Group__1 )
            // InternalMCL.g:4883:2: rule__GXorExpression__Group__0__Impl rule__GXorExpression__Group__1
            {
            pushFollow(FOLLOW_44);
            rule__GXorExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group__0"


    // $ANTLR start "rule__GXorExpression__Group__0__Impl"
    // InternalMCL.g:4890:1: rule__GXorExpression__Group__0__Impl : ( ruleGAndExpression ) ;
    public final void rule__GXorExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4894:1: ( ( ruleGAndExpression ) )
            // InternalMCL.g:4895:1: ( ruleGAndExpression )
            {
            // InternalMCL.g:4895:1: ( ruleGAndExpression )
            // InternalMCL.g:4896:2: ruleGAndExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getGAndExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAndExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getGAndExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group__0__Impl"


    // $ANTLR start "rule__GXorExpression__Group__1"
    // InternalMCL.g:4905:1: rule__GXorExpression__Group__1 : rule__GXorExpression__Group__1__Impl ;
    public final void rule__GXorExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4909:1: ( rule__GXorExpression__Group__1__Impl )
            // InternalMCL.g:4910:2: rule__GXorExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group__1"


    // $ANTLR start "rule__GXorExpression__Group__1__Impl"
    // InternalMCL.g:4916:1: rule__GXorExpression__Group__1__Impl : ( ( rule__GXorExpression__Group_1__0 )* ) ;
    public final void rule__GXorExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4920:1: ( ( ( rule__GXorExpression__Group_1__0 )* ) )
            // InternalMCL.g:4921:1: ( ( rule__GXorExpression__Group_1__0 )* )
            {
            // InternalMCL.g:4921:1: ( ( rule__GXorExpression__Group_1__0 )* )
            // InternalMCL.g:4922:2: ( rule__GXorExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:4923:2: ( rule__GXorExpression__Group_1__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==14) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalMCL.g:4923:3: rule__GXorExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_45);
            	    rule__GXorExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group__1__Impl"


    // $ANTLR start "rule__GXorExpression__Group_1__0"
    // InternalMCL.g:4932:1: rule__GXorExpression__Group_1__0 : rule__GXorExpression__Group_1__0__Impl rule__GXorExpression__Group_1__1 ;
    public final void rule__GXorExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4936:1: ( rule__GXorExpression__Group_1__0__Impl rule__GXorExpression__Group_1__1 )
            // InternalMCL.g:4937:2: rule__GXorExpression__Group_1__0__Impl rule__GXorExpression__Group_1__1
            {
            pushFollow(FOLLOW_44);
            rule__GXorExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__0"


    // $ANTLR start "rule__GXorExpression__Group_1__0__Impl"
    // InternalMCL.g:4944:1: rule__GXorExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GXorExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4948:1: ( ( () ) )
            // InternalMCL.g:4949:1: ( () )
            {
            // InternalMCL.g:4949:1: ( () )
            // InternalMCL.g:4950:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getGXorExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:4951:2: ()
            // InternalMCL.g:4951:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getGXorExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GXorExpression__Group_1__1"
    // InternalMCL.g:4959:1: rule__GXorExpression__Group_1__1 : rule__GXorExpression__Group_1__1__Impl rule__GXorExpression__Group_1__2 ;
    public final void rule__GXorExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4963:1: ( rule__GXorExpression__Group_1__1__Impl rule__GXorExpression__Group_1__2 )
            // InternalMCL.g:4964:2: rule__GXorExpression__Group_1__1__Impl rule__GXorExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GXorExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__1"


    // $ANTLR start "rule__GXorExpression__Group_1__1__Impl"
    // InternalMCL.g:4971:1: rule__GXorExpression__Group_1__1__Impl : ( ( rule__GXorExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GXorExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4975:1: ( ( ( rule__GXorExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:4976:1: ( ( rule__GXorExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:4976:1: ( ( rule__GXorExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:4977:2: ( rule__GXorExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:4978:2: ( rule__GXorExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:4978:3: rule__GXorExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GXorExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GXorExpression__Group_1__2"
    // InternalMCL.g:4986:1: rule__GXorExpression__Group_1__2 : rule__GXorExpression__Group_1__2__Impl ;
    public final void rule__GXorExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:4990:1: ( rule__GXorExpression__Group_1__2__Impl )
            // InternalMCL.g:4991:2: rule__GXorExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GXorExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__2"


    // $ANTLR start "rule__GXorExpression__Group_1__2__Impl"
    // InternalMCL.g:4997:1: rule__GXorExpression__Group_1__2__Impl : ( ( rule__GXorExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GXorExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5001:1: ( ( ( rule__GXorExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5002:1: ( ( rule__GXorExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5002:1: ( ( rule__GXorExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5003:2: ( rule__GXorExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5004:2: ( rule__GXorExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5004:3: rule__GXorExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GXorExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GAndExpression__Group__0"
    // InternalMCL.g:5013:1: rule__GAndExpression__Group__0 : rule__GAndExpression__Group__0__Impl rule__GAndExpression__Group__1 ;
    public final void rule__GAndExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5017:1: ( rule__GAndExpression__Group__0__Impl rule__GAndExpression__Group__1 )
            // InternalMCL.g:5018:2: rule__GAndExpression__Group__0__Impl rule__GAndExpression__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__GAndExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group__0"


    // $ANTLR start "rule__GAndExpression__Group__0__Impl"
    // InternalMCL.g:5025:1: rule__GAndExpression__Group__0__Impl : ( ruleGEqualityExpression ) ;
    public final void rule__GAndExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5029:1: ( ( ruleGEqualityExpression ) )
            // InternalMCL.g:5030:1: ( ruleGEqualityExpression )
            {
            // InternalMCL.g:5030:1: ( ruleGEqualityExpression )
            // InternalMCL.g:5031:2: ruleGEqualityExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getGEqualityExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGEqualityExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getGEqualityExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group__0__Impl"


    // $ANTLR start "rule__GAndExpression__Group__1"
    // InternalMCL.g:5040:1: rule__GAndExpression__Group__1 : rule__GAndExpression__Group__1__Impl ;
    public final void rule__GAndExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5044:1: ( rule__GAndExpression__Group__1__Impl )
            // InternalMCL.g:5045:2: rule__GAndExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group__1"


    // $ANTLR start "rule__GAndExpression__Group__1__Impl"
    // InternalMCL.g:5051:1: rule__GAndExpression__Group__1__Impl : ( ( rule__GAndExpression__Group_1__0 )* ) ;
    public final void rule__GAndExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5055:1: ( ( ( rule__GAndExpression__Group_1__0 )* ) )
            // InternalMCL.g:5056:1: ( ( rule__GAndExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5056:1: ( ( rule__GAndExpression__Group_1__0 )* )
            // InternalMCL.g:5057:2: ( rule__GAndExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5058:2: ( rule__GAndExpression__Group_1__0 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==13) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalMCL.g:5058:3: rule__GAndExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_22);
            	    rule__GAndExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group__1__Impl"


    // $ANTLR start "rule__GAndExpression__Group_1__0"
    // InternalMCL.g:5067:1: rule__GAndExpression__Group_1__0 : rule__GAndExpression__Group_1__0__Impl rule__GAndExpression__Group_1__1 ;
    public final void rule__GAndExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5071:1: ( rule__GAndExpression__Group_1__0__Impl rule__GAndExpression__Group_1__1 )
            // InternalMCL.g:5072:2: rule__GAndExpression__Group_1__0__Impl rule__GAndExpression__Group_1__1
            {
            pushFollow(FOLLOW_21);
            rule__GAndExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__0"


    // $ANTLR start "rule__GAndExpression__Group_1__0__Impl"
    // InternalMCL.g:5079:1: rule__GAndExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GAndExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5083:1: ( ( () ) )
            // InternalMCL.g:5084:1: ( () )
            {
            // InternalMCL.g:5084:1: ( () )
            // InternalMCL.g:5085:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getGAndExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:5086:2: ()
            // InternalMCL.g:5086:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getGAndExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GAndExpression__Group_1__1"
    // InternalMCL.g:5094:1: rule__GAndExpression__Group_1__1 : rule__GAndExpression__Group_1__1__Impl rule__GAndExpression__Group_1__2 ;
    public final void rule__GAndExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5098:1: ( rule__GAndExpression__Group_1__1__Impl rule__GAndExpression__Group_1__2 )
            // InternalMCL.g:5099:2: rule__GAndExpression__Group_1__1__Impl rule__GAndExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GAndExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__1"


    // $ANTLR start "rule__GAndExpression__Group_1__1__Impl"
    // InternalMCL.g:5106:1: rule__GAndExpression__Group_1__1__Impl : ( ( rule__GAndExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GAndExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5110:1: ( ( ( rule__GAndExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5111:1: ( ( rule__GAndExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5111:1: ( ( rule__GAndExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5112:2: ( rule__GAndExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5113:2: ( rule__GAndExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5113:3: rule__GAndExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GAndExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GAndExpression__Group_1__2"
    // InternalMCL.g:5121:1: rule__GAndExpression__Group_1__2 : rule__GAndExpression__Group_1__2__Impl ;
    public final void rule__GAndExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5125:1: ( rule__GAndExpression__Group_1__2__Impl )
            // InternalMCL.g:5126:2: rule__GAndExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GAndExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__2"


    // $ANTLR start "rule__GAndExpression__Group_1__2__Impl"
    // InternalMCL.g:5132:1: rule__GAndExpression__Group_1__2__Impl : ( ( rule__GAndExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GAndExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5136:1: ( ( ( rule__GAndExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5137:1: ( ( rule__GAndExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5137:1: ( ( rule__GAndExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5138:2: ( rule__GAndExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5139:2: ( rule__GAndExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5139:3: rule__GAndExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GAndExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GEqualityExpression__Group__0"
    // InternalMCL.g:5148:1: rule__GEqualityExpression__Group__0 : rule__GEqualityExpression__Group__0__Impl rule__GEqualityExpression__Group__1 ;
    public final void rule__GEqualityExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5152:1: ( rule__GEqualityExpression__Group__0__Impl rule__GEqualityExpression__Group__1 )
            // InternalMCL.g:5153:2: rule__GEqualityExpression__Group__0__Impl rule__GEqualityExpression__Group__1
            {
            pushFollow(FOLLOW_46);
            rule__GEqualityExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group__0"


    // $ANTLR start "rule__GEqualityExpression__Group__0__Impl"
    // InternalMCL.g:5160:1: rule__GEqualityExpression__Group__0__Impl : ( ruleGRelationExpression ) ;
    public final void rule__GEqualityExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5164:1: ( ( ruleGRelationExpression ) )
            // InternalMCL.g:5165:1: ( ruleGRelationExpression )
            {
            // InternalMCL.g:5165:1: ( ruleGRelationExpression )
            // InternalMCL.g:5166:2: ruleGRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getGRelationExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getGRelationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group__0__Impl"


    // $ANTLR start "rule__GEqualityExpression__Group__1"
    // InternalMCL.g:5175:1: rule__GEqualityExpression__Group__1 : rule__GEqualityExpression__Group__1__Impl ;
    public final void rule__GEqualityExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5179:1: ( rule__GEqualityExpression__Group__1__Impl )
            // InternalMCL.g:5180:2: rule__GEqualityExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group__1"


    // $ANTLR start "rule__GEqualityExpression__Group__1__Impl"
    // InternalMCL.g:5186:1: rule__GEqualityExpression__Group__1__Impl : ( ( rule__GEqualityExpression__Group_1__0 )* ) ;
    public final void rule__GEqualityExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5190:1: ( ( ( rule__GEqualityExpression__Group_1__0 )* ) )
            // InternalMCL.g:5191:1: ( ( rule__GEqualityExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5191:1: ( ( rule__GEqualityExpression__Group_1__0 )* )
            // InternalMCL.g:5192:2: ( rule__GEqualityExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5193:2: ( rule__GEqualityExpression__Group_1__0 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( ((LA34_0>=20 && LA34_0<=21)) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalMCL.g:5193:3: rule__GEqualityExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_47);
            	    rule__GEqualityExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group__1__Impl"


    // $ANTLR start "rule__GEqualityExpression__Group_1__0"
    // InternalMCL.g:5202:1: rule__GEqualityExpression__Group_1__0 : rule__GEqualityExpression__Group_1__0__Impl rule__GEqualityExpression__Group_1__1 ;
    public final void rule__GEqualityExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5206:1: ( rule__GEqualityExpression__Group_1__0__Impl rule__GEqualityExpression__Group_1__1 )
            // InternalMCL.g:5207:2: rule__GEqualityExpression__Group_1__0__Impl rule__GEqualityExpression__Group_1__1
            {
            pushFollow(FOLLOW_46);
            rule__GEqualityExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__0"


    // $ANTLR start "rule__GEqualityExpression__Group_1__0__Impl"
    // InternalMCL.g:5214:1: rule__GEqualityExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GEqualityExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5218:1: ( ( () ) )
            // InternalMCL.g:5219:1: ( () )
            {
            // InternalMCL.g:5219:1: ( () )
            // InternalMCL.g:5220:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getGEqualityExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:5221:2: ()
            // InternalMCL.g:5221:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getGEqualityExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GEqualityExpression__Group_1__1"
    // InternalMCL.g:5229:1: rule__GEqualityExpression__Group_1__1 : rule__GEqualityExpression__Group_1__1__Impl rule__GEqualityExpression__Group_1__2 ;
    public final void rule__GEqualityExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5233:1: ( rule__GEqualityExpression__Group_1__1__Impl rule__GEqualityExpression__Group_1__2 )
            // InternalMCL.g:5234:2: rule__GEqualityExpression__Group_1__1__Impl rule__GEqualityExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GEqualityExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__1"


    // $ANTLR start "rule__GEqualityExpression__Group_1__1__Impl"
    // InternalMCL.g:5241:1: rule__GEqualityExpression__Group_1__1__Impl : ( ( rule__GEqualityExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GEqualityExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5245:1: ( ( ( rule__GEqualityExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5246:1: ( ( rule__GEqualityExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5246:1: ( ( rule__GEqualityExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5247:2: ( rule__GEqualityExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5248:2: ( rule__GEqualityExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5248:3: rule__GEqualityExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GEqualityExpression__Group_1__2"
    // InternalMCL.g:5256:1: rule__GEqualityExpression__Group_1__2 : rule__GEqualityExpression__Group_1__2__Impl ;
    public final void rule__GEqualityExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5260:1: ( rule__GEqualityExpression__Group_1__2__Impl )
            // InternalMCL.g:5261:2: rule__GEqualityExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__2"


    // $ANTLR start "rule__GEqualityExpression__Group_1__2__Impl"
    // InternalMCL.g:5267:1: rule__GEqualityExpression__Group_1__2__Impl : ( ( rule__GEqualityExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GEqualityExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5271:1: ( ( ( rule__GEqualityExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5272:1: ( ( rule__GEqualityExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5272:1: ( ( rule__GEqualityExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5273:2: ( rule__GEqualityExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5274:2: ( rule__GEqualityExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5274:3: rule__GEqualityExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GEqualityExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GRelationExpression__Group__0"
    // InternalMCL.g:5283:1: rule__GRelationExpression__Group__0 : rule__GRelationExpression__Group__0__Impl rule__GRelationExpression__Group__1 ;
    public final void rule__GRelationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5287:1: ( rule__GRelationExpression__Group__0__Impl rule__GRelationExpression__Group__1 )
            // InternalMCL.g:5288:2: rule__GRelationExpression__Group__0__Impl rule__GRelationExpression__Group__1
            {
            pushFollow(FOLLOW_48);
            rule__GRelationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group__0"


    // $ANTLR start "rule__GRelationExpression__Group__0__Impl"
    // InternalMCL.g:5295:1: rule__GRelationExpression__Group__0__Impl : ( ruleGAdditionExpression ) ;
    public final void rule__GRelationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5299:1: ( ( ruleGAdditionExpression ) )
            // InternalMCL.g:5300:1: ( ruleGAdditionExpression )
            {
            // InternalMCL.g:5300:1: ( ruleGAdditionExpression )
            // InternalMCL.g:5301:2: ruleGAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getGAdditionExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getGAdditionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group__0__Impl"


    // $ANTLR start "rule__GRelationExpression__Group__1"
    // InternalMCL.g:5310:1: rule__GRelationExpression__Group__1 : rule__GRelationExpression__Group__1__Impl ;
    public final void rule__GRelationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5314:1: ( rule__GRelationExpression__Group__1__Impl )
            // InternalMCL.g:5315:2: rule__GRelationExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group__1"


    // $ANTLR start "rule__GRelationExpression__Group__1__Impl"
    // InternalMCL.g:5321:1: rule__GRelationExpression__Group__1__Impl : ( ( rule__GRelationExpression__Group_1__0 )* ) ;
    public final void rule__GRelationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5325:1: ( ( ( rule__GRelationExpression__Group_1__0 )* ) )
            // InternalMCL.g:5326:1: ( ( rule__GRelationExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5326:1: ( ( rule__GRelationExpression__Group_1__0 )* )
            // InternalMCL.g:5327:2: ( rule__GRelationExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5328:2: ( rule__GRelationExpression__Group_1__0 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( ((LA35_0>=22 && LA35_0<=25)) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalMCL.g:5328:3: rule__GRelationExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_49);
            	    rule__GRelationExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group__1__Impl"


    // $ANTLR start "rule__GRelationExpression__Group_1__0"
    // InternalMCL.g:5337:1: rule__GRelationExpression__Group_1__0 : rule__GRelationExpression__Group_1__0__Impl rule__GRelationExpression__Group_1__1 ;
    public final void rule__GRelationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5341:1: ( rule__GRelationExpression__Group_1__0__Impl rule__GRelationExpression__Group_1__1 )
            // InternalMCL.g:5342:2: rule__GRelationExpression__Group_1__0__Impl rule__GRelationExpression__Group_1__1
            {
            pushFollow(FOLLOW_48);
            rule__GRelationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__0"


    // $ANTLR start "rule__GRelationExpression__Group_1__0__Impl"
    // InternalMCL.g:5349:1: rule__GRelationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GRelationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5353:1: ( ( () ) )
            // InternalMCL.g:5354:1: ( () )
            {
            // InternalMCL.g:5354:1: ( () )
            // InternalMCL.g:5355:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getGRelationExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:5356:2: ()
            // InternalMCL.g:5356:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getGRelationExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GRelationExpression__Group_1__1"
    // InternalMCL.g:5364:1: rule__GRelationExpression__Group_1__1 : rule__GRelationExpression__Group_1__1__Impl rule__GRelationExpression__Group_1__2 ;
    public final void rule__GRelationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5368:1: ( rule__GRelationExpression__Group_1__1__Impl rule__GRelationExpression__Group_1__2 )
            // InternalMCL.g:5369:2: rule__GRelationExpression__Group_1__1__Impl rule__GRelationExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GRelationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__1"


    // $ANTLR start "rule__GRelationExpression__Group_1__1__Impl"
    // InternalMCL.g:5376:1: rule__GRelationExpression__Group_1__1__Impl : ( ( rule__GRelationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GRelationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5380:1: ( ( ( rule__GRelationExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5381:1: ( ( rule__GRelationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5381:1: ( ( rule__GRelationExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5382:2: ( rule__GRelationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5383:2: ( rule__GRelationExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5383:3: rule__GRelationExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GRelationExpression__Group_1__2"
    // InternalMCL.g:5391:1: rule__GRelationExpression__Group_1__2 : rule__GRelationExpression__Group_1__2__Impl ;
    public final void rule__GRelationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5395:1: ( rule__GRelationExpression__Group_1__2__Impl )
            // InternalMCL.g:5396:2: rule__GRelationExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__2"


    // $ANTLR start "rule__GRelationExpression__Group_1__2__Impl"
    // InternalMCL.g:5402:1: rule__GRelationExpression__Group_1__2__Impl : ( ( rule__GRelationExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GRelationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5406:1: ( ( ( rule__GRelationExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5407:1: ( ( rule__GRelationExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5407:1: ( ( rule__GRelationExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5408:2: ( rule__GRelationExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5409:2: ( rule__GRelationExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5409:3: rule__GRelationExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GRelationExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GAdditionExpression__Group__0"
    // InternalMCL.g:5418:1: rule__GAdditionExpression__Group__0 : rule__GAdditionExpression__Group__0__Impl rule__GAdditionExpression__Group__1 ;
    public final void rule__GAdditionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5422:1: ( rule__GAdditionExpression__Group__0__Impl rule__GAdditionExpression__Group__1 )
            // InternalMCL.g:5423:2: rule__GAdditionExpression__Group__0__Impl rule__GAdditionExpression__Group__1
            {
            pushFollow(FOLLOW_50);
            rule__GAdditionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group__0"


    // $ANTLR start "rule__GAdditionExpression__Group__0__Impl"
    // InternalMCL.g:5430:1: rule__GAdditionExpression__Group__0__Impl : ( ruleGMultiplicationExpression ) ;
    public final void rule__GAdditionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5434:1: ( ( ruleGMultiplicationExpression ) )
            // InternalMCL.g:5435:1: ( ruleGMultiplicationExpression )
            {
            // InternalMCL.g:5435:1: ( ruleGMultiplicationExpression )
            // InternalMCL.g:5436:2: ruleGMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getGMultiplicationExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getGMultiplicationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group__0__Impl"


    // $ANTLR start "rule__GAdditionExpression__Group__1"
    // InternalMCL.g:5445:1: rule__GAdditionExpression__Group__1 : rule__GAdditionExpression__Group__1__Impl ;
    public final void rule__GAdditionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5449:1: ( rule__GAdditionExpression__Group__1__Impl )
            // InternalMCL.g:5450:2: rule__GAdditionExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group__1"


    // $ANTLR start "rule__GAdditionExpression__Group__1__Impl"
    // InternalMCL.g:5456:1: rule__GAdditionExpression__Group__1__Impl : ( ( rule__GAdditionExpression__Group_1__0 )* ) ;
    public final void rule__GAdditionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5460:1: ( ( ( rule__GAdditionExpression__Group_1__0 )* ) )
            // InternalMCL.g:5461:1: ( ( rule__GAdditionExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5461:1: ( ( rule__GAdditionExpression__Group_1__0 )* )
            // InternalMCL.g:5462:2: ( rule__GAdditionExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5463:2: ( rule__GAdditionExpression__Group_1__0 )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( ((LA36_0>=26 && LA36_0<=27)) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalMCL.g:5463:3: rule__GAdditionExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_51);
            	    rule__GAdditionExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group__1__Impl"


    // $ANTLR start "rule__GAdditionExpression__Group_1__0"
    // InternalMCL.g:5472:1: rule__GAdditionExpression__Group_1__0 : rule__GAdditionExpression__Group_1__0__Impl rule__GAdditionExpression__Group_1__1 ;
    public final void rule__GAdditionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5476:1: ( rule__GAdditionExpression__Group_1__0__Impl rule__GAdditionExpression__Group_1__1 )
            // InternalMCL.g:5477:2: rule__GAdditionExpression__Group_1__0__Impl rule__GAdditionExpression__Group_1__1
            {
            pushFollow(FOLLOW_50);
            rule__GAdditionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__0"


    // $ANTLR start "rule__GAdditionExpression__Group_1__0__Impl"
    // InternalMCL.g:5484:1: rule__GAdditionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GAdditionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5488:1: ( ( () ) )
            // InternalMCL.g:5489:1: ( () )
            {
            // InternalMCL.g:5489:1: ( () )
            // InternalMCL.g:5490:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getGAdditionExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:5491:2: ()
            // InternalMCL.g:5491:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getGAdditionExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GAdditionExpression__Group_1__1"
    // InternalMCL.g:5499:1: rule__GAdditionExpression__Group_1__1 : rule__GAdditionExpression__Group_1__1__Impl rule__GAdditionExpression__Group_1__2 ;
    public final void rule__GAdditionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5503:1: ( rule__GAdditionExpression__Group_1__1__Impl rule__GAdditionExpression__Group_1__2 )
            // InternalMCL.g:5504:2: rule__GAdditionExpression__Group_1__1__Impl rule__GAdditionExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GAdditionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__1"


    // $ANTLR start "rule__GAdditionExpression__Group_1__1__Impl"
    // InternalMCL.g:5511:1: rule__GAdditionExpression__Group_1__1__Impl : ( ( rule__GAdditionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GAdditionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5515:1: ( ( ( rule__GAdditionExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5516:1: ( ( rule__GAdditionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5516:1: ( ( rule__GAdditionExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5517:2: ( rule__GAdditionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5518:2: ( rule__GAdditionExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5518:3: rule__GAdditionExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GAdditionExpression__Group_1__2"
    // InternalMCL.g:5526:1: rule__GAdditionExpression__Group_1__2 : rule__GAdditionExpression__Group_1__2__Impl ;
    public final void rule__GAdditionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5530:1: ( rule__GAdditionExpression__Group_1__2__Impl )
            // InternalMCL.g:5531:2: rule__GAdditionExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__2"


    // $ANTLR start "rule__GAdditionExpression__Group_1__2__Impl"
    // InternalMCL.g:5537:1: rule__GAdditionExpression__Group_1__2__Impl : ( ( rule__GAdditionExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GAdditionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5541:1: ( ( ( rule__GAdditionExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5542:1: ( ( rule__GAdditionExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5542:1: ( ( rule__GAdditionExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5543:2: ( rule__GAdditionExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5544:2: ( rule__GAdditionExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5544:3: rule__GAdditionExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GAdditionExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GMultiplicationExpression__Group__0"
    // InternalMCL.g:5553:1: rule__GMultiplicationExpression__Group__0 : rule__GMultiplicationExpression__Group__0__Impl rule__GMultiplicationExpression__Group__1 ;
    public final void rule__GMultiplicationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5557:1: ( rule__GMultiplicationExpression__Group__0__Impl rule__GMultiplicationExpression__Group__1 )
            // InternalMCL.g:5558:2: rule__GMultiplicationExpression__Group__0__Impl rule__GMultiplicationExpression__Group__1
            {
            pushFollow(FOLLOW_52);
            rule__GMultiplicationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group__0"


    // $ANTLR start "rule__GMultiplicationExpression__Group__0__Impl"
    // InternalMCL.g:5565:1: rule__GMultiplicationExpression__Group__0__Impl : ( ruleGNegationExpression ) ;
    public final void rule__GMultiplicationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5569:1: ( ( ruleGNegationExpression ) )
            // InternalMCL.g:5570:1: ( ruleGNegationExpression )
            {
            // InternalMCL.g:5570:1: ( ruleGNegationExpression )
            // InternalMCL.g:5571:2: ruleGNegationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getGNegationExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGNegationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getGNegationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group__0__Impl"


    // $ANTLR start "rule__GMultiplicationExpression__Group__1"
    // InternalMCL.g:5580:1: rule__GMultiplicationExpression__Group__1 : rule__GMultiplicationExpression__Group__1__Impl ;
    public final void rule__GMultiplicationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5584:1: ( rule__GMultiplicationExpression__Group__1__Impl )
            // InternalMCL.g:5585:2: rule__GMultiplicationExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group__1"


    // $ANTLR start "rule__GMultiplicationExpression__Group__1__Impl"
    // InternalMCL.g:5591:1: rule__GMultiplicationExpression__Group__1__Impl : ( ( rule__GMultiplicationExpression__Group_1__0 )* ) ;
    public final void rule__GMultiplicationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5595:1: ( ( ( rule__GMultiplicationExpression__Group_1__0 )* ) )
            // InternalMCL.g:5596:1: ( ( rule__GMultiplicationExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5596:1: ( ( rule__GMultiplicationExpression__Group_1__0 )* )
            // InternalMCL.g:5597:2: ( rule__GMultiplicationExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5598:2: ( rule__GMultiplicationExpression__Group_1__0 )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( ((LA37_0>=28 && LA37_0<=29)) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalMCL.g:5598:3: rule__GMultiplicationExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_53);
            	    rule__GMultiplicationExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group__1__Impl"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__0"
    // InternalMCL.g:5607:1: rule__GMultiplicationExpression__Group_1__0 : rule__GMultiplicationExpression__Group_1__0__Impl rule__GMultiplicationExpression__Group_1__1 ;
    public final void rule__GMultiplicationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5611:1: ( rule__GMultiplicationExpression__Group_1__0__Impl rule__GMultiplicationExpression__Group_1__1 )
            // InternalMCL.g:5612:2: rule__GMultiplicationExpression__Group_1__0__Impl rule__GMultiplicationExpression__Group_1__1
            {
            pushFollow(FOLLOW_52);
            rule__GMultiplicationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__0"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__0__Impl"
    // InternalMCL.g:5619:1: rule__GMultiplicationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GMultiplicationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5623:1: ( ( () ) )
            // InternalMCL.g:5624:1: ( () )
            {
            // InternalMCL.g:5624:1: ( () )
            // InternalMCL.g:5625:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getGMultiplicationExpressionLeftOperandAction_1_0()); 
            }
            // InternalMCL.g:5626:2: ()
            // InternalMCL.g:5626:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getGMultiplicationExpressionLeftOperandAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__1"
    // InternalMCL.g:5634:1: rule__GMultiplicationExpression__Group_1__1 : rule__GMultiplicationExpression__Group_1__1__Impl rule__GMultiplicationExpression__Group_1__2 ;
    public final void rule__GMultiplicationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5638:1: ( rule__GMultiplicationExpression__Group_1__1__Impl rule__GMultiplicationExpression__Group_1__2 )
            // InternalMCL.g:5639:2: rule__GMultiplicationExpression__Group_1__1__Impl rule__GMultiplicationExpression__Group_1__2
            {
            pushFollow(FOLLOW_43);
            rule__GMultiplicationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__1"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__1__Impl"
    // InternalMCL.g:5646:1: rule__GMultiplicationExpression__Group_1__1__Impl : ( ( rule__GMultiplicationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GMultiplicationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5650:1: ( ( ( rule__GMultiplicationExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5651:1: ( ( rule__GMultiplicationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5651:1: ( ( rule__GMultiplicationExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5652:2: ( rule__GMultiplicationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5653:2: ( rule__GMultiplicationExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5653:3: rule__GMultiplicationExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__2"
    // InternalMCL.g:5661:1: rule__GMultiplicationExpression__Group_1__2 : rule__GMultiplicationExpression__Group_1__2__Impl ;
    public final void rule__GMultiplicationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5665:1: ( rule__GMultiplicationExpression__Group_1__2__Impl )
            // InternalMCL.g:5666:2: rule__GMultiplicationExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__2"


    // $ANTLR start "rule__GMultiplicationExpression__Group_1__2__Impl"
    // InternalMCL.g:5672:1: rule__GMultiplicationExpression__Group_1__2__Impl : ( ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 ) ) ;
    public final void rule__GMultiplicationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5676:1: ( ( ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 ) ) )
            // InternalMCL.g:5677:1: ( ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5677:1: ( ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 ) )
            // InternalMCL.g:5678:2: ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getRightOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5679:2: ( rule__GMultiplicationExpression__RightOperandAssignment_1_2 )
            // InternalMCL.g:5679:3: rule__GMultiplicationExpression__RightOperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GMultiplicationExpression__RightOperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getRightOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GNegationExpression__Group_1__0"
    // InternalMCL.g:5688:1: rule__GNegationExpression__Group_1__0 : rule__GNegationExpression__Group_1__0__Impl rule__GNegationExpression__Group_1__1 ;
    public final void rule__GNegationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5692:1: ( rule__GNegationExpression__Group_1__0__Impl rule__GNegationExpression__Group_1__1 )
            // InternalMCL.g:5693:2: rule__GNegationExpression__Group_1__0__Impl rule__GNegationExpression__Group_1__1
            {
            pushFollow(FOLLOW_43);
            rule__GNegationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__0"


    // $ANTLR start "rule__GNegationExpression__Group_1__0__Impl"
    // InternalMCL.g:5700:1: rule__GNegationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GNegationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5704:1: ( ( () ) )
            // InternalMCL.g:5705:1: ( () )
            {
            // InternalMCL.g:5705:1: ( () )
            // InternalMCL.g:5706:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getGNegationExpressionAction_1_0()); 
            }
            // InternalMCL.g:5707:2: ()
            // InternalMCL.g:5707:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getGNegationExpressionAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GNegationExpression__Group_1__1"
    // InternalMCL.g:5715:1: rule__GNegationExpression__Group_1__1 : rule__GNegationExpression__Group_1__1__Impl rule__GNegationExpression__Group_1__2 ;
    public final void rule__GNegationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5719:1: ( rule__GNegationExpression__Group_1__1__Impl rule__GNegationExpression__Group_1__2 )
            // InternalMCL.g:5720:2: rule__GNegationExpression__Group_1__1__Impl rule__GNegationExpression__Group_1__2
            {
            pushFollow(FOLLOW_54);
            rule__GNegationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__1"


    // $ANTLR start "rule__GNegationExpression__Group_1__1__Impl"
    // InternalMCL.g:5727:1: rule__GNegationExpression__Group_1__1__Impl : ( ( rule__GNegationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__GNegationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5731:1: ( ( ( rule__GNegationExpression__OperatorAssignment_1_1 ) ) )
            // InternalMCL.g:5732:1: ( ( rule__GNegationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalMCL.g:5732:1: ( ( rule__GNegationExpression__OperatorAssignment_1_1 ) )
            // InternalMCL.g:5733:2: ( rule__GNegationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalMCL.g:5734:2: ( rule__GNegationExpression__OperatorAssignment_1_1 )
            // InternalMCL.g:5734:3: rule__GNegationExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GNegationExpression__Group_1__2"
    // InternalMCL.g:5742:1: rule__GNegationExpression__Group_1__2 : rule__GNegationExpression__Group_1__2__Impl ;
    public final void rule__GNegationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5746:1: ( rule__GNegationExpression__Group_1__2__Impl )
            // InternalMCL.g:5747:2: rule__GNegationExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__2"


    // $ANTLR start "rule__GNegationExpression__Group_1__2__Impl"
    // InternalMCL.g:5753:1: rule__GNegationExpression__Group_1__2__Impl : ( ( rule__GNegationExpression__OperandAssignment_1_2 ) ) ;
    public final void rule__GNegationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5757:1: ( ( ( rule__GNegationExpression__OperandAssignment_1_2 ) ) )
            // InternalMCL.g:5758:1: ( ( rule__GNegationExpression__OperandAssignment_1_2 ) )
            {
            // InternalMCL.g:5758:1: ( ( rule__GNegationExpression__OperandAssignment_1_2 ) )
            // InternalMCL.g:5759:2: ( rule__GNegationExpression__OperandAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getOperandAssignment_1_2()); 
            }
            // InternalMCL.g:5760:2: ( rule__GNegationExpression__OperandAssignment_1_2 )
            // InternalMCL.g:5760:3: rule__GNegationExpression__OperandAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GNegationExpression__OperandAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getOperandAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GNavigationExpression__Group__0"
    // InternalMCL.g:5769:1: rule__GNavigationExpression__Group__0 : rule__GNavigationExpression__Group__0__Impl rule__GNavigationExpression__Group__1 ;
    public final void rule__GNavigationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5773:1: ( rule__GNavigationExpression__Group__0__Impl rule__GNavigationExpression__Group__1 )
            // InternalMCL.g:5774:2: rule__GNavigationExpression__Group__0__Impl rule__GNavigationExpression__Group__1
            {
            pushFollow(FOLLOW_55);
            rule__GNavigationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group__0"


    // $ANTLR start "rule__GNavigationExpression__Group__0__Impl"
    // InternalMCL.g:5781:1: rule__GNavigationExpression__Group__0__Impl : ( ruleGReferenceExpression ) ;
    public final void rule__GNavigationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5785:1: ( ( ruleGReferenceExpression ) )
            // InternalMCL.g:5786:1: ( ruleGReferenceExpression )
            {
            // InternalMCL.g:5786:1: ( ruleGReferenceExpression )
            // InternalMCL.g:5787:2: ruleGReferenceExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getGReferenceExpressionParserRuleCall_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGReferenceExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getGReferenceExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group__0__Impl"


    // $ANTLR start "rule__GNavigationExpression__Group__1"
    // InternalMCL.g:5796:1: rule__GNavigationExpression__Group__1 : rule__GNavigationExpression__Group__1__Impl ;
    public final void rule__GNavigationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5800:1: ( rule__GNavigationExpression__Group__1__Impl )
            // InternalMCL.g:5801:2: rule__GNavigationExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group__1"


    // $ANTLR start "rule__GNavigationExpression__Group__1__Impl"
    // InternalMCL.g:5807:1: rule__GNavigationExpression__Group__1__Impl : ( ( rule__GNavigationExpression__Group_1__0 )* ) ;
    public final void rule__GNavigationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5811:1: ( ( ( rule__GNavigationExpression__Group_1__0 )* ) )
            // InternalMCL.g:5812:1: ( ( rule__GNavigationExpression__Group_1__0 )* )
            {
            // InternalMCL.g:5812:1: ( ( rule__GNavigationExpression__Group_1__0 )* )
            // InternalMCL.g:5813:2: ( rule__GNavigationExpression__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getGroup_1()); 
            }
            // InternalMCL.g:5814:2: ( rule__GNavigationExpression__Group_1__0 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( ((LA38_0>=18 && LA38_0<=19)) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalMCL.g:5814:3: rule__GNavigationExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_56);
            	    rule__GNavigationExpression__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group__1__Impl"


    // $ANTLR start "rule__GNavigationExpression__Group_1__0"
    // InternalMCL.g:5823:1: rule__GNavigationExpression__Group_1__0 : rule__GNavigationExpression__Group_1__0__Impl rule__GNavigationExpression__Group_1__1 ;
    public final void rule__GNavigationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5827:1: ( rule__GNavigationExpression__Group_1__0__Impl rule__GNavigationExpression__Group_1__1 )
            // InternalMCL.g:5828:2: rule__GNavigationExpression__Group_1__0__Impl rule__GNavigationExpression__Group_1__1
            {
            pushFollow(FOLLOW_55);
            rule__GNavigationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__0"


    // $ANTLR start "rule__GNavigationExpression__Group_1__0__Impl"
    // InternalMCL.g:5835:1: rule__GNavigationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GNavigationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5839:1: ( ( () ) )
            // InternalMCL.g:5840:1: ( () )
            {
            // InternalMCL.g:5840:1: ( () )
            // InternalMCL.g:5841:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getGNavigationExpressionBodyAction_1_0()); 
            }
            // InternalMCL.g:5842:2: ()
            // InternalMCL.g:5842:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getGNavigationExpressionBodyAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GNavigationExpression__Group_1__1"
    // InternalMCL.g:5850:1: rule__GNavigationExpression__Group_1__1 : rule__GNavigationExpression__Group_1__1__Impl rule__GNavigationExpression__Group_1__2 ;
    public final void rule__GNavigationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5854:1: ( rule__GNavigationExpression__Group_1__1__Impl rule__GNavigationExpression__Group_1__2 )
            // InternalMCL.g:5855:2: rule__GNavigationExpression__Group_1__1__Impl rule__GNavigationExpression__Group_1__2
            {
            pushFollow(FOLLOW_8);
            rule__GNavigationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__1"


    // $ANTLR start "rule__GNavigationExpression__Group_1__1__Impl"
    // InternalMCL.g:5862:1: rule__GNavigationExpression__Group_1__1__Impl : ( ruleNavigationOperator ) ;
    public final void rule__GNavigationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5866:1: ( ( ruleNavigationOperator ) )
            // InternalMCL.g:5867:1: ( ruleNavigationOperator )
            {
            // InternalMCL.g:5867:1: ( ruleNavigationOperator )
            // InternalMCL.g:5868:2: ruleNavigationOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getNavigationOperatorParserRuleCall_1_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleNavigationOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getNavigationOperatorParserRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GNavigationExpression__Group_1__2"
    // InternalMCL.g:5877:1: rule__GNavigationExpression__Group_1__2 : rule__GNavigationExpression__Group_1__2__Impl ;
    public final void rule__GNavigationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5881:1: ( rule__GNavigationExpression__Group_1__2__Impl )
            // InternalMCL.g:5882:2: rule__GNavigationExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__2"


    // $ANTLR start "rule__GNavigationExpression__Group_1__2__Impl"
    // InternalMCL.g:5888:1: rule__GNavigationExpression__Group_1__2__Impl : ( ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 ) ) ;
    public final void rule__GNavigationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5892:1: ( ( ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 ) ) )
            // InternalMCL.g:5893:1: ( ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 ) )
            {
            // InternalMCL.g:5893:1: ( ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 ) )
            // InternalMCL.g:5894:2: ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectAssignment_1_2()); 
            }
            // InternalMCL.g:5895:2: ( rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 )
            // InternalMCL.g:5895:3: rule__GNavigationExpression__ReferencedEObjectAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__GNavigationExpression__ReferencedEObjectAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__GReferenceExpression__Group_1__0"
    // InternalMCL.g:5904:1: rule__GReferenceExpression__Group_1__0 : rule__GReferenceExpression__Group_1__0__Impl rule__GReferenceExpression__Group_1__1 ;
    public final void rule__GReferenceExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5908:1: ( rule__GReferenceExpression__Group_1__0__Impl rule__GReferenceExpression__Group_1__1 )
            // InternalMCL.g:5909:2: rule__GReferenceExpression__Group_1__0__Impl rule__GReferenceExpression__Group_1__1
            {
            pushFollow(FOLLOW_54);
            rule__GReferenceExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GReferenceExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__Group_1__0"


    // $ANTLR start "rule__GReferenceExpression__Group_1__0__Impl"
    // InternalMCL.g:5916:1: rule__GReferenceExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__GReferenceExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5920:1: ( ( () ) )
            // InternalMCL.g:5921:1: ( () )
            {
            // InternalMCL.g:5921:1: ( () )
            // InternalMCL.g:5922:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionAccess().getGReferenceExpressionAction_1_0()); 
            }
            // InternalMCL.g:5923:2: ()
            // InternalMCL.g:5923:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionAccess().getGReferenceExpressionAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__Group_1__0__Impl"


    // $ANTLR start "rule__GReferenceExpression__Group_1__1"
    // InternalMCL.g:5931:1: rule__GReferenceExpression__Group_1__1 : rule__GReferenceExpression__Group_1__1__Impl ;
    public final void rule__GReferenceExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5935:1: ( rule__GReferenceExpression__Group_1__1__Impl )
            // InternalMCL.g:5936:2: rule__GReferenceExpression__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GReferenceExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__Group_1__1"


    // $ANTLR start "rule__GReferenceExpression__Group_1__1__Impl"
    // InternalMCL.g:5942:1: rule__GReferenceExpression__Group_1__1__Impl : ( ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 ) ) ;
    public final void rule__GReferenceExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5946:1: ( ( ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 ) ) )
            // InternalMCL.g:5947:1: ( ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 ) )
            {
            // InternalMCL.g:5947:1: ( ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 ) )
            // InternalMCL.g:5948:2: ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectAssignment_1_1()); 
            }
            // InternalMCL.g:5949:2: ( rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 )
            // InternalMCL.g:5949:3: rule__GReferenceExpression__ReferencedEObjectAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__GReferenceExpression__ReferencedEObjectAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__Group_1__1__Impl"


    // $ANTLR start "rule__GStringExpression__Group__0"
    // InternalMCL.g:5958:1: rule__GStringExpression__Group__0 : rule__GStringExpression__Group__0__Impl rule__GStringExpression__Group__1 ;
    public final void rule__GStringExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5962:1: ( rule__GStringExpression__Group__0__Impl rule__GStringExpression__Group__1 )
            // InternalMCL.g:5963:2: rule__GStringExpression__Group__0__Impl rule__GStringExpression__Group__1
            {
            pushFollow(FOLLOW_42);
            rule__GStringExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GStringExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GStringExpression__Group__0"


    // $ANTLR start "rule__GStringExpression__Group__0__Impl"
    // InternalMCL.g:5970:1: rule__GStringExpression__Group__0__Impl : ( () ) ;
    public final void rule__GStringExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5974:1: ( ( () ) )
            // InternalMCL.g:5975:1: ( () )
            {
            // InternalMCL.g:5975:1: ( () )
            // InternalMCL.g:5976:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGStringExpressionAccess().getGStringExpressionAction_0()); 
            }
            // InternalMCL.g:5977:2: ()
            // InternalMCL.g:5977:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGStringExpressionAccess().getGStringExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GStringExpression__Group__0__Impl"


    // $ANTLR start "rule__GStringExpression__Group__1"
    // InternalMCL.g:5985:1: rule__GStringExpression__Group__1 : rule__GStringExpression__Group__1__Impl ;
    public final void rule__GStringExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:5989:1: ( rule__GStringExpression__Group__1__Impl )
            // InternalMCL.g:5990:2: rule__GStringExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GStringExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GStringExpression__Group__1"


    // $ANTLR start "rule__GStringExpression__Group__1__Impl"
    // InternalMCL.g:5996:1: rule__GStringExpression__Group__1__Impl : ( ( rule__GStringExpression__ValueAssignment_1 ) ) ;
    public final void rule__GStringExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6000:1: ( ( ( rule__GStringExpression__ValueAssignment_1 ) ) )
            // InternalMCL.g:6001:1: ( ( rule__GStringExpression__ValueAssignment_1 ) )
            {
            // InternalMCL.g:6001:1: ( ( rule__GStringExpression__ValueAssignment_1 ) )
            // InternalMCL.g:6002:2: ( rule__GStringExpression__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGStringExpressionAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:6003:2: ( rule__GStringExpression__ValueAssignment_1 )
            // InternalMCL.g:6003:3: rule__GStringExpression__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GStringExpression__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGStringExpressionAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GStringExpression__Group__1__Impl"


    // $ANTLR start "rule__GBooleanExpression__Group__0"
    // InternalMCL.g:6012:1: rule__GBooleanExpression__Group__0 : rule__GBooleanExpression__Group__0__Impl rule__GBooleanExpression__Group__1 ;
    public final void rule__GBooleanExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6016:1: ( rule__GBooleanExpression__Group__0__Impl rule__GBooleanExpression__Group__1 )
            // InternalMCL.g:6017:2: rule__GBooleanExpression__Group__0__Impl rule__GBooleanExpression__Group__1
            {
            pushFollow(FOLLOW_57);
            rule__GBooleanExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GBooleanExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBooleanExpression__Group__0"


    // $ANTLR start "rule__GBooleanExpression__Group__0__Impl"
    // InternalMCL.g:6024:1: rule__GBooleanExpression__Group__0__Impl : ( () ) ;
    public final void rule__GBooleanExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6028:1: ( ( () ) )
            // InternalMCL.g:6029:1: ( () )
            {
            // InternalMCL.g:6029:1: ( () )
            // InternalMCL.g:6030:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBooleanExpressionAccess().getGBooleanExpressionAction_0()); 
            }
            // InternalMCL.g:6031:2: ()
            // InternalMCL.g:6031:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBooleanExpressionAccess().getGBooleanExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBooleanExpression__Group__0__Impl"


    // $ANTLR start "rule__GBooleanExpression__Group__1"
    // InternalMCL.g:6039:1: rule__GBooleanExpression__Group__1 : rule__GBooleanExpression__Group__1__Impl ;
    public final void rule__GBooleanExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6043:1: ( rule__GBooleanExpression__Group__1__Impl )
            // InternalMCL.g:6044:2: rule__GBooleanExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GBooleanExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBooleanExpression__Group__1"


    // $ANTLR start "rule__GBooleanExpression__Group__1__Impl"
    // InternalMCL.g:6050:1: rule__GBooleanExpression__Group__1__Impl : ( ( rule__GBooleanExpression__ValueAssignment_1 ) ) ;
    public final void rule__GBooleanExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6054:1: ( ( ( rule__GBooleanExpression__ValueAssignment_1 ) ) )
            // InternalMCL.g:6055:1: ( ( rule__GBooleanExpression__ValueAssignment_1 ) )
            {
            // InternalMCL.g:6055:1: ( ( rule__GBooleanExpression__ValueAssignment_1 ) )
            // InternalMCL.g:6056:2: ( rule__GBooleanExpression__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBooleanExpressionAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:6057:2: ( rule__GBooleanExpression__ValueAssignment_1 )
            // InternalMCL.g:6057:3: rule__GBooleanExpression__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GBooleanExpression__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBooleanExpressionAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBooleanExpression__Group__1__Impl"


    // $ANTLR start "rule__GIntegerExpression__Group__0"
    // InternalMCL.g:6066:1: rule__GIntegerExpression__Group__0 : rule__GIntegerExpression__Group__0__Impl rule__GIntegerExpression__Group__1 ;
    public final void rule__GIntegerExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6070:1: ( rule__GIntegerExpression__Group__0__Impl rule__GIntegerExpression__Group__1 )
            // InternalMCL.g:6071:2: rule__GIntegerExpression__Group__0__Impl rule__GIntegerExpression__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__GIntegerExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIntegerExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIntegerExpression__Group__0"


    // $ANTLR start "rule__GIntegerExpression__Group__0__Impl"
    // InternalMCL.g:6078:1: rule__GIntegerExpression__Group__0__Impl : ( () ) ;
    public final void rule__GIntegerExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6082:1: ( ( () ) )
            // InternalMCL.g:6083:1: ( () )
            {
            // InternalMCL.g:6083:1: ( () )
            // InternalMCL.g:6084:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIntegerExpressionAccess().getGIntegerExpressionAction_0()); 
            }
            // InternalMCL.g:6085:2: ()
            // InternalMCL.g:6085:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIntegerExpressionAccess().getGIntegerExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIntegerExpression__Group__0__Impl"


    // $ANTLR start "rule__GIntegerExpression__Group__1"
    // InternalMCL.g:6093:1: rule__GIntegerExpression__Group__1 : rule__GIntegerExpression__Group__1__Impl ;
    public final void rule__GIntegerExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6097:1: ( rule__GIntegerExpression__Group__1__Impl )
            // InternalMCL.g:6098:2: rule__GIntegerExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GIntegerExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIntegerExpression__Group__1"


    // $ANTLR start "rule__GIntegerExpression__Group__1__Impl"
    // InternalMCL.g:6104:1: rule__GIntegerExpression__Group__1__Impl : ( ( rule__GIntegerExpression__ValueAssignment_1 ) ) ;
    public final void rule__GIntegerExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6108:1: ( ( ( rule__GIntegerExpression__ValueAssignment_1 ) ) )
            // InternalMCL.g:6109:1: ( ( rule__GIntegerExpression__ValueAssignment_1 ) )
            {
            // InternalMCL.g:6109:1: ( ( rule__GIntegerExpression__ValueAssignment_1 ) )
            // InternalMCL.g:6110:2: ( rule__GIntegerExpression__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIntegerExpressionAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:6111:2: ( rule__GIntegerExpression__ValueAssignment_1 )
            // InternalMCL.g:6111:3: rule__GIntegerExpression__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GIntegerExpression__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIntegerExpressionAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIntegerExpression__Group__1__Impl"


    // $ANTLR start "rule__GDoubleExpression__Group__0"
    // InternalMCL.g:6120:1: rule__GDoubleExpression__Group__0 : rule__GDoubleExpression__Group__0__Impl rule__GDoubleExpression__Group__1 ;
    public final void rule__GDoubleExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6124:1: ( rule__GDoubleExpression__Group__0__Impl rule__GDoubleExpression__Group__1 )
            // InternalMCL.g:6125:2: rule__GDoubleExpression__Group__0__Impl rule__GDoubleExpression__Group__1
            {
            pushFollow(FOLLOW_58);
            rule__GDoubleExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GDoubleExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GDoubleExpression__Group__0"


    // $ANTLR start "rule__GDoubleExpression__Group__0__Impl"
    // InternalMCL.g:6132:1: rule__GDoubleExpression__Group__0__Impl : ( () ) ;
    public final void rule__GDoubleExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6136:1: ( ( () ) )
            // InternalMCL.g:6137:1: ( () )
            {
            // InternalMCL.g:6137:1: ( () )
            // InternalMCL.g:6138:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGDoubleExpressionAccess().getGDoubleExpressionAction_0()); 
            }
            // InternalMCL.g:6139:2: ()
            // InternalMCL.g:6139:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGDoubleExpressionAccess().getGDoubleExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GDoubleExpression__Group__0__Impl"


    // $ANTLR start "rule__GDoubleExpression__Group__1"
    // InternalMCL.g:6147:1: rule__GDoubleExpression__Group__1 : rule__GDoubleExpression__Group__1__Impl ;
    public final void rule__GDoubleExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6151:1: ( rule__GDoubleExpression__Group__1__Impl )
            // InternalMCL.g:6152:2: rule__GDoubleExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GDoubleExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GDoubleExpression__Group__1"


    // $ANTLR start "rule__GDoubleExpression__Group__1__Impl"
    // InternalMCL.g:6158:1: rule__GDoubleExpression__Group__1__Impl : ( ( rule__GDoubleExpression__ValueAssignment_1 ) ) ;
    public final void rule__GDoubleExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6162:1: ( ( ( rule__GDoubleExpression__ValueAssignment_1 ) ) )
            // InternalMCL.g:6163:1: ( ( rule__GDoubleExpression__ValueAssignment_1 ) )
            {
            // InternalMCL.g:6163:1: ( ( rule__GDoubleExpression__ValueAssignment_1 ) )
            // InternalMCL.g:6164:2: ( rule__GDoubleExpression__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGDoubleExpressionAccess().getValueAssignment_1()); 
            }
            // InternalMCL.g:6165:2: ( rule__GDoubleExpression__ValueAssignment_1 )
            // InternalMCL.g:6165:3: rule__GDoubleExpression__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GDoubleExpression__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGDoubleExpressionAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GDoubleExpression__Group__1__Impl"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__0"
    // InternalMCL.g:6174:1: rule__GEnumLiteralExpression__Group__0 : rule__GEnumLiteralExpression__Group__0__Impl rule__GEnumLiteralExpression__Group__1 ;
    public final void rule__GEnumLiteralExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6178:1: ( rule__GEnumLiteralExpression__Group__0__Impl rule__GEnumLiteralExpression__Group__1 )
            // InternalMCL.g:6179:2: rule__GEnumLiteralExpression__Group__0__Impl rule__GEnumLiteralExpression__Group__1
            {
            pushFollow(FOLLOW_59);
            rule__GEnumLiteralExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GEnumLiteralExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__0"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__0__Impl"
    // InternalMCL.g:6186:1: rule__GEnumLiteralExpression__Group__0__Impl : ( () ) ;
    public final void rule__GEnumLiteralExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6190:1: ( ( () ) )
            // InternalMCL.g:6191:1: ( () )
            {
            // InternalMCL.g:6191:1: ( () )
            // InternalMCL.g:6192:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getGEnumLiteralExpressionAction_0()); 
            }
            // InternalMCL.g:6193:2: ()
            // InternalMCL.g:6193:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getGEnumLiteralExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__0__Impl"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__1"
    // InternalMCL.g:6201:1: rule__GEnumLiteralExpression__Group__1 : rule__GEnumLiteralExpression__Group__1__Impl rule__GEnumLiteralExpression__Group__2 ;
    public final void rule__GEnumLiteralExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6205:1: ( rule__GEnumLiteralExpression__Group__1__Impl rule__GEnumLiteralExpression__Group__2 )
            // InternalMCL.g:6206:2: rule__GEnumLiteralExpression__Group__1__Impl rule__GEnumLiteralExpression__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__GEnumLiteralExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GEnumLiteralExpression__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__1"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__1__Impl"
    // InternalMCL.g:6213:1: rule__GEnumLiteralExpression__Group__1__Impl : ( '#' ) ;
    public final void rule__GEnumLiteralExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6217:1: ( ( '#' ) )
            // InternalMCL.g:6218:1: ( '#' )
            {
            // InternalMCL.g:6218:1: ( '#' )
            // InternalMCL.g:6219:2: '#'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getNumberSignKeyword_1()); 
            }
            match(input,57,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getNumberSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__1__Impl"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__2"
    // InternalMCL.g:6228:1: rule__GEnumLiteralExpression__Group__2 : rule__GEnumLiteralExpression__Group__2__Impl ;
    public final void rule__GEnumLiteralExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6232:1: ( rule__GEnumLiteralExpression__Group__2__Impl )
            // InternalMCL.g:6233:2: rule__GEnumLiteralExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GEnumLiteralExpression__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__2"


    // $ANTLR start "rule__GEnumLiteralExpression__Group__2__Impl"
    // InternalMCL.g:6239:1: rule__GEnumLiteralExpression__Group__2__Impl : ( ( rule__GEnumLiteralExpression__ValueAssignment_2 ) ) ;
    public final void rule__GEnumLiteralExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6243:1: ( ( ( rule__GEnumLiteralExpression__ValueAssignment_2 ) ) )
            // InternalMCL.g:6244:1: ( ( rule__GEnumLiteralExpression__ValueAssignment_2 ) )
            {
            // InternalMCL.g:6244:1: ( ( rule__GEnumLiteralExpression__ValueAssignment_2 ) )
            // InternalMCL.g:6245:2: ( rule__GEnumLiteralExpression__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getValueAssignment_2()); 
            }
            // InternalMCL.g:6246:2: ( rule__GEnumLiteralExpression__ValueAssignment_2 )
            // InternalMCL.g:6246:3: rule__GEnumLiteralExpression__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__GEnumLiteralExpression__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__Group__2__Impl"


    // $ANTLR start "rule__GIfExpression__Group__0"
    // InternalMCL.g:6255:1: rule__GIfExpression__Group__0 : rule__GIfExpression__Group__0__Impl rule__GIfExpression__Group__1 ;
    public final void rule__GIfExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6259:1: ( rule__GIfExpression__Group__0__Impl rule__GIfExpression__Group__1 )
            // InternalMCL.g:6260:2: rule__GIfExpression__Group__0__Impl rule__GIfExpression__Group__1
            {
            pushFollow(FOLLOW_60);
            rule__GIfExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__0"


    // $ANTLR start "rule__GIfExpression__Group__0__Impl"
    // InternalMCL.g:6267:1: rule__GIfExpression__Group__0__Impl : ( () ) ;
    public final void rule__GIfExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6271:1: ( ( () ) )
            // InternalMCL.g:6272:1: ( () )
            {
            // InternalMCL.g:6272:1: ( () )
            // InternalMCL.g:6273:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getGIfExpressionAction_0()); 
            }
            // InternalMCL.g:6274:2: ()
            // InternalMCL.g:6274:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getGIfExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__0__Impl"


    // $ANTLR start "rule__GIfExpression__Group__1"
    // InternalMCL.g:6282:1: rule__GIfExpression__Group__1 : rule__GIfExpression__Group__1__Impl rule__GIfExpression__Group__2 ;
    public final void rule__GIfExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6286:1: ( rule__GIfExpression__Group__1__Impl rule__GIfExpression__Group__2 )
            // InternalMCL.g:6287:2: rule__GIfExpression__Group__1__Impl rule__GIfExpression__Group__2
            {
            pushFollow(FOLLOW_43);
            rule__GIfExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__1"


    // $ANTLR start "rule__GIfExpression__Group__1__Impl"
    // InternalMCL.g:6294:1: rule__GIfExpression__Group__1__Impl : ( 'if' ) ;
    public final void rule__GIfExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6298:1: ( ( 'if' ) )
            // InternalMCL.g:6299:1: ( 'if' )
            {
            // InternalMCL.g:6299:1: ( 'if' )
            // InternalMCL.g:6300:2: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getIfKeyword_1()); 
            }
            match(input,58,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getIfKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__1__Impl"


    // $ANTLR start "rule__GIfExpression__Group__2"
    // InternalMCL.g:6309:1: rule__GIfExpression__Group__2 : rule__GIfExpression__Group__2__Impl rule__GIfExpression__Group__3 ;
    public final void rule__GIfExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6313:1: ( rule__GIfExpression__Group__2__Impl rule__GIfExpression__Group__3 )
            // InternalMCL.g:6314:2: rule__GIfExpression__Group__2__Impl rule__GIfExpression__Group__3
            {
            pushFollow(FOLLOW_61);
            rule__GIfExpression__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__2"


    // $ANTLR start "rule__GIfExpression__Group__2__Impl"
    // InternalMCL.g:6321:1: rule__GIfExpression__Group__2__Impl : ( ( rule__GIfExpression__ConditionAssignment_2 ) ) ;
    public final void rule__GIfExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6325:1: ( ( ( rule__GIfExpression__ConditionAssignment_2 ) ) )
            // InternalMCL.g:6326:1: ( ( rule__GIfExpression__ConditionAssignment_2 ) )
            {
            // InternalMCL.g:6326:1: ( ( rule__GIfExpression__ConditionAssignment_2 ) )
            // InternalMCL.g:6327:2: ( rule__GIfExpression__ConditionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getConditionAssignment_2()); 
            }
            // InternalMCL.g:6328:2: ( rule__GIfExpression__ConditionAssignment_2 )
            // InternalMCL.g:6328:3: rule__GIfExpression__ConditionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__GIfExpression__ConditionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getConditionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__2__Impl"


    // $ANTLR start "rule__GIfExpression__Group__3"
    // InternalMCL.g:6336:1: rule__GIfExpression__Group__3 : rule__GIfExpression__Group__3__Impl rule__GIfExpression__Group__4 ;
    public final void rule__GIfExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6340:1: ( rule__GIfExpression__Group__3__Impl rule__GIfExpression__Group__4 )
            // InternalMCL.g:6341:2: rule__GIfExpression__Group__3__Impl rule__GIfExpression__Group__4
            {
            pushFollow(FOLLOW_43);
            rule__GIfExpression__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__3"


    // $ANTLR start "rule__GIfExpression__Group__3__Impl"
    // InternalMCL.g:6348:1: rule__GIfExpression__Group__3__Impl : ( 'then' ) ;
    public final void rule__GIfExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6352:1: ( ( 'then' ) )
            // InternalMCL.g:6353:1: ( 'then' )
            {
            // InternalMCL.g:6353:1: ( 'then' )
            // InternalMCL.g:6354:2: 'then'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getThenKeyword_3()); 
            }
            match(input,59,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getThenKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__3__Impl"


    // $ANTLR start "rule__GIfExpression__Group__4"
    // InternalMCL.g:6363:1: rule__GIfExpression__Group__4 : rule__GIfExpression__Group__4__Impl rule__GIfExpression__Group__5 ;
    public final void rule__GIfExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6367:1: ( rule__GIfExpression__Group__4__Impl rule__GIfExpression__Group__5 )
            // InternalMCL.g:6368:2: rule__GIfExpression__Group__4__Impl rule__GIfExpression__Group__5
            {
            pushFollow(FOLLOW_62);
            rule__GIfExpression__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__4"


    // $ANTLR start "rule__GIfExpression__Group__4__Impl"
    // InternalMCL.g:6375:1: rule__GIfExpression__Group__4__Impl : ( ( rule__GIfExpression__ThenExpressionAssignment_4 ) ) ;
    public final void rule__GIfExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6379:1: ( ( ( rule__GIfExpression__ThenExpressionAssignment_4 ) ) )
            // InternalMCL.g:6380:1: ( ( rule__GIfExpression__ThenExpressionAssignment_4 ) )
            {
            // InternalMCL.g:6380:1: ( ( rule__GIfExpression__ThenExpressionAssignment_4 ) )
            // InternalMCL.g:6381:2: ( rule__GIfExpression__ThenExpressionAssignment_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getThenExpressionAssignment_4()); 
            }
            // InternalMCL.g:6382:2: ( rule__GIfExpression__ThenExpressionAssignment_4 )
            // InternalMCL.g:6382:3: rule__GIfExpression__ThenExpressionAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__GIfExpression__ThenExpressionAssignment_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getThenExpressionAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__4__Impl"


    // $ANTLR start "rule__GIfExpression__Group__5"
    // InternalMCL.g:6390:1: rule__GIfExpression__Group__5 : rule__GIfExpression__Group__5__Impl rule__GIfExpression__Group__6 ;
    public final void rule__GIfExpression__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6394:1: ( rule__GIfExpression__Group__5__Impl rule__GIfExpression__Group__6 )
            // InternalMCL.g:6395:2: rule__GIfExpression__Group__5__Impl rule__GIfExpression__Group__6
            {
            pushFollow(FOLLOW_43);
            rule__GIfExpression__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__5"


    // $ANTLR start "rule__GIfExpression__Group__5__Impl"
    // InternalMCL.g:6402:1: rule__GIfExpression__Group__5__Impl : ( 'else' ) ;
    public final void rule__GIfExpression__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6406:1: ( ( 'else' ) )
            // InternalMCL.g:6407:1: ( 'else' )
            {
            // InternalMCL.g:6407:1: ( 'else' )
            // InternalMCL.g:6408:2: 'else'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getElseKeyword_5()); 
            }
            match(input,60,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getElseKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__5__Impl"


    // $ANTLR start "rule__GIfExpression__Group__6"
    // InternalMCL.g:6417:1: rule__GIfExpression__Group__6 : rule__GIfExpression__Group__6__Impl rule__GIfExpression__Group__7 ;
    public final void rule__GIfExpression__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6421:1: ( rule__GIfExpression__Group__6__Impl rule__GIfExpression__Group__7 )
            // InternalMCL.g:6422:2: rule__GIfExpression__Group__6__Impl rule__GIfExpression__Group__7
            {
            pushFollow(FOLLOW_63);
            rule__GIfExpression__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__6"


    // $ANTLR start "rule__GIfExpression__Group__6__Impl"
    // InternalMCL.g:6429:1: rule__GIfExpression__Group__6__Impl : ( ( rule__GIfExpression__ElseExpressionAssignment_6 ) ) ;
    public final void rule__GIfExpression__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6433:1: ( ( ( rule__GIfExpression__ElseExpressionAssignment_6 ) ) )
            // InternalMCL.g:6434:1: ( ( rule__GIfExpression__ElseExpressionAssignment_6 ) )
            {
            // InternalMCL.g:6434:1: ( ( rule__GIfExpression__ElseExpressionAssignment_6 ) )
            // InternalMCL.g:6435:2: ( rule__GIfExpression__ElseExpressionAssignment_6 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getElseExpressionAssignment_6()); 
            }
            // InternalMCL.g:6436:2: ( rule__GIfExpression__ElseExpressionAssignment_6 )
            // InternalMCL.g:6436:3: rule__GIfExpression__ElseExpressionAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__GIfExpression__ElseExpressionAssignment_6();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getElseExpressionAssignment_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__6__Impl"


    // $ANTLR start "rule__GIfExpression__Group__7"
    // InternalMCL.g:6444:1: rule__GIfExpression__Group__7 : rule__GIfExpression__Group__7__Impl ;
    public final void rule__GIfExpression__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6448:1: ( rule__GIfExpression__Group__7__Impl )
            // InternalMCL.g:6449:2: rule__GIfExpression__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GIfExpression__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__7"


    // $ANTLR start "rule__GIfExpression__Group__7__Impl"
    // InternalMCL.g:6455:1: rule__GIfExpression__Group__7__Impl : ( 'endif' ) ;
    public final void rule__GIfExpression__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6459:1: ( ( 'endif' ) )
            // InternalMCL.g:6460:1: ( 'endif' )
            {
            // InternalMCL.g:6460:1: ( 'endif' )
            // InternalMCL.g:6461:2: 'endif'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getEndifKeyword_7()); 
            }
            match(input,61,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getEndifKeyword_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__Group__7__Impl"


    // $ANTLR start "rule__GBraceExpression__Group__0"
    // InternalMCL.g:6471:1: rule__GBraceExpression__Group__0 : rule__GBraceExpression__Group__0__Impl rule__GBraceExpression__Group__1 ;
    public final void rule__GBraceExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6475:1: ( rule__GBraceExpression__Group__0__Impl rule__GBraceExpression__Group__1 )
            // InternalMCL.g:6476:2: rule__GBraceExpression__Group__0__Impl rule__GBraceExpression__Group__1
            {
            pushFollow(FOLLOW_64);
            rule__GBraceExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__0"


    // $ANTLR start "rule__GBraceExpression__Group__0__Impl"
    // InternalMCL.g:6483:1: rule__GBraceExpression__Group__0__Impl : ( () ) ;
    public final void rule__GBraceExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6487:1: ( ( () ) )
            // InternalMCL.g:6488:1: ( () )
            {
            // InternalMCL.g:6488:1: ( () )
            // InternalMCL.g:6489:2: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getGBraceExpressionAction_0()); 
            }
            // InternalMCL.g:6490:2: ()
            // InternalMCL.g:6490:3: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getGBraceExpressionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__0__Impl"


    // $ANTLR start "rule__GBraceExpression__Group__1"
    // InternalMCL.g:6498:1: rule__GBraceExpression__Group__1 : rule__GBraceExpression__Group__1__Impl rule__GBraceExpression__Group__2 ;
    public final void rule__GBraceExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6502:1: ( rule__GBraceExpression__Group__1__Impl rule__GBraceExpression__Group__2 )
            // InternalMCL.g:6503:2: rule__GBraceExpression__Group__1__Impl rule__GBraceExpression__Group__2
            {
            pushFollow(FOLLOW_43);
            rule__GBraceExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__1"


    // $ANTLR start "rule__GBraceExpression__Group__1__Impl"
    // InternalMCL.g:6510:1: rule__GBraceExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__GBraceExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6514:1: ( ( '(' ) )
            // InternalMCL.g:6515:1: ( '(' )
            {
            // InternalMCL.g:6515:1: ( '(' )
            // InternalMCL.g:6516:2: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,34,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__1__Impl"


    // $ANTLR start "rule__GBraceExpression__Group__2"
    // InternalMCL.g:6525:1: rule__GBraceExpression__Group__2 : rule__GBraceExpression__Group__2__Impl rule__GBraceExpression__Group__3 ;
    public final void rule__GBraceExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6529:1: ( rule__GBraceExpression__Group__2__Impl rule__GBraceExpression__Group__3 )
            // InternalMCL.g:6530:2: rule__GBraceExpression__Group__2__Impl rule__GBraceExpression__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__GBraceExpression__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__2"


    // $ANTLR start "rule__GBraceExpression__Group__2__Impl"
    // InternalMCL.g:6537:1: rule__GBraceExpression__Group__2__Impl : ( ( rule__GBraceExpression__InnerExpressionAssignment_2 ) ) ;
    public final void rule__GBraceExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6541:1: ( ( ( rule__GBraceExpression__InnerExpressionAssignment_2 ) ) )
            // InternalMCL.g:6542:1: ( ( rule__GBraceExpression__InnerExpressionAssignment_2 ) )
            {
            // InternalMCL.g:6542:1: ( ( rule__GBraceExpression__InnerExpressionAssignment_2 ) )
            // InternalMCL.g:6543:2: ( rule__GBraceExpression__InnerExpressionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getInnerExpressionAssignment_2()); 
            }
            // InternalMCL.g:6544:2: ( rule__GBraceExpression__InnerExpressionAssignment_2 )
            // InternalMCL.g:6544:3: rule__GBraceExpression__InnerExpressionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__InnerExpressionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getInnerExpressionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__2__Impl"


    // $ANTLR start "rule__GBraceExpression__Group__3"
    // InternalMCL.g:6552:1: rule__GBraceExpression__Group__3 : rule__GBraceExpression__Group__3__Impl ;
    public final void rule__GBraceExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6556:1: ( rule__GBraceExpression__Group__3__Impl )
            // InternalMCL.g:6557:2: rule__GBraceExpression__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GBraceExpression__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__3"


    // $ANTLR start "rule__GBraceExpression__Group__3__Impl"
    // InternalMCL.g:6563:1: rule__GBraceExpression__Group__3__Impl : ( ')' ) ;
    public final void rule__GBraceExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6567:1: ( ( ')' ) )
            // InternalMCL.g:6568:1: ( ')' )
            {
            // InternalMCL.g:6568:1: ( ')' )
            // InternalMCL.g:6569:2: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,37,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__Group__3__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalMCL.g:6579:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6583:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalMCL.g:6584:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_65);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalMCL.g:6591:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6595:1: ( ( RULE_ID ) )
            // InternalMCL.g:6596:1: ( RULE_ID )
            {
            // InternalMCL.g:6596:1: ( RULE_ID )
            // InternalMCL.g:6597:2: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalMCL.g:6606:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6610:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalMCL.g:6611:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalMCL.g:6617:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6621:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalMCL.g:6622:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalMCL.g:6622:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalMCL.g:6623:2: ( rule__QualifiedName__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            }
            // InternalMCL.g:6624:2: ( rule__QualifiedName__Group_1__0 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==18) ) {
                    int LA39_2 = input.LA(2);

                    if ( (LA39_2==RULE_ID) ) {
                        int LA39_3 = input.LA(3);

                        if ( (synpred51_InternalMCL()) ) {
                            alt39=1;
                        }


                    }


                }


                switch (alt39) {
            	case 1 :
            	    // InternalMCL.g:6624:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_66);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalMCL.g:6633:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6637:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalMCL.g:6638:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_8);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalMCL.g:6645:1: rule__QualifiedName__Group_1__0__Impl : ( ( '.' ) ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6649:1: ( ( ( '.' ) ) )
            // InternalMCL.g:6650:1: ( ( '.' ) )
            {
            // InternalMCL.g:6650:1: ( ( '.' ) )
            // InternalMCL.g:6651:2: ( '.' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            }
            // InternalMCL.g:6652:2: ( '.' )
            // InternalMCL.g:6652:3: '.'
            {
            match(input,18,FOLLOW_2); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalMCL.g:6660:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6664:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalMCL.g:6665:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalMCL.g:6671:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6675:1: ( ( RULE_ID ) )
            // InternalMCL.g:6676:1: ( RULE_ID )
            {
            // InternalMCL.g:6676:1: ( RULE_ID )
            // InternalMCL.g:6677:2: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__MCSpecification__ImportedMBIAssignment_1_0"
    // InternalMCL.g:6687:1: rule__MCSpecification__ImportedMBIAssignment_1_0 : ( ruleImportInterfaceStatement ) ;
    public final void rule__MCSpecification__ImportedMBIAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6691:1: ( ( ruleImportInterfaceStatement ) )
            // InternalMCL.g:6692:2: ( ruleImportInterfaceStatement )
            {
            // InternalMCL.g:6692:2: ( ruleImportInterfaceStatement )
            // InternalMCL.g:6693:3: ruleImportInterfaceStatement
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleImportInterfaceStatement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__ImportedMBIAssignment_1_0"


    // $ANTLR start "rule__MCSpecification__ImportedMBIAssignment_1_1"
    // InternalMCL.g:6702:1: rule__MCSpecification__ImportedMBIAssignment_1_1 : ( ruleImportInterfaceStatement ) ;
    public final void rule__MCSpecification__ImportedMBIAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6706:1: ( ( ruleImportInterfaceStatement ) )
            // InternalMCL.g:6707:2: ( ruleImportInterfaceStatement )
            {
            // InternalMCL.g:6707:2: ( ruleImportInterfaceStatement )
            // InternalMCL.g:6708:3: ruleImportInterfaceStatement
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleImportInterfaceStatement();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__ImportedMBIAssignment_1_1"


    // $ANTLR start "rule__MCSpecification__OwnedConnectorsAssignment_2_0"
    // InternalMCL.g:6717:1: rule__MCSpecification__OwnedConnectorsAssignment_2_0 : ( ruleConnector ) ;
    public final void rule__MCSpecification__OwnedConnectorsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6721:1: ( ( ruleConnector ) )
            // InternalMCL.g:6722:2: ( ruleConnector )
            {
            // InternalMCL.g:6722:2: ( ruleConnector )
            // InternalMCL.g:6723:3: ruleConnector
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_0_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleConnector();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__OwnedConnectorsAssignment_2_0"


    // $ANTLR start "rule__MCSpecification__OwnedConnectorsAssignment_2_1"
    // InternalMCL.g:6732:1: rule__MCSpecification__OwnedConnectorsAssignment_2_1 : ( ruleConnector ) ;
    public final void rule__MCSpecification__OwnedConnectorsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6736:1: ( ( ruleConnector ) )
            // InternalMCL.g:6737:2: ( ruleConnector )
            {
            // InternalMCL.g:6737:2: ( ruleConnector )
            // InternalMCL.g:6738:3: ruleConnector
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleConnector();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MCSpecification__OwnedConnectorsAssignment_2_1"


    // $ANTLR start "rule__Connector__NameAssignment_1"
    // InternalMCL.g:6747:1: rule__Connector__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Connector__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6751:1: ( ( RULE_ID ) )
            // InternalMCL.g:6752:2: ( RULE_ID )
            {
            // InternalMCL.g:6752:2: ( RULE_ID )
            // InternalMCL.g:6753:3: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__NameAssignment_1"


    // $ANTLR start "rule__Connector__FromAssignment_2_2"
    // InternalMCL.g:6762:1: rule__Connector__FromAssignment_2_2 : ( rulePortRef ) ;
    public final void rule__Connector__FromAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6766:1: ( ( rulePortRef ) )
            // InternalMCL.g:6767:2: ( rulePortRef )
            {
            // InternalMCL.g:6767:2: ( rulePortRef )
            // InternalMCL.g:6768:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getFromPortRefParserRuleCall_2_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getFromPortRefParserRuleCall_2_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__FromAssignment_2_2"


    // $ANTLR start "rule__Connector__ToAssignment_2_4"
    // InternalMCL.g:6777:1: rule__Connector__ToAssignment_2_4 : ( rulePortRef ) ;
    public final void rule__Connector__ToAssignment_2_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6781:1: ( ( rulePortRef ) )
            // InternalMCL.g:6782:2: ( rulePortRef )
            {
            // InternalMCL.g:6782:2: ( rulePortRef )
            // InternalMCL.g:6783:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getToPortRefParserRuleCall_2_4_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getToPortRefParserRuleCall_2_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__ToAssignment_2_4"


    // $ANTLR start "rule__Connector__TriggerAssignment_4"
    // InternalMCL.g:6792:1: rule__Connector__TriggerAssignment_4 : ( ruleTriggeringCondition ) ;
    public final void rule__Connector__TriggerAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6796:1: ( ( ruleTriggeringCondition ) )
            // InternalMCL.g:6797:2: ( ruleTriggeringCondition )
            {
            // InternalMCL.g:6797:2: ( ruleTriggeringCondition )
            // InternalMCL.g:6798:3: ruleTriggeringCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getTriggerTriggeringConditionParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getTriggerTriggeringConditionParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__TriggerAssignment_4"


    // $ANTLR start "rule__Connector__SynchronizationRuleAssignment_5_1"
    // InternalMCL.g:6807:1: rule__Connector__SynchronizationRuleAssignment_5_1 : ( ruleSynchronizationConstraint ) ;
    public final void rule__Connector__SynchronizationRuleAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6811:1: ( ( ruleSynchronizationConstraint ) )
            // InternalMCL.g:6812:2: ( ruleSynchronizationConstraint )
            {
            // InternalMCL.g:6812:2: ( ruleSynchronizationConstraint )
            // InternalMCL.g:6813:3: ruleSynchronizationConstraint
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getSynchronizationRuleSynchronizationConstraintParserRuleCall_5_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleSynchronizationConstraint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getSynchronizationRuleSynchronizationConstraintParserRuleCall_5_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__SynchronizationRuleAssignment_5_1"


    // $ANTLR start "rule__Connector__InteractionstatementAssignment_7"
    // InternalMCL.g:6822:1: rule__Connector__InteractionstatementAssignment_7 : ( ruleAbstractInteraction ) ;
    public final void rule__Connector__InteractionstatementAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6826:1: ( ( ruleAbstractInteraction ) )
            // InternalMCL.g:6827:2: ( ruleAbstractInteraction )
            {
            // InternalMCL.g:6827:2: ( ruleAbstractInteraction )
            // InternalMCL.g:6828:3: ruleAbstractInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConnectorAccess().getInteractionstatementAbstractInteractionParserRuleCall_7_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleAbstractInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConnectorAccess().getInteractionstatementAbstractInteractionParserRuleCall_7_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Connector__InteractionstatementAssignment_7"


    // $ANTLR start "rule__ImportInterfaceStatement__ImportURIAssignment_1"
    // InternalMCL.g:6837:1: rule__ImportInterfaceStatement__ImportURIAssignment_1 : ( ruleEString ) ;
    public final void rule__ImportInterfaceStatement__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6841:1: ( ( ruleEString ) )
            // InternalMCL.g:6842:2: ( ruleEString )
            {
            // InternalMCL.g:6842:2: ( ruleEString )
            // InternalMCL.g:6843:3: ruleEString
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportInterfaceStatementAccess().getImportURIEStringParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportInterfaceStatementAccess().getImportURIEStringParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportInterfaceStatement__ImportURIAssignment_1"


    // $ANTLR start "rule__ThresholdExpression__TriggerSourcePortAssignment_0"
    // InternalMCL.g:6852:1: rule__ThresholdExpression__TriggerSourcePortAssignment_0 : ( rulePortRef ) ;
    public final void rule__ThresholdExpression__TriggerSourcePortAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6856:1: ( ( rulePortRef ) )
            // InternalMCL.g:6857:2: ( rulePortRef )
            {
            // InternalMCL.g:6857:2: ( rulePortRef )
            // InternalMCL.g:6858:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getTriggerSourcePortPortRefParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getTriggerSourcePortPortRefParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__TriggerSourcePortAssignment_0"


    // $ANTLR start "rule__ThresholdExpression__SetoperationAssignment_1"
    // InternalMCL.g:6867:1: rule__ThresholdExpression__SetoperationAssignment_1 : ( ruleExpOperation ) ;
    public final void rule__ThresholdExpression__SetoperationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6871:1: ( ( ruleExpOperation ) )
            // InternalMCL.g:6872:2: ( ruleExpOperation )
            {
            // InternalMCL.g:6872:2: ( ruleExpOperation )
            // InternalMCL.g:6873:3: ruleExpOperation
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getSetoperationExpOperationParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleExpOperation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getSetoperationExpOperationParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__SetoperationAssignment_1"


    // $ANTLR start "rule__ThresholdExpression__ValueAssignment_2"
    // InternalMCL.g:6882:1: rule__ThresholdExpression__ValueAssignment_2 : ( ruleVariable ) ;
    public final void rule__ThresholdExpression__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6886:1: ( ( ruleVariable ) )
            // InternalMCL.g:6887:2: ( ruleVariable )
            {
            // InternalMCL.g:6887:2: ( ruleVariable )
            // InternalMCL.g:6888:3: ruleVariable
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getThresholdExpressionAccess().getValueVariableParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleVariable();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getThresholdExpressionAccess().getValueVariableParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThresholdExpression__ValueAssignment_2"


    // $ANTLR start "rule__Disjoint__RightOpAssignment_1_2"
    // InternalMCL.g:6897:1: rule__Disjoint__RightOpAssignment_1_2 : ( ruleUnion ) ;
    public final void rule__Disjoint__RightOpAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6901:1: ( ( ruleUnion ) )
            // InternalMCL.g:6902:2: ( ruleUnion )
            {
            // InternalMCL.g:6902:2: ( ruleUnion )
            // InternalMCL.g:6903:3: ruleUnion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjointAccess().getRightOpUnionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleUnion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjointAccess().getRightOpUnionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Disjoint__RightOpAssignment_1_2"


    // $ANTLR start "rule__Union__RightOpAssignment_1_2"
    // InternalMCL.g:6912:1: rule__Union__RightOpAssignment_1_2 : ( ruleTriggeringCondition ) ;
    public final void rule__Union__RightOpAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6916:1: ( ( ruleTriggeringCondition ) )
            // InternalMCL.g:6917:2: ( ruleTriggeringCondition )
            {
            // InternalMCL.g:6917:2: ( ruleTriggeringCondition )
            // InternalMCL.g:6918:3: ruleTriggeringCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnionAccess().getRightOpTriggeringConditionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnionAccess().getRightOpTriggeringConditionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Union__RightOpAssignment_1_2"


    // $ANTLR start "rule__EventTrigger__PortAssignment_2"
    // InternalMCL.g:6927:1: rule__EventTrigger__PortAssignment_2 : ( rulePortRef ) ;
    public final void rule__EventTrigger__PortAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6931:1: ( ( rulePortRef ) )
            // InternalMCL.g:6932:2: ( rulePortRef )
            {
            // InternalMCL.g:6932:2: ( rulePortRef )
            // InternalMCL.g:6933:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventTriggerAccess().getPortPortRefParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventTriggerAccess().getPortPortRefParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTrigger__PortAssignment_2"


    // $ANTLR start "rule__PeriodicExpression__ValueAssignment_1"
    // InternalMCL.g:6942:1: rule__PeriodicExpression__ValueAssignment_1 : ( RULE_INT ) ;
    public final void rule__PeriodicExpression__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6946:1: ( ( RULE_INT ) )
            // InternalMCL.g:6947:2: ( RULE_INT )
            {
            // InternalMCL.g:6947:2: ( RULE_INT )
            // InternalMCL.g:6948:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getValueINTTerminalRuleCall_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getValueINTTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__ValueAssignment_1"


    // $ANTLR start "rule__PeriodicExpression__TemporalReferenceAssignment_2"
    // InternalMCL.g:6957:1: rule__PeriodicExpression__TemporalReferenceAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__PeriodicExpression__TemporalReferenceAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6961:1: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:6962:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:6962:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:6963:3: ( ruleQualifiedName )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceModelTemporalReferenceCrossReference_2_0()); 
            }
            // InternalMCL.g:6964:3: ( ruleQualifiedName )
            // InternalMCL.g:6965:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_2_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceModelTemporalReferenceCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PeriodicExpression__TemporalReferenceAssignment_2"


    // $ANTLR start "rule__Variable__ValueAssignment_1"
    // InternalMCL.g:6976:1: rule__Variable__ValueAssignment_1 : ( RULE_DOUBLE ) ;
    public final void rule__Variable__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6980:1: ( ( RULE_DOUBLE ) )
            // InternalMCL.g:6981:2: ( RULE_DOUBLE )
            {
            // InternalMCL.g:6981:2: ( RULE_DOUBLE )
            // InternalMCL.g:6982:3: RULE_DOUBLE
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAccess().getValueDOUBLETerminalRuleCall_1_0()); 
            }
            match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAccess().getValueDOUBLETerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__ValueAssignment_1"


    // $ANTLR start "rule__Updated__PortAssignment_2"
    // InternalMCL.g:6991:1: rule__Updated__PortAssignment_2 : ( rulePortRef ) ;
    public final void rule__Updated__PortAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:6995:1: ( ( rulePortRef ) )
            // InternalMCL.g:6996:2: ( rulePortRef )
            {
            // InternalMCL.g:6996:2: ( rulePortRef )
            // InternalMCL.g:6997:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUpdatedAccess().getPortPortRefParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUpdatedAccess().getPortPortRefParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Updated__PortAssignment_2"


    // $ANTLR start "rule__ReadyToRead__PortAssignment_2"
    // InternalMCL.g:7006:1: rule__ReadyToRead__PortAssignment_2 : ( rulePortRef ) ;
    public final void rule__ReadyToRead__PortAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7010:1: ( ( rulePortRef ) )
            // InternalMCL.g:7011:2: ( rulePortRef )
            {
            // InternalMCL.g:7011:2: ( rulePortRef )
            // InternalMCL.g:7012:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getReadyToReadAccess().getPortPortRefParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getReadyToReadAccess().getPortPortRefParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadyToRead__PortAssignment_2"


    // $ANTLR start "rule__ZeroCrossingDetection__StateAssignment_1"
    // InternalMCL.g:7021:1: rule__ZeroCrossingDetection__StateAssignment_1 : ( ( 'state' ) ) ;
    public final void rule__ZeroCrossingDetection__StateAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7025:1: ( ( ( 'state' ) ) )
            // InternalMCL.g:7026:2: ( ( 'state' ) )
            {
            // InternalMCL.g:7026:2: ( ( 'state' ) )
            // InternalMCL.g:7027:3: ( 'state' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getStateStateKeyword_1_0()); 
            }
            // InternalMCL.g:7028:3: ( 'state' )
            // InternalMCL.g:7029:4: 'state'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getZeroCrossingDetectionAccess().getStateStateKeyword_1_0()); 
            }
            match(input,62,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getStateStateKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getZeroCrossingDetectionAccess().getStateStateKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ZeroCrossingDetection__StateAssignment_1"


    // $ANTLR start "rule__Extrapolation__StateAssignment_1"
    // InternalMCL.g:7040:1: rule__Extrapolation__StateAssignment_1 : ( ( 'state' ) ) ;
    public final void rule__Extrapolation__StateAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7044:1: ( ( ( 'state' ) ) )
            // InternalMCL.g:7045:2: ( ( 'state' ) )
            {
            // InternalMCL.g:7045:2: ( ( 'state' ) )
            // InternalMCL.g:7046:3: ( 'state' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getStateStateKeyword_1_0()); 
            }
            // InternalMCL.g:7047:3: ( 'state' )
            // InternalMCL.g:7048:4: 'state'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExtrapolationAccess().getStateStateKeyword_1_0()); 
            }
            match(input,62,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getStateStateKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExtrapolationAccess().getStateStateKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Extrapolation__StateAssignment_1"


    // $ANTLR start "rule__Interpolation__StateAssignment_1"
    // InternalMCL.g:7059:1: rule__Interpolation__StateAssignment_1 : ( ( 'state' ) ) ;
    public final void rule__Interpolation__StateAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7063:1: ( ( ( 'state' ) ) )
            // InternalMCL.g:7064:2: ( ( 'state' ) )
            {
            // InternalMCL.g:7064:2: ( ( 'state' ) )
            // InternalMCL.g:7065:3: ( 'state' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getStateStateKeyword_1_0()); 
            }
            // InternalMCL.g:7066:3: ( 'state' )
            // InternalMCL.g:7067:4: 'state'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterpolationAccess().getStateStateKeyword_1_0()); 
            }
            match(input,62,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getStateStateKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterpolationAccess().getStateStateKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interpolation__StateAssignment_1"


    // $ANTLR start "rule__DiscontinuityLocator__StateAssignment_1"
    // InternalMCL.g:7078:1: rule__DiscontinuityLocator__StateAssignment_1 : ( ( 'state' ) ) ;
    public final void rule__DiscontinuityLocator__StateAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7082:1: ( ( ( 'state' ) ) )
            // InternalMCL.g:7083:2: ( ( 'state' ) )
            {
            // InternalMCL.g:7083:2: ( ( 'state' ) )
            // InternalMCL.g:7084:3: ( 'state' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getStateStateKeyword_1_0()); 
            }
            // InternalMCL.g:7085:3: ( 'state' )
            // InternalMCL.g:7086:4: 'state'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDiscontinuityLocatorAccess().getStateStateKeyword_1_0()); 
            }
            match(input,62,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getStateStateKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDiscontinuityLocatorAccess().getStateStateKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiscontinuityLocator__StateAssignment_1"


    // $ANTLR start "rule__HistoryProvider__StateAssignment_1"
    // InternalMCL.g:7097:1: rule__HistoryProvider__StateAssignment_1 : ( ( 'state' ) ) ;
    public final void rule__HistoryProvider__StateAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7101:1: ( ( ( 'state' ) ) )
            // InternalMCL.g:7102:2: ( ( 'state' ) )
            {
            // InternalMCL.g:7102:2: ( ( 'state' ) )
            // InternalMCL.g:7103:3: ( 'state' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getStateStateKeyword_1_0()); 
            }
            // InternalMCL.g:7104:3: ( 'state' )
            // InternalMCL.g:7105:4: 'state'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHistoryProviderAccess().getStateStateKeyword_1_0()); 
            }
            match(input,62,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getStateStateKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHistoryProviderAccess().getStateStateKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoryProvider__StateAssignment_1"


    // $ANTLR start "rule__UnaryMinus__OperandAssignment_1"
    // InternalMCL.g:7116:1: rule__UnaryMinus__OperandAssignment_1 : ( rulePortRef ) ;
    public final void rule__UnaryMinus__OperandAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7120:1: ( ( rulePortRef ) )
            // InternalMCL.g:7121:2: ( rulePortRef )
            {
            // InternalMCL.g:7121:2: ( rulePortRef )
            // InternalMCL.g:7122:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getUnaryMinusAccess().getOperandPortRefParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getUnaryMinusAccess().getOperandPortRefParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryMinus__OperandAssignment_1"


    // $ANTLR start "rule__Not__OperandAssignment_1"
    // InternalMCL.g:7131:1: rule__Not__OperandAssignment_1 : ( rulePortRef ) ;
    public final void rule__Not__OperandAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7135:1: ( ( rulePortRef ) )
            // InternalMCL.g:7136:2: ( rulePortRef )
            {
            // InternalMCL.g:7136:2: ( rulePortRef )
            // InternalMCL.g:7137:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getOperandPortRefParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getOperandPortRefParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__OperandAssignment_1"


    // $ANTLR start "rule__BinaryMinus__LeftOperandAssignment_0"
    // InternalMCL.g:7146:1: rule__BinaryMinus__LeftOperandAssignment_0 : ( rulePortRef ) ;
    public final void rule__BinaryMinus__LeftOperandAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7150:1: ( ( rulePortRef ) )
            // InternalMCL.g:7151:2: ( rulePortRef )
            {
            // InternalMCL.g:7151:2: ( rulePortRef )
            // InternalMCL.g:7152:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getLeftOperandPortRefParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getLeftOperandPortRefParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__LeftOperandAssignment_0"


    // $ANTLR start "rule__BinaryMinus__RightOperandAssignment_2"
    // InternalMCL.g:7161:1: rule__BinaryMinus__RightOperandAssignment_2 : ( rulePortRef ) ;
    public final void rule__BinaryMinus__RightOperandAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7165:1: ( ( rulePortRef ) )
            // InternalMCL.g:7166:2: ( rulePortRef )
            {
            // InternalMCL.g:7166:2: ( rulePortRef )
            // InternalMCL.g:7167:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryMinusAccess().getRightOperandPortRefParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryMinusAccess().getRightOperandPortRefParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryMinus__RightOperandAssignment_2"


    // $ANTLR start "rule__BinaryPlus__LeftOperandAssignment_0"
    // InternalMCL.g:7176:1: rule__BinaryPlus__LeftOperandAssignment_0 : ( rulePortRef ) ;
    public final void rule__BinaryPlus__LeftOperandAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7180:1: ( ( rulePortRef ) )
            // InternalMCL.g:7181:2: ( rulePortRef )
            {
            // InternalMCL.g:7181:2: ( rulePortRef )
            // InternalMCL.g:7182:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getLeftOperandPortRefParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getLeftOperandPortRefParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__LeftOperandAssignment_0"


    // $ANTLR start "rule__BinaryPlus__RightOperandAssignment_2"
    // InternalMCL.g:7191:1: rule__BinaryPlus__RightOperandAssignment_2 : ( rulePortRef ) ;
    public final void rule__BinaryPlus__RightOperandAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7195:1: ( ( rulePortRef ) )
            // InternalMCL.g:7196:2: ( rulePortRef )
            {
            // InternalMCL.g:7196:2: ( rulePortRef )
            // InternalMCL.g:7197:3: rulePortRef
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryPlusAccess().getRightOperandPortRefParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            rulePortRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryPlusAccess().getRightOperandPortRefParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryPlus__RightOperandAssignment_2"


    // $ANTLR start "rule__Assignment__LeftOperandAssignment_1"
    // InternalMCL.g:7206:1: rule__Assignment__LeftOperandAssignment_1 : ( ruleOtherExpression ) ;
    public final void rule__Assignment__LeftOperandAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7210:1: ( ( ruleOtherExpression ) )
            // InternalMCL.g:7211:2: ( ruleOtherExpression )
            {
            // InternalMCL.g:7211:2: ( ruleOtherExpression )
            // InternalMCL.g:7212:3: ruleOtherExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getLeftOperandOtherExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleOtherExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getLeftOperandOtherExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__LeftOperandAssignment_1"


    // $ANTLR start "rule__Assignment__RightOperandAssignment_3"
    // InternalMCL.g:7221:1: rule__Assignment__RightOperandAssignment_3 : ( ruleOtherExpression ) ;
    public final void rule__Assignment__RightOperandAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7225:1: ( ( ruleOtherExpression ) )
            // InternalMCL.g:7226:2: ( ruleOtherExpression )
            {
            // InternalMCL.g:7226:2: ( ruleOtherExpression )
            // InternalMCL.g:7227:3: ruleOtherExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAssignmentAccess().getRightOperandOtherExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleOtherExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAssignmentAccess().getRightOperandOtherExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__RightOperandAssignment_3"


    // $ANTLR start "rule__PortRef__RefPortAssignment"
    // InternalMCL.g:7236:1: rule__PortRef__RefPortAssignment : ( ( ruleQualifiedName ) ) ;
    public final void rule__PortRef__RefPortAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7240:1: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:7241:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:7241:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:7242:3: ( ruleQualifiedName )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPortRefAccess().getRefPortPortCrossReference_0()); 
            }
            // InternalMCL.g:7243:3: ( ruleQualifiedName )
            // InternalMCL.g:7244:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPortRefAccess().getRefPortPortQualifiedNameParserRuleCall_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPortRefAccess().getRefPortPortQualifiedNameParserRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPortRefAccess().getRefPortPortCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PortRef__RefPortAssignment"


    // $ANTLR start "rule__SimpleSync__RightTemporalReferenceAssignment_0"
    // InternalMCL.g:7255:1: rule__SimpleSync__RightTemporalReferenceAssignment_0 : ( ( ruleQualifiedName ) ) ;
    public final void rule__SimpleSync__RightTemporalReferenceAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7259:1: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:7260:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:7260:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:7261:3: ( ruleQualifiedName )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceModelTemporalReferenceCrossReference_0_0()); 
            }
            // InternalMCL.g:7262:3: ( ruleQualifiedName )
            // InternalMCL.g:7263:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_0_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceModelTemporalReferenceCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__RightTemporalReferenceAssignment_0"


    // $ANTLR start "rule__SimpleSync__LeftTemporalReferenceAssignment_2"
    // InternalMCL.g:7274:1: rule__SimpleSync__LeftTemporalReferenceAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__SimpleSync__LeftTemporalReferenceAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7278:1: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:7279:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:7279:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:7280:3: ( ruleQualifiedName )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceModelTemporalReferenceCrossReference_2_0()); 
            }
            // InternalMCL.g:7281:3: ( ruleQualifiedName )
            // InternalMCL.g:7282:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_2_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceModelTemporalReferenceQualifiedNameParserRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceModelTemporalReferenceCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__LeftTemporalReferenceAssignment_2"


    // $ANTLR start "rule__SimpleSync__ToleranceAssignment_3_1"
    // InternalMCL.g:7293:1: rule__SimpleSync__ToleranceAssignment_3_1 : ( RULE_DOUBLE ) ;
    public final void rule__SimpleSync__ToleranceAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7297:1: ( ( RULE_DOUBLE ) )
            // InternalMCL.g:7298:2: ( RULE_DOUBLE )
            {
            // InternalMCL.g:7298:2: ( RULE_DOUBLE )
            // InternalMCL.g:7299:3: RULE_DOUBLE
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSimpleSyncAccess().getToleranceDOUBLETerminalRuleCall_3_1_0()); 
            }
            match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSimpleSyncAccess().getToleranceDOUBLETerminalRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSync__ToleranceAssignment_3_1"


    // $ANTLR start "rule__GImportStatement__ImportURIAssignment_1"
    // InternalMCL.g:7308:1: rule__GImportStatement__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__GImportStatement__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7312:1: ( ( RULE_STRING ) )
            // InternalMCL.g:7313:2: ( RULE_STRING )
            {
            // InternalMCL.g:7313:2: ( RULE_STRING )
            // InternalMCL.g:7314:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GImportStatement__ImportURIAssignment_1"


    // $ANTLR start "rule__GOrExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7323:1: rule__GOrExpression__OperatorAssignment_1_1 : ( ruleGOrOperator ) ;
    public final void rule__GOrExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7327:1: ( ( ruleGOrOperator ) )
            // InternalMCL.g:7328:2: ( ruleGOrOperator )
            {
            // InternalMCL.g:7328:2: ( ruleGOrOperator )
            // InternalMCL.g:7329:3: ruleGOrOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getOperatorGOrOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGOrOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getOperatorGOrOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GOrExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7338:1: rule__GOrExpression__RightOperandAssignment_1_2 : ( ruleGXorExpression ) ;
    public final void rule__GOrExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7342:1: ( ( ruleGXorExpression ) )
            // InternalMCL.g:7343:2: ( ruleGXorExpression )
            {
            // InternalMCL.g:7343:2: ( ruleGXorExpression )
            // InternalMCL.g:7344:3: ruleGXorExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGOrExpressionAccess().getRightOperandGXorExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGXorExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGOrExpressionAccess().getRightOperandGXorExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GOrExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GXorExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7353:1: rule__GXorExpression__OperatorAssignment_1_1 : ( ruleGXorOperator ) ;
    public final void rule__GXorExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7357:1: ( ( ruleGXorOperator ) )
            // InternalMCL.g:7358:2: ( ruleGXorOperator )
            {
            // InternalMCL.g:7358:2: ( ruleGXorOperator )
            // InternalMCL.g:7359:3: ruleGXorOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getOperatorGXorOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGXorOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getOperatorGXorOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GXorExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7368:1: rule__GXorExpression__RightOperandAssignment_1_2 : ( ruleGAndExpression ) ;
    public final void rule__GXorExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7372:1: ( ( ruleGAndExpression ) )
            // InternalMCL.g:7373:2: ( ruleGAndExpression )
            {
            // InternalMCL.g:7373:2: ( ruleGAndExpression )
            // InternalMCL.g:7374:3: ruleGAndExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGXorExpressionAccess().getRightOperandGAndExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAndExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGXorExpressionAccess().getRightOperandGAndExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GXorExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GAndExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7383:1: rule__GAndExpression__OperatorAssignment_1_1 : ( ruleGAndOperator ) ;
    public final void rule__GAndExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7387:1: ( ( ruleGAndOperator ) )
            // InternalMCL.g:7388:2: ( ruleGAndOperator )
            {
            // InternalMCL.g:7388:2: ( ruleGAndOperator )
            // InternalMCL.g:7389:3: ruleGAndOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getOperatorGAndOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAndOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getOperatorGAndOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GAndExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7398:1: rule__GAndExpression__RightOperandAssignment_1_2 : ( ruleGEqualityExpression ) ;
    public final void rule__GAndExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7402:1: ( ( ruleGEqualityExpression ) )
            // InternalMCL.g:7403:2: ( ruleGEqualityExpression )
            {
            // InternalMCL.g:7403:2: ( ruleGEqualityExpression )
            // InternalMCL.g:7404:3: ruleGEqualityExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAndExpressionAccess().getRightOperandGEqualityExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGEqualityExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAndExpressionAccess().getRightOperandGEqualityExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAndExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GEqualityExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7413:1: rule__GEqualityExpression__OperatorAssignment_1_1 : ( ruleGEqualityOperator ) ;
    public final void rule__GEqualityExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7417:1: ( ( ruleGEqualityOperator ) )
            // InternalMCL.g:7418:2: ( ruleGEqualityOperator )
            {
            // InternalMCL.g:7418:2: ( ruleGEqualityOperator )
            // InternalMCL.g:7419:3: ruleGEqualityOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getOperatorGEqualityOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGEqualityOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getOperatorGEqualityOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GEqualityExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7428:1: rule__GEqualityExpression__RightOperandAssignment_1_2 : ( ruleGRelationExpression ) ;
    public final void rule__GEqualityExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7432:1: ( ( ruleGRelationExpression ) )
            // InternalMCL.g:7433:2: ( ruleGRelationExpression )
            {
            // InternalMCL.g:7433:2: ( ruleGRelationExpression )
            // InternalMCL.g:7434:3: ruleGRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEqualityExpressionAccess().getRightOperandGRelationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEqualityExpressionAccess().getRightOperandGRelationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEqualityExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GRelationExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7443:1: rule__GRelationExpression__OperatorAssignment_1_1 : ( ruleGRelationOperator ) ;
    public final void rule__GRelationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7447:1: ( ( ruleGRelationOperator ) )
            // InternalMCL.g:7448:2: ( ruleGRelationOperator )
            {
            // InternalMCL.g:7448:2: ( ruleGRelationOperator )
            // InternalMCL.g:7449:3: ruleGRelationOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getOperatorGRelationOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGRelationOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getOperatorGRelationOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GRelationExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7458:1: rule__GRelationExpression__RightOperandAssignment_1_2 : ( ruleGAdditionExpression ) ;
    public final void rule__GRelationExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7462:1: ( ( ruleGAdditionExpression ) )
            // InternalMCL.g:7463:2: ( ruleGAdditionExpression )
            {
            // InternalMCL.g:7463:2: ( ruleGAdditionExpression )
            // InternalMCL.g:7464:3: ruleGAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGRelationExpressionAccess().getRightOperandGAdditionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGRelationExpressionAccess().getRightOperandGAdditionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GRelationExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GAdditionExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7473:1: rule__GAdditionExpression__OperatorAssignment_1_1 : ( ruleGAdditionOperator ) ;
    public final void rule__GAdditionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7477:1: ( ( ruleGAdditionOperator ) )
            // InternalMCL.g:7478:2: ( ruleGAdditionOperator )
            {
            // InternalMCL.g:7478:2: ( ruleGAdditionOperator )
            // InternalMCL.g:7479:3: ruleGAdditionOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getOperatorGAdditionOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGAdditionOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getOperatorGAdditionOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GAdditionExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7488:1: rule__GAdditionExpression__RightOperandAssignment_1_2 : ( ruleGMultiplicationExpression ) ;
    public final void rule__GAdditionExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7492:1: ( ( ruleGMultiplicationExpression ) )
            // InternalMCL.g:7493:2: ( ruleGMultiplicationExpression )
            {
            // InternalMCL.g:7493:2: ( ruleGMultiplicationExpression )
            // InternalMCL.g:7494:3: ruleGMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGAdditionExpressionAccess().getRightOperandGMultiplicationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGAdditionExpressionAccess().getRightOperandGMultiplicationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GAdditionExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GMultiplicationExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7503:1: rule__GMultiplicationExpression__OperatorAssignment_1_1 : ( ruleGMultiplicationOperator ) ;
    public final void rule__GMultiplicationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7507:1: ( ( ruleGMultiplicationOperator ) )
            // InternalMCL.g:7508:2: ( ruleGMultiplicationOperator )
            {
            // InternalMCL.g:7508:2: ( ruleGMultiplicationOperator )
            // InternalMCL.g:7509:3: ruleGMultiplicationOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getOperatorGMultiplicationOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGMultiplicationOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getOperatorGMultiplicationOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GMultiplicationExpression__RightOperandAssignment_1_2"
    // InternalMCL.g:7518:1: rule__GMultiplicationExpression__RightOperandAssignment_1_2 : ( ruleGNegationExpression ) ;
    public final void rule__GMultiplicationExpression__RightOperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7522:1: ( ( ruleGNegationExpression ) )
            // InternalMCL.g:7523:2: ( ruleGNegationExpression )
            {
            // InternalMCL.g:7523:2: ( ruleGNegationExpression )
            // InternalMCL.g:7524:3: ruleGNegationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGMultiplicationExpressionAccess().getRightOperandGNegationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGNegationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGMultiplicationExpressionAccess().getRightOperandGNegationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GMultiplicationExpression__RightOperandAssignment_1_2"


    // $ANTLR start "rule__GNegationExpression__OperatorAssignment_1_1"
    // InternalMCL.g:7533:1: rule__GNegationExpression__OperatorAssignment_1_1 : ( ruleGNegationOperator ) ;
    public final void rule__GNegationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7537:1: ( ( ruleGNegationOperator ) )
            // InternalMCL.g:7538:2: ( ruleGNegationOperator )
            {
            // InternalMCL.g:7538:2: ( ruleGNegationOperator )
            // InternalMCL.g:7539:3: ruleGNegationOperator
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getOperatorGNegationOperatorEnumRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGNegationOperator();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getOperatorGNegationOperatorEnumRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__GNegationExpression__OperandAssignment_1_2"
    // InternalMCL.g:7548:1: rule__GNegationExpression__OperandAssignment_1_2 : ( ruleGNavigationExpression ) ;
    public final void rule__GNegationExpression__OperandAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7552:1: ( ( ruleGNavigationExpression ) )
            // InternalMCL.g:7553:2: ( ruleGNavigationExpression )
            {
            // InternalMCL.g:7553:2: ( ruleGNavigationExpression )
            // InternalMCL.g:7554:3: ruleGNavigationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNegationExpressionAccess().getOperandGNavigationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGNavigationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNegationExpressionAccess().getOperandGNavigationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNegationExpression__OperandAssignment_1_2"


    // $ANTLR start "rule__GNavigationExpression__ReferencedEObjectAssignment_1_2"
    // InternalMCL.g:7563:1: rule__GNavigationExpression__ReferencedEObjectAssignment_1_2 : ( ( RULE_ID ) ) ;
    public final void rule__GNavigationExpression__ReferencedEObjectAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7567:1: ( ( ( RULE_ID ) ) )
            // InternalMCL.g:7568:2: ( ( RULE_ID ) )
            {
            // InternalMCL.g:7568:2: ( ( RULE_ID ) )
            // InternalMCL.g:7569:3: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectEObjectCrossReference_1_2_0()); 
            }
            // InternalMCL.g:7570:3: ( RULE_ID )
            // InternalMCL.g:7571:4: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectEObjectIDTerminalRuleCall_1_2_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectEObjectIDTerminalRuleCall_1_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectEObjectCrossReference_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GNavigationExpression__ReferencedEObjectAssignment_1_2"


    // $ANTLR start "rule__GReferenceExpression__ReferencedEObjectAssignment_1_1"
    // InternalMCL.g:7582:1: rule__GReferenceExpression__ReferencedEObjectAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__GReferenceExpression__ReferencedEObjectAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7586:1: ( ( ( RULE_ID ) ) )
            // InternalMCL.g:7587:2: ( ( RULE_ID ) )
            {
            // InternalMCL.g:7587:2: ( ( RULE_ID ) )
            // InternalMCL.g:7588:3: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectEObjectCrossReference_1_1_0()); 
            }
            // InternalMCL.g:7589:3: ( RULE_ID )
            // InternalMCL.g:7590:4: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectEObjectIDTerminalRuleCall_1_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectEObjectIDTerminalRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectEObjectCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GReferenceExpression__ReferencedEObjectAssignment_1_1"


    // $ANTLR start "rule__GStringExpression__ValueAssignment_1"
    // InternalMCL.g:7601:1: rule__GStringExpression__ValueAssignment_1 : ( RULE_STRING ) ;
    public final void rule__GStringExpression__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7605:1: ( ( RULE_STRING ) )
            // InternalMCL.g:7606:2: ( RULE_STRING )
            {
            // InternalMCL.g:7606:2: ( RULE_STRING )
            // InternalMCL.g:7607:3: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGStringExpressionAccess().getValueSTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGStringExpressionAccess().getValueSTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GStringExpression__ValueAssignment_1"


    // $ANTLR start "rule__GBooleanExpression__ValueAssignment_1"
    // InternalMCL.g:7616:1: rule__GBooleanExpression__ValueAssignment_1 : ( RULE_BOOLEAN ) ;
    public final void rule__GBooleanExpression__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7620:1: ( ( RULE_BOOLEAN ) )
            // InternalMCL.g:7621:2: ( RULE_BOOLEAN )
            {
            // InternalMCL.g:7621:2: ( RULE_BOOLEAN )
            // InternalMCL.g:7622:3: RULE_BOOLEAN
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBooleanExpressionAccess().getValueBOOLEANTerminalRuleCall_1_0()); 
            }
            match(input,RULE_BOOLEAN,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBooleanExpressionAccess().getValueBOOLEANTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBooleanExpression__ValueAssignment_1"


    // $ANTLR start "rule__GIntegerExpression__ValueAssignment_1"
    // InternalMCL.g:7631:1: rule__GIntegerExpression__ValueAssignment_1 : ( RULE_INT ) ;
    public final void rule__GIntegerExpression__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7635:1: ( ( RULE_INT ) )
            // InternalMCL.g:7636:2: ( RULE_INT )
            {
            // InternalMCL.g:7636:2: ( RULE_INT )
            // InternalMCL.g:7637:3: RULE_INT
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIntegerExpressionAccess().getValueINTTerminalRuleCall_1_0()); 
            }
            match(input,RULE_INT,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIntegerExpressionAccess().getValueINTTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIntegerExpression__ValueAssignment_1"


    // $ANTLR start "rule__GDoubleExpression__ValueAssignment_1"
    // InternalMCL.g:7646:1: rule__GDoubleExpression__ValueAssignment_1 : ( RULE_DOUBLE ) ;
    public final void rule__GDoubleExpression__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7650:1: ( ( RULE_DOUBLE ) )
            // InternalMCL.g:7651:2: ( RULE_DOUBLE )
            {
            // InternalMCL.g:7651:2: ( RULE_DOUBLE )
            // InternalMCL.g:7652:3: RULE_DOUBLE
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGDoubleExpressionAccess().getValueDOUBLETerminalRuleCall_1_0()); 
            }
            match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGDoubleExpressionAccess().getValueDOUBLETerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GDoubleExpression__ValueAssignment_1"


    // $ANTLR start "rule__GEnumLiteralExpression__ValueAssignment_2"
    // InternalMCL.g:7661:1: rule__GEnumLiteralExpression__ValueAssignment_2 : ( ( ruleQualifiedName ) ) ;
    public final void rule__GEnumLiteralExpression__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7665:1: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:7666:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:7666:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:7667:3: ( ruleQualifiedName )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }
            // InternalMCL.g:7668:3: ( ruleQualifiedName )
            // InternalMCL.g:7669:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGEnumLiteralExpressionAccess().getValueEEnumLiteralQualifiedNameParserRuleCall_2_0_1()); 
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getValueEEnumLiteralQualifiedNameParserRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGEnumLiteralExpressionAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GEnumLiteralExpression__ValueAssignment_2"


    // $ANTLR start "rule__GIfExpression__ConditionAssignment_2"
    // InternalMCL.g:7680:1: rule__GIfExpression__ConditionAssignment_2 : ( ruleGExpression ) ;
    public final void rule__GIfExpression__ConditionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7684:1: ( ( ruleGExpression ) )
            // InternalMCL.g:7685:2: ( ruleGExpression )
            {
            // InternalMCL.g:7685:2: ( ruleGExpression )
            // InternalMCL.g:7686:3: ruleGExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getConditionGExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getConditionGExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__ConditionAssignment_2"


    // $ANTLR start "rule__GIfExpression__ThenExpressionAssignment_4"
    // InternalMCL.g:7695:1: rule__GIfExpression__ThenExpressionAssignment_4 : ( ruleGExpression ) ;
    public final void rule__GIfExpression__ThenExpressionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7699:1: ( ( ruleGExpression ) )
            // InternalMCL.g:7700:2: ( ruleGExpression )
            {
            // InternalMCL.g:7700:2: ( ruleGExpression )
            // InternalMCL.g:7701:3: ruleGExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getThenExpressionGExpressionParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getThenExpressionGExpressionParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__ThenExpressionAssignment_4"


    // $ANTLR start "rule__GIfExpression__ElseExpressionAssignment_6"
    // InternalMCL.g:7710:1: rule__GIfExpression__ElseExpressionAssignment_6 : ( ruleGExpression ) ;
    public final void rule__GIfExpression__ElseExpressionAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7714:1: ( ( ruleGExpression ) )
            // InternalMCL.g:7715:2: ( ruleGExpression )
            {
            // InternalMCL.g:7715:2: ( ruleGExpression )
            // InternalMCL.g:7716:3: ruleGExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGIfExpressionAccess().getElseExpressionGExpressionParserRuleCall_6_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGIfExpressionAccess().getElseExpressionGExpressionParserRuleCall_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GIfExpression__ElseExpressionAssignment_6"


    // $ANTLR start "rule__GBraceExpression__InnerExpressionAssignment_2"
    // InternalMCL.g:7725:1: rule__GBraceExpression__InnerExpressionAssignment_2 : ( ruleGExpression ) ;
    public final void rule__GBraceExpression__InnerExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMCL.g:7729:1: ( ( ruleGExpression ) )
            // InternalMCL.g:7730:2: ( ruleGExpression )
            {
            // InternalMCL.g:7730:2: ( ruleGExpression )
            // InternalMCL.g:7731:3: ruleGExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGBraceExpressionAccess().getInnerExpressionGExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_2);
            ruleGExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGBraceExpressionAccess().getInnerExpressionGExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GBraceExpression__InnerExpressionAssignment_2"

    // $ANTLR start synpred51_InternalMCL
    public final void synpred51_InternalMCL_fragment() throws RecognitionException {   
        // InternalMCL.g:6624:3: ( rule__QualifiedName__Group_1__0 )
        // InternalMCL.g:6624:3: rule__QualifiedName__Group_1__0
        {
        pushFollow(FOLLOW_2);
        rule__QualifiedName__Group_1__0();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred51_InternalMCL

    // Delegated rules

    public final boolean synpred51_InternalMCL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred51_InternalMCL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA3 dfa3 = new DFA3(this);
    protected DFA4 dfa4 = new DFA4(this);
    protected DFA5 dfa5 = new DFA5(this);
    static final String dfa_1s = "\14\uffff";
    static final String dfa_2s = "\1\5\3\uffff\1\51\1\uffff\1\5\1\22\1\5\2\uffff\1\22";
    static final String dfa_3s = "\1\54\3\uffff\1\51\1\uffff\1\5\1\60\1\5\2\uffff\1\60";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\3\1\uffff\1\6\3\uffff\1\4\1\5\1\uffff";
    static final String dfa_5s = "\14\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\34\uffff\1\5\5\uffff\1\3\2\uffff\1\1\1\4",
            "",
            "",
            "",
            "\1\6",
            "",
            "\1\7",
            "\1\10\32\uffff\1\11\2\uffff\1\12",
            "\1\13",
            "",
            "",
            "\1\10\32\uffff\1\11\2\uffff\1\12"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1623:1: rule__TriggeringCondition__Alternatives : ( ( rulePeriodicExpression ) | ( ruleThresholdExpression ) | ( ruleEventTrigger ) | ( ruleUpdated ) | ( ruleReadyToRead ) | ( ( rule__TriggeringCondition__Group_5__0 ) ) );";
        }
    }
    static final String dfa_7s = "\10\uffff";
    static final String dfa_8s = "\1\uffff\1\6\5\uffff\1\6";
    static final String dfa_9s = "\1\5\1\22\2\uffff\1\5\2\uffff\1\22";
    static final String dfa_10s = "\1\33\1\37\2\uffff\1\5\2\uffff\1\37";
    static final String dfa_11s = "\2\uffff\1\2\1\3\1\uffff\1\4\1\1\1\uffff";
    static final String dfa_12s = "\10\uffff}>";
    static final String[] dfa_13s = {
            "\1\1\12\uffff\2\3\11\uffff\1\2",
            "\1\4\1\6\6\uffff\2\5\3\uffff\1\6",
            "",
            "",
            "\1\7",
            "",
            "",
            "\1\4\1\6\6\uffff\2\5\3\uffff\1\6"
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "1668:1: rule__OtherExpression__Alternatives : ( ( rulePortRef ) | ( ruleUnaryMinus ) | ( ruleNot ) | ( ruleBinaryExpression ) );";
        }
    }
    static final String dfa_14s = "\6\uffff";
    static final String dfa_15s = "\1\5\1\22\1\5\2\uffff\1\22";
    static final String dfa_16s = "\1\5\1\33\1\5\2\uffff\1\33";
    static final String dfa_17s = "\3\uffff\1\2\1\1\1\uffff";
    static final String dfa_18s = "\6\uffff}>";
    static final String[] dfa_19s = {
            "\1\1",
            "\1\2\7\uffff\1\3\1\4",
            "\1\5",
            "",
            "",
            "\1\2\7\uffff\1\3\1\4"
    };

    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final char[] dfa_16 = DFA.unpackEncodedStringToUnsignedChars(dfa_16s);
    static final short[] dfa_17 = DFA.unpackEncodedString(dfa_17s);
    static final short[] dfa_18 = DFA.unpackEncodedString(dfa_18s);
    static final short[][] dfa_19 = unpackEncodedStringArray(dfa_19s);

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = dfa_14;
            this.eof = dfa_14;
            this.min = dfa_15;
            this.max = dfa_16;
            this.accept = dfa_17;
            this.special = dfa_18;
            this.transition = dfa_19;
        }
        public String getDescription() {
            return "1701:1: rule__BinaryExpression__Alternatives : ( ( ruleBinaryMinus ) | ( ruleBinaryPlus ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000008080000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000500000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000190400000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000004200000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000008030020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x4004000000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x4008000000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x4010000000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x4020000000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x4040000000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x06000004400201F0L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000300002L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000003C00000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000003C00002L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000030000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000030000002L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x06000004000001F0L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x00000000000C0002L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x00000000000000C0L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x06000004000001D0L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000000040002L});

}