package fr.inria.glose.mcl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.inria.glose.mcl.services.MCLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalMCLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_DOUBLE", "RULE_BOOLEAN", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Connector'", "'('", "'from'", "'to'", "')'", "'when'", "'sync'", "'do'", "'load'", "'or'", "'and'", "'event'", "'on'", "'occurs'", "'every'", "'value'", "'has'", "'been'", "'Updated'", "'is'", "'ReadyToRead'", "'state'", "'ZeroCrossingDetection'", "'Extrapolation'", "'Interpolation'", "'DiscontinuityLocator'", "'HistoryProvider'", "'>'", "'<'", "'-'", "'!'", "'not'", "'+'", "'->'", "'='", "'tolerance'", "'import'", "'#'", "'if'", "'then'", "'else'", "'endif'", "'.'", "'xor'", "'<>'", "'<='", "'>='", "'*'", "'/'", "'~'"
    };
    public static final int T__50=50;
    public static final int RULE_BOOLEAN=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=7;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMCLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMCLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMCLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMCL.g"; }



     	private MCLGrammarAccess grammarAccess;

        public InternalMCLParser(TokenStream input, MCLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "MCSpecification";
       	}

       	@Override
       	protected MCLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleMCSpecification"
    // InternalMCL.g:65:1: entryRuleMCSpecification returns [EObject current=null] : iv_ruleMCSpecification= ruleMCSpecification EOF ;
    public final EObject entryRuleMCSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMCSpecification = null;


        try {
            // InternalMCL.g:65:56: (iv_ruleMCSpecification= ruleMCSpecification EOF )
            // InternalMCL.g:66:2: iv_ruleMCSpecification= ruleMCSpecification EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMCSpecificationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMCSpecification=ruleMCSpecification();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMCSpecification; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMCSpecification"


    // $ANTLR start "ruleMCSpecification"
    // InternalMCL.g:72:1: ruleMCSpecification returns [EObject current=null] : ( () ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )? ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )? ) ;
    public final EObject ruleMCSpecification() throws RecognitionException {
        EObject current = null;

        EObject lv_importedMBI_1_0 = null;

        EObject lv_importedMBI_2_0 = null;

        EObject lv_ownedConnectors_3_0 = null;

        EObject lv_ownedConnectors_4_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:78:2: ( ( () ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )? ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )? ) )
            // InternalMCL.g:79:2: ( () ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )? ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )? )
            {
            // InternalMCL.g:79:2: ( () ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )? ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )? )
            // InternalMCL.g:80:3: () ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )? ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )?
            {
            // InternalMCL.g:80:3: ()
            // InternalMCL.g:81:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMCSpecificationAccess().getMCSpecificationAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:87:3: ( ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )* )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==21) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalMCL.g:88:4: ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) ) ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )*
                    {
                    // InternalMCL.g:88:4: ( (lv_importedMBI_1_0= ruleImportInterfaceStatement ) )
                    // InternalMCL.g:89:5: (lv_importedMBI_1_0= ruleImportInterfaceStatement )
                    {
                    // InternalMCL.g:89:5: (lv_importedMBI_1_0= ruleImportInterfaceStatement )
                    // InternalMCL.g:90:6: lv_importedMBI_1_0= ruleImportInterfaceStatement
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_3);
                    lv_importedMBI_1_0=ruleImportInterfaceStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getMCSpecificationRule());
                      						}
                      						add(
                      							current,
                      							"importedMBI",
                      							lv_importedMBI_1_0,
                      							"fr.inria.glose.mcl.MCL.ImportInterfaceStatement");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalMCL.g:107:4: ( (lv_importedMBI_2_0= ruleImportInterfaceStatement ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==21) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalMCL.g:108:5: (lv_importedMBI_2_0= ruleImportInterfaceStatement )
                    	    {
                    	    // InternalMCL.g:108:5: (lv_importedMBI_2_0= ruleImportInterfaceStatement )
                    	    // InternalMCL.g:109:6: lv_importedMBI_2_0= ruleImportInterfaceStatement
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getMCSpecificationAccess().getImportedMBIImportInterfaceStatementParserRuleCall_1_1_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_3);
                    	    lv_importedMBI_2_0=ruleImportInterfaceStatement();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getMCSpecificationRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"importedMBI",
                    	      							lv_importedMBI_2_0,
                    	      							"fr.inria.glose.mcl.MCL.ImportInterfaceStatement");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalMCL.g:127:3: ( ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalMCL.g:128:4: ( (lv_ownedConnectors_3_0= ruleConnector ) ) ( (lv_ownedConnectors_4_0= ruleConnector ) )*
                    {
                    // InternalMCL.g:128:4: ( (lv_ownedConnectors_3_0= ruleConnector ) )
                    // InternalMCL.g:129:5: (lv_ownedConnectors_3_0= ruleConnector )
                    {
                    // InternalMCL.g:129:5: (lv_ownedConnectors_3_0= ruleConnector )
                    // InternalMCL.g:130:6: lv_ownedConnectors_3_0= ruleConnector
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_0_0());
                      					
                    }
                    pushFollow(FOLLOW_4);
                    lv_ownedConnectors_3_0=ruleConnector();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getMCSpecificationRule());
                      						}
                      						add(
                      							current,
                      							"ownedConnectors",
                      							lv_ownedConnectors_3_0,
                      							"fr.inria.glose.mcl.MCL.Connector");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalMCL.g:147:4: ( (lv_ownedConnectors_4_0= ruleConnector ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==13) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalMCL.g:148:5: (lv_ownedConnectors_4_0= ruleConnector )
                    	    {
                    	    // InternalMCL.g:148:5: (lv_ownedConnectors_4_0= ruleConnector )
                    	    // InternalMCL.g:149:6: lv_ownedConnectors_4_0= ruleConnector
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getMCSpecificationAccess().getOwnedConnectorsConnectorParserRuleCall_2_1_0());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_4);
                    	    lv_ownedConnectors_4_0=ruleConnector();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						if (current==null) {
                    	      							current = createModelElementForParent(grammarAccess.getMCSpecificationRule());
                    	      						}
                    	      						add(
                    	      							current,
                    	      							"ownedConnectors",
                    	      							lv_ownedConnectors_4_0,
                    	      							"fr.inria.glose.mcl.MCL.Connector");
                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMCSpecification"


    // $ANTLR start "entryRuleSynchronizationConstraint"
    // InternalMCL.g:171:1: entryRuleSynchronizationConstraint returns [EObject current=null] : iv_ruleSynchronizationConstraint= ruleSynchronizationConstraint EOF ;
    public final EObject entryRuleSynchronizationConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSynchronizationConstraint = null;


        try {
            // InternalMCL.g:171:66: (iv_ruleSynchronizationConstraint= ruleSynchronizationConstraint EOF )
            // InternalMCL.g:172:2: iv_ruleSynchronizationConstraint= ruleSynchronizationConstraint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSynchronizationConstraintRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSynchronizationConstraint=ruleSynchronizationConstraint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSynchronizationConstraint; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSynchronizationConstraint"


    // $ANTLR start "ruleSynchronizationConstraint"
    // InternalMCL.g:178:1: ruleSynchronizationConstraint returns [EObject current=null] : this_SimpleSync_0= ruleSimpleSync ;
    public final EObject ruleSynchronizationConstraint() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleSync_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:184:2: (this_SimpleSync_0= ruleSimpleSync )
            // InternalMCL.g:185:2: this_SimpleSync_0= ruleSimpleSync
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getSynchronizationConstraintAccess().getSimpleSyncParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_SimpleSync_0=ruleSimpleSync();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_SimpleSync_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSynchronizationConstraint"


    // $ANTLR start "entryRuleExpOperation"
    // InternalMCL.g:196:1: entryRuleExpOperation returns [EObject current=null] : iv_ruleExpOperation= ruleExpOperation EOF ;
    public final EObject entryRuleExpOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpOperation = null;


        try {
            // InternalMCL.g:196:53: (iv_ruleExpOperation= ruleExpOperation EOF )
            // InternalMCL.g:197:2: iv_ruleExpOperation= ruleExpOperation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpOperationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExpOperation=ruleExpOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpOperation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpOperation"


    // $ANTLR start "ruleExpOperation"
    // InternalMCL.g:203:1: ruleExpOperation returns [EObject current=null] : (this_Greater_0= ruleGreater | this_Lesser_1= ruleLesser ) ;
    public final EObject ruleExpOperation() throws RecognitionException {
        EObject current = null;

        EObject this_Greater_0 = null;

        EObject this_Lesser_1 = null;



        	enterRule();

        try {
            // InternalMCL.g:209:2: ( (this_Greater_0= ruleGreater | this_Lesser_1= ruleLesser ) )
            // InternalMCL.g:210:2: (this_Greater_0= ruleGreater | this_Lesser_1= ruleLesser )
            {
            // InternalMCL.g:210:2: (this_Greater_0= ruleGreater | this_Lesser_1= ruleLesser )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==40) ) {
                alt5=1;
            }
            else if ( (LA5_0==41) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalMCL.g:211:3: this_Greater_0= ruleGreater
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpOperationAccess().getGreaterParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Greater_0=ruleGreater();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Greater_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:220:3: this_Lesser_1= ruleLesser
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getExpOperationAccess().getLesserParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Lesser_1=ruleLesser();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Lesser_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpOperation"


    // $ANTLR start "entryRuleConnector"
    // InternalMCL.g:232:1: entryRuleConnector returns [EObject current=null] : iv_ruleConnector= ruleConnector EOF ;
    public final EObject entryRuleConnector() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConnector = null;


        try {
            // InternalMCL.g:232:50: (iv_ruleConnector= ruleConnector EOF )
            // InternalMCL.g:233:2: iv_ruleConnector= ruleConnector EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConnectorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConnector=ruleConnector();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConnector; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConnector"


    // $ANTLR start "ruleConnector"
    // InternalMCL.g:239:1: ruleConnector returns [EObject current=null] : (otherlv_0= 'Connector' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )? otherlv_8= 'when' ( (lv_trigger_9_0= ruleTriggeringCondition ) ) (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )? otherlv_12= 'do' ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) ) ) ;
    public final EObject ruleConnector() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        EObject lv_from_4_0 = null;

        EObject lv_to_6_0 = null;

        EObject lv_trigger_9_0 = null;

        EObject lv_synchronizationRule_11_0 = null;

        EObject lv_interactionstatement_13_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:245:2: ( (otherlv_0= 'Connector' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )? otherlv_8= 'when' ( (lv_trigger_9_0= ruleTriggeringCondition ) ) (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )? otherlv_12= 'do' ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) ) ) )
            // InternalMCL.g:246:2: (otherlv_0= 'Connector' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )? otherlv_8= 'when' ( (lv_trigger_9_0= ruleTriggeringCondition ) ) (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )? otherlv_12= 'do' ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) ) )
            {
            // InternalMCL.g:246:2: (otherlv_0= 'Connector' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )? otherlv_8= 'when' ( (lv_trigger_9_0= ruleTriggeringCondition ) ) (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )? otherlv_12= 'do' ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) ) )
            // InternalMCL.g:247:3: otherlv_0= 'Connector' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )? otherlv_8= 'when' ( (lv_trigger_9_0= ruleTriggeringCondition ) ) (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )? otherlv_12= 'do' ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getConnectorAccess().getConnectorKeyword_0());
              		
            }
            // InternalMCL.g:251:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalMCL.g:252:4: (lv_name_1_0= RULE_ID )
            {
            // InternalMCL.g:252:4: (lv_name_1_0= RULE_ID )
            // InternalMCL.g:253:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getConnectorAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getConnectorRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalMCL.g:269:3: (otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMCL.g:270:4: otherlv_2= '(' otherlv_3= 'from' ( (lv_from_4_0= rulePortRef ) ) otherlv_5= 'to' ( (lv_to_6_0= rulePortRef ) ) otherlv_7= ')'
                    {
                    otherlv_2=(Token)match(input,14,FOLLOW_7); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getConnectorAccess().getLeftParenthesisKeyword_2_0());
                      			
                    }
                    otherlv_3=(Token)match(input,15,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getConnectorAccess().getFromKeyword_2_1());
                      			
                    }
                    // InternalMCL.g:278:4: ( (lv_from_4_0= rulePortRef ) )
                    // InternalMCL.g:279:5: (lv_from_4_0= rulePortRef )
                    {
                    // InternalMCL.g:279:5: (lv_from_4_0= rulePortRef )
                    // InternalMCL.g:280:6: lv_from_4_0= rulePortRef
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConnectorAccess().getFromPortRefParserRuleCall_2_2_0());
                      					
                    }
                    pushFollow(FOLLOW_8);
                    lv_from_4_0=rulePortRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConnectorRule());
                      						}
                      						set(
                      							current,
                      							"from",
                      							lv_from_4_0,
                      							"fr.inria.glose.mcl.MCL.PortRef");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,16,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getConnectorAccess().getToKeyword_2_3());
                      			
                    }
                    // InternalMCL.g:301:4: ( (lv_to_6_0= rulePortRef ) )
                    // InternalMCL.g:302:5: (lv_to_6_0= rulePortRef )
                    {
                    // InternalMCL.g:302:5: (lv_to_6_0= rulePortRef )
                    // InternalMCL.g:303:6: lv_to_6_0= rulePortRef
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConnectorAccess().getToPortRefParserRuleCall_2_4_0());
                      					
                    }
                    pushFollow(FOLLOW_9);
                    lv_to_6_0=rulePortRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConnectorRule());
                      						}
                      						set(
                      							current,
                      							"to",
                      							lv_to_6_0,
                      							"fr.inria.glose.mcl.MCL.PortRef");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_7=(Token)match(input,17,FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getConnectorAccess().getRightParenthesisKeyword_2_5());
                      			
                    }

                    }
                    break;

            }

            otherlv_8=(Token)match(input,18,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getConnectorAccess().getWhenKeyword_3());
              		
            }
            // InternalMCL.g:329:3: ( (lv_trigger_9_0= ruleTriggeringCondition ) )
            // InternalMCL.g:330:4: (lv_trigger_9_0= ruleTriggeringCondition )
            {
            // InternalMCL.g:330:4: (lv_trigger_9_0= ruleTriggeringCondition )
            // InternalMCL.g:331:5: lv_trigger_9_0= ruleTriggeringCondition
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConnectorAccess().getTriggerTriggeringConditionParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_12);
            lv_trigger_9_0=ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConnectorRule());
              					}
              					set(
              						current,
              						"trigger",
              						lv_trigger_9_0,
              						"fr.inria.glose.mcl.MCL.TriggeringCondition");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalMCL.g:348:3: (otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalMCL.g:349:4: otherlv_10= 'sync' ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) )
                    {
                    otherlv_10=(Token)match(input,19,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_10, grammarAccess.getConnectorAccess().getSyncKeyword_5_0());
                      			
                    }
                    // InternalMCL.g:353:4: ( (lv_synchronizationRule_11_0= ruleSynchronizationConstraint ) )
                    // InternalMCL.g:354:5: (lv_synchronizationRule_11_0= ruleSynchronizationConstraint )
                    {
                    // InternalMCL.g:354:5: (lv_synchronizationRule_11_0= ruleSynchronizationConstraint )
                    // InternalMCL.g:355:6: lv_synchronizationRule_11_0= ruleSynchronizationConstraint
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConnectorAccess().getSynchronizationRuleSynchronizationConstraintParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_13);
                    lv_synchronizationRule_11_0=ruleSynchronizationConstraint();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConnectorRule());
                      						}
                      						set(
                      							current,
                      							"synchronizationRule",
                      							lv_synchronizationRule_11_0,
                      							"fr.inria.glose.mcl.MCL.SynchronizationConstraint");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_12=(Token)match(input,20,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_12, grammarAccess.getConnectorAccess().getDoKeyword_6());
              		
            }
            // InternalMCL.g:377:3: ( (lv_interactionstatement_13_0= ruleAbstractInteraction ) )
            // InternalMCL.g:378:4: (lv_interactionstatement_13_0= ruleAbstractInteraction )
            {
            // InternalMCL.g:378:4: (lv_interactionstatement_13_0= ruleAbstractInteraction )
            // InternalMCL.g:379:5: lv_interactionstatement_13_0= ruleAbstractInteraction
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConnectorAccess().getInteractionstatementAbstractInteractionParserRuleCall_7_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_interactionstatement_13_0=ruleAbstractInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConnectorRule());
              					}
              					set(
              						current,
              						"interactionstatement",
              						lv_interactionstatement_13_0,
              						"fr.inria.glose.mcl.MCL.AbstractInteraction");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConnector"


    // $ANTLR start "entryRuleImportInterfaceStatement"
    // InternalMCL.g:400:1: entryRuleImportInterfaceStatement returns [EObject current=null] : iv_ruleImportInterfaceStatement= ruleImportInterfaceStatement EOF ;
    public final EObject entryRuleImportInterfaceStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportInterfaceStatement = null;


        try {
            // InternalMCL.g:400:65: (iv_ruleImportInterfaceStatement= ruleImportInterfaceStatement EOF )
            // InternalMCL.g:401:2: iv_ruleImportInterfaceStatement= ruleImportInterfaceStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportInterfaceStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImportInterfaceStatement=ruleImportInterfaceStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImportInterfaceStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportInterfaceStatement"


    // $ANTLR start "ruleImportInterfaceStatement"
    // InternalMCL.g:407:1: ruleImportInterfaceStatement returns [EObject current=null] : (otherlv_0= 'load' ( (lv_importURI_1_0= ruleEString ) ) ) ;
    public final EObject ruleImportInterfaceStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importURI_1_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:413:2: ( (otherlv_0= 'load' ( (lv_importURI_1_0= ruleEString ) ) ) )
            // InternalMCL.g:414:2: (otherlv_0= 'load' ( (lv_importURI_1_0= ruleEString ) ) )
            {
            // InternalMCL.g:414:2: (otherlv_0= 'load' ( (lv_importURI_1_0= ruleEString ) ) )
            // InternalMCL.g:415:3: otherlv_0= 'load' ( (lv_importURI_1_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getImportInterfaceStatementAccess().getLoadKeyword_0());
              		
            }
            // InternalMCL.g:419:3: ( (lv_importURI_1_0= ruleEString ) )
            // InternalMCL.g:420:4: (lv_importURI_1_0= ruleEString )
            {
            // InternalMCL.g:420:4: (lv_importURI_1_0= ruleEString )
            // InternalMCL.g:421:5: lv_importURI_1_0= ruleEString
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportInterfaceStatementAccess().getImportURIEStringParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_importURI_1_0=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportInterfaceStatementRule());
              					}
              					set(
              						current,
              						"importURI",
              						lv_importURI_1_0,
              						"fr.inria.glose.mcl.MCL.EString");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportInterfaceStatement"


    // $ANTLR start "entryRuleEString"
    // InternalMCL.g:442:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalMCL.g:442:47: (iv_ruleEString= ruleEString EOF )
            // InternalMCL.g:443:2: iv_ruleEString= ruleEString EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEStringRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEString.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMCL.g:449:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalMCL.g:455:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalMCL.g:456:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalMCL.g:456:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalMCL.g:457:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_STRING_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:465:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAbstractInteraction"
    // InternalMCL.g:476:1: entryRuleAbstractInteraction returns [EObject current=null] : iv_ruleAbstractInteraction= ruleAbstractInteraction EOF ;
    public final EObject entryRuleAbstractInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractInteraction = null;


        try {
            // InternalMCL.g:476:60: (iv_ruleAbstractInteraction= ruleAbstractInteraction EOF )
            // InternalMCL.g:477:2: iv_ruleAbstractInteraction= ruleAbstractInteraction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractInteractionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAbstractInteraction=ruleAbstractInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractInteraction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractInteraction"


    // $ANTLR start "ruleAbstractInteraction"
    // InternalMCL.g:483:1: ruleAbstractInteraction returns [EObject current=null] : this_AssignmentExpression_0= ruleAssignmentExpression ;
    public final EObject ruleAbstractInteraction() throws RecognitionException {
        EObject current = null;

        EObject this_AssignmentExpression_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:489:2: (this_AssignmentExpression_0= ruleAssignmentExpression )
            // InternalMCL.g:490:2: this_AssignmentExpression_0= ruleAssignmentExpression
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getAbstractInteractionAccess().getAssignmentExpressionParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_AssignmentExpression_0=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_AssignmentExpression_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractInteraction"


    // $ANTLR start "entryRuleThresholdExpression"
    // InternalMCL.g:501:1: entryRuleThresholdExpression returns [EObject current=null] : iv_ruleThresholdExpression= ruleThresholdExpression EOF ;
    public final EObject entryRuleThresholdExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThresholdExpression = null;


        try {
            // InternalMCL.g:501:60: (iv_ruleThresholdExpression= ruleThresholdExpression EOF )
            // InternalMCL.g:502:2: iv_ruleThresholdExpression= ruleThresholdExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getThresholdExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleThresholdExpression=ruleThresholdExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleThresholdExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThresholdExpression"


    // $ANTLR start "ruleThresholdExpression"
    // InternalMCL.g:508:1: ruleThresholdExpression returns [EObject current=null] : ( ( (lv_triggerSourcePort_0_0= rulePortRef ) ) ( (lv_setoperation_1_0= ruleExpOperation ) ) ( (lv_value_2_0= ruleVariable ) ) ) ;
    public final EObject ruleThresholdExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_triggerSourcePort_0_0 = null;

        EObject lv_setoperation_1_0 = null;

        EObject lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:514:2: ( ( ( (lv_triggerSourcePort_0_0= rulePortRef ) ) ( (lv_setoperation_1_0= ruleExpOperation ) ) ( (lv_value_2_0= ruleVariable ) ) ) )
            // InternalMCL.g:515:2: ( ( (lv_triggerSourcePort_0_0= rulePortRef ) ) ( (lv_setoperation_1_0= ruleExpOperation ) ) ( (lv_value_2_0= ruleVariable ) ) )
            {
            // InternalMCL.g:515:2: ( ( (lv_triggerSourcePort_0_0= rulePortRef ) ) ( (lv_setoperation_1_0= ruleExpOperation ) ) ( (lv_value_2_0= ruleVariable ) ) )
            // InternalMCL.g:516:3: ( (lv_triggerSourcePort_0_0= rulePortRef ) ) ( (lv_setoperation_1_0= ruleExpOperation ) ) ( (lv_value_2_0= ruleVariable ) )
            {
            // InternalMCL.g:516:3: ( (lv_triggerSourcePort_0_0= rulePortRef ) )
            // InternalMCL.g:517:4: (lv_triggerSourcePort_0_0= rulePortRef )
            {
            // InternalMCL.g:517:4: (lv_triggerSourcePort_0_0= rulePortRef )
            // InternalMCL.g:518:5: lv_triggerSourcePort_0_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getThresholdExpressionAccess().getTriggerSourcePortPortRefParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_16);
            lv_triggerSourcePort_0_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getThresholdExpressionRule());
              					}
              					set(
              						current,
              						"triggerSourcePort",
              						lv_triggerSourcePort_0_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalMCL.g:535:3: ( (lv_setoperation_1_0= ruleExpOperation ) )
            // InternalMCL.g:536:4: (lv_setoperation_1_0= ruleExpOperation )
            {
            // InternalMCL.g:536:4: (lv_setoperation_1_0= ruleExpOperation )
            // InternalMCL.g:537:5: lv_setoperation_1_0= ruleExpOperation
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getThresholdExpressionAccess().getSetoperationExpOperationParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_17);
            lv_setoperation_1_0=ruleExpOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getThresholdExpressionRule());
              					}
              					set(
              						current,
              						"setoperation",
              						lv_setoperation_1_0,
              						"fr.inria.glose.mcl.MCL.ExpOperation");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalMCL.g:554:3: ( (lv_value_2_0= ruleVariable ) )
            // InternalMCL.g:555:4: (lv_value_2_0= ruleVariable )
            {
            // InternalMCL.g:555:4: (lv_value_2_0= ruleVariable )
            // InternalMCL.g:556:5: lv_value_2_0= ruleVariable
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getThresholdExpressionAccess().getValueVariableParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getThresholdExpressionRule());
              					}
              					set(
              						current,
              						"value",
              						lv_value_2_0,
              						"fr.inria.glose.mcl.MCL.Variable");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThresholdExpression"


    // $ANTLR start "entryRuleDisjoint"
    // InternalMCL.g:577:1: entryRuleDisjoint returns [EObject current=null] : iv_ruleDisjoint= ruleDisjoint EOF ;
    public final EObject entryRuleDisjoint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjoint = null;


        try {
            // InternalMCL.g:577:49: (iv_ruleDisjoint= ruleDisjoint EOF )
            // InternalMCL.g:578:2: iv_ruleDisjoint= ruleDisjoint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjointRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDisjoint=ruleDisjoint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjoint; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjoint"


    // $ANTLR start "ruleDisjoint"
    // InternalMCL.g:584:1: ruleDisjoint returns [EObject current=null] : (this_Union_0= ruleUnion ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )* ) ;
    public final EObject ruleDisjoint() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Union_0 = null;

        EObject lv_rightOp_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:590:2: ( (this_Union_0= ruleUnion ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )* ) )
            // InternalMCL.g:591:2: (this_Union_0= ruleUnion ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )* )
            {
            // InternalMCL.g:591:2: (this_Union_0= ruleUnion ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )* )
            // InternalMCL.g:592:3: this_Union_0= ruleUnion ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getDisjointAccess().getUnionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_18);
            this_Union_0=ruleUnion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_Union_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:600:3: ( () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==22) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMCL.g:601:4: () otherlv_2= 'or' ( (lv_rightOp_3_0= ruleUnion ) )
            	    {
            	    // InternalMCL.g:601:4: ()
            	    // InternalMCL.g:602:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getDisjointAccess().getBinaryExpOperatorLeftOpAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    otherlv_2=(Token)match(input,22,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getDisjointAccess().getOrKeyword_1_1());
            	      			
            	    }
            	    // InternalMCL.g:612:4: ( (lv_rightOp_3_0= ruleUnion ) )
            	    // InternalMCL.g:613:5: (lv_rightOp_3_0= ruleUnion )
            	    {
            	    // InternalMCL.g:613:5: (lv_rightOp_3_0= ruleUnion )
            	    // InternalMCL.g:614:6: lv_rightOp_3_0= ruleUnion
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getDisjointAccess().getRightOpUnionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_rightOp_3_0=ruleUnion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getDisjointRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOp",
            	      							lv_rightOp_3_0,
            	      							"fr.inria.glose.mcl.MCL.Union");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjoint"


    // $ANTLR start "entryRuleUnion"
    // InternalMCL.g:636:1: entryRuleUnion returns [EObject current=null] : iv_ruleUnion= ruleUnion EOF ;
    public final EObject entryRuleUnion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnion = null;


        try {
            // InternalMCL.g:636:46: (iv_ruleUnion= ruleUnion EOF )
            // InternalMCL.g:637:2: iv_ruleUnion= ruleUnion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnion=ruleUnion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnion; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnion"


    // $ANTLR start "ruleUnion"
    // InternalMCL.g:643:1: ruleUnion returns [EObject current=null] : (this_TriggeringCondition_0= ruleTriggeringCondition ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )* ) ;
    public final EObject ruleUnion() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_TriggeringCondition_0 = null;

        EObject lv_rightOp_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:649:2: ( (this_TriggeringCondition_0= ruleTriggeringCondition ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )* ) )
            // InternalMCL.g:650:2: (this_TriggeringCondition_0= ruleTriggeringCondition ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )* )
            {
            // InternalMCL.g:650:2: (this_TriggeringCondition_0= ruleTriggeringCondition ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )* )
            // InternalMCL.g:651:3: this_TriggeringCondition_0= ruleTriggeringCondition ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getUnionAccess().getTriggeringConditionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_19);
            this_TriggeringCondition_0=ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_TriggeringCondition_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:659:3: ( () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==23) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMCL.g:660:4: () otherlv_2= 'and' ( (lv_rightOp_3_0= ruleTriggeringCondition ) )
            	    {
            	    // InternalMCL.g:660:4: ()
            	    // InternalMCL.g:661:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getUnionAccess().getBinaryExpOperatorLeftOpAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    otherlv_2=(Token)match(input,23,FOLLOW_11); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getUnionAccess().getAndKeyword_1_1());
            	      			
            	    }
            	    // InternalMCL.g:671:4: ( (lv_rightOp_3_0= ruleTriggeringCondition ) )
            	    // InternalMCL.g:672:5: (lv_rightOp_3_0= ruleTriggeringCondition )
            	    {
            	    // InternalMCL.g:672:5: (lv_rightOp_3_0= ruleTriggeringCondition )
            	    // InternalMCL.g:673:6: lv_rightOp_3_0= ruleTriggeringCondition
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getUnionAccess().getRightOpTriggeringConditionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_19);
            	    lv_rightOp_3_0=ruleTriggeringCondition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getUnionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOp",
            	      							lv_rightOp_3_0,
            	      							"fr.inria.glose.mcl.MCL.TriggeringCondition");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnion"


    // $ANTLR start "entryRuleTriggeringCondition"
    // InternalMCL.g:695:1: entryRuleTriggeringCondition returns [EObject current=null] : iv_ruleTriggeringCondition= ruleTriggeringCondition EOF ;
    public final EObject entryRuleTriggeringCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTriggeringCondition = null;


        try {
            // InternalMCL.g:695:60: (iv_ruleTriggeringCondition= ruleTriggeringCondition EOF )
            // InternalMCL.g:696:2: iv_ruleTriggeringCondition= ruleTriggeringCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTriggeringConditionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTriggeringCondition=ruleTriggeringCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTriggeringCondition; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTriggeringCondition"


    // $ANTLR start "ruleTriggeringCondition"
    // InternalMCL.g:702:1: ruleTriggeringCondition returns [EObject current=null] : (this_PeriodicExpression_0= rulePeriodicExpression | this_ThresholdExpression_1= ruleThresholdExpression | this_EventTrigger_2= ruleEventTrigger | this_Updated_3= ruleUpdated | this_ReadyToRead_4= ruleReadyToRead | (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' ) ) ;
    public final EObject ruleTriggeringCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject this_PeriodicExpression_0 = null;

        EObject this_ThresholdExpression_1 = null;

        EObject this_EventTrigger_2 = null;

        EObject this_Updated_3 = null;

        EObject this_ReadyToRead_4 = null;

        EObject this_Disjoint_6 = null;



        	enterRule();

        try {
            // InternalMCL.g:708:2: ( (this_PeriodicExpression_0= rulePeriodicExpression | this_ThresholdExpression_1= ruleThresholdExpression | this_EventTrigger_2= ruleEventTrigger | this_Updated_3= ruleUpdated | this_ReadyToRead_4= ruleReadyToRead | (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' ) ) )
            // InternalMCL.g:709:2: (this_PeriodicExpression_0= rulePeriodicExpression | this_ThresholdExpression_1= ruleThresholdExpression | this_EventTrigger_2= ruleEventTrigger | this_Updated_3= ruleUpdated | this_ReadyToRead_4= ruleReadyToRead | (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' ) )
            {
            // InternalMCL.g:709:2: (this_PeriodicExpression_0= rulePeriodicExpression | this_ThresholdExpression_1= ruleThresholdExpression | this_EventTrigger_2= ruleEventTrigger | this_Updated_3= ruleUpdated | this_ReadyToRead_4= ruleReadyToRead | (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' ) )
            int alt11=6;
            alt11 = dfa11.predict(input);
            switch (alt11) {
                case 1 :
                    // InternalMCL.g:710:3: this_PeriodicExpression_0= rulePeriodicExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTriggeringConditionAccess().getPeriodicExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_PeriodicExpression_0=rulePeriodicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_PeriodicExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:719:3: this_ThresholdExpression_1= ruleThresholdExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTriggeringConditionAccess().getThresholdExpressionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ThresholdExpression_1=ruleThresholdExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ThresholdExpression_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalMCL.g:728:3: this_EventTrigger_2= ruleEventTrigger
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTriggeringConditionAccess().getEventTriggerParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_EventTrigger_2=ruleEventTrigger();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_EventTrigger_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalMCL.g:737:3: this_Updated_3= ruleUpdated
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTriggeringConditionAccess().getUpdatedParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Updated_3=ruleUpdated();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Updated_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalMCL.g:746:3: this_ReadyToRead_4= ruleReadyToRead
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getTriggeringConditionAccess().getReadyToReadParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_ReadyToRead_4=ruleReadyToRead();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_ReadyToRead_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalMCL.g:755:3: (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' )
                    {
                    // InternalMCL.g:755:3: (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' )
                    // InternalMCL.g:756:4: otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')'
                    {
                    otherlv_5=(Token)match(input,14,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getTriggeringConditionAccess().getLeftParenthesisKeyword_5_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getTriggeringConditionAccess().getDisjointParserRuleCall_5_1());
                      			
                    }
                    pushFollow(FOLLOW_9);
                    this_Disjoint_6=ruleDisjoint();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_Disjoint_6;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    otherlv_7=(Token)match(input,17,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getTriggeringConditionAccess().getRightParenthesisKeyword_5_2());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTriggeringCondition"


    // $ANTLR start "entryRuleEventTrigger"
    // InternalMCL.g:777:1: entryRuleEventTrigger returns [EObject current=null] : iv_ruleEventTrigger= ruleEventTrigger EOF ;
    public final EObject entryRuleEventTrigger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventTrigger = null;


        try {
            // InternalMCL.g:777:53: (iv_ruleEventTrigger= ruleEventTrigger EOF )
            // InternalMCL.g:778:2: iv_ruleEventTrigger= ruleEventTrigger EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventTriggerRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEventTrigger=ruleEventTrigger();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEventTrigger; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventTrigger"


    // $ANTLR start "ruleEventTrigger"
    // InternalMCL.g:784:1: ruleEventTrigger returns [EObject current=null] : (otherlv_0= 'event' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'occurs' ) ;
    public final EObject ruleEventTrigger() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_port_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:790:2: ( (otherlv_0= 'event' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'occurs' ) )
            // InternalMCL.g:791:2: (otherlv_0= 'event' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'occurs' )
            {
            // InternalMCL.g:791:2: (otherlv_0= 'event' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'occurs' )
            // InternalMCL.g:792:3: otherlv_0= 'event' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'occurs'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getEventTriggerAccess().getEventKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,25,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getEventTriggerAccess().getOnKeyword_1());
              		
            }
            // InternalMCL.g:800:3: ( (lv_port_2_0= rulePortRef ) )
            // InternalMCL.g:801:4: (lv_port_2_0= rulePortRef )
            {
            // InternalMCL.g:801:4: (lv_port_2_0= rulePortRef )
            // InternalMCL.g:802:5: lv_port_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getEventTriggerAccess().getPortPortRefParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_21);
            lv_port_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getEventTriggerRule());
              					}
              					set(
              						current,
              						"port",
              						lv_port_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getEventTriggerAccess().getOccursKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventTrigger"


    // $ANTLR start "entryRulePeriodicExpression"
    // InternalMCL.g:827:1: entryRulePeriodicExpression returns [EObject current=null] : iv_rulePeriodicExpression= rulePeriodicExpression EOF ;
    public final EObject entryRulePeriodicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePeriodicExpression = null;


        try {
            // InternalMCL.g:827:59: (iv_rulePeriodicExpression= rulePeriodicExpression EOF )
            // InternalMCL.g:828:2: iv_rulePeriodicExpression= rulePeriodicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPeriodicExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePeriodicExpression=rulePeriodicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePeriodicExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePeriodicExpression"


    // $ANTLR start "rulePeriodicExpression"
    // InternalMCL.g:834:1: rulePeriodicExpression returns [EObject current=null] : (otherlv_0= 'every' ( (lv_value_1_0= RULE_INT ) ) ( ( ruleQualifiedName ) ) ) ;
    public final EObject rulePeriodicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:840:2: ( (otherlv_0= 'every' ( (lv_value_1_0= RULE_INT ) ) ( ( ruleQualifiedName ) ) ) )
            // InternalMCL.g:841:2: (otherlv_0= 'every' ( (lv_value_1_0= RULE_INT ) ) ( ( ruleQualifiedName ) ) )
            {
            // InternalMCL.g:841:2: (otherlv_0= 'every' ( (lv_value_1_0= RULE_INT ) ) ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:842:3: otherlv_0= 'every' ( (lv_value_1_0= RULE_INT ) ) ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getPeriodicExpressionAccess().getEveryKeyword_0());
              		
            }
            // InternalMCL.g:846:3: ( (lv_value_1_0= RULE_INT ) )
            // InternalMCL.g:847:4: (lv_value_1_0= RULE_INT )
            {
            // InternalMCL.g:847:4: (lv_value_1_0= RULE_INT )
            // InternalMCL.g:848:5: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getPeriodicExpressionAccess().getValueINTTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getPeriodicExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.INT");
              				
            }

            }


            }

            // InternalMCL.g:864:3: ( ( ruleQualifiedName ) )
            // InternalMCL.g:865:4: ( ruleQualifiedName )
            {
            // InternalMCL.g:865:4: ( ruleQualifiedName )
            // InternalMCL.g:866:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getPeriodicExpressionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getPeriodicExpressionAccess().getTemporalReferenceModelTemporalReferenceCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePeriodicExpression"


    // $ANTLR start "entryRuleVariable"
    // InternalMCL.g:884:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalMCL.g:884:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalMCL.g:885:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalMCL.g:891:1: ruleVariable returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_DOUBLE ) ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:897:2: ( ( () ( (lv_value_1_0= RULE_DOUBLE ) ) ) )
            // InternalMCL.g:898:2: ( () ( (lv_value_1_0= RULE_DOUBLE ) ) )
            {
            // InternalMCL.g:898:2: ( () ( (lv_value_1_0= RULE_DOUBLE ) ) )
            // InternalMCL.g:899:3: () ( (lv_value_1_0= RULE_DOUBLE ) )
            {
            // InternalMCL.g:899:3: ()
            // InternalMCL.g:900:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getVariableAccess().getVariableAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:906:3: ( (lv_value_1_0= RULE_DOUBLE ) )
            // InternalMCL.g:907:4: (lv_value_1_0= RULE_DOUBLE )
            {
            // InternalMCL.g:907:4: (lv_value_1_0= RULE_DOUBLE )
            // InternalMCL.g:908:5: lv_value_1_0= RULE_DOUBLE
            {
            lv_value_1_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getVariableAccess().getValueDOUBLETerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVariableRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.DOUBLE");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleUpdated"
    // InternalMCL.g:928:1: entryRuleUpdated returns [EObject current=null] : iv_ruleUpdated= ruleUpdated EOF ;
    public final EObject entryRuleUpdated() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUpdated = null;


        try {
            // InternalMCL.g:928:48: (iv_ruleUpdated= ruleUpdated EOF )
            // InternalMCL.g:929:2: iv_ruleUpdated= ruleUpdated EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUpdatedRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUpdated=ruleUpdated();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUpdated; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUpdated"


    // $ANTLR start "ruleUpdated"
    // InternalMCL.g:935:1: ruleUpdated returns [EObject current=null] : (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'has' otherlv_4= 'been' otherlv_5= 'Updated' ) ;
    public final EObject ruleUpdated() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_port_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:941:2: ( (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'has' otherlv_4= 'been' otherlv_5= 'Updated' ) )
            // InternalMCL.g:942:2: (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'has' otherlv_4= 'been' otherlv_5= 'Updated' )
            {
            // InternalMCL.g:942:2: (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'has' otherlv_4= 'been' otherlv_5= 'Updated' )
            // InternalMCL.g:943:3: otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'has' otherlv_4= 'been' otherlv_5= 'Updated'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUpdatedAccess().getValueKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,25,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getUpdatedAccess().getOnKeyword_1());
              		
            }
            // InternalMCL.g:951:3: ( (lv_port_2_0= rulePortRef ) )
            // InternalMCL.g:952:4: (lv_port_2_0= rulePortRef )
            {
            // InternalMCL.g:952:4: (lv_port_2_0= rulePortRef )
            // InternalMCL.g:953:5: lv_port_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUpdatedAccess().getPortPortRefParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_23);
            lv_port_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUpdatedRule());
              					}
              					set(
              						current,
              						"port",
              						lv_port_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,29,FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getUpdatedAccess().getHasKeyword_3());
              		
            }
            otherlv_4=(Token)match(input,30,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getUpdatedAccess().getBeenKeyword_4());
              		
            }
            otherlv_5=(Token)match(input,31,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getUpdatedAccess().getUpdatedKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUpdated"


    // $ANTLR start "entryRuleReadyToRead"
    // InternalMCL.g:986:1: entryRuleReadyToRead returns [EObject current=null] : iv_ruleReadyToRead= ruleReadyToRead EOF ;
    public final EObject entryRuleReadyToRead() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReadyToRead = null;


        try {
            // InternalMCL.g:986:52: (iv_ruleReadyToRead= ruleReadyToRead EOF )
            // InternalMCL.g:987:2: iv_ruleReadyToRead= ruleReadyToRead EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReadyToReadRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReadyToRead=ruleReadyToRead();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReadyToRead; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReadyToRead"


    // $ANTLR start "ruleReadyToRead"
    // InternalMCL.g:993:1: ruleReadyToRead returns [EObject current=null] : (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'is' otherlv_4= 'ReadyToRead' ) ;
    public final EObject ruleReadyToRead() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_port_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:999:2: ( (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'is' otherlv_4= 'ReadyToRead' ) )
            // InternalMCL.g:1000:2: (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'is' otherlv_4= 'ReadyToRead' )
            {
            // InternalMCL.g:1000:2: (otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'is' otherlv_4= 'ReadyToRead' )
            // InternalMCL.g:1001:3: otherlv_0= 'value' otherlv_1= 'on' ( (lv_port_2_0= rulePortRef ) ) otherlv_3= 'is' otherlv_4= 'ReadyToRead'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getReadyToReadAccess().getValueKeyword_0());
              		
            }
            otherlv_1=(Token)match(input,25,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getReadyToReadAccess().getOnKeyword_1());
              		
            }
            // InternalMCL.g:1009:3: ( (lv_port_2_0= rulePortRef ) )
            // InternalMCL.g:1010:4: (lv_port_2_0= rulePortRef )
            {
            // InternalMCL.g:1010:4: (lv_port_2_0= rulePortRef )
            // InternalMCL.g:1011:5: lv_port_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getReadyToReadAccess().getPortPortRefParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_26);
            lv_port_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getReadyToReadRule());
              					}
              					set(
              						current,
              						"port",
              						lv_port_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,32,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getReadyToReadAccess().getIsKeyword_3());
              		
            }
            otherlv_4=(Token)match(input,33,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getReadyToReadAccess().getReadyToReadKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReadyToRead"


    // $ANTLR start "entryRuleZeroCrossingDetection"
    // InternalMCL.g:1040:1: entryRuleZeroCrossingDetection returns [EObject current=null] : iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF ;
    public final EObject entryRuleZeroCrossingDetection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleZeroCrossingDetection = null;


        try {
            // InternalMCL.g:1040:62: (iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF )
            // InternalMCL.g:1041:2: iv_ruleZeroCrossingDetection= ruleZeroCrossingDetection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getZeroCrossingDetectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleZeroCrossingDetection=ruleZeroCrossingDetection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleZeroCrossingDetection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleZeroCrossingDetection"


    // $ANTLR start "ruleZeroCrossingDetection"
    // InternalMCL.g:1047:1: ruleZeroCrossingDetection returns [EObject current=null] : ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'ZeroCrossingDetection' ) ;
    public final EObject ruleZeroCrossingDetection() throws RecognitionException {
        EObject current = null;

        Token lv_state_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalMCL.g:1053:2: ( ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'ZeroCrossingDetection' ) )
            // InternalMCL.g:1054:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'ZeroCrossingDetection' )
            {
            // InternalMCL.g:1054:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'ZeroCrossingDetection' )
            // InternalMCL.g:1055:3: () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'ZeroCrossingDetection'
            {
            // InternalMCL.g:1055:3: ()
            // InternalMCL.g:1056:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1062:3: ( (lv_state_1_0= 'state' ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==34) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalMCL.g:1063:4: (lv_state_1_0= 'state' )
                    {
                    // InternalMCL.g:1063:4: (lv_state_1_0= 'state' )
                    // InternalMCL.g:1064:5: lv_state_1_0= 'state'
                    {
                    lv_state_1_0=(Token)match(input,34,FOLLOW_28); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getZeroCrossingDetectionAccess().getStateStateKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getZeroCrossingDetectionRule());
                      					}
                      					setWithLastConsumed(current, "state", true, "state");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getZeroCrossingDetectionAccess().getZeroCrossingDetectionKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleZeroCrossingDetection"


    // $ANTLR start "entryRuleExtrapolation"
    // InternalMCL.g:1084:1: entryRuleExtrapolation returns [EObject current=null] : iv_ruleExtrapolation= ruleExtrapolation EOF ;
    public final EObject entryRuleExtrapolation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtrapolation = null;


        try {
            // InternalMCL.g:1084:54: (iv_ruleExtrapolation= ruleExtrapolation EOF )
            // InternalMCL.g:1085:2: iv_ruleExtrapolation= ruleExtrapolation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExtrapolationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExtrapolation=ruleExtrapolation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExtrapolation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtrapolation"


    // $ANTLR start "ruleExtrapolation"
    // InternalMCL.g:1091:1: ruleExtrapolation returns [EObject current=null] : ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Extrapolation' ) ;
    public final EObject ruleExtrapolation() throws RecognitionException {
        EObject current = null;

        Token lv_state_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalMCL.g:1097:2: ( ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Extrapolation' ) )
            // InternalMCL.g:1098:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Extrapolation' )
            {
            // InternalMCL.g:1098:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Extrapolation' )
            // InternalMCL.g:1099:3: () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Extrapolation'
            {
            // InternalMCL.g:1099:3: ()
            // InternalMCL.g:1100:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getExtrapolationAccess().getExtrapolationAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1106:3: ( (lv_state_1_0= 'state' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==34) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalMCL.g:1107:4: (lv_state_1_0= 'state' )
                    {
                    // InternalMCL.g:1107:4: (lv_state_1_0= 'state' )
                    // InternalMCL.g:1108:5: lv_state_1_0= 'state'
                    {
                    lv_state_1_0=(Token)match(input,34,FOLLOW_29); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getExtrapolationAccess().getStateStateKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getExtrapolationRule());
                      					}
                      					setWithLastConsumed(current, "state", true, "state");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,36,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getExtrapolationAccess().getExtrapolationKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtrapolation"


    // $ANTLR start "entryRuleInterpolation"
    // InternalMCL.g:1128:1: entryRuleInterpolation returns [EObject current=null] : iv_ruleInterpolation= ruleInterpolation EOF ;
    public final EObject entryRuleInterpolation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterpolation = null;


        try {
            // InternalMCL.g:1128:54: (iv_ruleInterpolation= ruleInterpolation EOF )
            // InternalMCL.g:1129:2: iv_ruleInterpolation= ruleInterpolation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterpolationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInterpolation=ruleInterpolation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterpolation; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterpolation"


    // $ANTLR start "ruleInterpolation"
    // InternalMCL.g:1135:1: ruleInterpolation returns [EObject current=null] : ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Interpolation' ) ;
    public final EObject ruleInterpolation() throws RecognitionException {
        EObject current = null;

        Token lv_state_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalMCL.g:1141:2: ( ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Interpolation' ) )
            // InternalMCL.g:1142:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Interpolation' )
            {
            // InternalMCL.g:1142:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Interpolation' )
            // InternalMCL.g:1143:3: () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'Interpolation'
            {
            // InternalMCL.g:1143:3: ()
            // InternalMCL.g:1144:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getInterpolationAccess().getInterpolationAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1150:3: ( (lv_state_1_0= 'state' ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==34) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalMCL.g:1151:4: (lv_state_1_0= 'state' )
                    {
                    // InternalMCL.g:1151:4: (lv_state_1_0= 'state' )
                    // InternalMCL.g:1152:5: lv_state_1_0= 'state'
                    {
                    lv_state_1_0=(Token)match(input,34,FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getInterpolationAccess().getStateStateKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getInterpolationRule());
                      					}
                      					setWithLastConsumed(current, "state", true, "state");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,37,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInterpolationAccess().getInterpolationKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterpolation"


    // $ANTLR start "entryRuleDiscontinuityLocator"
    // InternalMCL.g:1172:1: entryRuleDiscontinuityLocator returns [EObject current=null] : iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF ;
    public final EObject entryRuleDiscontinuityLocator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDiscontinuityLocator = null;


        try {
            // InternalMCL.g:1172:61: (iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF )
            // InternalMCL.g:1173:2: iv_ruleDiscontinuityLocator= ruleDiscontinuityLocator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDiscontinuityLocatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDiscontinuityLocator=ruleDiscontinuityLocator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDiscontinuityLocator; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDiscontinuityLocator"


    // $ANTLR start "ruleDiscontinuityLocator"
    // InternalMCL.g:1179:1: ruleDiscontinuityLocator returns [EObject current=null] : ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'DiscontinuityLocator' ) ;
    public final EObject ruleDiscontinuityLocator() throws RecognitionException {
        EObject current = null;

        Token lv_state_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalMCL.g:1185:2: ( ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'DiscontinuityLocator' ) )
            // InternalMCL.g:1186:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'DiscontinuityLocator' )
            {
            // InternalMCL.g:1186:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'DiscontinuityLocator' )
            // InternalMCL.g:1187:3: () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'DiscontinuityLocator'
            {
            // InternalMCL.g:1187:3: ()
            // InternalMCL.g:1188:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1194:3: ( (lv_state_1_0= 'state' ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==34) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalMCL.g:1195:4: (lv_state_1_0= 'state' )
                    {
                    // InternalMCL.g:1195:4: (lv_state_1_0= 'state' )
                    // InternalMCL.g:1196:5: lv_state_1_0= 'state'
                    {
                    lv_state_1_0=(Token)match(input,34,FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getDiscontinuityLocatorAccess().getStateStateKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getDiscontinuityLocatorRule());
                      					}
                      					setWithLastConsumed(current, "state", true, "state");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,38,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getDiscontinuityLocatorAccess().getDiscontinuityLocatorKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDiscontinuityLocator"


    // $ANTLR start "entryRuleHistoryProvider"
    // InternalMCL.g:1216:1: entryRuleHistoryProvider returns [EObject current=null] : iv_ruleHistoryProvider= ruleHistoryProvider EOF ;
    public final EObject entryRuleHistoryProvider() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHistoryProvider = null;


        try {
            // InternalMCL.g:1216:56: (iv_ruleHistoryProvider= ruleHistoryProvider EOF )
            // InternalMCL.g:1217:2: iv_ruleHistoryProvider= ruleHistoryProvider EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getHistoryProviderRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleHistoryProvider=ruleHistoryProvider();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleHistoryProvider; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHistoryProvider"


    // $ANTLR start "ruleHistoryProvider"
    // InternalMCL.g:1223:1: ruleHistoryProvider returns [EObject current=null] : ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'HistoryProvider' ) ;
    public final EObject ruleHistoryProvider() throws RecognitionException {
        EObject current = null;

        Token lv_state_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalMCL.g:1229:2: ( ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'HistoryProvider' ) )
            // InternalMCL.g:1230:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'HistoryProvider' )
            {
            // InternalMCL.g:1230:2: ( () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'HistoryProvider' )
            // InternalMCL.g:1231:3: () ( (lv_state_1_0= 'state' ) )? otherlv_2= 'HistoryProvider'
            {
            // InternalMCL.g:1231:3: ()
            // InternalMCL.g:1232:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getHistoryProviderAccess().getHistoryProviderAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1238:3: ( (lv_state_1_0= 'state' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==34) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalMCL.g:1239:4: (lv_state_1_0= 'state' )
                    {
                    // InternalMCL.g:1239:4: (lv_state_1_0= 'state' )
                    // InternalMCL.g:1240:5: lv_state_1_0= 'state'
                    {
                    lv_state_1_0=(Token)match(input,34,FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getHistoryProviderAccess().getStateStateKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getHistoryProviderRule());
                      					}
                      					setWithLastConsumed(current, "state", true, "state");
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,39,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getHistoryProviderAccess().getHistoryProviderKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHistoryProvider"


    // $ANTLR start "entryRuleGreater"
    // InternalMCL.g:1260:1: entryRuleGreater returns [EObject current=null] : iv_ruleGreater= ruleGreater EOF ;
    public final EObject entryRuleGreater() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGreater = null;


        try {
            // InternalMCL.g:1260:48: (iv_ruleGreater= ruleGreater EOF )
            // InternalMCL.g:1261:2: iv_ruleGreater= ruleGreater EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGreaterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGreater=ruleGreater();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGreater; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGreater"


    // $ANTLR start "ruleGreater"
    // InternalMCL.g:1267:1: ruleGreater returns [EObject current=null] : ( () otherlv_1= '>' ) ;
    public final EObject ruleGreater() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMCL.g:1273:2: ( ( () otherlv_1= '>' ) )
            // InternalMCL.g:1274:2: ( () otherlv_1= '>' )
            {
            // InternalMCL.g:1274:2: ( () otherlv_1= '>' )
            // InternalMCL.g:1275:3: () otherlv_1= '>'
            {
            // InternalMCL.g:1275:3: ()
            // InternalMCL.g:1276:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGreaterAccess().getGreaterAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,40,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getGreaterAccess().getGreaterThanSignKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGreater"


    // $ANTLR start "entryRuleLesser"
    // InternalMCL.g:1290:1: entryRuleLesser returns [EObject current=null] : iv_ruleLesser= ruleLesser EOF ;
    public final EObject entryRuleLesser() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLesser = null;


        try {
            // InternalMCL.g:1290:47: (iv_ruleLesser= ruleLesser EOF )
            // InternalMCL.g:1291:2: iv_ruleLesser= ruleLesser EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLesserRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLesser=ruleLesser();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLesser; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLesser"


    // $ANTLR start "ruleLesser"
    // InternalMCL.g:1297:1: ruleLesser returns [EObject current=null] : ( () otherlv_1= '<' ) ;
    public final EObject ruleLesser() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMCL.g:1303:2: ( ( () otherlv_1= '<' ) )
            // InternalMCL.g:1304:2: ( () otherlv_1= '<' )
            {
            // InternalMCL.g:1304:2: ( () otherlv_1= '<' )
            // InternalMCL.g:1305:3: () otherlv_1= '<'
            {
            // InternalMCL.g:1305:3: ()
            // InternalMCL.g:1306:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getLesserAccess().getLesserAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,41,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getLesserAccess().getLessThanSignKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLesser"


    // $ANTLR start "entryRuleAssignmentExpression"
    // InternalMCL.g:1320:1: entryRuleAssignmentExpression returns [EObject current=null] : iv_ruleAssignmentExpression= ruleAssignmentExpression EOF ;
    public final EObject entryRuleAssignmentExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentExpression = null;


        try {
            // InternalMCL.g:1320:61: (iv_ruleAssignmentExpression= ruleAssignmentExpression EOF )
            // InternalMCL.g:1321:2: iv_ruleAssignmentExpression= ruleAssignmentExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentExpression=ruleAssignmentExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentExpression"


    // $ANTLR start "ruleAssignmentExpression"
    // InternalMCL.g:1327:1: ruleAssignmentExpression returns [EObject current=null] : this_Assignment_0= ruleAssignment ;
    public final EObject ruleAssignmentExpression() throws RecognitionException {
        EObject current = null;

        EObject this_Assignment_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1333:2: (this_Assignment_0= ruleAssignment )
            // InternalMCL.g:1334:2: this_Assignment_0= ruleAssignment
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getAssignmentExpressionAccess().getAssignmentParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_Assignment_0=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_Assignment_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentExpression"


    // $ANTLR start "entryRuleOtherExpression"
    // InternalMCL.g:1345:1: entryRuleOtherExpression returns [EObject current=null] : iv_ruleOtherExpression= ruleOtherExpression EOF ;
    public final EObject entryRuleOtherExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOtherExpression = null;


        try {
            // InternalMCL.g:1345:56: (iv_ruleOtherExpression= ruleOtherExpression EOF )
            // InternalMCL.g:1346:2: iv_ruleOtherExpression= ruleOtherExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOtherExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOtherExpression=ruleOtherExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOtherExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOtherExpression"


    // $ANTLR start "ruleOtherExpression"
    // InternalMCL.g:1352:1: ruleOtherExpression returns [EObject current=null] : (this_PortRef_0= rulePortRef | this_UnaryMinus_1= ruleUnaryMinus | this_Not_2= ruleNot | this_BinaryExpression_3= ruleBinaryExpression ) ;
    public final EObject ruleOtherExpression() throws RecognitionException {
        EObject current = null;

        EObject this_PortRef_0 = null;

        EObject this_UnaryMinus_1 = null;

        EObject this_Not_2 = null;

        EObject this_BinaryExpression_3 = null;



        	enterRule();

        try {
            // InternalMCL.g:1358:2: ( (this_PortRef_0= rulePortRef | this_UnaryMinus_1= ruleUnaryMinus | this_Not_2= ruleNot | this_BinaryExpression_3= ruleBinaryExpression ) )
            // InternalMCL.g:1359:2: (this_PortRef_0= rulePortRef | this_UnaryMinus_1= ruleUnaryMinus | this_Not_2= ruleNot | this_BinaryExpression_3= ruleBinaryExpression )
            {
            // InternalMCL.g:1359:2: (this_PortRef_0= rulePortRef | this_UnaryMinus_1= ruleUnaryMinus | this_Not_2= ruleNot | this_BinaryExpression_3= ruleBinaryExpression )
            int alt17=4;
            alt17 = dfa17.predict(input);
            switch (alt17) {
                case 1 :
                    // InternalMCL.g:1360:3: this_PortRef_0= rulePortRef
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getOtherExpressionAccess().getPortRefParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_PortRef_0=rulePortRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_PortRef_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:1369:3: this_UnaryMinus_1= ruleUnaryMinus
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getOtherExpressionAccess().getUnaryMinusParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_UnaryMinus_1=ruleUnaryMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_UnaryMinus_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalMCL.g:1378:3: this_Not_2= ruleNot
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getOtherExpressionAccess().getNotParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Not_2=ruleNot();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Not_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalMCL.g:1387:3: this_BinaryExpression_3= ruleBinaryExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getOtherExpressionAccess().getBinaryExpressionParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BinaryExpression_3=ruleBinaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BinaryExpression_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOtherExpression"


    // $ANTLR start "entryRuleBinaryExpression"
    // InternalMCL.g:1399:1: entryRuleBinaryExpression returns [EObject current=null] : iv_ruleBinaryExpression= ruleBinaryExpression EOF ;
    public final EObject entryRuleBinaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryExpression = null;


        try {
            // InternalMCL.g:1399:57: (iv_ruleBinaryExpression= ruleBinaryExpression EOF )
            // InternalMCL.g:1400:2: iv_ruleBinaryExpression= ruleBinaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryExpression=ruleBinaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryExpression"


    // $ANTLR start "ruleBinaryExpression"
    // InternalMCL.g:1406:1: ruleBinaryExpression returns [EObject current=null] : (this_BinaryMinus_0= ruleBinaryMinus | this_BinaryPlus_1= ruleBinaryPlus ) ;
    public final EObject ruleBinaryExpression() throws RecognitionException {
        EObject current = null;

        EObject this_BinaryMinus_0 = null;

        EObject this_BinaryPlus_1 = null;



        	enterRule();

        try {
            // InternalMCL.g:1412:2: ( (this_BinaryMinus_0= ruleBinaryMinus | this_BinaryPlus_1= ruleBinaryPlus ) )
            // InternalMCL.g:1413:2: (this_BinaryMinus_0= ruleBinaryMinus | this_BinaryPlus_1= ruleBinaryPlus )
            {
            // InternalMCL.g:1413:2: (this_BinaryMinus_0= ruleBinaryMinus | this_BinaryPlus_1= ruleBinaryPlus )
            int alt18=2;
            alt18 = dfa18.predict(input);
            switch (alt18) {
                case 1 :
                    // InternalMCL.g:1414:3: this_BinaryMinus_0= ruleBinaryMinus
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBinaryExpressionAccess().getBinaryMinusParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BinaryMinus_0=ruleBinaryMinus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BinaryMinus_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:1423:3: this_BinaryPlus_1= ruleBinaryPlus
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getBinaryExpressionAccess().getBinaryPlusParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_BinaryPlus_1=ruleBinaryPlus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_BinaryPlus_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryExpression"


    // $ANTLR start "entryRuleUnaryMinus"
    // InternalMCL.g:1435:1: entryRuleUnaryMinus returns [EObject current=null] : iv_ruleUnaryMinus= ruleUnaryMinus EOF ;
    public final EObject entryRuleUnaryMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryMinus = null;


        try {
            // InternalMCL.g:1435:51: (iv_ruleUnaryMinus= ruleUnaryMinus EOF )
            // InternalMCL.g:1436:2: iv_ruleUnaryMinus= ruleUnaryMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryMinusRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnaryMinus=ruleUnaryMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryMinus; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryMinus"


    // $ANTLR start "ruleUnaryMinus"
    // InternalMCL.g:1442:1: ruleUnaryMinus returns [EObject current=null] : (otherlv_0= '-' ( (lv_operand_1_0= rulePortRef ) ) ) ;
    public final EObject ruleUnaryMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_operand_1_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1448:2: ( (otherlv_0= '-' ( (lv_operand_1_0= rulePortRef ) ) ) )
            // InternalMCL.g:1449:2: (otherlv_0= '-' ( (lv_operand_1_0= rulePortRef ) ) )
            {
            // InternalMCL.g:1449:2: (otherlv_0= '-' ( (lv_operand_1_0= rulePortRef ) ) )
            // InternalMCL.g:1450:3: otherlv_0= '-' ( (lv_operand_1_0= rulePortRef ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getUnaryMinusAccess().getHyphenMinusKeyword_0());
              		
            }
            // InternalMCL.g:1454:3: ( (lv_operand_1_0= rulePortRef ) )
            // InternalMCL.g:1455:4: (lv_operand_1_0= rulePortRef )
            {
            // InternalMCL.g:1455:4: (lv_operand_1_0= rulePortRef )
            // InternalMCL.g:1456:5: lv_operand_1_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getUnaryMinusAccess().getOperandPortRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_operand_1_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getUnaryMinusRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_1_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryMinus"


    // $ANTLR start "entryRuleNot"
    // InternalMCL.g:1477:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // InternalMCL.g:1477:44: (iv_ruleNot= ruleNot EOF )
            // InternalMCL.g:1478:2: iv_ruleNot= ruleNot EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNot=ruleNot();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNot; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // InternalMCL.g:1484:1: ruleNot returns [EObject current=null] : ( (otherlv_0= '!' | otherlv_1= 'not' ) ( (lv_operand_2_0= rulePortRef ) ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_operand_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1490:2: ( ( (otherlv_0= '!' | otherlv_1= 'not' ) ( (lv_operand_2_0= rulePortRef ) ) ) )
            // InternalMCL.g:1491:2: ( (otherlv_0= '!' | otherlv_1= 'not' ) ( (lv_operand_2_0= rulePortRef ) ) )
            {
            // InternalMCL.g:1491:2: ( (otherlv_0= '!' | otherlv_1= 'not' ) ( (lv_operand_2_0= rulePortRef ) ) )
            // InternalMCL.g:1492:3: (otherlv_0= '!' | otherlv_1= 'not' ) ( (lv_operand_2_0= rulePortRef ) )
            {
            // InternalMCL.g:1492:3: (otherlv_0= '!' | otherlv_1= 'not' )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==43) ) {
                alt19=1;
            }
            else if ( (LA19_0==44) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalMCL.g:1493:4: otherlv_0= '!'
                    {
                    otherlv_0=(Token)match(input,43,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getNotAccess().getExclamationMarkKeyword_0_0());
                      			
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:1498:4: otherlv_1= 'not'
                    {
                    otherlv_1=(Token)match(input,44,FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getNotAccess().getNotKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            // InternalMCL.g:1503:3: ( (lv_operand_2_0= rulePortRef ) )
            // InternalMCL.g:1504:4: (lv_operand_2_0= rulePortRef )
            {
            // InternalMCL.g:1504:4: (lv_operand_2_0= rulePortRef )
            // InternalMCL.g:1505:5: lv_operand_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getNotAccess().getOperandPortRefParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_operand_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getNotRule());
              					}
              					set(
              						current,
              						"operand",
              						lv_operand_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleBinaryMinus"
    // InternalMCL.g:1526:1: entryRuleBinaryMinus returns [EObject current=null] : iv_ruleBinaryMinus= ruleBinaryMinus EOF ;
    public final EObject entryRuleBinaryMinus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryMinus = null;


        try {
            // InternalMCL.g:1526:52: (iv_ruleBinaryMinus= ruleBinaryMinus EOF )
            // InternalMCL.g:1527:2: iv_ruleBinaryMinus= ruleBinaryMinus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryMinusRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryMinus=ruleBinaryMinus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryMinus; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryMinus"


    // $ANTLR start "ruleBinaryMinus"
    // InternalMCL.g:1533:1: ruleBinaryMinus returns [EObject current=null] : ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '-' ( (lv_rightOperand_2_0= rulePortRef ) ) ) ;
    public final EObject ruleBinaryMinus() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_leftOperand_0_0 = null;

        EObject lv_rightOperand_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1539:2: ( ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '-' ( (lv_rightOperand_2_0= rulePortRef ) ) ) )
            // InternalMCL.g:1540:2: ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '-' ( (lv_rightOperand_2_0= rulePortRef ) ) )
            {
            // InternalMCL.g:1540:2: ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '-' ( (lv_rightOperand_2_0= rulePortRef ) ) )
            // InternalMCL.g:1541:3: ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '-' ( (lv_rightOperand_2_0= rulePortRef ) )
            {
            // InternalMCL.g:1541:3: ( (lv_leftOperand_0_0= rulePortRef ) )
            // InternalMCL.g:1542:4: (lv_leftOperand_0_0= rulePortRef )
            {
            // InternalMCL.g:1542:4: (lv_leftOperand_0_0= rulePortRef )
            // InternalMCL.g:1543:5: lv_leftOperand_0_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBinaryMinusAccess().getLeftOperandPortRefParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_33);
            lv_leftOperand_0_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBinaryMinusRule());
              					}
              					set(
              						current,
              						"leftOperand",
              						lv_leftOperand_0_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,42,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBinaryMinusAccess().getHyphenMinusKeyword_1());
              		
            }
            // InternalMCL.g:1564:3: ( (lv_rightOperand_2_0= rulePortRef ) )
            // InternalMCL.g:1565:4: (lv_rightOperand_2_0= rulePortRef )
            {
            // InternalMCL.g:1565:4: (lv_rightOperand_2_0= rulePortRef )
            // InternalMCL.g:1566:5: lv_rightOperand_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBinaryMinusAccess().getRightOperandPortRefParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_rightOperand_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBinaryMinusRule());
              					}
              					set(
              						current,
              						"rightOperand",
              						lv_rightOperand_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryMinus"


    // $ANTLR start "entryRuleBinaryPlus"
    // InternalMCL.g:1587:1: entryRuleBinaryPlus returns [EObject current=null] : iv_ruleBinaryPlus= ruleBinaryPlus EOF ;
    public final EObject entryRuleBinaryPlus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryPlus = null;


        try {
            // InternalMCL.g:1587:51: (iv_ruleBinaryPlus= ruleBinaryPlus EOF )
            // InternalMCL.g:1588:2: iv_ruleBinaryPlus= ruleBinaryPlus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryPlusRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBinaryPlus=ruleBinaryPlus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryPlus; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryPlus"


    // $ANTLR start "ruleBinaryPlus"
    // InternalMCL.g:1594:1: ruleBinaryPlus returns [EObject current=null] : ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '+' ( (lv_rightOperand_2_0= rulePortRef ) ) ) ;
    public final EObject ruleBinaryPlus() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_leftOperand_0_0 = null;

        EObject lv_rightOperand_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1600:2: ( ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '+' ( (lv_rightOperand_2_0= rulePortRef ) ) ) )
            // InternalMCL.g:1601:2: ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '+' ( (lv_rightOperand_2_0= rulePortRef ) ) )
            {
            // InternalMCL.g:1601:2: ( ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '+' ( (lv_rightOperand_2_0= rulePortRef ) ) )
            // InternalMCL.g:1602:3: ( (lv_leftOperand_0_0= rulePortRef ) ) otherlv_1= '+' ( (lv_rightOperand_2_0= rulePortRef ) )
            {
            // InternalMCL.g:1602:3: ( (lv_leftOperand_0_0= rulePortRef ) )
            // InternalMCL.g:1603:4: (lv_leftOperand_0_0= rulePortRef )
            {
            // InternalMCL.g:1603:4: (lv_leftOperand_0_0= rulePortRef )
            // InternalMCL.g:1604:5: lv_leftOperand_0_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBinaryPlusAccess().getLeftOperandPortRefParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_leftOperand_0_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBinaryPlusRule());
              					}
              					set(
              						current,
              						"leftOperand",
              						lv_leftOperand_0_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,45,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getBinaryPlusAccess().getPlusSignKeyword_1());
              		
            }
            // InternalMCL.g:1625:3: ( (lv_rightOperand_2_0= rulePortRef ) )
            // InternalMCL.g:1626:4: (lv_rightOperand_2_0= rulePortRef )
            {
            // InternalMCL.g:1626:4: (lv_rightOperand_2_0= rulePortRef )
            // InternalMCL.g:1627:5: lv_rightOperand_2_0= rulePortRef
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBinaryPlusAccess().getRightOperandPortRefParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_rightOperand_2_0=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBinaryPlusRule());
              					}
              					set(
              						current,
              						"rightOperand",
              						lv_rightOperand_2_0,
              						"fr.inria.glose.mcl.MCL.PortRef");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryPlus"


    // $ANTLR start "entryRuleAssignment"
    // InternalMCL.g:1648:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // InternalMCL.g:1648:51: (iv_ruleAssignment= ruleAssignment EOF )
            // InternalMCL.g:1649:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalMCL.g:1655:1: ruleAssignment returns [EObject current=null] : ( () ( (lv_leftOperand_1_0= ruleOtherExpression ) ) otherlv_2= '->' ( (lv_rightOperand_3_0= ruleOtherExpression ) ) ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_leftOperand_1_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1661:2: ( ( () ( (lv_leftOperand_1_0= ruleOtherExpression ) ) otherlv_2= '->' ( (lv_rightOperand_3_0= ruleOtherExpression ) ) ) )
            // InternalMCL.g:1662:2: ( () ( (lv_leftOperand_1_0= ruleOtherExpression ) ) otherlv_2= '->' ( (lv_rightOperand_3_0= ruleOtherExpression ) ) )
            {
            // InternalMCL.g:1662:2: ( () ( (lv_leftOperand_1_0= ruleOtherExpression ) ) otherlv_2= '->' ( (lv_rightOperand_3_0= ruleOtherExpression ) ) )
            // InternalMCL.g:1663:3: () ( (lv_leftOperand_1_0= ruleOtherExpression ) ) otherlv_2= '->' ( (lv_rightOperand_3_0= ruleOtherExpression ) )
            {
            // InternalMCL.g:1663:3: ()
            // InternalMCL.g:1664:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getAssignmentAccess().getAssignmentAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:1670:3: ( (lv_leftOperand_1_0= ruleOtherExpression ) )
            // InternalMCL.g:1671:4: (lv_leftOperand_1_0= ruleOtherExpression )
            {
            // InternalMCL.g:1671:4: (lv_leftOperand_1_0= ruleOtherExpression )
            // InternalMCL.g:1672:5: lv_leftOperand_1_0= ruleOtherExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAssignmentAccess().getLeftOperandOtherExpressionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_35);
            lv_leftOperand_1_0=ruleOtherExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAssignmentRule());
              					}
              					set(
              						current,
              						"leftOperand",
              						lv_leftOperand_1_0,
              						"fr.inria.glose.mcl.MCL.OtherExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,46,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getAssignmentAccess().getHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalMCL.g:1693:3: ( (lv_rightOperand_3_0= ruleOtherExpression ) )
            // InternalMCL.g:1694:4: (lv_rightOperand_3_0= ruleOtherExpression )
            {
            // InternalMCL.g:1694:4: (lv_rightOperand_3_0= ruleOtherExpression )
            // InternalMCL.g:1695:5: lv_rightOperand_3_0= ruleOtherExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAssignmentAccess().getRightOperandOtherExpressionParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_rightOperand_3_0=ruleOtherExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAssignmentRule());
              					}
              					set(
              						current,
              						"rightOperand",
              						lv_rightOperand_3_0,
              						"fr.inria.glose.mcl.MCL.OtherExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRulePortRef"
    // InternalMCL.g:1716:1: entryRulePortRef returns [EObject current=null] : iv_rulePortRef= rulePortRef EOF ;
    public final EObject entryRulePortRef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePortRef = null;


        try {
            // InternalMCL.g:1716:48: (iv_rulePortRef= rulePortRef EOF )
            // InternalMCL.g:1717:2: iv_rulePortRef= rulePortRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPortRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePortRef=rulePortRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePortRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePortRef"


    // $ANTLR start "rulePortRef"
    // InternalMCL.g:1723:1: rulePortRef returns [EObject current=null] : ( ( ruleQualifiedName ) ) ;
    public final EObject rulePortRef() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalMCL.g:1729:2: ( ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:1730:2: ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:1730:2: ( ( ruleQualifiedName ) )
            // InternalMCL.g:1731:3: ( ruleQualifiedName )
            {
            // InternalMCL.g:1731:3: ( ruleQualifiedName )
            // InternalMCL.g:1732:4: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getPortRefRule());
              				}
              			
            }
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getPortRefAccess().getRefPortPortCrossReference_0());
              			
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePortRef"


    // $ANTLR start "entryRuleSimpleSync"
    // InternalMCL.g:1749:1: entryRuleSimpleSync returns [EObject current=null] : iv_ruleSimpleSync= ruleSimpleSync EOF ;
    public final EObject entryRuleSimpleSync() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleSync = null;


        try {
            // InternalMCL.g:1749:51: (iv_ruleSimpleSync= ruleSimpleSync EOF )
            // InternalMCL.g:1750:2: iv_ruleSimpleSync= ruleSimpleSync EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleSyncRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSimpleSync=ruleSimpleSync();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleSync; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleSync"


    // $ANTLR start "ruleSimpleSync"
    // InternalMCL.g:1756:1: ruleSimpleSync returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) otherlv_1= '=' ( ( ruleQualifiedName ) ) (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )? ) ;
    public final EObject ruleSimpleSync() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_tolerance_4_0=null;


        	enterRule();

        try {
            // InternalMCL.g:1762:2: ( ( ( ( ruleQualifiedName ) ) otherlv_1= '=' ( ( ruleQualifiedName ) ) (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )? ) )
            // InternalMCL.g:1763:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '=' ( ( ruleQualifiedName ) ) (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )? )
            {
            // InternalMCL.g:1763:2: ( ( ( ruleQualifiedName ) ) otherlv_1= '=' ( ( ruleQualifiedName ) ) (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )? )
            // InternalMCL.g:1764:3: ( ( ruleQualifiedName ) ) otherlv_1= '=' ( ( ruleQualifiedName ) ) (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )?
            {
            // InternalMCL.g:1764:3: ( ( ruleQualifiedName ) )
            // InternalMCL.g:1765:4: ( ruleQualifiedName )
            {
            // InternalMCL.g:1765:4: ( ruleQualifiedName )
            // InternalMCL.g:1766:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSimpleSyncRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSimpleSyncAccess().getRightTemporalReferenceModelTemporalReferenceCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_36);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,47,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSimpleSyncAccess().getEqualsSignKeyword_1());
              		
            }
            // InternalMCL.g:1784:3: ( ( ruleQualifiedName ) )
            // InternalMCL.g:1785:4: ( ruleQualifiedName )
            {
            // InternalMCL.g:1785:4: ( ruleQualifiedName )
            // InternalMCL.g:1786:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSimpleSyncRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSimpleSyncAccess().getLeftTemporalReferenceModelTemporalReferenceCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_37);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalMCL.g:1800:3: (otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==48) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalMCL.g:1801:4: otherlv_3= 'tolerance' ( (lv_tolerance_4_0= RULE_DOUBLE ) )
                    {
                    otherlv_3=(Token)match(input,48,FOLLOW_17); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_3, grammarAccess.getSimpleSyncAccess().getToleranceKeyword_3_0());
                      			
                    }
                    // InternalMCL.g:1805:4: ( (lv_tolerance_4_0= RULE_DOUBLE ) )
                    // InternalMCL.g:1806:5: (lv_tolerance_4_0= RULE_DOUBLE )
                    {
                    // InternalMCL.g:1806:5: (lv_tolerance_4_0= RULE_DOUBLE )
                    // InternalMCL.g:1807:6: lv_tolerance_4_0= RULE_DOUBLE
                    {
                    lv_tolerance_4_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_tolerance_4_0, grammarAccess.getSimpleSyncAccess().getToleranceDOUBLETerminalRuleCall_3_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSimpleSyncRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"tolerance",
                      							lv_tolerance_4_0,
                      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.DOUBLE");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleSync"


    // $ANTLR start "entryRuleGImportStatement"
    // InternalMCL.g:1828:1: entryRuleGImportStatement returns [EObject current=null] : iv_ruleGImportStatement= ruleGImportStatement EOF ;
    public final EObject entryRuleGImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGImportStatement = null;


        try {
            // InternalMCL.g:1828:57: (iv_ruleGImportStatement= ruleGImportStatement EOF )
            // InternalMCL.g:1829:2: iv_ruleGImportStatement= ruleGImportStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGImportStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGImportStatement=ruleGImportStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGImportStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGImportStatement"


    // $ANTLR start "ruleGImportStatement"
    // InternalMCL.g:1835:1: ruleGImportStatement returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleGImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:1841:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalMCL.g:1842:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalMCL.g:1842:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalMCL.g:1843:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,49,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getGImportStatementAccess().getImportKeyword_0());
              		
            }
            // InternalMCL.g:1847:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalMCL.g:1848:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalMCL.g:1848:4: (lv_importURI_1_0= RULE_STRING )
            // InternalMCL.g:1849:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_importURI_1_0, grammarAccess.getGImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGImportStatementRule());
              					}
              					setWithLastConsumed(
              						current,
              						"importURI",
              						lv_importURI_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGImportStatement"


    // $ANTLR start "entryRuleGExpression"
    // InternalMCL.g:1869:1: entryRuleGExpression returns [EObject current=null] : iv_ruleGExpression= ruleGExpression EOF ;
    public final EObject entryRuleGExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGExpression = null;


        try {
            // InternalMCL.g:1869:52: (iv_ruleGExpression= ruleGExpression EOF )
            // InternalMCL.g:1870:2: iv_ruleGExpression= ruleGExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGExpression=ruleGExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGExpression"


    // $ANTLR start "ruleGExpression"
    // InternalMCL.g:1876:1: ruleGExpression returns [EObject current=null] : this_GOrExpression_0= ruleGOrExpression ;
    public final EObject ruleGExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GOrExpression_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1882:2: (this_GOrExpression_0= ruleGOrExpression )
            // InternalMCL.g:1883:2: this_GOrExpression_0= ruleGOrExpression
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getGExpressionAccess().getGOrExpressionParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_GOrExpression_0=ruleGOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_GOrExpression_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGExpression"


    // $ANTLR start "entryRuleGOrExpression"
    // InternalMCL.g:1894:1: entryRuleGOrExpression returns [EObject current=null] : iv_ruleGOrExpression= ruleGOrExpression EOF ;
    public final EObject entryRuleGOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGOrExpression = null;


        try {
            // InternalMCL.g:1894:54: (iv_ruleGOrExpression= ruleGOrExpression EOF )
            // InternalMCL.g:1895:2: iv_ruleGOrExpression= ruleGOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGOrExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGOrExpression=ruleGOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGOrExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGOrExpression"


    // $ANTLR start "ruleGOrExpression"
    // InternalMCL.g:1901:1: ruleGOrExpression returns [EObject current=null] : (this_GXorExpression_0= ruleGXorExpression ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )* ) ;
    public final EObject ruleGOrExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GXorExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1907:2: ( (this_GXorExpression_0= ruleGXorExpression ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )* ) )
            // InternalMCL.g:1908:2: (this_GXorExpression_0= ruleGXorExpression ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )* )
            {
            // InternalMCL.g:1908:2: (this_GXorExpression_0= ruleGXorExpression ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )* )
            // InternalMCL.g:1909:3: this_GXorExpression_0= ruleGXorExpression ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGOrExpressionAccess().getGXorExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_18);
            this_GXorExpression_0=ruleGXorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GXorExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:1917:3: ( () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==22) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalMCL.g:1918:4: () ( (lv_operator_2_0= ruleGOrOperator ) ) ( (lv_rightOperand_3_0= ruleGXorExpression ) )
            	    {
            	    // InternalMCL.g:1918:4: ()
            	    // InternalMCL.g:1919:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGOrExpressionAccess().getGOrExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:1925:4: ( (lv_operator_2_0= ruleGOrOperator ) )
            	    // InternalMCL.g:1926:5: (lv_operator_2_0= ruleGOrOperator )
            	    {
            	    // InternalMCL.g:1926:5: (lv_operator_2_0= ruleGOrOperator )
            	    // InternalMCL.g:1927:6: lv_operator_2_0= ruleGOrOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGOrExpressionAccess().getOperatorGOrOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGOrOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGOrExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GOrOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:1944:4: ( (lv_rightOperand_3_0= ruleGXorExpression ) )
            	    // InternalMCL.g:1945:5: (lv_rightOperand_3_0= ruleGXorExpression )
            	    {
            	    // InternalMCL.g:1945:5: (lv_rightOperand_3_0= ruleGXorExpression )
            	    // InternalMCL.g:1946:6: lv_rightOperand_3_0= ruleGXorExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGOrExpressionAccess().getRightOperandGXorExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_18);
            	    lv_rightOperand_3_0=ruleGXorExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGOrExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GXorExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGOrExpression"


    // $ANTLR start "entryRuleGXorExpression"
    // InternalMCL.g:1968:1: entryRuleGXorExpression returns [EObject current=null] : iv_ruleGXorExpression= ruleGXorExpression EOF ;
    public final EObject entryRuleGXorExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGXorExpression = null;


        try {
            // InternalMCL.g:1968:55: (iv_ruleGXorExpression= ruleGXorExpression EOF )
            // InternalMCL.g:1969:2: iv_ruleGXorExpression= ruleGXorExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGXorExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGXorExpression=ruleGXorExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGXorExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGXorExpression"


    // $ANTLR start "ruleGXorExpression"
    // InternalMCL.g:1975:1: ruleGXorExpression returns [EObject current=null] : (this_GAndExpression_0= ruleGAndExpression ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )* ) ;
    public final EObject ruleGXorExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GAndExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:1981:2: ( (this_GAndExpression_0= ruleGAndExpression ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )* ) )
            // InternalMCL.g:1982:2: (this_GAndExpression_0= ruleGAndExpression ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )* )
            {
            // InternalMCL.g:1982:2: (this_GAndExpression_0= ruleGAndExpression ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )* )
            // InternalMCL.g:1983:3: this_GAndExpression_0= ruleGAndExpression ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGXorExpressionAccess().getGAndExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_40);
            this_GAndExpression_0=ruleGAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GAndExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:1991:3: ( () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==56) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalMCL.g:1992:4: () ( (lv_operator_2_0= ruleGXorOperator ) ) ( (lv_rightOperand_3_0= ruleGAndExpression ) )
            	    {
            	    // InternalMCL.g:1992:4: ()
            	    // InternalMCL.g:1993:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGXorExpressionAccess().getGXorExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:1999:4: ( (lv_operator_2_0= ruleGXorOperator ) )
            	    // InternalMCL.g:2000:5: (lv_operator_2_0= ruleGXorOperator )
            	    {
            	    // InternalMCL.g:2000:5: (lv_operator_2_0= ruleGXorOperator )
            	    // InternalMCL.g:2001:6: lv_operator_2_0= ruleGXorOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGXorExpressionAccess().getOperatorGXorOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGXorOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGXorExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GXorOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2018:4: ( (lv_rightOperand_3_0= ruleGAndExpression ) )
            	    // InternalMCL.g:2019:5: (lv_rightOperand_3_0= ruleGAndExpression )
            	    {
            	    // InternalMCL.g:2019:5: (lv_rightOperand_3_0= ruleGAndExpression )
            	    // InternalMCL.g:2020:6: lv_rightOperand_3_0= ruleGAndExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGXorExpressionAccess().getRightOperandGAndExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_40);
            	    lv_rightOperand_3_0=ruleGAndExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGXorExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GAndExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGXorExpression"


    // $ANTLR start "entryRuleGAndExpression"
    // InternalMCL.g:2042:1: entryRuleGAndExpression returns [EObject current=null] : iv_ruleGAndExpression= ruleGAndExpression EOF ;
    public final EObject entryRuleGAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGAndExpression = null;


        try {
            // InternalMCL.g:2042:55: (iv_ruleGAndExpression= ruleGAndExpression EOF )
            // InternalMCL.g:2043:2: iv_ruleGAndExpression= ruleGAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGAndExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGAndExpression=ruleGAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGAndExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGAndExpression"


    // $ANTLR start "ruleGAndExpression"
    // InternalMCL.g:2049:1: ruleGAndExpression returns [EObject current=null] : (this_GEqualityExpression_0= ruleGEqualityExpression ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )* ) ;
    public final EObject ruleGAndExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GEqualityExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2055:2: ( (this_GEqualityExpression_0= ruleGEqualityExpression ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )* ) )
            // InternalMCL.g:2056:2: (this_GEqualityExpression_0= ruleGEqualityExpression ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )* )
            {
            // InternalMCL.g:2056:2: (this_GEqualityExpression_0= ruleGEqualityExpression ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )* )
            // InternalMCL.g:2057:3: this_GEqualityExpression_0= ruleGEqualityExpression ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGAndExpressionAccess().getGEqualityExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_19);
            this_GEqualityExpression_0=ruleGEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GEqualityExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2065:3: ( () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==23) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalMCL.g:2066:4: () ( (lv_operator_2_0= ruleGAndOperator ) ) ( (lv_rightOperand_3_0= ruleGEqualityExpression ) )
            	    {
            	    // InternalMCL.g:2066:4: ()
            	    // InternalMCL.g:2067:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGAndExpressionAccess().getGAndExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:2073:4: ( (lv_operator_2_0= ruleGAndOperator ) )
            	    // InternalMCL.g:2074:5: (lv_operator_2_0= ruleGAndOperator )
            	    {
            	    // InternalMCL.g:2074:5: (lv_operator_2_0= ruleGAndOperator )
            	    // InternalMCL.g:2075:6: lv_operator_2_0= ruleGAndOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGAndExpressionAccess().getOperatorGAndOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGAndOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGAndExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GAndOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2092:4: ( (lv_rightOperand_3_0= ruleGEqualityExpression ) )
            	    // InternalMCL.g:2093:5: (lv_rightOperand_3_0= ruleGEqualityExpression )
            	    {
            	    // InternalMCL.g:2093:5: (lv_rightOperand_3_0= ruleGEqualityExpression )
            	    // InternalMCL.g:2094:6: lv_rightOperand_3_0= ruleGEqualityExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGAndExpressionAccess().getRightOperandGEqualityExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_19);
            	    lv_rightOperand_3_0=ruleGEqualityExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGAndExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GEqualityExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGAndExpression"


    // $ANTLR start "entryRuleGEqualityExpression"
    // InternalMCL.g:2116:1: entryRuleGEqualityExpression returns [EObject current=null] : iv_ruleGEqualityExpression= ruleGEqualityExpression EOF ;
    public final EObject entryRuleGEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGEqualityExpression = null;


        try {
            // InternalMCL.g:2116:60: (iv_ruleGEqualityExpression= ruleGEqualityExpression EOF )
            // InternalMCL.g:2117:2: iv_ruleGEqualityExpression= ruleGEqualityExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGEqualityExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGEqualityExpression=ruleGEqualityExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGEqualityExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGEqualityExpression"


    // $ANTLR start "ruleGEqualityExpression"
    // InternalMCL.g:2123:1: ruleGEqualityExpression returns [EObject current=null] : (this_GRelationExpression_0= ruleGRelationExpression ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )* ) ;
    public final EObject ruleGEqualityExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GRelationExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2129:2: ( (this_GRelationExpression_0= ruleGRelationExpression ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )* ) )
            // InternalMCL.g:2130:2: (this_GRelationExpression_0= ruleGRelationExpression ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )* )
            {
            // InternalMCL.g:2130:2: (this_GRelationExpression_0= ruleGRelationExpression ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )* )
            // InternalMCL.g:2131:3: this_GRelationExpression_0= ruleGRelationExpression ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGEqualityExpressionAccess().getGRelationExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_41);
            this_GRelationExpression_0=ruleGRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GRelationExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2139:3: ( () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==47||LA24_0==57) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalMCL.g:2140:4: () ( (lv_operator_2_0= ruleGEqualityOperator ) ) ( (lv_rightOperand_3_0= ruleGRelationExpression ) )
            	    {
            	    // InternalMCL.g:2140:4: ()
            	    // InternalMCL.g:2141:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGEqualityExpressionAccess().getGEqualityExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:2147:4: ( (lv_operator_2_0= ruleGEqualityOperator ) )
            	    // InternalMCL.g:2148:5: (lv_operator_2_0= ruleGEqualityOperator )
            	    {
            	    // InternalMCL.g:2148:5: (lv_operator_2_0= ruleGEqualityOperator )
            	    // InternalMCL.g:2149:6: lv_operator_2_0= ruleGEqualityOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGEqualityExpressionAccess().getOperatorGEqualityOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGEqualityOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGEqualityExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GEqualityOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2166:4: ( (lv_rightOperand_3_0= ruleGRelationExpression ) )
            	    // InternalMCL.g:2167:5: (lv_rightOperand_3_0= ruleGRelationExpression )
            	    {
            	    // InternalMCL.g:2167:5: (lv_rightOperand_3_0= ruleGRelationExpression )
            	    // InternalMCL.g:2168:6: lv_rightOperand_3_0= ruleGRelationExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGEqualityExpressionAccess().getRightOperandGRelationExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_41);
            	    lv_rightOperand_3_0=ruleGRelationExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGEqualityExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GRelationExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGEqualityExpression"


    // $ANTLR start "entryRuleGRelationExpression"
    // InternalMCL.g:2190:1: entryRuleGRelationExpression returns [EObject current=null] : iv_ruleGRelationExpression= ruleGRelationExpression EOF ;
    public final EObject entryRuleGRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGRelationExpression = null;


        try {
            // InternalMCL.g:2190:60: (iv_ruleGRelationExpression= ruleGRelationExpression EOF )
            // InternalMCL.g:2191:2: iv_ruleGRelationExpression= ruleGRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGRelationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGRelationExpression=ruleGRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGRelationExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGRelationExpression"


    // $ANTLR start "ruleGRelationExpression"
    // InternalMCL.g:2197:1: ruleGRelationExpression returns [EObject current=null] : (this_GAdditionExpression_0= ruleGAdditionExpression ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )* ) ;
    public final EObject ruleGRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GAdditionExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2203:2: ( (this_GAdditionExpression_0= ruleGAdditionExpression ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )* ) )
            // InternalMCL.g:2204:2: (this_GAdditionExpression_0= ruleGAdditionExpression ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )* )
            {
            // InternalMCL.g:2204:2: (this_GAdditionExpression_0= ruleGAdditionExpression ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )* )
            // InternalMCL.g:2205:3: this_GAdditionExpression_0= ruleGAdditionExpression ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGRelationExpressionAccess().getGAdditionExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_42);
            this_GAdditionExpression_0=ruleGAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GAdditionExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2213:3: ( () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=40 && LA25_0<=41)||(LA25_0>=58 && LA25_0<=59)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalMCL.g:2214:4: () ( (lv_operator_2_0= ruleGRelationOperator ) ) ( (lv_rightOperand_3_0= ruleGAdditionExpression ) )
            	    {
            	    // InternalMCL.g:2214:4: ()
            	    // InternalMCL.g:2215:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGRelationExpressionAccess().getGRelationExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:2221:4: ( (lv_operator_2_0= ruleGRelationOperator ) )
            	    // InternalMCL.g:2222:5: (lv_operator_2_0= ruleGRelationOperator )
            	    {
            	    // InternalMCL.g:2222:5: (lv_operator_2_0= ruleGRelationOperator )
            	    // InternalMCL.g:2223:6: lv_operator_2_0= ruleGRelationOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGRelationExpressionAccess().getOperatorGRelationOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGRelationOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGRelationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GRelationOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2240:4: ( (lv_rightOperand_3_0= ruleGAdditionExpression ) )
            	    // InternalMCL.g:2241:5: (lv_rightOperand_3_0= ruleGAdditionExpression )
            	    {
            	    // InternalMCL.g:2241:5: (lv_rightOperand_3_0= ruleGAdditionExpression )
            	    // InternalMCL.g:2242:6: lv_rightOperand_3_0= ruleGAdditionExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGRelationExpressionAccess().getRightOperandGAdditionExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_42);
            	    lv_rightOperand_3_0=ruleGAdditionExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGRelationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GAdditionExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGRelationExpression"


    // $ANTLR start "entryRuleGAdditionExpression"
    // InternalMCL.g:2264:1: entryRuleGAdditionExpression returns [EObject current=null] : iv_ruleGAdditionExpression= ruleGAdditionExpression EOF ;
    public final EObject entryRuleGAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGAdditionExpression = null;


        try {
            // InternalMCL.g:2264:60: (iv_ruleGAdditionExpression= ruleGAdditionExpression EOF )
            // InternalMCL.g:2265:2: iv_ruleGAdditionExpression= ruleGAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGAdditionExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGAdditionExpression=ruleGAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGAdditionExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGAdditionExpression"


    // $ANTLR start "ruleGAdditionExpression"
    // InternalMCL.g:2271:1: ruleGAdditionExpression returns [EObject current=null] : (this_GMultiplicationExpression_0= ruleGMultiplicationExpression ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )* ) ;
    public final EObject ruleGAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GMultiplicationExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2277:2: ( (this_GMultiplicationExpression_0= ruleGMultiplicationExpression ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )* ) )
            // InternalMCL.g:2278:2: (this_GMultiplicationExpression_0= ruleGMultiplicationExpression ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )* )
            {
            // InternalMCL.g:2278:2: (this_GMultiplicationExpression_0= ruleGMultiplicationExpression ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )* )
            // InternalMCL.g:2279:3: this_GMultiplicationExpression_0= ruleGMultiplicationExpression ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGAdditionExpressionAccess().getGMultiplicationExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_43);
            this_GMultiplicationExpression_0=ruleGMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GMultiplicationExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2287:3: ( () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==42||LA26_0==45) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalMCL.g:2288:4: () ( (lv_operator_2_0= ruleGAdditionOperator ) ) ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) )
            	    {
            	    // InternalMCL.g:2288:4: ()
            	    // InternalMCL.g:2289:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGAdditionExpressionAccess().getGAdditionExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:2295:4: ( (lv_operator_2_0= ruleGAdditionOperator ) )
            	    // InternalMCL.g:2296:5: (lv_operator_2_0= ruleGAdditionOperator )
            	    {
            	    // InternalMCL.g:2296:5: (lv_operator_2_0= ruleGAdditionOperator )
            	    // InternalMCL.g:2297:6: lv_operator_2_0= ruleGAdditionOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGAdditionExpressionAccess().getOperatorGAdditionOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGAdditionOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGAdditionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GAdditionOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2314:4: ( (lv_rightOperand_3_0= ruleGMultiplicationExpression ) )
            	    // InternalMCL.g:2315:5: (lv_rightOperand_3_0= ruleGMultiplicationExpression )
            	    {
            	    // InternalMCL.g:2315:5: (lv_rightOperand_3_0= ruleGMultiplicationExpression )
            	    // InternalMCL.g:2316:6: lv_rightOperand_3_0= ruleGMultiplicationExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGAdditionExpressionAccess().getRightOperandGMultiplicationExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_43);
            	    lv_rightOperand_3_0=ruleGMultiplicationExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGAdditionExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GMultiplicationExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGAdditionExpression"


    // $ANTLR start "entryRuleGMultiplicationExpression"
    // InternalMCL.g:2338:1: entryRuleGMultiplicationExpression returns [EObject current=null] : iv_ruleGMultiplicationExpression= ruleGMultiplicationExpression EOF ;
    public final EObject entryRuleGMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGMultiplicationExpression = null;


        try {
            // InternalMCL.g:2338:66: (iv_ruleGMultiplicationExpression= ruleGMultiplicationExpression EOF )
            // InternalMCL.g:2339:2: iv_ruleGMultiplicationExpression= ruleGMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGMultiplicationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGMultiplicationExpression=ruleGMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGMultiplicationExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGMultiplicationExpression"


    // $ANTLR start "ruleGMultiplicationExpression"
    // InternalMCL.g:2345:1: ruleGMultiplicationExpression returns [EObject current=null] : (this_GNegationExpression_0= ruleGNegationExpression ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )* ) ;
    public final EObject ruleGMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GNegationExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightOperand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2351:2: ( (this_GNegationExpression_0= ruleGNegationExpression ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )* ) )
            // InternalMCL.g:2352:2: (this_GNegationExpression_0= ruleGNegationExpression ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )* )
            {
            // InternalMCL.g:2352:2: (this_GNegationExpression_0= ruleGNegationExpression ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )* )
            // InternalMCL.g:2353:3: this_GNegationExpression_0= ruleGNegationExpression ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGMultiplicationExpressionAccess().getGNegationExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_44);
            this_GNegationExpression_0=ruleGNegationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GNegationExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2361:3: ( () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>=60 && LA27_0<=61)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalMCL.g:2362:4: () ( (lv_operator_2_0= ruleGMultiplicationOperator ) ) ( (lv_rightOperand_3_0= ruleGNegationExpression ) )
            	    {
            	    // InternalMCL.g:2362:4: ()
            	    // InternalMCL.g:2363:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGMultiplicationExpressionAccess().getGMultiplicationExpressionLeftOperandAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalMCL.g:2369:4: ( (lv_operator_2_0= ruleGMultiplicationOperator ) )
            	    // InternalMCL.g:2370:5: (lv_operator_2_0= ruleGMultiplicationOperator )
            	    {
            	    // InternalMCL.g:2370:5: (lv_operator_2_0= ruleGMultiplicationOperator )
            	    // InternalMCL.g:2371:6: lv_operator_2_0= ruleGMultiplicationOperator
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGMultiplicationExpressionAccess().getOperatorGMultiplicationOperatorEnumRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_39);
            	    lv_operator_2_0=ruleGMultiplicationOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGMultiplicationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"operator",
            	      							lv_operator_2_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GMultiplicationOperator");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalMCL.g:2388:4: ( (lv_rightOperand_3_0= ruleGNegationExpression ) )
            	    // InternalMCL.g:2389:5: (lv_rightOperand_3_0= ruleGNegationExpression )
            	    {
            	    // InternalMCL.g:2389:5: (lv_rightOperand_3_0= ruleGNegationExpression )
            	    // InternalMCL.g:2390:6: lv_rightOperand_3_0= ruleGNegationExpression
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getGMultiplicationExpressionAccess().getRightOperandGNegationExpressionParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_44);
            	    lv_rightOperand_3_0=ruleGNegationExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getGMultiplicationExpressionRule());
            	      						}
            	      						set(
            	      							current,
            	      							"rightOperand",
            	      							lv_rightOperand_3_0,
            	      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GNegationExpression");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGMultiplicationExpression"


    // $ANTLR start "entryRuleGNegationExpression"
    // InternalMCL.g:2412:1: entryRuleGNegationExpression returns [EObject current=null] : iv_ruleGNegationExpression= ruleGNegationExpression EOF ;
    public final EObject entryRuleGNegationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGNegationExpression = null;


        try {
            // InternalMCL.g:2412:60: (iv_ruleGNegationExpression= ruleGNegationExpression EOF )
            // InternalMCL.g:2413:2: iv_ruleGNegationExpression= ruleGNegationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGNegationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGNegationExpression=ruleGNegationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGNegationExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGNegationExpression"


    // $ANTLR start "ruleGNegationExpression"
    // InternalMCL.g:2419:1: ruleGNegationExpression returns [EObject current=null] : (this_GNavigationExpression_0= ruleGNavigationExpression | ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) ) ) ;
    public final EObject ruleGNegationExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GNavigationExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_operand_3_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2425:2: ( (this_GNavigationExpression_0= ruleGNavigationExpression | ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) ) ) )
            // InternalMCL.g:2426:2: (this_GNavigationExpression_0= ruleGNavigationExpression | ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) ) )
            {
            // InternalMCL.g:2426:2: (this_GNavigationExpression_0= ruleGNavigationExpression | ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( ((LA28_0>=RULE_ID && LA28_0<=RULE_BOOLEAN)||LA28_0==14||(LA28_0>=50 && LA28_0<=51)) ) {
                alt28=1;
            }
            else if ( (LA28_0==44||LA28_0==62) ) {
                alt28=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalMCL.g:2427:3: this_GNavigationExpression_0= ruleGNavigationExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGNegationExpressionAccess().getGNavigationExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GNavigationExpression_0=ruleGNavigationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GNavigationExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:2436:3: ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) )
                    {
                    // InternalMCL.g:2436:3: ( () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) ) )
                    // InternalMCL.g:2437:4: () ( (lv_operator_2_0= ruleGNegationOperator ) ) ( (lv_operand_3_0= ruleGNavigationExpression ) )
                    {
                    // InternalMCL.g:2437:4: ()
                    // InternalMCL.g:2438:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getGNegationExpressionAccess().getGNegationExpressionAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalMCL.g:2444:4: ( (lv_operator_2_0= ruleGNegationOperator ) )
                    // InternalMCL.g:2445:5: (lv_operator_2_0= ruleGNegationOperator )
                    {
                    // InternalMCL.g:2445:5: (lv_operator_2_0= ruleGNegationOperator )
                    // InternalMCL.g:2446:6: lv_operator_2_0= ruleGNegationOperator
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getGNegationExpressionAccess().getOperatorGNegationOperatorEnumRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_45);
                    lv_operator_2_0=ruleGNegationOperator();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getGNegationExpressionRule());
                      						}
                      						set(
                      							current,
                      							"operator",
                      							lv_operator_2_0,
                      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GNegationOperator");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalMCL.g:2463:4: ( (lv_operand_3_0= ruleGNavigationExpression ) )
                    // InternalMCL.g:2464:5: (lv_operand_3_0= ruleGNavigationExpression )
                    {
                    // InternalMCL.g:2464:5: (lv_operand_3_0= ruleGNavigationExpression )
                    // InternalMCL.g:2465:6: lv_operand_3_0= ruleGNavigationExpression
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getGNegationExpressionAccess().getOperandGNavigationExpressionParserRuleCall_1_2_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_operand_3_0=ruleGNavigationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getGNegationExpressionRule());
                      						}
                      						set(
                      							current,
                      							"operand",
                      							lv_operand_3_0,
                      							"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GNavigationExpression");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGNegationExpression"


    // $ANTLR start "entryRuleGNavigationExpression"
    // InternalMCL.g:2487:1: entryRuleGNavigationExpression returns [EObject current=null] : iv_ruleGNavigationExpression= ruleGNavigationExpression EOF ;
    public final EObject entryRuleGNavigationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGNavigationExpression = null;


        try {
            // InternalMCL.g:2487:62: (iv_ruleGNavigationExpression= ruleGNavigationExpression EOF )
            // InternalMCL.g:2488:2: iv_ruleGNavigationExpression= ruleGNavigationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGNavigationExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGNavigationExpression=ruleGNavigationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGNavigationExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGNavigationExpression"


    // $ANTLR start "ruleGNavigationExpression"
    // InternalMCL.g:2494:1: ruleGNavigationExpression returns [EObject current=null] : (this_GReferenceExpression_0= ruleGReferenceExpression ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )* ) ;
    public final EObject ruleGNavigationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        EObject this_GReferenceExpression_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2500:2: ( (this_GReferenceExpression_0= ruleGReferenceExpression ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )* ) )
            // InternalMCL.g:2501:2: (this_GReferenceExpression_0= ruleGReferenceExpression ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )* )
            {
            // InternalMCL.g:2501:2: (this_GReferenceExpression_0= ruleGReferenceExpression ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )* )
            // InternalMCL.g:2502:3: this_GReferenceExpression_0= ruleGReferenceExpression ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getGNavigationExpressionAccess().getGReferenceExpressionParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_46);
            this_GReferenceExpression_0=ruleGReferenceExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_GReferenceExpression_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalMCL.g:2510:3: ( () ruleNavigationOperator ( (otherlv_3= RULE_ID ) ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==46||LA29_0==55) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalMCL.g:2511:4: () ruleNavigationOperator ( (otherlv_3= RULE_ID ) )
            	    {
            	    // InternalMCL.g:2511:4: ()
            	    // InternalMCL.g:2512:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getGNavigationExpressionAccess().getGNavigationExpressionBodyAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getGNavigationExpressionAccess().getNavigationOperatorParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_5);
            	    ruleNavigationOperator();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }
            	    // InternalMCL.g:2525:4: ( (otherlv_3= RULE_ID ) )
            	    // InternalMCL.g:2526:5: (otherlv_3= RULE_ID )
            	    {
            	    // InternalMCL.g:2526:5: (otherlv_3= RULE_ID )
            	    // InternalMCL.g:2527:6: otherlv_3= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElement(grammarAccess.getGNavigationExpressionRule());
            	      						}
            	      					
            	    }
            	    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_46); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						newLeafNode(otherlv_3, grammarAccess.getGNavigationExpressionAccess().getReferencedEObjectEObjectCrossReference_1_2_0());
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGNavigationExpression"


    // $ANTLR start "entryRuleGReferenceExpression"
    // InternalMCL.g:2543:1: entryRuleGReferenceExpression returns [EObject current=null] : iv_ruleGReferenceExpression= ruleGReferenceExpression EOF ;
    public final EObject entryRuleGReferenceExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGReferenceExpression = null;


        try {
            // InternalMCL.g:2543:61: (iv_ruleGReferenceExpression= ruleGReferenceExpression EOF )
            // InternalMCL.g:2544:2: iv_ruleGReferenceExpression= ruleGReferenceExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGReferenceExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGReferenceExpression=ruleGReferenceExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGReferenceExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGReferenceExpression"


    // $ANTLR start "ruleGReferenceExpression"
    // InternalMCL.g:2550:1: ruleGReferenceExpression returns [EObject current=null] : (this_GPrimaryExpression_0= ruleGPrimaryExpression | ( () ( (otherlv_2= RULE_ID ) ) ) ) ;
    public final EObject ruleGReferenceExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_GPrimaryExpression_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2556:2: ( (this_GPrimaryExpression_0= ruleGPrimaryExpression | ( () ( (otherlv_2= RULE_ID ) ) ) ) )
            // InternalMCL.g:2557:2: (this_GPrimaryExpression_0= ruleGPrimaryExpression | ( () ( (otherlv_2= RULE_ID ) ) ) )
            {
            // InternalMCL.g:2557:2: (this_GPrimaryExpression_0= ruleGPrimaryExpression | ( () ( (otherlv_2= RULE_ID ) ) ) )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=RULE_STRING && LA30_0<=RULE_BOOLEAN)||LA30_0==14||(LA30_0>=50 && LA30_0<=51)) ) {
                alt30=1;
            }
            else if ( (LA30_0==RULE_ID) ) {
                alt30=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalMCL.g:2558:3: this_GPrimaryExpression_0= ruleGPrimaryExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGReferenceExpressionAccess().getGPrimaryExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GPrimaryExpression_0=ruleGPrimaryExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GPrimaryExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:2567:3: ( () ( (otherlv_2= RULE_ID ) ) )
                    {
                    // InternalMCL.g:2567:3: ( () ( (otherlv_2= RULE_ID ) ) )
                    // InternalMCL.g:2568:4: () ( (otherlv_2= RULE_ID ) )
                    {
                    // InternalMCL.g:2568:4: ()
                    // InternalMCL.g:2569:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getGReferenceExpressionAccess().getGReferenceExpressionAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalMCL.g:2575:4: ( (otherlv_2= RULE_ID ) )
                    // InternalMCL.g:2576:5: (otherlv_2= RULE_ID )
                    {
                    // InternalMCL.g:2576:5: (otherlv_2= RULE_ID )
                    // InternalMCL.g:2577:6: otherlv_2= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getGReferenceExpressionRule());
                      						}
                      					
                    }
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(otherlv_2, grammarAccess.getGReferenceExpressionAccess().getReferencedEObjectEObjectCrossReference_1_1_0());
                      					
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGReferenceExpression"


    // $ANTLR start "entryRuleGPrimaryExpression"
    // InternalMCL.g:2593:1: entryRuleGPrimaryExpression returns [EObject current=null] : iv_ruleGPrimaryExpression= ruleGPrimaryExpression EOF ;
    public final EObject entryRuleGPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGPrimaryExpression = null;


        try {
            // InternalMCL.g:2593:59: (iv_ruleGPrimaryExpression= ruleGPrimaryExpression EOF )
            // InternalMCL.g:2594:2: iv_ruleGPrimaryExpression= ruleGPrimaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGPrimaryExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGPrimaryExpression=ruleGPrimaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGPrimaryExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGPrimaryExpression"


    // $ANTLR start "ruleGPrimaryExpression"
    // InternalMCL.g:2600:1: ruleGPrimaryExpression returns [EObject current=null] : (this_GStringExpression_0= ruleGStringExpression | this_GBooleanExpression_1= ruleGBooleanExpression | this_GNumericExpression_2= ruleGNumericExpression | this_GEnumLiteralExpression_3= ruleGEnumLiteralExpression | this_GIfExpression_4= ruleGIfExpression | this_GBraceExpression_5= ruleGBraceExpression ) ;
    public final EObject ruleGPrimaryExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GStringExpression_0 = null;

        EObject this_GBooleanExpression_1 = null;

        EObject this_GNumericExpression_2 = null;

        EObject this_GEnumLiteralExpression_3 = null;

        EObject this_GIfExpression_4 = null;

        EObject this_GBraceExpression_5 = null;



        	enterRule();

        try {
            // InternalMCL.g:2606:2: ( (this_GStringExpression_0= ruleGStringExpression | this_GBooleanExpression_1= ruleGBooleanExpression | this_GNumericExpression_2= ruleGNumericExpression | this_GEnumLiteralExpression_3= ruleGEnumLiteralExpression | this_GIfExpression_4= ruleGIfExpression | this_GBraceExpression_5= ruleGBraceExpression ) )
            // InternalMCL.g:2607:2: (this_GStringExpression_0= ruleGStringExpression | this_GBooleanExpression_1= ruleGBooleanExpression | this_GNumericExpression_2= ruleGNumericExpression | this_GEnumLiteralExpression_3= ruleGEnumLiteralExpression | this_GIfExpression_4= ruleGIfExpression | this_GBraceExpression_5= ruleGBraceExpression )
            {
            // InternalMCL.g:2607:2: (this_GStringExpression_0= ruleGStringExpression | this_GBooleanExpression_1= ruleGBooleanExpression | this_GNumericExpression_2= ruleGNumericExpression | this_GEnumLiteralExpression_3= ruleGEnumLiteralExpression | this_GIfExpression_4= ruleGIfExpression | this_GBraceExpression_5= ruleGBraceExpression )
            int alt31=6;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt31=1;
                }
                break;
            case RULE_BOOLEAN:
                {
                alt31=2;
                }
                break;
            case RULE_INT:
            case RULE_DOUBLE:
                {
                alt31=3;
                }
                break;
            case 50:
                {
                alt31=4;
                }
                break;
            case 51:
                {
                alt31=5;
                }
                break;
            case 14:
                {
                alt31=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalMCL.g:2608:3: this_GStringExpression_0= ruleGStringExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGStringExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GStringExpression_0=ruleGStringExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GStringExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:2617:3: this_GBooleanExpression_1= ruleGBooleanExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGBooleanExpressionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GBooleanExpression_1=ruleGBooleanExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GBooleanExpression_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalMCL.g:2626:3: this_GNumericExpression_2= ruleGNumericExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGNumericExpressionParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GNumericExpression_2=ruleGNumericExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GNumericExpression_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalMCL.g:2635:3: this_GEnumLiteralExpression_3= ruleGEnumLiteralExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGEnumLiteralExpressionParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GEnumLiteralExpression_3=ruleGEnumLiteralExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GEnumLiteralExpression_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalMCL.g:2644:3: this_GIfExpression_4= ruleGIfExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGIfExpressionParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GIfExpression_4=ruleGIfExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GIfExpression_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalMCL.g:2653:3: this_GBraceExpression_5= ruleGBraceExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGPrimaryExpressionAccess().getGBraceExpressionParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GBraceExpression_5=ruleGBraceExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GBraceExpression_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGPrimaryExpression"


    // $ANTLR start "entryRuleGStringExpression"
    // InternalMCL.g:2665:1: entryRuleGStringExpression returns [EObject current=null] : iv_ruleGStringExpression= ruleGStringExpression EOF ;
    public final EObject entryRuleGStringExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGStringExpression = null;


        try {
            // InternalMCL.g:2665:58: (iv_ruleGStringExpression= ruleGStringExpression EOF )
            // InternalMCL.g:2666:2: iv_ruleGStringExpression= ruleGStringExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGStringExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGStringExpression=ruleGStringExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGStringExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGStringExpression"


    // $ANTLR start "ruleGStringExpression"
    // InternalMCL.g:2672:1: ruleGStringExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleGStringExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:2678:2: ( ( () ( (lv_value_1_0= RULE_STRING ) ) ) )
            // InternalMCL.g:2679:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            {
            // InternalMCL.g:2679:2: ( () ( (lv_value_1_0= RULE_STRING ) ) )
            // InternalMCL.g:2680:3: () ( (lv_value_1_0= RULE_STRING ) )
            {
            // InternalMCL.g:2680:3: ()
            // InternalMCL.g:2681:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGStringExpressionAccess().getGStringExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:2687:3: ( (lv_value_1_0= RULE_STRING ) )
            // InternalMCL.g:2688:4: (lv_value_1_0= RULE_STRING )
            {
            // InternalMCL.g:2688:4: (lv_value_1_0= RULE_STRING )
            // InternalMCL.g:2689:5: lv_value_1_0= RULE_STRING
            {
            lv_value_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getGStringExpressionAccess().getValueSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGStringExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGStringExpression"


    // $ANTLR start "entryRuleGBooleanExpression"
    // InternalMCL.g:2709:1: entryRuleGBooleanExpression returns [EObject current=null] : iv_ruleGBooleanExpression= ruleGBooleanExpression EOF ;
    public final EObject entryRuleGBooleanExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGBooleanExpression = null;


        try {
            // InternalMCL.g:2709:59: (iv_ruleGBooleanExpression= ruleGBooleanExpression EOF )
            // InternalMCL.g:2710:2: iv_ruleGBooleanExpression= ruleGBooleanExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGBooleanExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGBooleanExpression=ruleGBooleanExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGBooleanExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGBooleanExpression"


    // $ANTLR start "ruleGBooleanExpression"
    // InternalMCL.g:2716:1: ruleGBooleanExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_BOOLEAN ) ) ) ;
    public final EObject ruleGBooleanExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:2722:2: ( ( () ( (lv_value_1_0= RULE_BOOLEAN ) ) ) )
            // InternalMCL.g:2723:2: ( () ( (lv_value_1_0= RULE_BOOLEAN ) ) )
            {
            // InternalMCL.g:2723:2: ( () ( (lv_value_1_0= RULE_BOOLEAN ) ) )
            // InternalMCL.g:2724:3: () ( (lv_value_1_0= RULE_BOOLEAN ) )
            {
            // InternalMCL.g:2724:3: ()
            // InternalMCL.g:2725:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGBooleanExpressionAccess().getGBooleanExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:2731:3: ( (lv_value_1_0= RULE_BOOLEAN ) )
            // InternalMCL.g:2732:4: (lv_value_1_0= RULE_BOOLEAN )
            {
            // InternalMCL.g:2732:4: (lv_value_1_0= RULE_BOOLEAN )
            // InternalMCL.g:2733:5: lv_value_1_0= RULE_BOOLEAN
            {
            lv_value_1_0=(Token)match(input,RULE_BOOLEAN,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getGBooleanExpressionAccess().getValueBOOLEANTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGBooleanExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.BOOLEAN");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGBooleanExpression"


    // $ANTLR start "entryRuleGNumericExpression"
    // InternalMCL.g:2753:1: entryRuleGNumericExpression returns [EObject current=null] : iv_ruleGNumericExpression= ruleGNumericExpression EOF ;
    public final EObject entryRuleGNumericExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGNumericExpression = null;


        try {
            // InternalMCL.g:2753:59: (iv_ruleGNumericExpression= ruleGNumericExpression EOF )
            // InternalMCL.g:2754:2: iv_ruleGNumericExpression= ruleGNumericExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGNumericExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGNumericExpression=ruleGNumericExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGNumericExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGNumericExpression"


    // $ANTLR start "ruleGNumericExpression"
    // InternalMCL.g:2760:1: ruleGNumericExpression returns [EObject current=null] : (this_GIntegerExpression_0= ruleGIntegerExpression | this_GDoubleExpression_1= ruleGDoubleExpression ) ;
    public final EObject ruleGNumericExpression() throws RecognitionException {
        EObject current = null;

        EObject this_GIntegerExpression_0 = null;

        EObject this_GDoubleExpression_1 = null;



        	enterRule();

        try {
            // InternalMCL.g:2766:2: ( (this_GIntegerExpression_0= ruleGIntegerExpression | this_GDoubleExpression_1= ruleGDoubleExpression ) )
            // InternalMCL.g:2767:2: (this_GIntegerExpression_0= ruleGIntegerExpression | this_GDoubleExpression_1= ruleGDoubleExpression )
            {
            // InternalMCL.g:2767:2: (this_GIntegerExpression_0= ruleGIntegerExpression | this_GDoubleExpression_1= ruleGDoubleExpression )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==RULE_INT) ) {
                alt32=1;
            }
            else if ( (LA32_0==RULE_DOUBLE) ) {
                alt32=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalMCL.g:2768:3: this_GIntegerExpression_0= ruleGIntegerExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGNumericExpressionAccess().getGIntegerExpressionParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GIntegerExpression_0=ruleGIntegerExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GIntegerExpression_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:2777:3: this_GDoubleExpression_1= ruleGDoubleExpression
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getGNumericExpressionAccess().getGDoubleExpressionParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_GDoubleExpression_1=ruleGDoubleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_GDoubleExpression_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGNumericExpression"


    // $ANTLR start "entryRuleGIntegerExpression"
    // InternalMCL.g:2789:1: entryRuleGIntegerExpression returns [EObject current=null] : iv_ruleGIntegerExpression= ruleGIntegerExpression EOF ;
    public final EObject entryRuleGIntegerExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGIntegerExpression = null;


        try {
            // InternalMCL.g:2789:59: (iv_ruleGIntegerExpression= ruleGIntegerExpression EOF )
            // InternalMCL.g:2790:2: iv_ruleGIntegerExpression= ruleGIntegerExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGIntegerExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGIntegerExpression=ruleGIntegerExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGIntegerExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGIntegerExpression"


    // $ANTLR start "ruleGIntegerExpression"
    // InternalMCL.g:2796:1: ruleGIntegerExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_INT ) ) ) ;
    public final EObject ruleGIntegerExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:2802:2: ( ( () ( (lv_value_1_0= RULE_INT ) ) ) )
            // InternalMCL.g:2803:2: ( () ( (lv_value_1_0= RULE_INT ) ) )
            {
            // InternalMCL.g:2803:2: ( () ( (lv_value_1_0= RULE_INT ) ) )
            // InternalMCL.g:2804:3: () ( (lv_value_1_0= RULE_INT ) )
            {
            // InternalMCL.g:2804:3: ()
            // InternalMCL.g:2805:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGIntegerExpressionAccess().getGIntegerExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:2811:3: ( (lv_value_1_0= RULE_INT ) )
            // InternalMCL.g:2812:4: (lv_value_1_0= RULE_INT )
            {
            // InternalMCL.g:2812:4: (lv_value_1_0= RULE_INT )
            // InternalMCL.g:2813:5: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getGIntegerExpressionAccess().getValueINTTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGIntegerExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.xtext.common.Terminals.INT");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGIntegerExpression"


    // $ANTLR start "entryRuleGDoubleExpression"
    // InternalMCL.g:2833:1: entryRuleGDoubleExpression returns [EObject current=null] : iv_ruleGDoubleExpression= ruleGDoubleExpression EOF ;
    public final EObject entryRuleGDoubleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGDoubleExpression = null;


        try {
            // InternalMCL.g:2833:58: (iv_ruleGDoubleExpression= ruleGDoubleExpression EOF )
            // InternalMCL.g:2834:2: iv_ruleGDoubleExpression= ruleGDoubleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGDoubleExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGDoubleExpression=ruleGDoubleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGDoubleExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGDoubleExpression"


    // $ANTLR start "ruleGDoubleExpression"
    // InternalMCL.g:2840:1: ruleGDoubleExpression returns [EObject current=null] : ( () ( (lv_value_1_0= RULE_DOUBLE ) ) ) ;
    public final EObject ruleGDoubleExpression() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;


        	enterRule();

        try {
            // InternalMCL.g:2846:2: ( ( () ( (lv_value_1_0= RULE_DOUBLE ) ) ) )
            // InternalMCL.g:2847:2: ( () ( (lv_value_1_0= RULE_DOUBLE ) ) )
            {
            // InternalMCL.g:2847:2: ( () ( (lv_value_1_0= RULE_DOUBLE ) ) )
            // InternalMCL.g:2848:3: () ( (lv_value_1_0= RULE_DOUBLE ) )
            {
            // InternalMCL.g:2848:3: ()
            // InternalMCL.g:2849:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGDoubleExpressionAccess().getGDoubleExpressionAction_0(),
              					current);
              			
            }

            }

            // InternalMCL.g:2855:3: ( (lv_value_1_0= RULE_DOUBLE ) )
            // InternalMCL.g:2856:4: (lv_value_1_0= RULE_DOUBLE )
            {
            // InternalMCL.g:2856:4: (lv_value_1_0= RULE_DOUBLE )
            // InternalMCL.g:2857:5: lv_value_1_0= RULE_DOUBLE
            {
            lv_value_1_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_value_1_0, grammarAccess.getGDoubleExpressionAccess().getValueDOUBLETerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGDoubleExpressionRule());
              					}
              					setWithLastConsumed(
              						current,
              						"value",
              						lv_value_1_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.DOUBLE");
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGDoubleExpression"


    // $ANTLR start "entryRuleGEnumLiteralExpression"
    // InternalMCL.g:2877:1: entryRuleGEnumLiteralExpression returns [EObject current=null] : iv_ruleGEnumLiteralExpression= ruleGEnumLiteralExpression EOF ;
    public final EObject entryRuleGEnumLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGEnumLiteralExpression = null;


        try {
            // InternalMCL.g:2877:63: (iv_ruleGEnumLiteralExpression= ruleGEnumLiteralExpression EOF )
            // InternalMCL.g:2878:2: iv_ruleGEnumLiteralExpression= ruleGEnumLiteralExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGEnumLiteralExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGEnumLiteralExpression=ruleGEnumLiteralExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGEnumLiteralExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGEnumLiteralExpression"


    // $ANTLR start "ruleGEnumLiteralExpression"
    // InternalMCL.g:2884:1: ruleGEnumLiteralExpression returns [EObject current=null] : ( () otherlv_1= '#' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleGEnumLiteralExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalMCL.g:2890:2: ( ( () otherlv_1= '#' ( ( ruleQualifiedName ) ) ) )
            // InternalMCL.g:2891:2: ( () otherlv_1= '#' ( ( ruleQualifiedName ) ) )
            {
            // InternalMCL.g:2891:2: ( () otherlv_1= '#' ( ( ruleQualifiedName ) ) )
            // InternalMCL.g:2892:3: () otherlv_1= '#' ( ( ruleQualifiedName ) )
            {
            // InternalMCL.g:2892:3: ()
            // InternalMCL.g:2893:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGEnumLiteralExpressionAccess().getGEnumLiteralExpressionAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,50,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getGEnumLiteralExpressionAccess().getNumberSignKeyword_1());
              		
            }
            // InternalMCL.g:2903:3: ( ( ruleQualifiedName ) )
            // InternalMCL.g:2904:4: ( ruleQualifiedName )
            {
            // InternalMCL.g:2904:4: ( ruleQualifiedName )
            // InternalMCL.g:2905:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getGEnumLiteralExpressionRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGEnumLiteralExpressionAccess().getValueEEnumLiteralCrossReference_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGEnumLiteralExpression"


    // $ANTLR start "entryRuleGIfExpression"
    // InternalMCL.g:2923:1: entryRuleGIfExpression returns [EObject current=null] : iv_ruleGIfExpression= ruleGIfExpression EOF ;
    public final EObject entryRuleGIfExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGIfExpression = null;


        try {
            // InternalMCL.g:2923:54: (iv_ruleGIfExpression= ruleGIfExpression EOF )
            // InternalMCL.g:2924:2: iv_ruleGIfExpression= ruleGIfExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGIfExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGIfExpression=ruleGIfExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGIfExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGIfExpression"


    // $ANTLR start "ruleGIfExpression"
    // InternalMCL.g:2930:1: ruleGIfExpression returns [EObject current=null] : ( () otherlv_1= 'if' ( (lv_condition_2_0= ruleGExpression ) ) otherlv_3= 'then' ( (lv_thenExpression_4_0= ruleGExpression ) ) otherlv_5= 'else' ( (lv_elseExpression_6_0= ruleGExpression ) ) otherlv_7= 'endif' ) ;
    public final EObject ruleGIfExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_condition_2_0 = null;

        EObject lv_thenExpression_4_0 = null;

        EObject lv_elseExpression_6_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:2936:2: ( ( () otherlv_1= 'if' ( (lv_condition_2_0= ruleGExpression ) ) otherlv_3= 'then' ( (lv_thenExpression_4_0= ruleGExpression ) ) otherlv_5= 'else' ( (lv_elseExpression_6_0= ruleGExpression ) ) otherlv_7= 'endif' ) )
            // InternalMCL.g:2937:2: ( () otherlv_1= 'if' ( (lv_condition_2_0= ruleGExpression ) ) otherlv_3= 'then' ( (lv_thenExpression_4_0= ruleGExpression ) ) otherlv_5= 'else' ( (lv_elseExpression_6_0= ruleGExpression ) ) otherlv_7= 'endif' )
            {
            // InternalMCL.g:2937:2: ( () otherlv_1= 'if' ( (lv_condition_2_0= ruleGExpression ) ) otherlv_3= 'then' ( (lv_thenExpression_4_0= ruleGExpression ) ) otherlv_5= 'else' ( (lv_elseExpression_6_0= ruleGExpression ) ) otherlv_7= 'endif' )
            // InternalMCL.g:2938:3: () otherlv_1= 'if' ( (lv_condition_2_0= ruleGExpression ) ) otherlv_3= 'then' ( (lv_thenExpression_4_0= ruleGExpression ) ) otherlv_5= 'else' ( (lv_elseExpression_6_0= ruleGExpression ) ) otherlv_7= 'endif'
            {
            // InternalMCL.g:2938:3: ()
            // InternalMCL.g:2939:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGIfExpressionAccess().getGIfExpressionAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,51,FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getGIfExpressionAccess().getIfKeyword_1());
              		
            }
            // InternalMCL.g:2949:3: ( (lv_condition_2_0= ruleGExpression ) )
            // InternalMCL.g:2950:4: (lv_condition_2_0= ruleGExpression )
            {
            // InternalMCL.g:2950:4: (lv_condition_2_0= ruleGExpression )
            // InternalMCL.g:2951:5: lv_condition_2_0= ruleGExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGIfExpressionAccess().getConditionGExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_47);
            lv_condition_2_0=ruleGExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getGIfExpressionRule());
              					}
              					set(
              						current,
              						"condition",
              						lv_condition_2_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,52,FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getGIfExpressionAccess().getThenKeyword_3());
              		
            }
            // InternalMCL.g:2972:3: ( (lv_thenExpression_4_0= ruleGExpression ) )
            // InternalMCL.g:2973:4: (lv_thenExpression_4_0= ruleGExpression )
            {
            // InternalMCL.g:2973:4: (lv_thenExpression_4_0= ruleGExpression )
            // InternalMCL.g:2974:5: lv_thenExpression_4_0= ruleGExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGIfExpressionAccess().getThenExpressionGExpressionParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_48);
            lv_thenExpression_4_0=ruleGExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getGIfExpressionRule());
              					}
              					set(
              						current,
              						"thenExpression",
              						lv_thenExpression_4_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_5=(Token)match(input,53,FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getGIfExpressionAccess().getElseKeyword_5());
              		
            }
            // InternalMCL.g:2995:3: ( (lv_elseExpression_6_0= ruleGExpression ) )
            // InternalMCL.g:2996:4: (lv_elseExpression_6_0= ruleGExpression )
            {
            // InternalMCL.g:2996:4: (lv_elseExpression_6_0= ruleGExpression )
            // InternalMCL.g:2997:5: lv_elseExpression_6_0= ruleGExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGIfExpressionAccess().getElseExpressionGExpressionParserRuleCall_6_0());
              				
            }
            pushFollow(FOLLOW_49);
            lv_elseExpression_6_0=ruleGExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getGIfExpressionRule());
              					}
              					set(
              						current,
              						"elseExpression",
              						lv_elseExpression_6_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_7=(Token)match(input,54,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_7, grammarAccess.getGIfExpressionAccess().getEndifKeyword_7());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGIfExpression"


    // $ANTLR start "entryRuleGBraceExpression"
    // InternalMCL.g:3022:1: entryRuleGBraceExpression returns [EObject current=null] : iv_ruleGBraceExpression= ruleGBraceExpression EOF ;
    public final EObject entryRuleGBraceExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGBraceExpression = null;


        try {
            // InternalMCL.g:3022:57: (iv_ruleGBraceExpression= ruleGBraceExpression EOF )
            // InternalMCL.g:3023:2: iv_ruleGBraceExpression= ruleGBraceExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGBraceExpressionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleGBraceExpression=ruleGBraceExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGBraceExpression; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGBraceExpression"


    // $ANTLR start "ruleGBraceExpression"
    // InternalMCL.g:3029:1: ruleGBraceExpression returns [EObject current=null] : ( () otherlv_1= '(' ( (lv_innerExpression_2_0= ruleGExpression ) ) otherlv_3= ')' ) ;
    public final EObject ruleGBraceExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_innerExpression_2_0 = null;



        	enterRule();

        try {
            // InternalMCL.g:3035:2: ( ( () otherlv_1= '(' ( (lv_innerExpression_2_0= ruleGExpression ) ) otherlv_3= ')' ) )
            // InternalMCL.g:3036:2: ( () otherlv_1= '(' ( (lv_innerExpression_2_0= ruleGExpression ) ) otherlv_3= ')' )
            {
            // InternalMCL.g:3036:2: ( () otherlv_1= '(' ( (lv_innerExpression_2_0= ruleGExpression ) ) otherlv_3= ')' )
            // InternalMCL.g:3037:3: () otherlv_1= '(' ( (lv_innerExpression_2_0= ruleGExpression ) ) otherlv_3= ')'
            {
            // InternalMCL.g:3037:3: ()
            // InternalMCL.g:3038:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getGBraceExpressionAccess().getGBraceExpressionAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,14,FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getGBraceExpressionAccess().getLeftParenthesisKeyword_1());
              		
            }
            // InternalMCL.g:3048:3: ( (lv_innerExpression_2_0= ruleGExpression ) )
            // InternalMCL.g:3049:4: (lv_innerExpression_2_0= ruleGExpression )
            {
            // InternalMCL.g:3049:4: (lv_innerExpression_2_0= ruleGExpression )
            // InternalMCL.g:3050:5: lv_innerExpression_2_0= ruleGExpression
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getGBraceExpressionAccess().getInnerExpressionGExpressionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_9);
            lv_innerExpression_2_0=ruleGExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getGBraceExpressionRule());
              					}
              					set(
              						current,
              						"innerExpression",
              						lv_innerExpression_2_0,
              						"org.eclipse.gemoc.gexpressions.xtext.GExpressions.GExpression");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getGBraceExpressionAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGBraceExpression"


    // $ANTLR start "entryRuleNavigationOperator"
    // InternalMCL.g:3075:1: entryRuleNavigationOperator returns [String current=null] : iv_ruleNavigationOperator= ruleNavigationOperator EOF ;
    public final String entryRuleNavigationOperator() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNavigationOperator = null;


        try {
            // InternalMCL.g:3075:58: (iv_ruleNavigationOperator= ruleNavigationOperator EOF )
            // InternalMCL.g:3076:2: iv_ruleNavigationOperator= ruleNavigationOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNavigationOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNavigationOperator=ruleNavigationOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNavigationOperator.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNavigationOperator"


    // $ANTLR start "ruleNavigationOperator"
    // InternalMCL.g:3082:1: ruleNavigationOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '.' | kw= '->' ) ;
    public final AntlrDatatypeRuleToken ruleNavigationOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMCL.g:3088:2: ( (kw= '.' | kw= '->' ) )
            // InternalMCL.g:3089:2: (kw= '.' | kw= '->' )
            {
            // InternalMCL.g:3089:2: (kw= '.' | kw= '->' )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==55) ) {
                alt33=1;
            }
            else if ( (LA33_0==46) ) {
                alt33=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalMCL.g:3090:3: kw= '.'
                    {
                    kw=(Token)match(input,55,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getNavigationOperatorAccess().getFullStopKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalMCL.g:3096:3: kw= '->'
                    {
                    kw=(Token)match(input,46,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getNavigationOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNavigationOperator"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalMCL.g:3105:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalMCL.g:3105:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalMCL.g:3106:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalMCL.g:3112:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalMCL.g:3118:2: ( (this_ID_0= RULE_ID ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )* ) )
            // InternalMCL.g:3119:2: (this_ID_0= RULE_ID ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )* )
            {
            // InternalMCL.g:3119:2: (this_ID_0= RULE_ID ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )* )
            // InternalMCL.g:3120:3: this_ID_0= RULE_ID ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
              		
            }
            // InternalMCL.g:3127:3: ( ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==55) ) {
                    int LA34_2 = input.LA(2);

                    if ( (LA34_2==RULE_ID) ) {
                        int LA34_3 = input.LA(3);

                        if ( (synpred1_InternalMCL()) ) {
                            alt34=1;
                        }


                    }


                }


                switch (alt34) {
            	case 1 :
            	    // InternalMCL.g:3128:4: ( ( '.' )=>kw= '.' ) this_ID_2= RULE_ID
            	    {
            	    // InternalMCL.g:3128:4: ( ( '.' )=>kw= '.' )
            	    // InternalMCL.g:3129:5: ( '.' )=>kw= '.'
            	    {
            	    kw=(Token)match(input,55,FOLLOW_5); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					current.merge(kw);
            	      					newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      				
            	    }

            	    }

            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_50); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "ruleGAndOperator"
    // InternalMCL.g:3148:1: ruleGAndOperator returns [Enumerator current=null] : (enumLiteral_0= 'and' ) ;
    public final Enumerator ruleGAndOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalMCL.g:3154:2: ( (enumLiteral_0= 'and' ) )
            // InternalMCL.g:3155:2: (enumLiteral_0= 'and' )
            {
            // InternalMCL.g:3155:2: (enumLiteral_0= 'and' )
            // InternalMCL.g:3156:3: enumLiteral_0= 'and'
            {
            enumLiteral_0=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getGAndOperatorAccess().getANDEnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getGAndOperatorAccess().getANDEnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGAndOperator"


    // $ANTLR start "ruleGXorOperator"
    // InternalMCL.g:3165:1: ruleGXorOperator returns [Enumerator current=null] : (enumLiteral_0= 'xor' ) ;
    public final Enumerator ruleGXorOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalMCL.g:3171:2: ( (enumLiteral_0= 'xor' ) )
            // InternalMCL.g:3172:2: (enumLiteral_0= 'xor' )
            {
            // InternalMCL.g:3172:2: (enumLiteral_0= 'xor' )
            // InternalMCL.g:3173:3: enumLiteral_0= 'xor'
            {
            enumLiteral_0=(Token)match(input,56,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getGXorOperatorAccess().getXOREnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getGXorOperatorAccess().getXOREnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGXorOperator"


    // $ANTLR start "ruleGOrOperator"
    // InternalMCL.g:3182:1: ruleGOrOperator returns [Enumerator current=null] : (enumLiteral_0= 'or' ) ;
    public final Enumerator ruleGOrOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalMCL.g:3188:2: ( (enumLiteral_0= 'or' ) )
            // InternalMCL.g:3189:2: (enumLiteral_0= 'or' )
            {
            // InternalMCL.g:3189:2: (enumLiteral_0= 'or' )
            // InternalMCL.g:3190:3: enumLiteral_0= 'or'
            {
            enumLiteral_0=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = grammarAccess.getGOrOperatorAccess().getOREnumLiteralDeclaration().getEnumLiteral().getInstance();
              			newLeafNode(enumLiteral_0, grammarAccess.getGOrOperatorAccess().getOREnumLiteralDeclaration());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGOrOperator"


    // $ANTLR start "ruleGEqualityOperator"
    // InternalMCL.g:3199:1: ruleGEqualityOperator returns [Enumerator current=null] : ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) ) ;
    public final Enumerator ruleGEqualityOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMCL.g:3205:2: ( ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) ) )
            // InternalMCL.g:3206:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) )
            {
            // InternalMCL.g:3206:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '<>' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==47) ) {
                alt35=1;
            }
            else if ( (LA35_0==57) ) {
                alt35=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalMCL.g:3207:3: (enumLiteral_0= '=' )
                    {
                    // InternalMCL.g:3207:3: (enumLiteral_0= '=' )
                    // InternalMCL.g:3208:4: enumLiteral_0= '='
                    {
                    enumLiteral_0=(Token)match(input,47,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGEqualityOperatorAccess().getEQUALEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getGEqualityOperatorAccess().getEQUALEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:3215:3: (enumLiteral_1= '<>' )
                    {
                    // InternalMCL.g:3215:3: (enumLiteral_1= '<>' )
                    // InternalMCL.g:3216:4: enumLiteral_1= '<>'
                    {
                    enumLiteral_1=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGEqualityOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getGEqualityOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGEqualityOperator"


    // $ANTLR start "ruleGRelationOperator"
    // InternalMCL.g:3226:1: ruleGRelationOperator returns [Enumerator current=null] : ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '>=' ) ) ;
    public final Enumerator ruleGRelationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalMCL.g:3232:2: ( ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '>=' ) ) )
            // InternalMCL.g:3233:2: ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '>=' ) )
            {
            // InternalMCL.g:3233:2: ( (enumLiteral_0= '<' ) | (enumLiteral_1= '>' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '>=' ) )
            int alt36=4;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt36=1;
                }
                break;
            case 40:
                {
                alt36=2;
                }
                break;
            case 58:
                {
                alt36=3;
                }
                break;
            case 59:
                {
                alt36=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalMCL.g:3234:3: (enumLiteral_0= '<' )
                    {
                    // InternalMCL.g:3234:3: (enumLiteral_0= '<' )
                    // InternalMCL.g:3235:4: enumLiteral_0= '<'
                    {
                    enumLiteral_0=(Token)match(input,41,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGRelationOperatorAccess().getLESSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getGRelationOperatorAccess().getLESSEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:3242:3: (enumLiteral_1= '>' )
                    {
                    // InternalMCL.g:3242:3: (enumLiteral_1= '>' )
                    // InternalMCL.g:3243:4: enumLiteral_1= '>'
                    {
                    enumLiteral_1=(Token)match(input,40,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGRelationOperatorAccess().getGREATEREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getGRelationOperatorAccess().getGREATEREnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalMCL.g:3250:3: (enumLiteral_2= '<=' )
                    {
                    // InternalMCL.g:3250:3: (enumLiteral_2= '<=' )
                    // InternalMCL.g:3251:4: enumLiteral_2= '<='
                    {
                    enumLiteral_2=(Token)match(input,58,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGRelationOperatorAccess().getLESSEQUALEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_2, grammarAccess.getGRelationOperatorAccess().getLESSEQUALEnumLiteralDeclaration_2());
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalMCL.g:3258:3: (enumLiteral_3= '>=' )
                    {
                    // InternalMCL.g:3258:3: (enumLiteral_3= '>=' )
                    // InternalMCL.g:3259:4: enumLiteral_3= '>='
                    {
                    enumLiteral_3=(Token)match(input,59,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGRelationOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_3, grammarAccess.getGRelationOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGRelationOperator"


    // $ANTLR start "ruleGAdditionOperator"
    // InternalMCL.g:3269:1: ruleGAdditionOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleGAdditionOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMCL.g:3275:2: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) )
            // InternalMCL.g:3276:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            {
            // InternalMCL.g:3276:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==45) ) {
                alt37=1;
            }
            else if ( (LA37_0==42) ) {
                alt37=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }
            switch (alt37) {
                case 1 :
                    // InternalMCL.g:3277:3: (enumLiteral_0= '+' )
                    {
                    // InternalMCL.g:3277:3: (enumLiteral_0= '+' )
                    // InternalMCL.g:3278:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,45,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGAdditionOperatorAccess().getADDITIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getGAdditionOperatorAccess().getADDITIONEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:3285:3: (enumLiteral_1= '-' )
                    {
                    // InternalMCL.g:3285:3: (enumLiteral_1= '-' )
                    // InternalMCL.g:3286:4: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,42,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGAdditionOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getGAdditionOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGAdditionOperator"


    // $ANTLR start "ruleGMultiplicationOperator"
    // InternalMCL.g:3296:1: ruleGMultiplicationOperator returns [Enumerator current=null] : ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) ;
    public final Enumerator ruleGMultiplicationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMCL.g:3302:2: ( ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) )
            // InternalMCL.g:3303:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            {
            // InternalMCL.g:3303:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==60) ) {
                alt38=1;
            }
            else if ( (LA38_0==61) ) {
                alt38=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }
            switch (alt38) {
                case 1 :
                    // InternalMCL.g:3304:3: (enumLiteral_0= '*' )
                    {
                    // InternalMCL.g:3304:3: (enumLiteral_0= '*' )
                    // InternalMCL.g:3305:4: enumLiteral_0= '*'
                    {
                    enumLiteral_0=(Token)match(input,60,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGMultiplicationOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getGMultiplicationOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:3312:3: (enumLiteral_1= '/' )
                    {
                    // InternalMCL.g:3312:3: (enumLiteral_1= '/' )
                    // InternalMCL.g:3313:4: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,61,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGMultiplicationOperatorAccess().getDIVISIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getGMultiplicationOperatorAccess().getDIVISIONEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGMultiplicationOperator"


    // $ANTLR start "ruleGNegationOperator"
    // InternalMCL.g:3323:1: ruleGNegationOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '~' ) ) ;
    public final Enumerator ruleGNegationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalMCL.g:3329:2: ( ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '~' ) ) )
            // InternalMCL.g:3330:2: ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '~' ) )
            {
            // InternalMCL.g:3330:2: ( (enumLiteral_0= 'not' ) | (enumLiteral_1= '~' ) )
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==44) ) {
                alt39=1;
            }
            else if ( (LA39_0==62) ) {
                alt39=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }
            switch (alt39) {
                case 1 :
                    // InternalMCL.g:3331:3: (enumLiteral_0= 'not' )
                    {
                    // InternalMCL.g:3331:3: (enumLiteral_0= 'not' )
                    // InternalMCL.g:3332:4: enumLiteral_0= 'not'
                    {
                    enumLiteral_0=(Token)match(input,44,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGNegationOperatorAccess().getNEGATIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_0, grammarAccess.getGNegationOperatorAccess().getNEGATIONEnumLiteralDeclaration_0());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalMCL.g:3339:3: (enumLiteral_1= '~' )
                    {
                    // InternalMCL.g:3339:3: (enumLiteral_1= '~' )
                    // InternalMCL.g:3340:4: enumLiteral_1= '~'
                    {
                    enumLiteral_1=(Token)match(input,62,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = grammarAccess.getGNegationOperatorAccess().getMINUSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                      				newLeafNode(enumLiteral_1, grammarAccess.getGNegationOperatorAccess().getMINUSEnumLiteralDeclaration_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGNegationOperator"

    // $ANTLR start synpred1_InternalMCL
    public final void synpred1_InternalMCL_fragment() throws RecognitionException {   
        // InternalMCL.g:3129:5: ( '.' )
        // InternalMCL.g:3129:6: '.'
        {
        match(input,55,FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalMCL

    // Delegated rules

    public final boolean synpred1_InternalMCL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalMCL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA11 dfa11 = new DFA11(this);
    protected DFA17 dfa17 = new DFA17(this);
    protected DFA18 dfa18 = new DFA18(this);
    static final String dfa_1s = "\14\uffff";
    static final String dfa_2s = "\1\4\3\uffff\1\31\1\uffff\1\4\1\35\1\4\2\uffff\1\35";
    static final String dfa_3s = "\1\34\3\uffff\1\31\1\uffff\1\4\1\67\1\4\2\uffff\1\67";
    static final String dfa_4s = "\1\uffff\1\1\1\2\1\3\1\uffff\1\6\3\uffff\1\4\1\5\1\uffff";
    static final String dfa_5s = "\14\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\11\uffff\1\5\11\uffff\1\3\2\uffff\1\1\1\4",
            "",
            "",
            "",
            "\1\6",
            "",
            "\1\7",
            "\1\11\2\uffff\1\12\26\uffff\1\10",
            "\1\13",
            "",
            "",
            "\1\11\2\uffff\1\12\26\uffff\1\10"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA11 extends DFA {

        public DFA11(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 11;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "709:2: (this_PeriodicExpression_0= rulePeriodicExpression | this_ThresholdExpression_1= ruleThresholdExpression | this_EventTrigger_2= ruleEventTrigger | this_Updated_3= ruleUpdated | this_ReadyToRead_4= ruleReadyToRead | (otherlv_5= '(' this_Disjoint_6= ruleDisjoint otherlv_7= ')' ) )";
        }
    }
    static final String dfa_7s = "\10\uffff";
    static final String dfa_8s = "\1\uffff\1\6\5\uffff\1\6";
    static final String dfa_9s = "\1\4\1\15\2\uffff\1\4\2\uffff\1\15";
    static final String dfa_10s = "\1\54\1\67\2\uffff\1\4\2\uffff\1\67";
    static final String dfa_11s = "\2\uffff\1\2\1\3\1\uffff\1\4\1\1\1\uffff";
    static final String dfa_12s = "\10\uffff}>";
    static final String[] dfa_13s = {
            "\1\1\45\uffff\1\2\2\3",
            "\1\6\34\uffff\1\5\2\uffff\1\5\1\6\10\uffff\1\4",
            "",
            "",
            "\1\7",
            "",
            "",
            "\1\6\34\uffff\1\5\2\uffff\1\5\1\6\10\uffff\1\4"
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "1359:2: (this_PortRef_0= rulePortRef | this_UnaryMinus_1= ruleUnaryMinus | this_Not_2= ruleNot | this_BinaryExpression_3= ruleBinaryExpression )";
        }
    }
    static final String dfa_14s = "\6\uffff";
    static final String dfa_15s = "\1\4\1\52\1\4\2\uffff\1\52";
    static final String dfa_16s = "\1\4\1\67\1\4\2\uffff\1\67";
    static final String dfa_17s = "\3\uffff\1\2\1\1\1\uffff";
    static final String dfa_18s = "\6\uffff}>";
    static final String[] dfa_19s = {
            "\1\1",
            "\1\4\2\uffff\1\3\11\uffff\1\2",
            "\1\5",
            "",
            "",
            "\1\4\2\uffff\1\3\11\uffff\1\2"
    };

    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final char[] dfa_16 = DFA.unpackEncodedStringToUnsignedChars(dfa_16s);
    static final short[] dfa_17 = DFA.unpackEncodedString(dfa_17s);
    static final short[] dfa_18 = DFA.unpackEncodedString(dfa_18s);
    static final short[][] dfa_19 = unpackEncodedStringArray(dfa_19s);

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = dfa_14;
            this.eof = dfa_14;
            this.min = dfa_15;
            this.max = dfa_16;
            this.accept = dfa_17;
            this.special = dfa_18;
            this.transition = dfa_19;
        }
        public String getDescription() {
            return "1413:2: (this_BinaryMinus_0= ruleBinaryMinus | this_BinaryPlus_1= ruleBinaryPlus )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000202002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000044000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000019004010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00001C0000000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000030000000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x400C1000000041F0L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0100000000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0200800000000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0C00030000000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000240000000002L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x3000000000000002L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x000C0000000041F0L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0080400000000002L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0080000000000002L});

}