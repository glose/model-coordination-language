package fr.inria.glose.mcl.generator;

import com.google.common.base.Objects;
import fr.inria.glose.mbilang.DataType;
import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mcl.generator.Language;

@SuppressWarnings("all")
public class Utils {
  public static String getDataTypeAsString(final DataType type, final Language lang) {
    String dataType = "";
    boolean _equals = Objects.equal(lang, Language.JAVA);
    if (_equals) {
      if (type != null) {
        switch (type) {
          case BOOLEAN:
            dataType = "boolean";
            break;
          case ENUMERATION:
            break;
          case INTEGER:
            dataType = "Integer";
            break;
          case REAL:
            dataType = "double";
            break;
          case STRING:
            dataType = "String";
            break;
          default:
            break;
        }
      }
    }
    return dataType;
  }
  
  public static String getVariableName(final Port port) {
    String internalVariableName = port.getName();
    int _compareTo = port.getVariableName().compareTo("");
    boolean _notEquals = (_compareTo != 0);
    if (_notEquals) {
      internalVariableName = port.getVariableName();
    }
    return internalVariableName;
  }
}
