package fr.inria.glose.mcl.generator;

import com.google.common.base.Objects;
import fr.inria.glose.mbilang.DataType;
import fr.inria.glose.mbilang.Interface;
import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mbilang.PortDirection;
import fr.inria.glose.mbilang.Updated;
import fr.inria.glose.mcl.Connector;
import fr.inria.glose.mcl.EventTrigger;
import fr.inria.glose.mcl.PeriodicExpression;
import fr.inria.glose.mcl.ReadyToRead;
import fr.inria.glose.mcl.TCExpression;
import fr.inria.glose.mcl.generator.Language;
import fr.inria.glose.mcl.generator.Utils;
import fr.inria.glose.mcl.impl.PeriodicExpressionImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class VirtualPort {
  public enum PortType {
    FOLLOWER,
    
    INITIATOR,
    
    FOLLOWERDETERMINISTIC;
  }
  
  public Port port;
  
  public Connector c;
  
  public String URL;
  
  public String SyncURL;
  
  public String ID;
  
  public Interface i;
  
  public VirtualPort.PortType portType;
  
  public Integer plotterIndex;
  
  public Port connectedPort;
  
  public VirtualPort(final Port port, final Port connectedPort, final Connector c) {
    this.port = port;
    this.c = c;
    EObject _eContainer = port.eContainer();
    this.i = ((Interface) _eContainer);
    this.connectedPort = connectedPort;
    String _name = port.getName();
    String _plus = (_name + "_");
    String _name_1 = c.getName();
    String _plus_1 = (_plus + _name_1);
    this.ID = _plus_1;
    this.retrievePortType(c);
  }
  
  public VirtualPort(final Port port, final Interface i, final Connector c) {
    this.port = port;
    this.c = c;
    this.i = i;
    String _name = port.getName();
    String _plus = (_name + "_");
    String _name_1 = c.getName();
    String _plus_1 = (_plus + _name_1);
    this.ID = _plus_1;
    this.retrievePortType(c);
  }
  
  public String getConnectedPortID() {
    String _name = this.connectedPort.getName();
    String _plus = (_name + "_");
    String _name_1 = this.c.getName();
    return (_plus + _name_1);
  }
  
  public String getVariableName() {
    String internalVariableName = this.port.getName();
    int _compareTo = this.port.getVariableName().compareTo("");
    boolean _notEquals = (_compareTo != 0);
    if (_notEquals) {
      internalVariableName = this.port.getVariableName();
    }
    return internalVariableName;
  }
  
  public String getRTDReference() {
    String _rTD = this.port.getRTD();
    boolean _notEquals = (!Objects.equal(_rTD, null));
    if (_notEquals) {
      String _variableName = this.getVariableName();
      String _plus = (_variableName + "::");
      String _rTD_1 = this.port.getRTD();
      return (_plus + _rTD_1);
    } else {
      return this.getVariableName();
    }
  }
  
  @Override
  public boolean equals(final Object arg0) {
    if ((arg0 instanceof VirtualPort)) {
      return (this.port.equals(((VirtualPort) arg0).port) && this.c.equals(((VirtualPort) arg0).c));
    }
    return (arg0 instanceof VirtualPort);
  }
  
  @Override
  public int hashCode() {
    int _hashCode = this.port.getName().hashCode();
    int _hashCode_1 = this.c.getName().hashCode();
    return (_hashCode + _hashCode_1);
  }
  
  public boolean isTypeAdapterRequired() {
    DataType _type = this.port.getType();
    DataType _type_1 = this.connectedPort.getType();
    return (!Objects.equal(_type, _type_1));
  }
  
  /**
   * VirtualPort vp is the port linked to the currentAction
   */
  public String getAdapterFor(final VirtualPort vp) {
    return this.getAdapterFor(vp.port);
  }
  
  public String getDataType(final Language lang) {
    return Utils.getDataTypeAsString(this.port.getType(), lang);
  }
  
  /**
   * TODO: Modify this method to be target language independent
   */
  public String getAdapterFor(final Port p) {
    Object _switchResult = null;
    DataType _type = this.port.getType();
    if (_type != null) {
      switch (_type) {
        case BOOLEAN:
          Object _switchResult_1 = null;
          DataType _type_1 = p.getType();
          if (_type_1 != null) {
            switch (_type_1) {
              case ENUMERATION:
                _switchResult_1 = null;
                break;
              case INTEGER:
                StringConcatenation _builder = new StringConcatenation();
                _builder.append("Boolean value = (");
                String _dataTypeAsString = Utils.getDataTypeAsString(p.getType(), Language.JAVA);
                _builder.append(_dataTypeAsString);
                _builder.append(") currentAction.getValue() > 0 ;");
                _builder.newLineIfNotEmpty();
                return _builder.toString();
              case REAL:
                _switchResult_1 = null;
                break;
              case STRING:
                _switchResult_1 = null;
                break;
              default:
                break;
            }
          }
          _switchResult = _switchResult_1;
          break;
        case ENUMERATION:
          _switchResult = null;
          break;
        case INTEGER:
          Object _switchResult_2 = null;
          DataType _type_2 = p.getType();
          if (_type_2 != null) {
            switch (_type_2) {
              case BOOLEAN:
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("Integer value = (Boolean) currentAction.getValue() ? 1 : 0;");
                _builder_1.newLine();
                return _builder_1.toString();
              case ENUMERATION:
                _switchResult_2 = null;
                break;
              case INTEGER:
                _switchResult_2 = null;
                break;
              case REAL:
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("BigDecimal temp = new BigDecimal(currentAction.getValue().toString());");
                _builder_2.newLine();
                _builder_2.append("int value = temp.intValue();");
                _builder_2.newLine();
                return _builder_2.toString();
              case STRING:
                _switchResult_2 = null;
                break;
              default:
                break;
            }
          }
          _switchResult = _switchResult_2;
          break;
        case REAL:
          _switchResult = null;
          break;
        case STRING:
          _switchResult = null;
          break;
        default:
          break;
      }
    }
    return ((String)_switchResult);
  }
  
  /**
   * Right now, the definition of the type is based on the first occurrence of Assignment
   */
  public VirtualPort.PortType retrievePortType(final Connector c) {
    VirtualPort.PortType _xblockexpression = null;
    {
      Port sourcePort = c.getFrom().getRefPort();
      Port targetPort = c.getTo().getRefPort();
      TCExpression _trigger = c.getTrigger();
      if ((_trigger instanceof ReadyToRead)) {
        boolean _equals = sourcePort.equals(this.port);
        if (_equals) {
          this.portType = VirtualPort.PortType.FOLLOWER;
        } else {
          boolean _equals_1 = targetPort.equals(this.port);
          if (_equals_1) {
            this.portType = VirtualPort.PortType.INITIATOR;
          }
        }
      }
      if (((c.getTrigger() instanceof Updated) || (c.getTrigger() instanceof EventTrigger))) {
        boolean _equals_2 = sourcePort.equals(this.port);
        if (_equals_2) {
          this.portType = VirtualPort.PortType.INITIATOR;
        } else {
          boolean _equals_3 = targetPort.equals(this.port);
          if (_equals_3) {
            this.portType = VirtualPort.PortType.FOLLOWER;
          }
        }
      }
      VirtualPort.PortType _xifexpression = null;
      TCExpression _trigger_1 = c.getTrigger();
      if ((_trigger_1 instanceof PeriodicExpression)) {
        VirtualPort.PortType _xifexpression_1 = null;
        boolean _equals_4 = sourcePort.equals(this.port);
        if (_equals_4) {
          _xifexpression_1 = this.portType = VirtualPort.PortType.INITIATOR;
        } else {
          VirtualPort.PortType _xifexpression_2 = null;
          boolean _equals_5 = targetPort.equals(this.port);
          if (_equals_5) {
            _xifexpression_2 = this.portType = VirtualPort.PortType.FOLLOWERDETERMINISTIC;
          }
          _xifexpression_1 = _xifexpression_2;
        }
        _xifexpression = _xifexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public Object compileInteraction() {
    return null;
  }
  
  public String compileDeclaration() {
    try {
      EObject _eContainer = this.connectedPort.eContainer();
      String targetPortInterfaceName = ((Interface) _eContainer).getName();
      String initValue = "0";
      String _initValue = this.port.getInitValue();
      boolean _tripleNotEquals = (_initValue != null);
      if (_tripleNotEquals) {
        initValue = this.port.getInitValue();
      }
      final VirtualPort.PortType portType = this.portType;
      if (portType != null) {
        switch (portType) {
          case FOLLOWER:
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("public Port ");
            _builder.append(this.ID);
            _builder.append(" = new FollowerPort(\"");
            _builder.append(this.ID);
            _builder.append("\", \"");
            String _variableName = this.getVariableName();
            _builder.append(_variableName);
            _builder.append("\", \"");
            _builder.append(targetPortInterfaceName);
            _builder.append("\", ");
            _builder.append(initValue);
            _builder.append(");");
            return _builder.toString();
          case INITIATOR:
            PortDirection _direction = this.port.getDirection();
            boolean _equals = Objects.equal(_direction, PortDirection.INPUT);
            if (_equals) {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("public Port ");
              _builder_1.append(this.ID);
              _builder_1.append(" = new InitiatorPort(\"");
              _builder_1.append(this.ID);
              _builder_1.append("\", \"");
              String _variableName_1 = this.getVariableName();
              _builder_1.append(_variableName_1);
              _builder_1.append("\", \"");
              _builder_1.append(targetPortInterfaceName);
              _builder_1.append("\", ");
              _builder_1.append(initValue);
              _builder_1.append("); // INPUT ");
              return _builder_1.toString();
            } else {
              StringConcatenation _builder_2 = new StringConcatenation();
              _builder_2.append("public Port ");
              _builder_2.append(this.ID);
              _builder_2.append(" = new InitiatorPort(\"");
              _builder_2.append(this.ID);
              _builder_2.append("\", \"");
              String _variableName_2 = this.getVariableName();
              _builder_2.append(_variableName_2);
              _builder_2.append("\", \"");
              _builder_2.append(targetPortInterfaceName);
              _builder_2.append("\", ");
              _builder_2.append(initValue);
              _builder_2.append("); // OUTPUT");
              return _builder_2.toString();
            }
          case FOLLOWERDETERMINISTIC:
            TCExpression _trigger = this.c.getTrigger();
            PeriodicExpressionImpl periodicExpressionImpl = ((PeriodicExpressionImpl) _trigger);
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("public FollowerDeterministicPort ");
            _builder_3.append(this.ID);
            _builder_3.append(" = new FollowerDeterministicPort(\"");
            _builder_3.append(this.ID);
            _builder_3.append("\", \"");
            String _variableName_3 = this.getVariableName();
            _builder_3.append(_variableName_3);
            _builder_3.append("\", \"");
            _builder_3.append(targetPortInterfaceName);
            _builder_3.append("\", 0, new TemporalPredicate(");
            int _value = periodicExpressionImpl.getValue();
            _builder_3.append(_value);
            _builder_3.append("));");
            return _builder_3.toString();
          default:
            throw new Exception((("No port type defined on port " + this.ID) + ". A type is mandatory for every port."));
        }
      } else {
        throw new Exception((("No port type defined on port " + this.ID) + ". A type is mandatory for every port."));
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String compileMonitoredDeclaration() {
    boolean _isMonitored = this.port.isMonitored();
    if (_isMonitored) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("PrintWriter writer4");
      String _lowerCase = this.ID.toLowerCase();
      _builder.append(_lowerCase);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      return _builder.toString();
    }
    return null;
  }
  
  public String compileMonitoredInitialization() {
    boolean _isMonitored = this.port.isMonitored();
    if (_isMonitored) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("try {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("writer4");
      String _lowerCase = this.ID.toLowerCase();
      _builder.append(_lowerCase, "\t");
      _builder.append(" = new PrintWriter(new File(\"");
      String _name = this.i.getName();
      _builder.append(_name, "\t");
      _builder.append("_");
      String _lowerCase_1 = this.ID.toLowerCase();
      _builder.append(_lowerCase_1, "\t");
      _builder.append(".csv\"));");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("writer4");
      String _lowerCase_2 = this.ID.toLowerCase();
      _builder.append(_lowerCase_2, "\t");
      _builder.append(".write(\"time,");
      String _name_1 = this.port.getName();
      _builder.append(_name_1, "\t");
      _builder.append("\\n\");");
      _builder.newLineIfNotEmpty();
      _builder.append("} catch (FileNotFoundException e) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("e.printStackTrace();");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      return _builder.toString();
    }
    return null;
  }
  
  public Object compileEventAssignment() {
    return null;
  }
}
