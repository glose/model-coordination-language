package fr.inria.glose.mcl;

import com.google.inject.Inject;
import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mcl.ImportInterfaceStatement;
import fr.inria.glose.mcl.MCSpecification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.scoping.impl.ImportUriResolver;
import org.eclipse.xtext.util.IAcceptor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class MCLResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {
  public static final String INCLUDES = "importedMBI";
  
  @Inject
  private ImportUriResolver uriResolver;
  
  @Override
  public boolean createEObjectDescriptions(final EObject eObject, final IAcceptor<IEObjectDescription> acceptor) {
    boolean _xblockexpression = false;
    {
      if ((eObject instanceof MCSpecification)) {
        this.createEObjectDescriptionForMCSpecification(((MCSpecification)eObject), acceptor);
        return true;
      }
      boolean _xifexpression = false;
      if ((eObject instanceof Port)) {
        this.createEObjectDescriptionForPort(((Port)eObject), acceptor);
        return true;
      } else {
        _xifexpression = super.createEObjectDescriptions(eObject, acceptor);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public void createEObjectDescriptionForMCSpecification(final MCSpecification model, final IAcceptor<IEObjectDescription> acceptor) {
    final ArrayList<Object> uris = CollectionLiterals.<Object>newArrayList();
    final Consumer<ImportInterfaceStatement> _function = (ImportInterfaceStatement it) -> {
      uris.add(this.uriResolver.apply(it));
    };
    model.getImportedMBI().forEach(_function);
    final HashMap<String, String> userData = new HashMap<String, String>();
    userData.put(MCLResourceDescriptionStrategy.INCLUDES, IterableExtensions.join(uris, ","));
    acceptor.accept(EObjectDescription.create(QualifiedName.create(model.eResource().getURI().toString()), model, userData));
  }
  
  public void createEObjectDescriptionForPort(final Port model, final IAcceptor<IEObjectDescription> acceptor) {
    final HashMap<String, String> userData = new HashMap<String, String>();
    userData.put("direction", model.getDirection().toString());
    acceptor.accept(EObjectDescription.create(QualifiedName.create(model.eResource().getURI().toString()), model, userData));
  }
}
