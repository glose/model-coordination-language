package fr.inria.glose.mcl

import com.google.inject.Inject
import java.util.HashMap
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy
import org.eclipse.xtext.scoping.impl.ImportUriResolver
import org.eclipse.xtext.util.IAcceptor
import org.eclipse.emf.ecore.EObject
import fr.inria.glose.mbilang.Port

class MCLResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {
	public static final String INCLUDES = "importedMBI"
	@Inject
	ImportUriResolver uriResolver

	override createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		if(eObject instanceof MCSpecification) {
			this.createEObjectDescriptionForMCSpecification(eObject, acceptor)
			return true
		}
		if (eObject instanceof Port) {
			this.createEObjectDescriptionForPort(eObject, acceptor)
			return true
		}
		else {
			super.createEObjectDescriptions(eObject, acceptor)
		}
	}

	def void createEObjectDescriptionForMCSpecification(MCSpecification model, IAcceptor<IEObjectDescription> acceptor) {
		val uris = newArrayList()
		model.importedMBI.forEach[uris.add(uriResolver.apply(it))]
		
		val userData = new HashMap<String,String>
		userData.put(INCLUDES, uris.join(","))
		acceptor.accept(EObjectDescription.create(QualifiedName.create(model.eResource.URI.toString), model, userData))
	}
	
	def void createEObjectDescriptionForPort(Port model, IAcceptor<IEObjectDescription> acceptor) {	
		val userData = new HashMap<String,String>
		userData.put("direction", model.direction.toString)
		acceptor.accept(EObjectDescription.create(QualifiedName.create(model.eResource.URI.toString), model, userData))
	}
}