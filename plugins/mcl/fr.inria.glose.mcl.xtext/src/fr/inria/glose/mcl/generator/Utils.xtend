package fr.inria.glose.mcl.generator

import fr.inria.glose.mbilang.DataType
import fr.inria.glose.mbilang.Port

enum Language {
	JAVA
}

class Utils {
	static def getDataTypeAsString(DataType type, Language lang) {
		var dataType = ""
		if (lang == Language.JAVA) {
			switch (type) {
				case BOOLEAN: {
					dataType = "boolean"
				}
				case ENUMERATION: {
				}
				case INTEGER: {
					dataType = "Integer"
				}
				case REAL: {
					dataType = "double"
				}
				case STRING: {
					dataType = "String"
				}
			}	
		}
		// Else C/C++, Python, ...
		return dataType
	}
	static def getVariableName(Port port){
		// internalVariableName can be or the variable name specifed in the MBI or the name of the port
		var internalVariableName = port.name
		if (port.variableName.compareTo("") != 0) {
			internalVariableName = port.variableName
		}
		return internalVariableName
	}
}