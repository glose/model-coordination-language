package fr.inria.glose.mcl.generator;

import fr.inria.glose.mbilang.Port
import java.util.ArrayList
import fr.inria.glose.mbilang.Interface
import fr.inria.glose.mcl.Connector
import fr.inria.glose.mbilang.Updated
import fr.inria.glose.mcl.PeriodicExpression
import fr.inria.glose.mbilang.Triggered
import fr.inria.glose.mcl.TriggeringCondition
import fr.inria.glose.mcl.ReadyToRead
import fr.inria.glose.mcl.AssignmentExpression
import fr.inria.glose.mcl.impl.AssignmentExpressionImpl
import fr.inria.glose.mcl.Assignment
import fr.inria.glose.mcl.PortRef
import fr.inria.glose.mbilang.PortDirection
import fr.inria.glose.mcl.impl.PeriodicExpressionImpl
import org.eclipse.xtext.EcoreUtil2
import fr.inria.glose.mcl.EventTrigger

class VirtualPort {
	enum PortType {
		FOLLOWER,
		INITIATOR,
		FOLLOWERDETERMINISTIC
	}

	public Port port
	public Connector c;
//	public ArrayList<VirtualPort> connectedPorts = new ArrayList<VirtualPort>();
	public String URL;
	public String SyncURL;
	public String ID;
	public Interface i;
	public PortType portType
	public Integer plotterIndex
	public Port connectedPort
		
	new(Port port, Port connectedPort, Connector c) {
		this.port =port
		this.c = c
		this.i = port.eContainer as Interface
		this.connectedPort = connectedPort
		ID = port.name + "_" + c.name
		retrievePortType(c)
	}
	
	new(Port port, Interface i, Connector c) {
		this.port = port
		this.c = c
		this.i = i
		ID = port.name + "_" + c.name
		retrievePortType(c)
	}
	
//	new (Port port, Interface i) {
//		this.port = port
//		this.i = i
//		ID = port.name + "_" + c.name
//	}
	
	def getConnectedPortID() {
		return connectedPort.name + "_" + c.name
	}
	
	def getVariableName() {
		// internalVariableName can be or the variable name specifed in the MBI or the name of the port
		var internalVariableName = port.name
		if (port.variableName.compareTo("") != 0) {
			internalVariableName = port.variableName
		}
		return internalVariableName
	}
	
	def getRTDReference() {
		if (port.RTD != null)
			return variableName + "::"+ port.RTD
		else
			return variableName
	}

	override equals(Object arg0) {
		if (arg0 instanceof VirtualPort) {
			return port.equals((arg0 as VirtualPort).port) && c.equals((arg0 as VirtualPort).c)
		}
		return arg0 instanceof VirtualPort
	}
	
	override hashCode() {
		return port.name.hashCode + c.name.hashCode
	}

	def isTypeAdapterRequired(){
		return port.type != connectedPort.type
	}
	
	/*
	 * VirtualPort vp is the port linked to the currentAction
	 */
	def getAdapterFor(VirtualPort vp) {
		getAdapterFor(vp.port)
	}
	def getDataType(Language lang) {
		return Utils.getDataTypeAsString(port.type, lang)
	}
	/*
	 * TODO: Modify this method to be target language independent
	 */
	def getAdapterFor(Port p) {
		// This port data type
		switch(port.type) {
			case BOOLEAN: {
				// Connected port data type
				switch (p.type) {
					case ENUMERATION: {
					}
					case INTEGER: {
						return
						'''
						Boolean value = («Utils.getDataTypeAsString(p.type, Language.JAVA)») currentAction.getValue() > 0 ;
						'''
					}
					case REAL: {
					}
					case STRING: {
					}			
				}
			}
			case ENUMERATION: {
			}
			case INTEGER: {
				// Connected port data type
				switch (p.type) {
					// Must return an integer
					case BOOLEAN: {
						return
						'''
						Integer value = (Boolean) currentAction.getValue() ? 1 : 0;
						'''
					}
					case ENUMERATION: {
					}
					case INTEGER: {
					}
					case REAL: {
						return
						'''
						BigDecimal temp = new BigDecimal(currentAction.getValue().toString());
						int value = temp.intValue();
						'''
					}
					case STRING: {
					}			
				}
			}
			case REAL: {
			}
			case STRING: {
			}
		}
	}

	/*
	 * Right now, the definition of the type is based on the first occurrence of Assignment
	 */
	def retrievePortType(Connector c) {
		
		var sourcePort = c.from.refPort
		var targetPort = c.to.refPort

		if (c.trigger instanceof ReadyToRead) {
			if (sourcePort.equals(port)) {
				portType = PortType.FOLLOWER
			}
			else if ( targetPort.equals(port)) {
				portType = PortType.INITIATOR
			}
		}
		
		if (c.trigger instanceof Updated || c.trigger instanceof EventTrigger) {
			if (sourcePort.equals(port)) {
				portType = PortType.INITIATOR
			}
			else if ( targetPort.equals(port)) {
				portType = PortType.FOLLOWER
			}
		}
		
		if (c.trigger instanceof PeriodicExpression) {
			if (sourcePort.equals(port)) {
				portType = PortType.INITIATOR
			}
			else if ( targetPort.equals(port)) {
				portType = PortType.FOLLOWERDETERMINISTIC
			}		
		}
		
		
			
		// TODO: improve this fix
//		for (tc : c.eAllContents.filter[e | e instanceof TriggeringCondition].toIterable) {
//			
//			var p = il.sourcePort
//			if (c.trigger instanceof IOEventRef) {
//				var trigger = c.trigger as IOEventRef;
//				if (trigger.ioevent instanceof ReadyToRead) {
//					// SourcePort is alwayes defined as OUTPUT port
//					if (il.sourcePort.equals(port)) {
//						portType = PortType.FOLLOWER
//					}
//					for (tp : il.targetPort) {
//						// TargetPorts are always defined as INPUT port
//						if (tp.equals(port)) {
//							portType = PortType.INITIATOR
//
//						}
//					}
//				} else if (trigger.ioevent instanceof Updated || trigger.ioevent instanceof Triggered) {
					// SourcePort is alwayes defined as OUTPUT port
//					if (il.sourcePort.equals(port)) {
//						portType = PortType.INITIATOR
//					}
//					for (tp : il.targetPort) {
//						// TargetPorts are always defined as INPUT port
//						if (tp.equals(port)) {
//							portType = PortType.FOLLOWER
//
//						}
//					}
//				}
//			} else if (c.trigger instanceof PeriodicExpression) {
//				if (il.sourcePort.equals(port)) {
//					portType = PortType.INITIATOR
//				}
//				for (tp : il.targetPort) {
//					// TargetPorts are always defined as INPUT port
//					if (tp.equals(port)) {
//						portType = PortType.FOLLOWERDETERMINISTIC
//
//					}
//				}
//			}
//		}
	}
	
	def compileInteraction() {
		// The interaction defines how the port is set
		
		// If the port is an output, then it's not possible to assign a value
		
		// If the port is an input then take into consideration the rightOperand of the assignment to compile the expression
	}
	
	def compileDeclaration() {
		var targetPortInterfaceName = (connectedPort.eContainer as Interface).name
		var initValue = "0"
		if (port.initValue !== null) {
			initValue = port.initValue
		}

		switch (portType) {
			case FOLLOWER: {
				return '''public Port «ID» = new FollowerPort("«ID»", "«variableName»", "«targetPortInterfaceName»", «initValue»);'''
			}
			case INITIATOR: {
//				If the port has direction Input and it's an Initiator, it means it has R2R condition defined on it
				if (port.direction == PortDirection.INPUT) {
					return '''public Port «ID» = new InitiatorPort("«ID»", "«variableName»", "«targetPortInterfaceName»", «initValue»); // INPUT '''	
				} else {
					return '''public Port «ID» = new InitiatorPort("«ID»", "«variableName»", "«targetPortInterfaceName»", «initValue»); // OUTPUT'''
				}
			}
			case FOLLOWERDETERMINISTIC: {
				var periodicExpressionImpl = c.trigger as PeriodicExpressionImpl
				return '''public FollowerDeterministicPort «ID» = new FollowerDeterministicPort("«ID»", "«variableName»", "«targetPortInterfaceName»", 0, new TemporalPredicate(«periodicExpressionImpl.value»));'''
			}
			default: {
				throw new Exception("No port type defined on port " + ID + ". A type is mandatory for every port.")
			}
		}
	}
	
	def compileMonitoredDeclaration() {
		if (port.monitored) {
			return 
			'''
			PrintWriter writer4«ID.toLowerCase»;
			'''
		}
	}
	
	def compileMonitoredInitialization() {
		if (port.monitored) {
			return
			'''
			try {
				writer4«ID.toLowerCase» = new PrintWriter(new File("«i.name»_«ID.toLowerCase».csv"));
				writer4«ID.toLowerCase».write("time,«port.name»\n");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			'''
		}
	}
	
	def compileEventAssignment() {
		
	}
}
