/*
 * generated by Xtext 2.14.0
 */
package fr.inria.glose.mcl.scoping

import fr.inria.glose.mcl.MclPackage
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class MCLScopeProvider extends AbstractMCLScopeProvider {

//	@Inject IResourceDescriptions descriptions;
	
//	@Inject IResourceDescription.Manager descriptionManager;
	
//	@Inject MCLGlobalScopeProvider globalScope;
	
	override getScope(EObject context, EReference reference) {
		// We want to define the Scope for the Element's superElement cross-reference
//		if (reference == MclPackage.Literals.INTERACTION__TARGET_PORT) {
//			val rootElement = EcoreUtil2.getRootContainer(context, true)
//			val candidates = reference.EReferenceType.eContainer.eAllContents.filter(Port)
//			val filtered = candidates.filter[t | t.direction == PortDirection.OUTPUT].toList
			// Create IEObjectDescriptions and puts them into an IScope instance
//			return Scopes.scopeFor(filtered)
//		}
		return super.getScope(context, reference);
	}	
}
