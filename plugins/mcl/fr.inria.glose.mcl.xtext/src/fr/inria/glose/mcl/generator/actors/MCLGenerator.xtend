package fr.inria.glose.mcl.generator.actors;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import fr.inria.glose.mcl.MCSpecification
import fr.inria.glose.mbilang.Port

class MCLGenerator extends AbstractGenerator {

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
//		In case there are several MCSpecification, it is mandatory to merge them into a single MCSpecification (checks for scoping, naming and so on) and then parse the resulting MCSpecification
		for (e : resource.allContents.toIterable.filter(MCSpecification)) {
//			fsa.generateFile(e.eResource.URI.lastSegment.replaceFirst(e.eResource.URI.fileExtension, "") + "c", e.compile)
			// Retrieve all the information from each MCSpecification files
//			compile(e)
		}
		
		// Create the associated actor for each model
		
		// Create the main actor
		compileMainActor()
	}
	
//	def retrieveSystem(MCSpecification e ) {
//				// Retrieve all the source ports and declare the corresponding port to exchange data during the execution
//		var sourcePorts = new ArrayList<Port>()
//
//		// Triggering Conditions in the MCSpecification
//		var tcExpressions = new ArrayList<TCExpression>()
//				
//		// Iterate over the contents to retrieve some useful information (dependencyGraph, workingGrph, sourcePorts, ...)
//		for (i : e.eAllContents.toIterable) {
//			// ----------------------------------------------------------------------------------------------------------------
//			// Find all the Interfaces used in the specification
//			// ----------------------------------------------------------------------------------------------------------------
//
//			// Retrieve all the interfaces that appears at least once in all interactions
//			if (i instanceof Interaction) {
//				interfaces.add((i.sourcePort.eContainer as Interface))
//				for (targetPort : i.targetPort) {
//					interfaces.add((targetPort.eContainer as Interface))
//										
//					for ( tc : (i as Interaction).triggeringConditions) {
//						// Create the dependency graph based on Interactions
//						// If the condition is based on a read event, it means that the dependency is inverted (A -> B) => (B -> A) due to the fact that the reader needs the value from the writer
//						dependencyGraph.addDirectedSimpleEdge(targetPort.eContainer as Interface, tc, i.sourcePort.eContainer as Interface)
//						
//						// -------------------------------------------------------------------
//						// Working graph, it represents the system as a grph where the nodes are the models and the edges the link between the models and its triggering condition
//						// It is possible to have multiple links between two models according to the triggering conditions specified
//						// -------------------------------------------------------------------
//						workingGrph.addDirectedSimpleEdge(i.sourcePort.eContainer as Interface, tc, targetPort.eContainer as Interface)	
//						
//						// Port Grph
//						portGrph.addDirectedSimpleEdge(i.sourcePort, tc, targetPort)
//															
//					}
//				}
//			}
//			// TODO: We have also to look for the condition and the sync constraint (everywhere there is a reference to a port)
//
//			// ------------------------------------------------------------------------------------------------------------
//			// Find all the source ports
//			// ------------------------------------------------------------------------------------------------------------
//			if (i instanceof Interaction) {
//				sourcePorts.add(i.sourcePort)
//			}
//
//			// ------------------------------------------------------------------------------------------------------------
//			// Find all the triggering conditions used
//			// ------------------------------------------------------------------------------------------------------------
//			if (i instanceof TCExpression) {
//				tcExpressions.add(i)
//			}
//		}
//	}
	
	def compileSendMethod(Port p) {
		'''
		    public void send() {
		        ACLMessage msg = new ACLMessage(ACLMessage.PROPAGATE);
		        msg.addReceiver(new AID("a2", AID.ISLOCALNAME));
		        msg.setOntology(SimulationHostAgent.SET_OPENCLOSE);
		        try {
		            msg.setContentObject(new Port(simulation.read("gen").asBoolean(), simulation.getCurrentTime()));
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        send(msg);
		    }
		'''
	}
	
	def compileMainActor() {
		'''
		package fr.inria.glose.ctrlenv;
		
		import jade.core.AID;
		import jade.core.Agent;
		import jade.core.behaviours.OneShotBehaviour;
		import jade.domain.DFService;
		import jade.domain.FIPAAgentManagement.DFAgentDescription;
		import jade.lang.acl.ACLMessage;
		import jade.wrapper.AgentController;
		import jade.wrapper.PlatformController;
		
		import java.util.Vector;
		
		public class SimulationHostAgent extends Agent {
		
		    // -----------------------------------------
		    // Class variables
		    // -----------------------------------------
		    public final static String STEP = "STEP";
		    public final static String STEP_EVENT = "STEP_EVENT";
		    public final static String STEP_UP = "STEP_UP";
		    public final static String STEP_R2R = "STEP_R2R";
		    public final static String STEP_TIME = "STEP_TIME";
		
		    public final static String STEP_PERFORMED = "STEP_PERFORMED";
		    public final static String GET_PORT_VALUE = "GET_PORT_VALUE";
		    public final static String SET_PORT_VALUE = "SET_PORT_VALUE";
		
		    // -----------------------------------------
		    // Simulation Specific Ontology codes
		    // -----------------------------------------
		    public final static String SET_GEN = "SET_GEN";
		    public final static String SET_OPENCLOSE = "SET_OPENCLOSE";
		    public final static String SET_VALUE = "SET_VALUE";
		    public final static String GET_GEN = "GET_GEN";
		    public final static String GET_OPENCLOSE = "GET_OPENCLOSE";
		
		
		
		    // -----------------------------------------
		    // Instance variables
		    // -----------------------------------------
		    protected Vector<AID> agentsList = new Vector();
		
		
		    protected void setup() {
		        System.out.println( getLocalName() + " setting up");
		        try {
		            // create the agent descrption of itself
		            DFAgentDescription dfd = new DFAgentDescription();
		            dfd.setName( getAID() );
		            DFService.register( this, dfd);
		        }
		        catch (Exception e) {
		            System.out.println( "Saw exception in HostAgent: " + e );
		            e.printStackTrace();
		        }
		
		        addBehaviour(new OneShotBehaviour(this) {
		            public void action() {
		                // Create other agents
		                PlatformController container = getContainerController(); // get a container controller for creating new agents
		                // create N guest agents
		
		                String localName;
		                AgentController agent;
		                try {
		                        // create a new agent
		                        localName = "a1";
		                        agent = container.createNewAgent(localName, "fr.inria.glose.ctrlenv.FMUOneAgent", null);
		                        agent.start();
		                        // keep the guest's ID on a local list
		                        agentsList.add( new AID(localName, AID.ISLOCALNAME) );
		
		
		                        localName = "a2";
		                        agent = container.createNewAgent(localName, "fr.inria.glose.ctrlenv.FMUTwoAgent", null);
		                        agent.start();
		                        // keep the guest's ID on a local list
		                        agentsList.add( new AID(localName, AID.ISLOCALNAME) );
		                }
		                catch (Exception e) {
		                    System.err.println( "Exception while adding guests: " + e );
		                    e.printStackTrace();
		                }
		            }
		        });
		    }
		}
		'''
	}
}
