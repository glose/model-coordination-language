package fr.inria.glose.mcl.generator

import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import fr.inria.glose.mcl.MCSpecification
import fr.inria.glose.mbilang.Interface
import java.util.ArrayList
import fr.inria.glose.mcl.Connector
import fr.inria.glose.mbilang.Port
import fr.inria.glose.mbilang.PortDirection
import java.util.HashMap
import fr.inria.glose.mcl.impl.PeriodicExpressionImpl
import java.net.ServerSocket
import java.io.File
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import fr.inria.glose.mcl.generator.VirtualPort.PortType
import fr.inria.glose.mbilang.DataNature
import fr.inria.glose.mbilang.ReadyToRead
import fr.inria.glose.mbilang.Triggered
import fr.inria.glose.mbilang.Updated
import java.net.URI
import java.util.Random
import java.awt.Color
import fr.inria.glose.mbilang.DataType
import com.google.googlejavaformat.java.Formatter
import java.util.Set
import fr.inria.glose.mcl.PortRef
import fr.inria.glose.mcl.EventTrigger
import fr.inria.glose.mcl.VariableTrigger
import org.eclipse.xtext.EcoreUtil2
import java.util.stream.Collectors
import java.util.List
import org.eclipse.ui.PlatformUI
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.core.runtime.IAdaptable
import org.eclipse.core.resources.IProject

class MCLGenerator extends AbstractGenerator {	
	List<Interface> interfaces
	HashMap<Port, Connector> port2Connector 
	List<VirtualPort> globalPorts
	String outputPath
	String packageOutputPath
	String projectName = "CoSim20GeneratedSystem"
	
	int oscilloscupServerPort

	// Map Port -> Connector
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		interfaces = new ArrayList<Interface>()
		port2Connector = new HashMap<Port, Connector>
		globalPorts = new ArrayList<VirtualPort>()
				
		outputPath = new File(ResourcesPlugin.workspace.root.getFile(
			new Path(
				resource.URI.toPlatformString(true)
			)
		).rawLocation.toOSString).getParentFile().toString;
		
		oscilloscupServerPort = getAFreePort()
		
		val root = ResourcesPlugin.getWorkspace().getRoot();
		val project = root.getProjects().head
				
		// Create the main package directory
		packageOutputPath = "src/main/java/"+projectName+"/";
		new File(packageOutputPath).mkdirs
		
		// Create the test directory
		new File("src/test/java/").mkdirs
		
		// In case there are several MCSpecification, it is mandatory to merge them into a single MCSpecification (checks for scoping, naming and so on) and then parse the resulting MCSpecification
		for (e : resource.allContents.toIterable.filter(MCSpecification)) {
			// Retrieve all the information needed to compile
			e.setup
			// Generate all the files from the interfaces, MCL specifications, ...
			e.compileAll(fsa)
		}
		
		fsa.generateFile("pom.xml", generatePOM);
		
		fsa.generateFile(".project", generateDotProject)
		
		fsa.generateFile(".classpath", generateDotClasspath)
		
		port2Connector.clear
		interfaces.clear
		interface2syncURL.clear
		globalPorts.clear
		
	}
	
	// Model Coordination Interface (MCI) 
	def getMCIInterface(PortRef pr) {
		return EcoreUtil2.getContainerOfType(pr.refPort, Interface) as Interface
	}
	
	// Setup method to call BEFORE e.compileAll
	def setup(MCSpecification e) {	
		// Build the globalPorts list with the actual connection between ports							
		var connectors = e.eAllContents.filter[ p | p instanceof Connector].toList
		/*
		 * For each connector, retrieve all the possible tuples (port_to, port_from, connector) and save
		 * them as VirtualPort
		 */
		connectors.stream.forEach(x | {
			var c = x as Connector
			if (c.from !== null && c.to !== null) {
//				var from = new VirtualPort(c.from.refPort, c.from.MCIInterface, c)
//				var to = new VirtualPort(c.to.refPort, c.to.MCIInterface, c)
//				from.connectedPorts.add(to)
//				to.connectedPorts.add(from)
//				globalPorts.add(from)
//				globalPorts.add(to)
//				
				globalPorts.add(new VirtualPort(c.from.refPort, c.to.refPort, c))
				globalPorts.add(new VirtualPort(c.to.refPort, c.from.refPort, c))
				
			}
		})
		/*
		 * Create a distinct list of VirtualPorts
		 */
		// globalPorts = globalPorts.toList.stream.distinct.collect(Collectors.toList)
		
		// Retrieve the list of Interfaces
		globalPorts.stream.map(x | x.port).distinct()
								.forEach( p | {
									var i = EcoreUtil2.getContainerOfType(p, Interface) as Interface
									interfaces.add(i)
								})
		interfaces = interfaces.toList.stream.distinct.collect(Collectors.toList)									
	}
	
	def compileAll(MCSpecification e, IFileSystemAccess2 fsa) {
		
		var formattedSource = new Formatter().formatSource(e.compilePlotterServer);
		
		fsa.generateFile(packageOutputPath + "OscilloscupServer.java", formattedSource)
		
		// Compile all the interfaces
		for (i : interfaces.toSet) {
			
			formattedSource = new Formatter().formatSource(i.compile);
			fsa.generateFile(packageOutputPath + i.name.toFirstUpper + ".java", formattedSource)
		}

		// Compile the system file
		formattedSource = new Formatter().formatSource(e.compile);
		fsa.generateFile(packageOutputPath + "Cosim20System.java", formattedSource)
		
		
	}
	


	/*
	 * For the moment it is only on localhost. In the next releases, it could be defined as an attribute on the interface of the model 
	 */
	var interface2URL = new HashMap<Interface, URI>	
	var interface2syncURL = new HashMap<Interface, URI>
	
	def getZeroMQURL(Interface i) {
		if (interface2URL.containsKey(i)) {
			return interface2URL.get(i);
		} else {
			var socket = new ServerSocket(0)
			var s = "tcp://localhost:" + socket.localPort;			
			interface2URL.put(i, new URI(s))
			socket.close
			return interface2URL.get(i)
		}
	}
	
	def getZeroMQSyncURL(Interface i) {
		if (interface2syncURL.containsKey(i)) {
			return interface2syncURL.get(i);
		} else {
			var socket = new ServerSocket(0)
			var s = "tcp://localhost:" + socket.localPort;			
			interface2syncURL.put(i, new URI(s))
			socket.close
			return interface2syncURL.get(i)
		}
	}
	
	def getAFreePort() {
		var socket = new ServerSocket(0)
		var freePort = socket.localPort
		socket.close
		return freePort
	}

	def connectedInitiators(Interface i) {
		return globalPorts.filter[vp|vp.i.equals(i)].filter[vp | vp.portType == PortType.FOLLOWER].size
	}
	
	def connectedQueues(Interface i) {
		var portsOnInterface = globalPorts.filter[vp|vp.i.equals(i)]
		var numberOfOutputs = portsOnInterface.filter[vp | vp.port.direction == PortDirection.OUTPUT].size
		
		var IOEventsPorts = portsOnInterface.filter[vp | vp.port.direction == PortDirection.INPUT]
			.filter[vp | vp.port.ioevents.length > 0]
		var numberOfready2ReadPorts = 0	
		for (ioEventPort : IOEventsPorts) {
			if (ioEventPort.port.ioevents.head instanceof ReadyToRead) {
				numberOfready2ReadPorts++
			}
		}
//		var outputs = globalPorts.filter[vp|vp.i.equals(i)].filter[vp | vp.port.direction == PortDirection.OUTPUT].size
		// TODO: Check if the trigger conditions is a R2R ioevent
//		var ready2ReadPorts = globalPorts.filter[vp|vp.i.equals(i)].filter[vp | vp.port.direction == PortDirection.INPUT].filter[vp | vp.c.trigger instanceof ReadyToRead].size
		return numberOfOutputs + numberOfready2ReadPorts
	}
	
	def compile(Interface i) {
		var virtualPorts = globalPorts.filter[vp|vp.i.equals(i)].toSet
		return '''
			package «projectName»;
			
			import fr.inria.glose.cosim20.*;
			import fr.inria.glose.cosim20.interfaces.*;
			import org.eclipse.gemoc.execution.commons.commands.*;
			import org.eclipse.gemoc.execution.commons.predicates.*;
			import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
			import java.io.IOException;
			import fr.inria.glose.cosim20.CONFIG;
			
			import java.io.File;
			import java.io.FileNotFoundException;
			import java.io.IOException;
			import java.io.PrintWriter;
			import java.math.BigDecimal;
			import java.util.Arrays;
			import java.util.ArrayList;
			import java.util.concurrent.TimeUnit;
			import com.google.common.base.Stopwatch;
			

			public class «i.name.toFirstUpper» extends CoordinationInterface {
				
			    //-------------------------------
			    //Plotter stuff
			    //-------------------------------
			    OscilloscupClient plotterClient;
				Stopwatch stopwatch = Stopwatch.createStarted();   
				
				«IF i.FMUPath !== null && i.FMUPath.compareTo("") != 0»
					// ----------------------------------------------------------------
					// FMI Standard
					// ----------------------------------------------------------------
					double startTime = 0.0;
					double stopTime = CONFIG.EndOfSimulation.doubleValue();
					String fmuPath = "«outputPath + "/" + i.FMUPath»";
				«ENDIF»
				
				«FOR port : virtualPorts»
					«port.compileDeclaration»
					«port.compileMonitoredDeclaration»
				«ENDFOR»
							
			   public «i.name.toFirstUpper»() {
			       super("«i.name»", "localhost", «i.zeroMQURL.port», «i.zeroMQSyncURL.port», «i.connectedInitiators», «i.connectedQueues»);
			
					«IF i.FMUPath !== null && i.FMUPath.compareTo("") != 0»
						// Setup the FMI model
						model = new FMIInterface(fmuPath, startTime, stopTime);
					«ENDIF»
					
					«IF i.gemocExecutablePath !== null»
						// -------------------------------------------------------
						// Initialize the model
						// -------------------------------------------------------
						this.model = new GemocInterface("«outputPath + "/" + i.gemocExecutablePath.split(":").get(0)»", "localhost", «i.gemocExecutablePath.split(":").get(1)»);
					«ENDIF»
				
					try {
					    plotterClient  = new OscilloscupClient("localhost", «oscilloscupServerPort»);
					} catch (IOException e) {
					    e.printStackTrace();
					}
					
					«FOR port : virtualPorts»
						«port.compileMonitoredInitialization»
					«ENDFOR»
					        
			        initiatorPorts = new ArrayList<>();
			        followerPorts = new ArrayList<>();
			        followerDeterministicPorts = new ArrayList<>();
			        
					«FOR port : virtualPorts»
						«IF port.portType == PortType.FOLLOWER»
							followerPorts.add(«port.ID»);
						«ENDIF»
						«IF port.portType == PortType.FOLLOWERDETERMINISTIC»
							followerDeterministicPorts.add(«port.ID»);
						«ENDIF»
						«IF port.portType == PortType.INITIATOR»
							initiatorPorts.add(«port.ID»);
							«IF port.c.trigger instanceof PeriodicExpressionImpl»
								todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, «port.ID», "«i.zeroMQURL»", now, new TemporalPredicate(«(port.c.trigger as PeriodicExpressionImpl).value»)));
							«ENDIF»
						«ENDIF»								
					«ENDFOR»
			
			        // The <key> is the id of the external port linked with a port of this model (<value>)
			        «FOR port : virtualPorts»
			        	«IF port.getConnectedPortID.compareTo(port.ID) == 0»
			        		portMap.put("«port.getConnectedPortID»", «port.ID»);
			        	«ELSE»
			        		portMap.put("«port.getConnectedPortID»", «port.ID»);
			        		portMap.put("«port.ID»", «port.ID»);
			        	«ENDIF»
			        	«IF port.ID.compareTo(port.variableName) != 0»
			        		portMap.put("«port.variableName»", «port.ID»);
			        	«ENDIF»

«««			        	portMap.put("«port.variableName»", «port.ID»);
			        «ENDFOR»
			
					«FOR port : virtualPorts.filter[vp | vp.portType == PortType.FOLLOWER || vp.portType == PortType.FOLLOWERDETERMINISTIC || vp.port.direction == PortDirection.INPUT]»
						addNewInputPort("«port.getConnectedPortID»", "«(port.connectedPort.eContainer as Interface).zeroMQURL»", "«(port.connectedPort.eContainer as Interface).zeroMQSyncURL»");
					«ENDFOR»
					
					««« IMPORTANT! Constant variables are not generated into the code
					«FOR port : i.ports.filter[port | port.nature == DataNature.CONSTANT]»
						«IF port.initValue !== null && port.initValue.compareTo("") != 0»
						model.set("«Utils.getVariableName(port)»", «port.initValue»);
						«ENDIF»
					«ENDFOR»
					
					«FOR vp : virtualPorts.filter[vp | vp.port.direction == PortDirection.INPUT]»
						«IF vp.port.initValue !== null && vp.port.initValue.compareTo("") != 0»
						model.set("«vp.RTDReference»", «vp.port.initValue»);
						«ENDIF»
					«ENDFOR»
					  }
			
			    @Override
			    public void onTime(Action currentAction, StopCondition sr) {
			        if (now.compareTo(currentAction.temporalHorizon) == 0) {
«««			        	«FOR port : ports.filter[vp | vp.c.trigger instanceof PeriodicExpression]»
			        	«FOR vp : virtualPorts»
			        	// Execute the corresponding action
			        	if (currentAction.port.ID.compareTo(«vp.ID».ID) == 0) {
			        		«IF vp.port.direction == PortDirection.INPUT»
«««			        		If the type is different, then we need to provide an adapter
			        		«IF vp.isTypeAdapterRequired»
			        			«vp.getAdapterFor(vp.connectedPort)»
			        		«ELSE»
			        			«vp.getDataType(Language.JAVA)» value = («vp.getDataType(Language.JAVA)») currentAction.getValue();
			        		«ENDIF»
			        		
			        		model.set("«vp.RTDReference»", value);
			        		if (CONFIG.showDebugMessage) System.out.println("["+ID+"] «vp.ID» = " + model.get("«vp.RTDReference»") + " @ " + now.doubleValue());
			        		«IF vp.port.monitored»
			        		try {
			        			«IF vp.port.type == DataType.BOOLEAN»
			        			plotterClient.addPoint("cosimPlotter", «vp.plotterIndex», now.doubleValue(), ((boolean) currentAction.getValue() == false) ? 0 : 20);
			        			«ELSE»
			        			plotterClient.addPoint("cosimPlotter", «vp.plotterIndex», now.doubleValue(), («vp.getDataType(Language.JAVA)») currentAction.getValue());
			        			«ENDIF»
			        			writer4«vp.ID.toLowerCase».write(now.doubleValue() + "," + currentAction.getValue() + "\n");
			        			writer4«vp.ID.toLowerCase».flush();
			        		} catch (IOException e) {
			        			e.printStackTrace();
			        		}
			        		«ENDIF»
			        		«ENDIF»
			        		«IF vp.port.direction == PortDirection.OUTPUT»
			        			«IF vp.port.nature == DataNature.SPURIOUS»
			        				«vp.getDataType(Language.JAVA)» value = («vp.getDataType(Language.JAVA)») «vp.ID».getValue();
			        			«ELSE»
									«vp.getDataType(Language.JAVA)» value = («vp.getDataType(Language.JAVA)») model.get(«vp.ID».associatedModelVariableName);
			        			«ENDIF»
			        		
			        		publish(«vp.ID», value, now);
				        		«IF vp.port.monitored»
				        		try {
				        			plotterClient.addPoint("cosimPlotter", «vp.plotterIndex», now.doubleValue(), value);
				        			writer4«vp.ID.toLowerCase».write(now.doubleValue() + "," + value + "\n");
				        			writer4«vp.ID.toLowerCase».flush();
				        		} catch (IOException e) {
				        			e.printStackTrace();
				        		}
				        		«ENDIF»
				        		
			        		«ENDIF»
			        	}
			        	«ENDFOR»
			
						// Extra plot to show the actual data nature of the variable
						«FOR vp : virtualPorts.filter[vp | vp.port.monitored].filter[vp | vp.port.nature == DataNature.PIECEWISE_CONSTANT]»
					        try {
					        	«IF vp.port.type == DataType.BOOLEAN»
			        			plotterClient.addPoint("cosimPlotter", «vp.plotterIndex», now.doubleValue(), (double) model.get(«vp.ID».associatedModelVariableName) * 20);
			        			«ELSE»
			        			plotterClient.addPoint("cosimPlotter", «vp.plotterIndex», now.doubleValue(), (double) model.get(«vp.ID».associatedModelVariableName);
			        			«ENDIF»
					        } catch (IOException e) {
					            e.printStackTrace();
					        }
						«ENDFOR»
			            // Remove the current action from the to-do list
			            currentAction.setDone();
			
			            if (currentAction instanceof PeriodicAction) {
			                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
			            }
			        }
			    }
			
			    @Override
			    public void onEvent(Action currentAction, StopCondition sc) {
			    	«FOR vp : virtualPorts.filter[vp | vp.port.nature == DataNature.SPURIOUS]»
			    		// If there is a Port defined as Transient, then save its changements of state
			    		«vp.ID».setValue(!((«vp.getDataType(Language.JAVA)»)«vp.ID».getValue()));
			    		
			    		«IF vp.c.trigger instanceof EventTrigger»
				    		«IF vp.port.direction == PortDirection.OUTPUT»
		«««							If the datatype of the target port is PWC
«««				        		«IF vp.targetPorts.head.port.nature == DataNature.PIECEWISE_CONSTANT»
				        			«vp.getDataType(Language.JAVA)» value = («vp.getDataType(Language.JAVA)») «vp.ID».getValue();
«««				        		«ENDIF»
		«««			        		«IF vp.targetPorts.head.port.nature == DataNature.PIECEWISE_CONTINUOUS»
		«««			        			// Illegal statement: Datanature not compatile
		«««			        		«ENDIF»
		«««			        		«IF vp.targetPorts.head.port.nature == DataNature.CONTINUOUS»
		«««			        			// Illegal statement: Datanature not compatile
		«««			        		«ENDIF»
		«««			        		«IF vp.targetPorts.head.port.nature == DataNature.CONSTANT»
		«««			        			// Illegal statement: Datanature not compatile
		«««			        		«ENDIF»
				        				        		
				    			publish(«vp.ID», value, Utils.toBigDecimal(sc.timeValue) );
				    		«ENDIF»
			    		«ENDIF»
			    	«ENDFOR»
			    	
			    	
«««			    	«FOR vp : ports.filter[vp | vp.c.trigger instanceof IOEventRef].filter[ vp | (vp.c.trigger as IOEventRef).ioevent instanceof TriggeredImpl ]»
«««			    	
«««		    		«IF vp.port.direction == PortDirection.OUTPUT»
««««««							If the datatype of the target port is PWC
«««		        		«IF vp.targetPorts.head.port.nature == DataNature.PIECEWISE_CONSTANT»
«««		        			«vp.getID».setValue(!((«vp.javaDataType»)«vp.getID».getValue()));
«««		        			«vp.javaDataType» value = («vp.javaDataType») «vp.getID».getValue();
«««		        		«ENDIF»
««««««			        		«IF vp.targetPorts.head.port.nature == DataNature.PIECEWISE_CONTINUOUS»
««««««			        			// Illegal statement: Datanature not compatile
««««««			        		«ENDIF»
««««««			        		«IF vp.targetPorts.head.port.nature == DataNature.CONTINUOUS»
««««««			        			// Illegal statement: Datanature not compatile
««««««			        		«ENDIF»
««««««			        		«IF vp.targetPorts.head.port.nature == DataNature.CONSTANT»
««««««			        			// Illegal statement: Datanature not compatile
««««««			        		«ENDIF»
«««		        				        		
«««		    			publish(«vp.getID», value, Utils.toBigDecimal(sc.timeValue) );
«««		    		«ENDIF»
«««		    	«ENDFOR»
			    }
			
			    @Override
			    public void onReadyToRead(Action currentAction, StopCondition sc) {
			    	«IF !virtualPorts.filter[vp | vp.c.trigger instanceof fr.inria.glose.mcl.ReadyToRead ].filter[vp | vp.port.direction == PortDirection.INPUT].empty»
			    	retrieve(portMap.get(sc.objectQualifiedName), now);
			    	// Set this action as done, after the doWait there is the new action into the queue with the new value requested.
			    	// To sync this model with the other model, we need to re-execute the doStep method
			    	currentAction.setDone();
			        «ENDIF»
			    }
			
			    @Override
			    public void onUpdated(Action currentAction, StopCondition sc) {
					// Not implemented yet
			    }
			
			    @Override
			    public void onSync(Action currentAction) {
			    	«IF i.FMUPath !== null »
			    	// Statically generated ONLY if the BI abstracts a FMU
			    	// model.doStep(new TemporalPredicate(0));
			    	«ENDIF»
					onTime(currentAction, null);
			    }
			
			    @Override
			    public CoordinationPredicate setInitiatorsPredicate() {			    
			    	«compilePredicate(virtualPorts)»	
			    	«IF virtualPorts.filter[vp | vp.c.trigger instanceof VariableTrigger].filter[ vp | vp.portType == PortType.INITIATOR].toSet.empty && virtualPorts.filter[vp | vp.port.nature == DataNature.SPURIOUS].toSet.empty»
			    		return null;			        
			        «ELSE»
			        		«compilePredicates(virtualPorts)»
			        «ENDIF»
			    }
		        @Override
		        public void onEnd() {
				«FOR port : virtualPorts.filter[p | p.port.monitored]»
					writer4«port.ID.toLowerCase».flush();
				«ENDFOR»
				stopwatch.stop(); // optional
				System.out.println("[«i.name»] Elapsed time: "+ stopwatch.elapsed(TimeUnit.SECONDS));
				
		        }
			        
			}
		'''
	}
	
	def compilePredicate(Set<VirtualPort> vports) {
		// TODO it should the set of events (triggered, updated and ready2read)
		var vps = vports.filter[vp | (!EcoreUtil2.getAllContentsOfType(vp.c, VariableTrigger).empty && vp.portType == PortType.INITIATOR) || (vp.port.nature == DataNature.SPURIOUS)].toList
		var res = ""
		for (vp : vps ) {
			res += 
			'''
			«FOR ioevent : vp.port.ioevents»
	    		«IF ioevent instanceof ReadyToRead»
					ReadyToReadPredicate «vp.ID»_predicate = new ReadyToReadPredicate("«vp.port.RTD»", "«vp.port.variableName»");
	    		«ENDIF»
	    		«IF ioevent instanceof Triggered»
	    			EventPredicate «vp.ID»_predicate = new EventPredicate("«ioevent.name»", "«vp.port.variableName»");
	    		«ENDIF»
	    		«IF ioevent instanceof Updated»
	    			UpdatedPredicate «vp.ID»_predicate = new UpdatedPredicate("«vp.port.RTD»", "«vp.port.variableName»");
	    		«ENDIF»
			«ENDFOR»
			'''
		}
		return res
	}
	
	def compilePredicates(Set<VirtualPort> vports) {
		
		var vps = vports.filter[vp | (!EcoreUtil2.getAllContentsOfType(vp.c, VariableTrigger).empty && vp.portType == PortType.INITIATOR) || (vp.port.nature == DataNature.SPURIOUS)].toList
				
		if (vps.size == 1) {
			return
			'''
			return «vps.get(0).ID»_predicate;
			'''
		}
		else {
			// size > 2
			var result = '''
			BinaryPredicate binaryPredicate0 = new BinaryPredicate(«vps.get(0).ID»_predicate, «vps.get(1).ID»_predicate, BinaryPredicate.BooleanBinaryOperator.OR);
			'''
			var i = 2
			var lastPredicateName = "binaryPredicate0"
			for (; i < vps.size(); i++) {
				result = result + 
				'''
				BinaryPredicate binaryPredicate«i-1» = new BinaryPredicate(«lastPredicateName», «vps.get(i).ID»_predicate, BinaryPredicate.BooleanBinaryOperator.OR);			
				'''
				lastPredicateName = "binaryPredicate" + (i-1)
			}
			
			result = result + 
			'''
			return «lastPredicateName»;
			'''
			return result
		}
	}


	def compile(MCSpecification e) {
		return '''
			package «projectName»;
			
			import fr.inria.glose.cosim20.Utils;
			import fr.inria.glose.cosim20.CONFIG;
			
			import java.math.BigDecimal;
			import java.util.ArrayList;
			import java.util.List;
			import java.util.concurrent.Callable;
			import java.util.concurrent.ExecutorService;
			import java.util.concurrent.Executors;
			import java.io.IOException;
						
			public class Cosim20System {
			    // Define the "infinite" time as the time of the end of simulation plus an Epsilon
			    public static void main(String[] args) {
			    	CONFIG.showDebugMessage = true;
			    	CONFIG.EndOfSimulation = Utils.toBigDecimal(30000);
					try {
					    OscilloscupServer.createAutoRefreshServer(«oscilloscupServerPort», "cosimPlotter", 1000);
					} catch (IOException e) {
					    e.printStackTrace();
					}
			    	
			    	List<Callable<Object>> calls = new ArrayList<>();
			    	«FOR i : interfaces.toSet»
			    		«i.name.toFirstUpper» «i.name.toLowerCase» = new «i.name.toFirstUpper»();
			    		calls.add(Executors.callable(«i.name.toLowerCase»));
			    	«ENDFOR»
			        ExecutorService executor = Executors.newFixedThreadPool(calls.size());
			
			        System.out.println("Starting the simulation");
			        try {
			            executor.invokeAll(calls);
			        } catch (InterruptedException e) {
			            e.printStackTrace();
			        }
			    }
			}
		'''
	}
	
	def compilePlotterClient(MCSpecification e) {
		'''
		'''
	}

	def compilePlotterServer(MCSpecification e) {
		return '''
		package «projectName»;
		
		import oscilloscup.Graphics2DPlotter;
		import oscilloscup.InteractiveSwingPlotter;
		import oscilloscup.SwingPlotter;
		import oscilloscup.data.Figure;
		import oscilloscup.data.Point;
		import oscilloscup.data.rendering.figure.ConnectedLineFigureRenderer;
		import oscilloscup.data.rendering.point.CirclePointRenderer;
		import java.util.Random;
		import javax.swing.*;
		import java.awt.*;
		import java.io.BufferedReader;
		import java.io.IOException;
		import java.io.InputStreamReader;
		import java.net.ServerSocket;
		import java.net.Socket;
		import java.util.Map;
		import java.util.concurrent.ConcurrentHashMap;
		
		public class OscilloscupServer {
		    private final ServerSocket ss;
		    public final Map<String, Graphics2DPlotter> plots = new ConcurrentHashMap<>();
		    public OscilloscupServer(int port) throws IOException {
		        this.ss = new ServerSocket(port);
		        // listens in the background
		        new Thread(() -> {
		            try {
		                listen();
		            }
		            catch (IOException e) {
		                e.printStackTrace();
		            }
		        }).start();
		    }
		    private Object listen() throws IOException {
		        while (true) {
		            Socket client = ss.accept();
		            // each new client will be processed in
		            new Thread(() -> {
		                try {
		                    newClient(client);
		                }
		                catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }).start();
		        }
		    }
		    private void newClient(Socket client) throws IOException {
		        BufferedReader br = new BufferedReader(
		                new InputStreamReader(client.getInputStream()));
		        while (true) {
		            String line = br.readLine();
		            if (line == null) {
		                break;
		            }
		            else {
		                String[] path = line.split("/");
		                Graphics2DPlotter plot = plots.get(path[0]);
		                if (path.length != 4) {
		                    throw new IllegalStateException("malformed command: " + line);
		                }
		                if (plot == null) {
		                    throw new IllegalStateException("unknown plot: " + path[0]);
		                }
		                int figureIndex = Integer.valueOf(path[1]);
		                if (figureIndex >= plot.getFigure().getFigureCount()) {
		                    throw new IllegalStateException("no such figure: " + figureIndex);
		                }
		                Figure figure = plot.getFigure().getFigureAt(figureIndex);
		                double x = Double.valueOf(path[2]);
		                double y = Double.valueOf(path[3]);
		                figure.addPoint(new Point(x, y));
		            }
		        }
		    }
		
		    public static void createAutoRefreshServer(int port, String plotID, int refreshPeriodInMs) throws IOException {
		        SwingPlotter sp = new InteractiveSwingPlotter();
		        Figure rootFigure = new Figure();
		        «FOR vp : globalPorts.filter[vp | vp.port.monitored].toSet»
		        rootFigure.addFigure(new Figure());
		        «ENDFOR»
		        CirclePointRenderer renderer = new CirclePointRenderer();
		        renderer.setRadius(3);
		        «var i = 0»
		        «FOR vp : globalPorts.filter[vp | vp.port.monitored].toSet»
				rootFigure.getFigureAt(«vp.plotterIndex = i++»).addRenderer(new ConnectedLineFigureRenderer());
					«IF i == 0»
					rootFigure.getFigureAt(«i»).addRenderer(renderer);
					«ENDIF»
				rootFigure.getFigureAt(«vp.plotterIndex»).setName("«vp.ID»");
				«««		        	rootFigure.getFigureAt(«vp.plotterIndex»).setColor(Color.getHSBColor(«random.nextFloat()»f, «(random.nextInt(2000) + 1000) / 10000f»f, 0.9f));
				rootFigure.getFigureAt(«vp.plotterIndex»).setColor(«vp.port.color»);
		        	
		        «ENDFOR»

		        
«««		        CirclePointRenderer renderer = new CirclePointRenderer();
«««		        renderer.setRadius(3);
«««		        rootFigure.getFigureAt(0).addRenderer(renderer);

		        sp.getGraphics2DPlotter().getSpace().getYDimension().getOriginAxis().setVisible(false);
		        sp.getGraphics2DPlotter().getSpace().getXDimension().getOriginAxis().setVisible(false);
		        sp.getGraphics2DPlotter().getSpace().getYDimension().getGrid().setVisible(false);
		//        sp.getGraphics2DPlotter().getSpace().getXDimension().setBounds(0,10000);
		//        sp.getGraphics2DPlotter().getSpace().getXDimension().setMaximumIsAutomatic(false);
		//        sp.getGraphics2DPlotter().getSpace().getXDimension().setMinimumIsAutomatic(false);
		        sp.getGraphics2DPlotter().setFigure(rootFigure);
		        sp.getGraphics2DPlotter().getSpace().getXDimension().getLegend().setText("time(ms)");
		        sp.getGraphics2DPlotter().getSpace().getYDimension().getLegend().setText("CPU Temperature (°C)");
		        JFrame f = new JFrame();
		        f.setContentPane(sp);
		        f.setSize(1200, 600);
		        f.setVisible(true);
		        // starts the server in background
		        OscilloscupServer server = new OscilloscupServer(port);
		
		        new Thread(() -> {
		            while (true) {
		                sp.repaint();
		                try {
		                    Thread.sleep(refreshPeriodInMs);
		                }
		                catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
		            }
		
		        }).start();
		
		        // allows the server to control myPlotter, known by the client as "plot"
		        server.plots.put(plotID, sp.getGraphics2DPlotter());
		    }
		
		    public static void main(String[] args) throws IOException {
		        SwingPlotter sp = new SwingPlotter();
		        Figure rootFigure = new Figure();
		        rootFigure.addFigure(new Figure());
		        rootFigure.addFigure(new Figure());
		        rootFigure.getFigureAt(0).addRenderer(new ConnectedLineFigureRenderer());
		        rootFigure.getFigureAt(1).addRenderer(new ConnectedLineFigureRenderer());
		        sp.getGraphics2DPlotter().setFigure(rootFigure);
		        JFrame f = new JFrame();
		        f.setContentPane(sp);
		        f.setSize(500, 500);
		        f.setVisible(true);
		        // starts the server in background
		        OscilloscupServer server = new OscilloscupServer(43444);
		
		        new Thread(() -> {
		            while (true) {
		                sp.repaint();
		                try {
		                    Thread.sleep(1000);
		                }
		                catch (InterruptedException e) {
		                    e.printStackTrace();
		                }
		            }
		
		        }).start();
		
		        // allows the server to control myPlotter, known by the client as "plot"
		        server.plots.put("plot", sp.getGraphics2DPlotter());
		    }
		}
		'''
	}
	
	var random = new Random();
	
	def getRandomColor(){
		var hue = random.nextFloat();
		// Saturation between 0.1 and 0.3
		var saturation = (random.nextInt(2000) + 1000) / 10000f;
		var luminance = 0.9f;
		var color = Color.getHSBColor(hue, saturation, luminance);
		return color
	}
	
		def generatePOM() {
		return '''
		<?xml version="1.0" encoding="UTF-8"?>
		<project xmlns="http://maven.apache.org/POM/4.0.0"
		         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		    <modelVersion>4.0.0</modelVersion>
		
		    <groupId>«projectName»</groupId>
		    <artifactId>«projectName»</artifactId>
		    <version>1.0-SNAPSHOT</version>
		    <packaging>jar</packaging>
		    <build>
		        <plugins>
		            <plugin>
		                <groupId>org.apache.maven.plugins</groupId>
		                <artifactId>maven-compiler-plugin</artifactId>
		                <configuration>
		                    <source>8</source>
		                    <target>8</target>
		                </configuration>
		            </plugin>
		        </plugins>
		    </build>
		
		
		    <repositories>
		        <repository>
		            <id>lhogie.i3s.maven.repo</id>
		            <name>Luc Hogie Maven repository</name>
		            <url>http://www.i3s.unice.fr/~hogie/maven_repository/</url>
		        </repository>
		    </repositories>
		
		    <dependencies>
		        <dependency>
		            <groupId>oscilloscup</groupId>
		            <artifactId>oscilloscup</artifactId>
		            <version>0.4.2</version>
		        </dependency>
		        <dependency>
		            <groupId>toools</groupId>
		            <artifactId>toools</artifactId>
		            <version>0.4.1</version>
		        </dependency>
		        <dependency>
		            <groupId>org.eclipse.emf</groupId>
		            <artifactId>org.eclipse.emf.ecore</artifactId>
		            <version>2.15.0</version>
		        </dependency>
		        <dependency>
		            <groupId>org.siani.javafmi</groupId>
		            <artifactId>fmu-wrapper</artifactId>
		            <version>LATEST</version>
		        </dependency>
		        <dependency>
		            <groupId>org.knowm.xchart</groupId>
		            <artifactId>xchart</artifactId>
		            <version>3.6.1</version>
		        </dependency>
		        <dependency>
		            <groupId>com.google.guava</groupId>
		            <artifactId>guava</artifactId>
		            <version>28.2-jre</version>
		        </dependency>
		        <dependency>
		            <groupId>org.awaitility</groupId>
		            <artifactId>awaitility</artifactId>
		            <version>4.0.3</version>
		            <scope>test</scope>
		        </dependency>
		        <dependency>
		            <groupId>org.zeromq</groupId>
		            <artifactId>jeromq</artifactId>
		            <version>0.5.2</version>
		        </dependency>
		
		        <dependency>
		            <groupId>com.fasterxml.jackson.core</groupId>
		            <artifactId>jackson-databind</artifactId>
		            <version>2.9.8</version>
		        </dependency>
		        <dependency>
		            <groupId>org.awaitility</groupId>
		            <artifactId>awaitility</artifactId>
		            <version>4.0.3</version>
		            <scope>compile</scope>
		        </dependency>
		        
	            <dependency>
	                <groupId>fr.inria.glose</groupId>
	                <artifactId>cosim20-java</artifactId>
	                <version>1.1</version>
	                <scope>system</scope>
	                <systemPath>${project.basedir}/../ext-lib/cosim20-java-1.1-SNAPSHOT.jar</systemPath>
	            </dependency>
		            
		    </dependencies>
		
		
		</project>
		'''
	}
	
	def generateDotProject() {
		return 
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<projectDescription>
			<name>CoSim20GeneratedSystem</name>
			<comment></comment>
			<projects>
			</projects>
			<buildSpec>
				<buildCommand>
					<name>org.eclipse.jdt.core.javabuilder</name>
					<arguments>
					</arguments>
				</buildCommand>
				<buildCommand>
					<name>org.eclipse.m2e.core.maven2Builder</name>
					<arguments>
					</arguments>
				</buildCommand>
			</buildSpec>
			<natures>
				<nature>org.eclipse.jdt.core.javanature</nature>
				<nature>org.eclipse.m2e.core.maven2Nature</nature>
			</natures>
		</projectDescription>
		'''
	}
	
	def generateDotClasspath() {
		return 
		'''
		<?xml version="1.0" encoding="UTF-8"?>
		<classpath>
			<classpathentry kind="src" output="target/classes" path="src/main/java">
				<attributes>
					<attribute name="optional" value="true"/>
					<attribute name="maven.pomderived" value="true"/>
				</attributes>
			</classpathentry>
			<classpathentry kind="src" output="target/test-classes" path="src/test/java">
				<attributes>
					<attribute name="optional" value="true"/>
					<attribute name="maven.pomderived" value="true"/>
					<attribute name="test" value="true"/>
				</attributes>
			</classpathentry>
			<classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.8">
				<attributes>
					<attribute name="maven.pomderived" value="true"/>
				</attributes>
			</classpathentry>
			<classpathentry kind="con" path="org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER">
				<attributes>
					<attribute name="maven.pomderived" value="true"/>
				</attributes>
			</classpathentry>
			<classpathentry kind="output" path="target/classes"/>
		</classpath>
		'''
	}
}
