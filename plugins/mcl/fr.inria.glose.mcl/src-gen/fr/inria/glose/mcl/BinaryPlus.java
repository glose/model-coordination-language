/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Plus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getBinaryPlus()
 * @model
 * @generated
 */
public interface BinaryPlus extends BinaryExpression {
} // BinaryPlus
