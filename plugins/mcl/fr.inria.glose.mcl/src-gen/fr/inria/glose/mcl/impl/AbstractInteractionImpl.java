/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.AbstractInteraction;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Interaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AbstractInteractionImpl extends MinimalEObjectImpl.Container implements AbstractInteraction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractInteractionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.ABSTRACT_INTERACTION;
	}

} //AbstractInteractionImpl
