/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.BinaryMinus;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BinaryMinusImpl extends BinaryExpressionImpl implements BinaryMinus {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryMinusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.BINARY_MINUS;
	}

} //BinaryMinusImpl
