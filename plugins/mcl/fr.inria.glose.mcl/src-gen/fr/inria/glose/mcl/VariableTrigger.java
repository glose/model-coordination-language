/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.VariableTrigger#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getVariableTrigger()
 * @model abstract="true"
 * @generated
 */
public interface VariableTrigger extends TriggeringCondition {
	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference.
	 * @see #setPort(PortRef)
	 * @see fr.inria.glose.mcl.MclPackage#getVariableTrigger_Port()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PortRef getPort();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.VariableTrigger#getPort <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' containment reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(PortRef value);

} // VariableTrigger
