/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.BinaryPlus;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Plus</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BinaryPlusImpl extends BinaryExpressionImpl implements BinaryPlus {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryPlusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.BINARY_PLUS;
	}

} //BinaryPlusImpl
