/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Sync</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getExternalSync()
 * @model abstract="true"
 * @generated
 */
public interface ExternalSync extends SynchronizationConstraint {
} // ExternalSync
