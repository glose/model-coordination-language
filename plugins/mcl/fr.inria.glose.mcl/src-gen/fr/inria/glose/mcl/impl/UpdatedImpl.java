/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.Updated;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Updated</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UpdatedImpl extends VariableTriggerImpl implements Updated {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdatedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.UPDATED;
	}

} //UpdatedImpl
