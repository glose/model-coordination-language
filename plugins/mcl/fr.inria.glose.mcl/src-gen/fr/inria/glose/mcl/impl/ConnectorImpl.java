/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.AbstractInteraction;
import fr.inria.glose.mcl.Connector;
import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.PortRef;
import fr.inria.glose.mcl.SynchronizationConstraint;
import fr.inria.glose.mcl.TCExpression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.ConnectorImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ConnectorImpl#getSynchronizationRule <em>Synchronization Rule</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ConnectorImpl#getInteractionstatement <em>Interactionstatement</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ConnectorImpl#getFrom <em>From</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ConnectorImpl#getTo <em>To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectorImpl extends NamedElementImpl implements Connector {
	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected TCExpression trigger;

	/**
	 * The cached value of the '{@link #getSynchronizationRule() <em>Synchronization Rule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronizationRule()
	 * @generated
	 * @ordered
	 */
	protected SynchronizationConstraint synchronizationRule;

	/**
	 * The cached value of the '{@link #getInteractionstatement() <em>Interactionstatement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionstatement()
	 * @generated
	 * @ordered
	 */
	protected AbstractInteraction interactionstatement;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected PortRef from;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected PortRef to;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TCExpression getTrigger() {
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTrigger(TCExpression newTrigger, NotificationChain msgs) {
		TCExpression oldTrigger = trigger;
		trigger = newTrigger;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.CONNECTOR__TRIGGER, oldTrigger, newTrigger);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTrigger(TCExpression newTrigger) {
		if (newTrigger != trigger) {
			NotificationChain msgs = null;
			if (trigger != null)
				msgs = ((InternalEObject) trigger).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__TRIGGER, null, msgs);
			if (newTrigger != null)
				msgs = ((InternalEObject) newTrigger).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__TRIGGER, null, msgs);
			msgs = basicSetTrigger(newTrigger, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__TRIGGER, newTrigger,
					newTrigger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SynchronizationConstraint getSynchronizationRule() {
		return synchronizationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSynchronizationRule(SynchronizationConstraint newSynchronizationRule,
			NotificationChain msgs) {
		SynchronizationConstraint oldSynchronizationRule = synchronizationRule;
		synchronizationRule = newSynchronizationRule;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.CONNECTOR__SYNCHRONIZATION_RULE, oldSynchronizationRule, newSynchronizationRule);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSynchronizationRule(SynchronizationConstraint newSynchronizationRule) {
		if (newSynchronizationRule != synchronizationRule) {
			NotificationChain msgs = null;
			if (synchronizationRule != null)
				msgs = ((InternalEObject) synchronizationRule).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__SYNCHRONIZATION_RULE, null, msgs);
			if (newSynchronizationRule != null)
				msgs = ((InternalEObject) newSynchronizationRule).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__SYNCHRONIZATION_RULE, null, msgs);
			msgs = basicSetSynchronizationRule(newSynchronizationRule, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__SYNCHRONIZATION_RULE,
					newSynchronizationRule, newSynchronizationRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractInteraction getInteractionstatement() {
		return interactionstatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInteractionstatement(AbstractInteraction newInteractionstatement,
			NotificationChain msgs) {
		AbstractInteraction oldInteractionstatement = interactionstatement;
		interactionstatement = newInteractionstatement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.CONNECTOR__INTERACTIONSTATEMENT, oldInteractionstatement, newInteractionstatement);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInteractionstatement(AbstractInteraction newInteractionstatement) {
		if (newInteractionstatement != interactionstatement) {
			NotificationChain msgs = null;
			if (interactionstatement != null)
				msgs = ((InternalEObject) interactionstatement).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__INTERACTIONSTATEMENT, null, msgs);
			if (newInteractionstatement != null)
				msgs = ((InternalEObject) newInteractionstatement).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__INTERACTIONSTATEMENT, null, msgs);
			msgs = basicSetInteractionstatement(newInteractionstatement, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__INTERACTIONSTATEMENT,
					newInteractionstatement, newInteractionstatement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PortRef getFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrom(PortRef newFrom, NotificationChain msgs) {
		PortRef oldFrom = from;
		from = newFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__FROM,
					oldFrom, newFrom);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrom(PortRef newFrom) {
		if (newFrom != from) {
			NotificationChain msgs = null;
			if (from != null)
				msgs = ((InternalEObject) from).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__FROM, null, msgs);
			if (newFrom != null)
				msgs = ((InternalEObject) newFrom).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__FROM, null, msgs);
			msgs = basicSetFrom(newFrom, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__FROM, newFrom, newFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PortRef getTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTo(PortRef newTo, NotificationChain msgs) {
		PortRef oldTo = to;
		to = newTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__TO,
					oldTo, newTo);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTo(PortRef newTo) {
		if (newTo != to) {
			NotificationChain msgs = null;
			if (to != null)
				msgs = ((InternalEObject) to).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__TO,
						null, msgs);
			if (newTo != null)
				msgs = ((InternalEObject) newTo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MclPackage.CONNECTOR__TO,
						null, msgs);
			msgs = basicSetTo(newTo, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.CONNECTOR__TO, newTo, newTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MclPackage.CONNECTOR__TRIGGER:
			return basicSetTrigger(null, msgs);
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
			return basicSetSynchronizationRule(null, msgs);
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
			return basicSetInteractionstatement(null, msgs);
		case MclPackage.CONNECTOR__FROM:
			return basicSetFrom(null, msgs);
		case MclPackage.CONNECTOR__TO:
			return basicSetTo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.CONNECTOR__TRIGGER:
			return getTrigger();
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
			return getSynchronizationRule();
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
			return getInteractionstatement();
		case MclPackage.CONNECTOR__FROM:
			return getFrom();
		case MclPackage.CONNECTOR__TO:
			return getTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.CONNECTOR__TRIGGER:
			setTrigger((TCExpression) newValue);
			return;
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
			setSynchronizationRule((SynchronizationConstraint) newValue);
			return;
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
			setInteractionstatement((AbstractInteraction) newValue);
			return;
		case MclPackage.CONNECTOR__FROM:
			setFrom((PortRef) newValue);
			return;
		case MclPackage.CONNECTOR__TO:
			setTo((PortRef) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.CONNECTOR__TRIGGER:
			setTrigger((TCExpression) null);
			return;
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
			setSynchronizationRule((SynchronizationConstraint) null);
			return;
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
			setInteractionstatement((AbstractInteraction) null);
			return;
		case MclPackage.CONNECTOR__FROM:
			setFrom((PortRef) null);
			return;
		case MclPackage.CONNECTOR__TO:
			setTo((PortRef) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.CONNECTOR__TRIGGER:
			return trigger != null;
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
			return synchronizationRule != null;
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
			return interactionstatement != null;
		case MclPackage.CONNECTOR__FROM:
			return from != null;
		case MclPackage.CONNECTOR__TO:
			return to != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectorImpl
