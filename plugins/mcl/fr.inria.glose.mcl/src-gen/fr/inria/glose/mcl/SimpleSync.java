/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Sync</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.SimpleSync#getTolerance <em>Tolerance</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getSimpleSync()
 * @model
 * @generated
 */
public interface SimpleSync extends SynchronizationConstraint {
	/**
	 * Returns the value of the '<em><b>Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tolerance</em>' attribute.
	 * @see #setTolerance(double)
	 * @see fr.inria.glose.mcl.MclPackage#getSimpleSync_Tolerance()
	 * @model
	 * @generated
	 */
	double getTolerance();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.SimpleSync#getTolerance <em>Tolerance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tolerance</em>' attribute.
	 * @see #getTolerance()
	 * @generated
	 */
	void setTolerance(double value);

} // SimpleSync
