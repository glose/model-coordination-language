/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mbilang.Port;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.PortRef;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.PortRefImpl#getRefPort <em>Ref Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortRefImpl extends AssignmentExpressionImpl implements PortRef {
	/**
	 * The cached value of the '{@link #getRefPort() <em>Ref Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefPort()
	 * @generated
	 * @ordered
	 */
	protected Port refPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortRefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.PORT_REF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Port getRefPort() {
		if (refPort != null && refPort.eIsProxy()) {
			InternalEObject oldRefPort = (InternalEObject) refPort;
			refPort = (Port) eResolveProxy(oldRefPort);
			if (refPort != oldRefPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MclPackage.PORT_REF__REF_PORT, oldRefPort,
							refPort));
			}
		}
		return refPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetRefPort() {
		return refPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRefPort(Port newRefPort) {
		Port oldRefPort = refPort;
		refPort = newRefPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.PORT_REF__REF_PORT, oldRefPort, refPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.PORT_REF__REF_PORT:
			if (resolve)
				return getRefPort();
			return basicGetRefPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.PORT_REF__REF_PORT:
			setRefPort((Port) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.PORT_REF__REF_PORT:
			setRefPort((Port) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.PORT_REF__REF_PORT:
			return refPort != null;
		}
		return super.eIsSet(featureID);
	}

} //PortRefImpl
