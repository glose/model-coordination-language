/**
 */
package fr.inria.glose.mcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exp Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getExpOperation()
 * @model abstract="true"
 * @generated
 */
public interface ExpOperation extends EObject {
} // ExpOperation
