/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.Connector;
import fr.inria.glose.mcl.ImportInterfaceStatement;
import fr.inria.glose.mcl.MCSpecification;
import fr.inria.glose.mcl.MclPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MC Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.MCSpecificationImpl#getOwnedConnectors <em>Owned Connectors</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.MCSpecificationImpl#getImportedMBI <em>Imported MBI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MCSpecificationImpl extends MinimalEObjectImpl.Container implements MCSpecification {
	/**
	 * The cached value of the '{@link #getOwnedConnectors() <em>Owned Connectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<Connector> ownedConnectors;

	/**
	 * The cached value of the '{@link #getImportedMBI() <em>Imported MBI</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportedMBI()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportInterfaceStatement> importedMBI;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.MC_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Connector> getOwnedConnectors() {
		if (ownedConnectors == null) {
			ownedConnectors = new EObjectContainmentEList<Connector>(Connector.class, this,
					MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS);
		}
		return ownedConnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ImportInterfaceStatement> getImportedMBI() {
		if (importedMBI == null) {
			importedMBI = new EObjectContainmentEList<ImportInterfaceStatement>(ImportInterfaceStatement.class, this,
					MclPackage.MC_SPECIFICATION__IMPORTED_MBI);
		}
		return importedMBI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS:
			return ((InternalEList<?>) getOwnedConnectors()).basicRemove(otherEnd, msgs);
		case MclPackage.MC_SPECIFICATION__IMPORTED_MBI:
			return ((InternalEList<?>) getImportedMBI()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS:
			return getOwnedConnectors();
		case MclPackage.MC_SPECIFICATION__IMPORTED_MBI:
			return getImportedMBI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS:
			getOwnedConnectors().clear();
			getOwnedConnectors().addAll((Collection<? extends Connector>) newValue);
			return;
		case MclPackage.MC_SPECIFICATION__IMPORTED_MBI:
			getImportedMBI().clear();
			getImportedMBI().addAll((Collection<? extends ImportInterfaceStatement>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS:
			getOwnedConnectors().clear();
			return;
		case MclPackage.MC_SPECIFICATION__IMPORTED_MBI:
			getImportedMBI().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.MC_SPECIFICATION__OWNED_CONNECTORS:
			return ownedConnectors != null && !ownedConnectors.isEmpty();
		case MclPackage.MC_SPECIFICATION__IMPORTED_MBI:
			return importedMBI != null && !importedMBI.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MCSpecificationImpl
