/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MclFactoryImpl extends EFactoryImpl implements MclFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MclFactory init() {
		try {
			MclFactory theMclFactory = (MclFactory) EPackage.Registry.INSTANCE.getEFactory(MclPackage.eNS_URI);
			if (theMclFactory != null) {
				return theMclFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MclFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MclFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case MclPackage.MC_SPECIFICATION:
			return createMCSpecification();
		case MclPackage.CONNECTOR:
			return createConnector();
		case MclPackage.IMPORT_INTERFACE_STATEMENT:
			return createImportInterfaceStatement();
		case MclPackage.THRESHOLD_EXPRESSION:
			return createThresholdExpression();
		case MclPackage.GREATER:
			return createGreater();
		case MclPackage.LESSER:
			return createLesser();
		case MclPackage.BINARY_EXP_OPERATOR:
			return createBinaryExpOperator();
		case MclPackage.VARIABLE:
			return createVariable();
		case MclPackage.PERIODIC_EXPRESSION:
			return createPeriodicExpression();
		case MclPackage.SIMPLE_SYNC:
			return createSimpleSync();
		case MclPackage.UPDATED:
			return createUpdated();
		case MclPackage.READY_TO_READ:
			return createReadyToRead();
		case MclPackage.EVENT_TRIGGER:
			return createEventTrigger();
		case MclPackage.NOT:
			return createNot();
		case MclPackage.UNARY_MINUS:
			return createUnaryMinus();
		case MclPackage.ASSIGNMENT:
			return createAssignment();
		case MclPackage.BINARY_PLUS:
			return createBinaryPlus();
		case MclPackage.BINARY_MINUS:
			return createBinaryMinus();
		case MclPackage.PORT_REF:
			return createPortRef();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case MclPackage.BINARY_OPERATOR:
			return createBinaryOperatorFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case MclPackage.BINARY_OPERATOR:
			return convertBinaryOperatorToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MCSpecification createMCSpecification() {
		MCSpecificationImpl mcSpecification = new MCSpecificationImpl();
		return mcSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Connector createConnector() {
		ConnectorImpl connector = new ConnectorImpl();
		return connector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ImportInterfaceStatement createImportInterfaceStatement() {
		ImportInterfaceStatementImpl importInterfaceStatement = new ImportInterfaceStatementImpl();
		return importInterfaceStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ThresholdExpression createThresholdExpression() {
		ThresholdExpressionImpl thresholdExpression = new ThresholdExpressionImpl();
		return thresholdExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Greater createGreater() {
		GreaterImpl greater = new GreaterImpl();
		return greater;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Lesser createLesser() {
		LesserImpl lesser = new LesserImpl();
		return lesser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryExpOperator createBinaryExpOperator() {
		BinaryExpOperatorImpl binaryExpOperator = new BinaryExpOperatorImpl();
		return binaryExpOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Variable createVariable() {
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PeriodicExpression createPeriodicExpression() {
		PeriodicExpressionImpl periodicExpression = new PeriodicExpressionImpl();
		return periodicExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SimpleSync createSimpleSync() {
		SimpleSyncImpl simpleSync = new SimpleSyncImpl();
		return simpleSync;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Updated createUpdated() {
		UpdatedImpl updated = new UpdatedImpl();
		return updated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReadyToRead createReadyToRead() {
		ReadyToReadImpl readyToRead = new ReadyToReadImpl();
		return readyToRead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventTrigger createEventTrigger() {
		EventTriggerImpl eventTrigger = new EventTriggerImpl();
		return eventTrigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Not createNot() {
		NotImpl not = new NotImpl();
		return not;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UnaryMinus createUnaryMinus() {
		UnaryMinusImpl unaryMinus = new UnaryMinusImpl();
		return unaryMinus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Assignment createAssignment() {
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryPlus createBinaryPlus() {
		BinaryPlusImpl binaryPlus = new BinaryPlusImpl();
		return binaryPlus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryMinus createBinaryMinus() {
		BinaryMinusImpl binaryMinus = new BinaryMinusImpl();
		return binaryMinus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PortRef createPortRef() {
		PortRefImpl portRef = new PortRefImpl();
		return portRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperator createBinaryOperatorFromString(EDataType eDataType, String initialValue) {
		BinaryOperator result = BinaryOperator.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBinaryOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MclPackage getMclPackage() {
		return (MclPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MclPackage getPackage() {
		return MclPackage.eINSTANCE;
	}

} //MclFactoryImpl
