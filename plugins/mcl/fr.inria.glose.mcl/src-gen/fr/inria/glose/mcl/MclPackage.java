/**
 */
package fr.inria.glose.mcl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.inria.glose.mcl.MclFactory
 * @model kind="package"
 * @generated
 */
public interface MclPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mcl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.inria.fr/glose/mcl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mcl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MclPackage eINSTANCE = fr.inria.glose.mcl.impl.MclPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.MCSpecificationImpl <em>MC Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.MCSpecificationImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getMCSpecification()
	 * @generated
	 */
	int MC_SPECIFICATION = 0;

	/**
	 * The feature id for the '<em><b>Owned Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MC_SPECIFICATION__OWNED_CONNECTORS = 0;

	/**
	 * The feature id for the '<em><b>Imported MBI</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MC_SPECIFICATION__IMPORTED_MBI = 1;

	/**
	 * The number of structural features of the '<em>MC Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MC_SPECIFICATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>MC Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MC_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.TCExpressionImpl <em>TC Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.TCExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getTCExpression()
	 * @generated
	 */
	int TC_EXPRESSION = 1;

	/**
	 * The number of structural features of the '<em>TC Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TC_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>TC Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TC_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.SynchronizationConstraintImpl <em>Synchronization Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.SynchronizationConstraintImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getSynchronizationConstraint()
	 * @generated
	 */
	int SYNCHRONIZATION_CONSTRAINT = 2;

	/**
	 * The feature id for the '<em><b>Right Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE = 0;

	/**
	 * The feature id for the '<em><b>Left Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE = 1;

	/**
	 * The number of structural features of the '<em>Synchronization Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_CONSTRAINT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Synchronization Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCHRONIZATION_CONSTRAINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.NamedElementImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ConnectorImpl <em>Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ConnectorImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getConnector()
	 * @generated
	 */
	int CONNECTOR = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__TRIGGER = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Synchronization Rule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__SYNCHRONIZATION_RULE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interactionstatement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__INTERACTIONSTATEMENT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__FROM = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__TO = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ImportInterfaceStatementImpl <em>Import Interface Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ImportInterfaceStatementImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getImportInterfaceStatement()
	 * @generated
	 */
	int IMPORT_INTERFACE_STATEMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_INTERFACE_STATEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_INTERFACE_STATEMENT__IMPORT_URI = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Import Interface Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_INTERFACE_STATEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Import Interface Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_INTERFACE_STATEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.TriggeringConditionImpl <em>Triggering Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.TriggeringConditionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getTriggeringCondition()
	 * @generated
	 */
	int TRIGGERING_CONDITION = 15;

	/**
	 * The number of structural features of the '<em>Triggering Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERING_CONDITION_FEATURE_COUNT = TC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Triggering Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERING_CONDITION_OPERATION_COUNT = TC_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ThresholdExpressionImpl <em>Threshold Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ThresholdExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getThresholdExpression()
	 * @generated
	 */
	int THRESHOLD_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Setoperation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_EXPRESSION__SETOPERATION = TRIGGERING_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_EXPRESSION__VALUE = TRIGGERING_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Trigger Source Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT = TRIGGERING_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Threshold Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_EXPRESSION_FEATURE_COUNT = TRIGGERING_CONDITION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Threshold Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THRESHOLD_EXPRESSION_OPERATION_COUNT = TRIGGERING_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ExpOperationImpl <em>Exp Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ExpOperationImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getExpOperation()
	 * @generated
	 */
	int EXP_OPERATION = 7;

	/**
	 * The number of structural features of the '<em>Exp Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXP_OPERATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Exp Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXP_OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.GreaterImpl <em>Greater</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.GreaterImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getGreater()
	 * @generated
	 */
	int GREATER = 8;

	/**
	 * The number of structural features of the '<em>Greater</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_FEATURE_COUNT = EXP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Greater</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_OPERATION_COUNT = EXP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.LesserImpl <em>Lesser</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.LesserImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getLesser()
	 * @generated
	 */
	int LESSER = 9;

	/**
	 * The number of structural features of the '<em>Lesser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESSER_FEATURE_COUNT = EXP_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lesser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESSER_OPERATION_COUNT = EXP_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.BinaryExpImpl <em>Binary Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.BinaryExpImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExp()
	 * @generated
	 */
	int BINARY_EXP = 10;

	/**
	 * The feature id for the '<em><b>Left Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP__LEFT_OP = TC_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP__RIGHT_OP = TC_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_FEATURE_COUNT = TC_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Binary Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATION_COUNT = TC_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.BinaryExpOperatorImpl <em>Binary Exp Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.BinaryExpOperatorImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExpOperator()
	 * @generated
	 */
	int BINARY_EXP_OPERATOR = 11;

	/**
	 * The feature id for the '<em><b>Left Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATOR__LEFT_OP = BINARY_EXP__LEFT_OP;

	/**
	 * The feature id for the '<em><b>Right Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATOR__RIGHT_OP = BINARY_EXP__RIGHT_OP;

	/**
	 * The feature id for the '<em><b>Binary Exp Actual Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR = BINARY_EXP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Binary Exp Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATOR_FEATURE_COUNT = BINARY_EXP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Binary Exp Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXP_OPERATOR_OPERATION_COUNT = BINARY_EXP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.VariableImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 12;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.VariableTriggerImpl <em>Variable Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.VariableTriggerImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getVariableTrigger()
	 * @generated
	 */
	int VARIABLE_TRIGGER = 13;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TRIGGER__PORT = TRIGGERING_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TRIGGER_FEATURE_COUNT = TRIGGERING_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Variable Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TRIGGER_OPERATION_COUNT = TRIGGERING_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.PeriodicExpressionImpl <em>Periodic Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.PeriodicExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getPeriodicExpression()
	 * @generated
	 */
	int PERIODIC_EXPRESSION = 14;

	/**
	 * The feature id for the '<em><b>Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXPRESSION__TEMPORAL_REFERENCE = TRIGGERING_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXPRESSION__VALUE = TRIGGERING_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Periodic Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXPRESSION_FEATURE_COUNT = TRIGGERING_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Periodic Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXPRESSION_OPERATION_COUNT = TRIGGERING_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.SimpleSyncImpl <em>Simple Sync</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.SimpleSyncImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getSimpleSync()
	 * @generated
	 */
	int SIMPLE_SYNC = 16;

	/**
	 * The feature id for the '<em><b>Right Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SYNC__RIGHT_TEMPORAL_REFERENCE = SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE;

	/**
	 * The feature id for the '<em><b>Left Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SYNC__LEFT_TEMPORAL_REFERENCE = SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE;

	/**
	 * The feature id for the '<em><b>Tolerance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SYNC__TOLERANCE = SYNCHRONIZATION_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple Sync</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SYNC_FEATURE_COUNT = SYNCHRONIZATION_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple Sync</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_SYNC_OPERATION_COUNT = SYNCHRONIZATION_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ExternalSyncImpl <em>External Sync</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ExternalSyncImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getExternalSync()
	 * @generated
	 */
	int EXTERNAL_SYNC = 17;

	/**
	 * The feature id for the '<em><b>Right Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SYNC__RIGHT_TEMPORAL_REFERENCE = SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE;

	/**
	 * The feature id for the '<em><b>Left Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SYNC__LEFT_TEMPORAL_REFERENCE = SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE;

	/**
	 * The number of structural features of the '<em>External Sync</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SYNC_FEATURE_COUNT = SYNCHRONIZATION_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>External Sync</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SYNC_OPERATION_COUNT = SYNCHRONIZATION_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.UpdatedImpl <em>Updated</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.UpdatedImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUpdated()
	 * @generated
	 */
	int UPDATED = 18;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED__PORT = VARIABLE_TRIGGER__PORT;

	/**
	 * The number of structural features of the '<em>Updated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED_FEATURE_COUNT = VARIABLE_TRIGGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Updated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPDATED_OPERATION_COUNT = VARIABLE_TRIGGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.ReadyToReadImpl <em>Ready To Read</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.ReadyToReadImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getReadyToRead()
	 * @generated
	 */
	int READY_TO_READ = 19;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ__PORT = VARIABLE_TRIGGER__PORT;

	/**
	 * The number of structural features of the '<em>Ready To Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ_FEATURE_COUNT = VARIABLE_TRIGGER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ready To Read</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_TO_READ_OPERATION_COUNT = VARIABLE_TRIGGER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.EventTriggerImpl <em>Event Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.EventTriggerImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getEventTrigger()
	 * @generated
	 */
	int EVENT_TRIGGER = 20;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER__PORT = TRIGGERING_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Event Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_FEATURE_COUNT = TRIGGERING_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Event Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_OPERATION_COUNT = TRIGGERING_CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.AbstractInteractionImpl <em>Abstract Interaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.AbstractInteractionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAbstractInteraction()
	 * @generated
	 */
	int ABSTRACT_INTERACTION = 30;

	/**
	 * The number of structural features of the '<em>Abstract Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_INTERACTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Abstract Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_INTERACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.AssignmentExpressionImpl <em>Assignment Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.AssignmentExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAssignmentExpression()
	 * @generated
	 */
	int ASSIGNMENT_EXPRESSION = 21;

	/**
	 * The number of structural features of the '<em>Assignment Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_EXPRESSION_FEATURE_COUNT = ABSTRACT_INTERACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Assignment Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_EXPRESSION_OPERATION_COUNT = ABSTRACT_INTERACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.UnaryExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 22;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERAND = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_OPERATION_COUNT = ASSIGNMENT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.BinaryExpressionImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 23;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LEFT_OPERAND = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RIGHT_OPERAND = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_OPERATION_COUNT = ASSIGNMENT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.NotImpl <em>Not</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.NotImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getNot()
	 * @generated
	 */
	int NOT = 24;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__OPERAND = UNARY_EXPRESSION__OPERAND;

	/**
	 * The number of structural features of the '<em>Not</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_FEATURE_COUNT = UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Not</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_OPERATION_COUNT = UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.UnaryMinusImpl <em>Unary Minus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.UnaryMinusImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUnaryMinus()
	 * @generated
	 */
	int UNARY_MINUS = 25;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_MINUS__OPERAND = UNARY_EXPRESSION__OPERAND;

	/**
	 * The number of structural features of the '<em>Unary Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_MINUS_FEATURE_COUNT = UNARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Unary Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_MINUS_OPERATION_COUNT = UNARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.AssignmentImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 26;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__LEFT_OPERAND = BINARY_EXPRESSION__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__RIGHT_OPERAND = BINARY_EXPRESSION__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.BinaryPlusImpl <em>Binary Plus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.BinaryPlusImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryPlus()
	 * @generated
	 */
	int BINARY_PLUS = 27;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PLUS__LEFT_OPERAND = BINARY_EXPRESSION__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PLUS__RIGHT_OPERAND = BINARY_EXPRESSION__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Binary Plus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PLUS_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Binary Plus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PLUS_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.BinaryMinusImpl <em>Binary Minus</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.BinaryMinusImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryMinus()
	 * @generated
	 */
	int BINARY_MINUS = 28;

	/**
	 * The feature id for the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_MINUS__LEFT_OPERAND = BINARY_EXPRESSION__LEFT_OPERAND;

	/**
	 * The feature id for the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_MINUS__RIGHT_OPERAND = BINARY_EXPRESSION__RIGHT_OPERAND;

	/**
	 * The number of structural features of the '<em>Binary Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_MINUS_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Binary Minus</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_MINUS_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.impl.PortRefImpl <em>Port Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.impl.PortRefImpl
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getPortRef()
	 * @generated
	 */
	int PORT_REF = 29;

	/**
	 * The feature id for the '<em><b>Ref Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REF__REF_PORT = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REF_FEATURE_COUNT = ASSIGNMENT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Port Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_REF_OPERATION_COUNT = ASSIGNMENT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.inria.glose.mcl.BinaryOperator <em>Binary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.inria.glose.mcl.BinaryOperator
	 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryOperator()
	 * @generated
	 */
	int BINARY_OPERATOR = 31;

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.MCSpecification <em>MC Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MC Specification</em>'.
	 * @see fr.inria.glose.mcl.MCSpecification
	 * @generated
	 */
	EClass getMCSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mcl.MCSpecification#getOwnedConnectors <em>Owned Connectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Connectors</em>'.
	 * @see fr.inria.glose.mcl.MCSpecification#getOwnedConnectors()
	 * @see #getMCSpecification()
	 * @generated
	 */
	EReference getMCSpecification_OwnedConnectors();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.inria.glose.mcl.MCSpecification#getImportedMBI <em>Imported MBI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imported MBI</em>'.
	 * @see fr.inria.glose.mcl.MCSpecification#getImportedMBI()
	 * @see #getMCSpecification()
	 * @generated
	 */
	EReference getMCSpecification_ImportedMBI();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.TCExpression <em>TC Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TC Expression</em>'.
	 * @see fr.inria.glose.mcl.TCExpression
	 * @generated
	 */
	EClass getTCExpression();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.SynchronizationConstraint <em>Synchronization Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synchronization Constraint</em>'.
	 * @see fr.inria.glose.mcl.SynchronizationConstraint
	 * @generated
	 */
	EClass getSynchronizationConstraint();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mcl.SynchronizationConstraint#getRightTemporalReference <em>Right Temporal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Right Temporal Reference</em>'.
	 * @see fr.inria.glose.mcl.SynchronizationConstraint#getRightTemporalReference()
	 * @see #getSynchronizationConstraint()
	 * @generated
	 */
	EReference getSynchronizationConstraint_RightTemporalReference();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mcl.SynchronizationConstraint#getLeftTemporalReference <em>Left Temporal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Left Temporal Reference</em>'.
	 * @see fr.inria.glose.mcl.SynchronizationConstraint#getLeftTemporalReference()
	 * @see #getSynchronizationConstraint()
	 * @generated
	 */
	EReference getSynchronizationConstraint_LeftTemporalReference();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Connector <em>Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connector</em>'.
	 * @see fr.inria.glose.mcl.Connector
	 * @generated
	 */
	EClass getConnector();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.Connector#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Trigger</em>'.
	 * @see fr.inria.glose.mcl.Connector#getTrigger()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_Trigger();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.Connector#getSynchronizationRule <em>Synchronization Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Synchronization Rule</em>'.
	 * @see fr.inria.glose.mcl.Connector#getSynchronizationRule()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_SynchronizationRule();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.Connector#getInteractionstatement <em>Interactionstatement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Interactionstatement</em>'.
	 * @see fr.inria.glose.mcl.Connector#getInteractionstatement()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_Interactionstatement();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.Connector#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>From</em>'.
	 * @see fr.inria.glose.mcl.Connector#getFrom()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_From();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.Connector#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>To</em>'.
	 * @see fr.inria.glose.mcl.Connector#getTo()
	 * @see #getConnector()
	 * @generated
	 */
	EReference getConnector_To();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.inria.glose.mcl.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.inria.glose.mcl.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.ImportInterfaceStatement <em>Import Interface Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Interface Statement</em>'.
	 * @see fr.inria.glose.mcl.ImportInterfaceStatement
	 * @generated
	 */
	EClass getImportInterfaceStatement();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.ImportInterfaceStatement#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see fr.inria.glose.mcl.ImportInterfaceStatement#getImportURI()
	 * @see #getImportInterfaceStatement()
	 * @generated
	 */
	EAttribute getImportInterfaceStatement_ImportURI();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.ThresholdExpression <em>Threshold Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Threshold Expression</em>'.
	 * @see fr.inria.glose.mcl.ThresholdExpression
	 * @generated
	 */
	EClass getThresholdExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.ThresholdExpression#getSetoperation <em>Setoperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Setoperation</em>'.
	 * @see fr.inria.glose.mcl.ThresholdExpression#getSetoperation()
	 * @see #getThresholdExpression()
	 * @generated
	 */
	EReference getThresholdExpression_Setoperation();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.ThresholdExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see fr.inria.glose.mcl.ThresholdExpression#getValue()
	 * @see #getThresholdExpression()
	 * @generated
	 */
	EReference getThresholdExpression_Value();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.ThresholdExpression#getTriggerSourcePort <em>Trigger Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Trigger Source Port</em>'.
	 * @see fr.inria.glose.mcl.ThresholdExpression#getTriggerSourcePort()
	 * @see #getThresholdExpression()
	 * @generated
	 */
	EReference getThresholdExpression_TriggerSourcePort();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.ExpOperation <em>Exp Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exp Operation</em>'.
	 * @see fr.inria.glose.mcl.ExpOperation
	 * @generated
	 */
	EClass getExpOperation();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Greater <em>Greater</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Greater</em>'.
	 * @see fr.inria.glose.mcl.Greater
	 * @generated
	 */
	EClass getGreater();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Lesser <em>Lesser</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lesser</em>'.
	 * @see fr.inria.glose.mcl.Lesser
	 * @generated
	 */
	EClass getLesser();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.BinaryExp <em>Binary Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Exp</em>'.
	 * @see fr.inria.glose.mcl.BinaryExp
	 * @generated
	 */
	EClass getBinaryExp();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.BinaryExp#getLeftOp <em>Left Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Op</em>'.
	 * @see fr.inria.glose.mcl.BinaryExp#getLeftOp()
	 * @see #getBinaryExp()
	 * @generated
	 */
	EReference getBinaryExp_LeftOp();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.BinaryExp#getRightOp <em>Right Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Op</em>'.
	 * @see fr.inria.glose.mcl.BinaryExp#getRightOp()
	 * @see #getBinaryExp()
	 * @generated
	 */
	EReference getBinaryExp_RightOp();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.BinaryExpOperator <em>Binary Exp Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Exp Operator</em>'.
	 * @see fr.inria.glose.mcl.BinaryExpOperator
	 * @generated
	 */
	EClass getBinaryExpOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.BinaryExpOperator#getBinaryExpActualOperator <em>Binary Exp Actual Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Binary Exp Actual Operator</em>'.
	 * @see fr.inria.glose.mcl.BinaryExpOperator#getBinaryExpActualOperator()
	 * @see #getBinaryExpOperator()
	 * @generated
	 */
	EAttribute getBinaryExpOperator_BinaryExpActualOperator();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see fr.inria.glose.mcl.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.Variable#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.glose.mcl.Variable#getValue()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.VariableTrigger <em>Variable Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Trigger</em>'.
	 * @see fr.inria.glose.mcl.VariableTrigger
	 * @generated
	 */
	EClass getVariableTrigger();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.VariableTrigger#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see fr.inria.glose.mcl.VariableTrigger#getPort()
	 * @see #getVariableTrigger()
	 * @generated
	 */
	EReference getVariableTrigger_Port();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.PeriodicExpression <em>Periodic Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Periodic Expression</em>'.
	 * @see fr.inria.glose.mcl.PeriodicExpression
	 * @generated
	 */
	EClass getPeriodicExpression();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mcl.PeriodicExpression#getTemporalReference <em>Temporal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Temporal Reference</em>'.
	 * @see fr.inria.glose.mcl.PeriodicExpression#getTemporalReference()
	 * @see #getPeriodicExpression()
	 * @generated
	 */
	EReference getPeriodicExpression_TemporalReference();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.PeriodicExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.inria.glose.mcl.PeriodicExpression#getValue()
	 * @see #getPeriodicExpression()
	 * @generated
	 */
	EAttribute getPeriodicExpression_Value();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.TriggeringCondition <em>Triggering Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triggering Condition</em>'.
	 * @see fr.inria.glose.mcl.TriggeringCondition
	 * @generated
	 */
	EClass getTriggeringCondition();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.SimpleSync <em>Simple Sync</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Sync</em>'.
	 * @see fr.inria.glose.mcl.SimpleSync
	 * @generated
	 */
	EClass getSimpleSync();

	/**
	 * Returns the meta object for the attribute '{@link fr.inria.glose.mcl.SimpleSync#getTolerance <em>Tolerance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tolerance</em>'.
	 * @see fr.inria.glose.mcl.SimpleSync#getTolerance()
	 * @see #getSimpleSync()
	 * @generated
	 */
	EAttribute getSimpleSync_Tolerance();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.ExternalSync <em>External Sync</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Sync</em>'.
	 * @see fr.inria.glose.mcl.ExternalSync
	 * @generated
	 */
	EClass getExternalSync();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Updated <em>Updated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Updated</em>'.
	 * @see fr.inria.glose.mcl.Updated
	 * @generated
	 */
	EClass getUpdated();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.ReadyToRead <em>Ready To Read</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ready To Read</em>'.
	 * @see fr.inria.glose.mcl.ReadyToRead
	 * @generated
	 */
	EClass getReadyToRead();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.EventTrigger <em>Event Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Trigger</em>'.
	 * @see fr.inria.glose.mcl.EventTrigger
	 * @generated
	 */
	EClass getEventTrigger();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.EventTrigger#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see fr.inria.glose.mcl.EventTrigger#getPort()
	 * @see #getEventTrigger()
	 * @generated
	 */
	EReference getEventTrigger_Port();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.AssignmentExpression <em>Assignment Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment Expression</em>'.
	 * @see fr.inria.glose.mcl.AssignmentExpression
	 * @generated
	 */
	EClass getAssignmentExpression();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see fr.inria.glose.mcl.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.UnaryExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see fr.inria.glose.mcl.UnaryExpression#getOperand()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Operand();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see fr.inria.glose.mcl.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.BinaryExpression#getLeftOperand <em>Left Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Operand</em>'.
	 * @see fr.inria.glose.mcl.BinaryExpression#getLeftOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_LeftOperand();

	/**
	 * Returns the meta object for the containment reference '{@link fr.inria.glose.mcl.BinaryExpression#getRightOperand <em>Right Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Operand</em>'.
	 * @see fr.inria.glose.mcl.BinaryExpression#getRightOperand()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_RightOperand();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Not <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not</em>'.
	 * @see fr.inria.glose.mcl.Not
	 * @generated
	 */
	EClass getNot();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.UnaryMinus <em>Unary Minus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Minus</em>'.
	 * @see fr.inria.glose.mcl.UnaryMinus
	 * @generated
	 */
	EClass getUnaryMinus();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see fr.inria.glose.mcl.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.BinaryPlus <em>Binary Plus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Plus</em>'.
	 * @see fr.inria.glose.mcl.BinaryPlus
	 * @generated
	 */
	EClass getBinaryPlus();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.BinaryMinus <em>Binary Minus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Minus</em>'.
	 * @see fr.inria.glose.mcl.BinaryMinus
	 * @generated
	 */
	EClass getBinaryMinus();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.PortRef <em>Port Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Ref</em>'.
	 * @see fr.inria.glose.mcl.PortRef
	 * @generated
	 */
	EClass getPortRef();

	/**
	 * Returns the meta object for the reference '{@link fr.inria.glose.mcl.PortRef#getRefPort <em>Ref Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ref Port</em>'.
	 * @see fr.inria.glose.mcl.PortRef#getRefPort()
	 * @see #getPortRef()
	 * @generated
	 */
	EReference getPortRef_RefPort();

	/**
	 * Returns the meta object for class '{@link fr.inria.glose.mcl.AbstractInteraction <em>Abstract Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Interaction</em>'.
	 * @see fr.inria.glose.mcl.AbstractInteraction
	 * @generated
	 */
	EClass getAbstractInteraction();

	/**
	 * Returns the meta object for enum '{@link fr.inria.glose.mcl.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binary Operator</em>'.
	 * @see fr.inria.glose.mcl.BinaryOperator
	 * @generated
	 */
	EEnum getBinaryOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MclFactory getMclFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.MCSpecificationImpl <em>MC Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.MCSpecificationImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getMCSpecification()
		 * @generated
		 */
		EClass MC_SPECIFICATION = eINSTANCE.getMCSpecification();

		/**
		 * The meta object literal for the '<em><b>Owned Connectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MC_SPECIFICATION__OWNED_CONNECTORS = eINSTANCE.getMCSpecification_OwnedConnectors();

		/**
		 * The meta object literal for the '<em><b>Imported MBI</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MC_SPECIFICATION__IMPORTED_MBI = eINSTANCE.getMCSpecification_ImportedMBI();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.TCExpressionImpl <em>TC Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.TCExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getTCExpression()
		 * @generated
		 */
		EClass TC_EXPRESSION = eINSTANCE.getTCExpression();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.SynchronizationConstraintImpl <em>Synchronization Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.SynchronizationConstraintImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getSynchronizationConstraint()
		 * @generated
		 */
		EClass SYNCHRONIZATION_CONSTRAINT = eINSTANCE.getSynchronizationConstraint();

		/**
		 * The meta object literal for the '<em><b>Right Temporal Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE = eINSTANCE
				.getSynchronizationConstraint_RightTemporalReference();

		/**
		 * The meta object literal for the '<em><b>Left Temporal Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE = eINSTANCE
				.getSynchronizationConstraint_LeftTemporalReference();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ConnectorImpl <em>Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ConnectorImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getConnector()
		 * @generated
		 */
		EClass CONNECTOR = eINSTANCE.getConnector();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__TRIGGER = eINSTANCE.getConnector_Trigger();

		/**
		 * The meta object literal for the '<em><b>Synchronization Rule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__SYNCHRONIZATION_RULE = eINSTANCE.getConnector_SynchronizationRule();

		/**
		 * The meta object literal for the '<em><b>Interactionstatement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__INTERACTIONSTATEMENT = eINSTANCE.getConnector_Interactionstatement();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__FROM = eINSTANCE.getConnector_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTOR__TO = eINSTANCE.getConnector_To();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.NamedElementImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ImportInterfaceStatementImpl <em>Import Interface Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ImportInterfaceStatementImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getImportInterfaceStatement()
		 * @generated
		 */
		EClass IMPORT_INTERFACE_STATEMENT = eINSTANCE.getImportInterfaceStatement();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT_INTERFACE_STATEMENT__IMPORT_URI = eINSTANCE.getImportInterfaceStatement_ImportURI();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ThresholdExpressionImpl <em>Threshold Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ThresholdExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getThresholdExpression()
		 * @generated
		 */
		EClass THRESHOLD_EXPRESSION = eINSTANCE.getThresholdExpression();

		/**
		 * The meta object literal for the '<em><b>Setoperation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLD_EXPRESSION__SETOPERATION = eINSTANCE.getThresholdExpression_Setoperation();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLD_EXPRESSION__VALUE = eINSTANCE.getThresholdExpression_Value();

		/**
		 * The meta object literal for the '<em><b>Trigger Source Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT = eINSTANCE.getThresholdExpression_TriggerSourcePort();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ExpOperationImpl <em>Exp Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ExpOperationImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getExpOperation()
		 * @generated
		 */
		EClass EXP_OPERATION = eINSTANCE.getExpOperation();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.GreaterImpl <em>Greater</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.GreaterImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getGreater()
		 * @generated
		 */
		EClass GREATER = eINSTANCE.getGreater();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.LesserImpl <em>Lesser</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.LesserImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getLesser()
		 * @generated
		 */
		EClass LESSER = eINSTANCE.getLesser();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.BinaryExpImpl <em>Binary Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.BinaryExpImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExp()
		 * @generated
		 */
		EClass BINARY_EXP = eINSTANCE.getBinaryExp();

		/**
		 * The meta object literal for the '<em><b>Left Op</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXP__LEFT_OP = eINSTANCE.getBinaryExp_LeftOp();

		/**
		 * The meta object literal for the '<em><b>Right Op</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXP__RIGHT_OP = eINSTANCE.getBinaryExp_RightOp();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.BinaryExpOperatorImpl <em>Binary Exp Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.BinaryExpOperatorImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExpOperator()
		 * @generated
		 */
		EClass BINARY_EXP_OPERATOR = eINSTANCE.getBinaryExpOperator();

		/**
		 * The meta object literal for the '<em><b>Binary Exp Actual Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR = eINSTANCE
				.getBinaryExpOperator_BinaryExpActualOperator();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.VariableImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE__VALUE = eINSTANCE.getVariable_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.VariableTriggerImpl <em>Variable Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.VariableTriggerImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getVariableTrigger()
		 * @generated
		 */
		EClass VARIABLE_TRIGGER = eINSTANCE.getVariableTrigger();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TRIGGER__PORT = eINSTANCE.getVariableTrigger_Port();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.PeriodicExpressionImpl <em>Periodic Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.PeriodicExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getPeriodicExpression()
		 * @generated
		 */
		EClass PERIODIC_EXPRESSION = eINSTANCE.getPeriodicExpression();

		/**
		 * The meta object literal for the '<em><b>Temporal Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERIODIC_EXPRESSION__TEMPORAL_REFERENCE = eINSTANCE.getPeriodicExpression_TemporalReference();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODIC_EXPRESSION__VALUE = eINSTANCE.getPeriodicExpression_Value();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.TriggeringConditionImpl <em>Triggering Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.TriggeringConditionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getTriggeringCondition()
		 * @generated
		 */
		EClass TRIGGERING_CONDITION = eINSTANCE.getTriggeringCondition();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.SimpleSyncImpl <em>Simple Sync</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.SimpleSyncImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getSimpleSync()
		 * @generated
		 */
		EClass SIMPLE_SYNC = eINSTANCE.getSimpleSync();

		/**
		 * The meta object literal for the '<em><b>Tolerance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_SYNC__TOLERANCE = eINSTANCE.getSimpleSync_Tolerance();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ExternalSyncImpl <em>External Sync</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ExternalSyncImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getExternalSync()
		 * @generated
		 */
		EClass EXTERNAL_SYNC = eINSTANCE.getExternalSync();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.UpdatedImpl <em>Updated</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.UpdatedImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUpdated()
		 * @generated
		 */
		EClass UPDATED = eINSTANCE.getUpdated();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.ReadyToReadImpl <em>Ready To Read</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.ReadyToReadImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getReadyToRead()
		 * @generated
		 */
		EClass READY_TO_READ = eINSTANCE.getReadyToRead();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.EventTriggerImpl <em>Event Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.EventTriggerImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getEventTrigger()
		 * @generated
		 */
		EClass EVENT_TRIGGER = eINSTANCE.getEventTrigger();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRIGGER__PORT = eINSTANCE.getEventTrigger_Port();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.AssignmentExpressionImpl <em>Assignment Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.AssignmentExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAssignmentExpression()
		 * @generated
		 */
		EClass ASSIGNMENT_EXPRESSION = eINSTANCE.getAssignmentExpression();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.UnaryExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__OPERAND = eINSTANCE.getUnaryExpression_Operand();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.BinaryExpressionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Left Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LEFT_OPERAND = eINSTANCE.getBinaryExpression_LeftOperand();

		/**
		 * The meta object literal for the '<em><b>Right Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RIGHT_OPERAND = eINSTANCE.getBinaryExpression_RightOperand();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.NotImpl <em>Not</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.NotImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getNot()
		 * @generated
		 */
		EClass NOT = eINSTANCE.getNot();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.UnaryMinusImpl <em>Unary Minus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.UnaryMinusImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getUnaryMinus()
		 * @generated
		 */
		EClass UNARY_MINUS = eINSTANCE.getUnaryMinus();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.AssignmentImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.BinaryPlusImpl <em>Binary Plus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.BinaryPlusImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryPlus()
		 * @generated
		 */
		EClass BINARY_PLUS = eINSTANCE.getBinaryPlus();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.BinaryMinusImpl <em>Binary Minus</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.BinaryMinusImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryMinus()
		 * @generated
		 */
		EClass BINARY_MINUS = eINSTANCE.getBinaryMinus();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.PortRefImpl <em>Port Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.PortRefImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getPortRef()
		 * @generated
		 */
		EClass PORT_REF = eINSTANCE.getPortRef();

		/**
		 * The meta object literal for the '<em><b>Ref Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_REF__REF_PORT = eINSTANCE.getPortRef_RefPort();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.impl.AbstractInteractionImpl <em>Abstract Interaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.impl.AbstractInteractionImpl
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getAbstractInteraction()
		 * @generated
		 */
		EClass ABSTRACT_INTERACTION = eINSTANCE.getAbstractInteraction();

		/**
		 * The meta object literal for the '{@link fr.inria.glose.mcl.BinaryOperator <em>Binary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.inria.glose.mcl.BinaryOperator
		 * @see fr.inria.glose.mcl.impl.MclPackageImpl#getBinaryOperator()
		 * @generated
		 */
		EEnum BINARY_OPERATOR = eINSTANCE.getBinaryOperator();

	}

} //MclPackage
