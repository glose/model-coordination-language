/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.Lesser;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lesser</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LesserImpl extends ExpOperationImpl implements Lesser {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LesserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.LESSER;
	}

} //LesserImpl
