/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.Assignment;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssignmentImpl extends BinaryExpressionImpl implements Assignment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.ASSIGNMENT;
	}

} //AssignmentImpl
