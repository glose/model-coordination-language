/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.BinaryExpOperator;
import fr.inria.glose.mcl.BinaryOperator;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Exp Operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.BinaryExpOperatorImpl#getBinaryExpActualOperator <em>Binary Exp Actual Operator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryExpOperatorImpl extends BinaryExpImpl implements BinaryExpOperator {
	/**
	 * The default value of the '{@link #getBinaryExpActualOperator() <em>Binary Exp Actual Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinaryExpActualOperator()
	 * @generated
	 * @ordered
	 */
	protected static final BinaryOperator BINARY_EXP_ACTUAL_OPERATOR_EDEFAULT = BinaryOperator.OR;

	/**
	 * The cached value of the '{@link #getBinaryExpActualOperator() <em>Binary Exp Actual Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBinaryExpActualOperator()
	 * @generated
	 * @ordered
	 */
	protected BinaryOperator binaryExpActualOperator = BINARY_EXP_ACTUAL_OPERATOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryExpOperatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.BINARY_EXP_OPERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BinaryOperator getBinaryExpActualOperator() {
		return binaryExpActualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBinaryExpActualOperator(BinaryOperator newBinaryExpActualOperator) {
		BinaryOperator oldBinaryExpActualOperator = binaryExpActualOperator;
		binaryExpActualOperator = newBinaryExpActualOperator == null ? BINARY_EXP_ACTUAL_OPERATOR_EDEFAULT
				: newBinaryExpActualOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MclPackage.BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR, oldBinaryExpActualOperator,
					binaryExpActualOperator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR:
			return getBinaryExpActualOperator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR:
			setBinaryExpActualOperator((BinaryOperator) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR:
			setBinaryExpActualOperator(BINARY_EXP_ACTUAL_OPERATOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.BINARY_EXP_OPERATOR__BINARY_EXP_ACTUAL_OPERATOR:
			return binaryExpActualOperator != BINARY_EXP_ACTUAL_OPERATOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (binaryExpActualOperator: ");
		result.append(binaryExpActualOperator);
		result.append(')');
		return result.toString();
	}

} //BinaryExpOperatorImpl
