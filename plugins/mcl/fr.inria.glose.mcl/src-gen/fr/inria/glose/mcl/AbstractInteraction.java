/**
 */
package fr.inria.glose.mcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Interaction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getAbstractInteraction()
 * @model abstract="true"
 * @generated
 */
public interface AbstractInteraction extends EObject {
} // AbstractInteraction
