/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mbilang.ModelTemporalReference;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.SynchronizationConstraint;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synchronization Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.SynchronizationConstraintImpl#getRightTemporalReference <em>Right Temporal Reference</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.SynchronizationConstraintImpl#getLeftTemporalReference <em>Left Temporal Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SynchronizationConstraintImpl extends MinimalEObjectImpl.Container
		implements SynchronizationConstraint {
	/**
	 * The cached value of the '{@link #getRightTemporalReference() <em>Right Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightTemporalReference()
	 * @generated
	 * @ordered
	 */
	protected ModelTemporalReference rightTemporalReference;

	/**
	 * The cached value of the '{@link #getLeftTemporalReference() <em>Left Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftTemporalReference()
	 * @generated
	 * @ordered
	 */
	protected ModelTemporalReference leftTemporalReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SynchronizationConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.SYNCHRONIZATION_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelTemporalReference getRightTemporalReference() {
		if (rightTemporalReference != null && rightTemporalReference.eIsProxy()) {
			InternalEObject oldRightTemporalReference = (InternalEObject) rightTemporalReference;
			rightTemporalReference = (ModelTemporalReference) eResolveProxy(oldRightTemporalReference);
			if (rightTemporalReference != oldRightTemporalReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE, oldRightTemporalReference,
							rightTemporalReference));
			}
		}
		return rightTemporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference basicGetRightTemporalReference() {
		return rightTemporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRightTemporalReference(ModelTemporalReference newRightTemporalReference) {
		ModelTemporalReference oldRightTemporalReference = rightTemporalReference;
		rightTemporalReference = newRightTemporalReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE, oldRightTemporalReference,
					rightTemporalReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelTemporalReference getLeftTemporalReference() {
		if (leftTemporalReference != null && leftTemporalReference.eIsProxy()) {
			InternalEObject oldLeftTemporalReference = (InternalEObject) leftTemporalReference;
			leftTemporalReference = (ModelTemporalReference) eResolveProxy(oldLeftTemporalReference);
			if (leftTemporalReference != oldLeftTemporalReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE, oldLeftTemporalReference,
							leftTemporalReference));
			}
		}
		return leftTemporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference basicGetLeftTemporalReference() {
		return leftTemporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeftTemporalReference(ModelTemporalReference newLeftTemporalReference) {
		ModelTemporalReference oldLeftTemporalReference = leftTemporalReference;
		leftTemporalReference = newLeftTemporalReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE, oldLeftTemporalReference,
					leftTemporalReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE:
			if (resolve)
				return getRightTemporalReference();
			return basicGetRightTemporalReference();
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE:
			if (resolve)
				return getLeftTemporalReference();
			return basicGetLeftTemporalReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE:
			setRightTemporalReference((ModelTemporalReference) newValue);
			return;
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE:
			setLeftTemporalReference((ModelTemporalReference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE:
			setRightTemporalReference((ModelTemporalReference) null);
			return;
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE:
			setLeftTemporalReference((ModelTemporalReference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__RIGHT_TEMPORAL_REFERENCE:
			return rightTemporalReference != null;
		case MclPackage.SYNCHRONIZATION_CONSTRAINT__LEFT_TEMPORAL_REFERENCE:
			return leftTemporalReference != null;
		}
		return super.eIsSet(featureID);
	}

} //SynchronizationConstraintImpl
