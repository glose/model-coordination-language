/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.ExternalSync;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Sync</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ExternalSyncImpl extends SynchronizationConstraintImpl implements ExternalSync {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalSyncImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.EXTERNAL_SYNC;
	}

} //ExternalSyncImpl
