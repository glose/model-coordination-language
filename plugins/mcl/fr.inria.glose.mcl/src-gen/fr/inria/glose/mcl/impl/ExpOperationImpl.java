/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.ExpOperation;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exp Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ExpOperationImpl extends MinimalEObjectImpl.Container implements ExpOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.EXP_OPERATION;
	}

} //ExpOperationImpl
