/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.ExpOperation;
import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.PortRef;
import fr.inria.glose.mcl.ThresholdExpression;
import fr.inria.glose.mcl.Variable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Threshold Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.ThresholdExpressionImpl#getSetoperation <em>Setoperation</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ThresholdExpressionImpl#getValue <em>Value</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.ThresholdExpressionImpl#getTriggerSourcePort <em>Trigger Source Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ThresholdExpressionImpl extends TriggeringConditionImpl implements ThresholdExpression {
	/**
	 * The cached value of the '{@link #getSetoperation() <em>Setoperation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetoperation()
	 * @generated
	 * @ordered
	 */
	protected ExpOperation setoperation;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Variable value;

	/**
	 * The cached value of the '{@link #getTriggerSourcePort() <em>Trigger Source Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerSourcePort()
	 * @generated
	 * @ordered
	 */
	protected PortRef triggerSourcePort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThresholdExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.THRESHOLD_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExpOperation getSetoperation() {
		return setoperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSetoperation(ExpOperation newSetoperation, NotificationChain msgs) {
		ExpOperation oldSetoperation = setoperation;
		setoperation = newSetoperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.THRESHOLD_EXPRESSION__SETOPERATION, oldSetoperation, newSetoperation);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSetoperation(ExpOperation newSetoperation) {
		if (newSetoperation != setoperation) {
			NotificationChain msgs = null;
			if (setoperation != null)
				msgs = ((InternalEObject) setoperation).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__SETOPERATION, null, msgs);
			if (newSetoperation != null)
				msgs = ((InternalEObject) newSetoperation).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__SETOPERATION, null, msgs);
			msgs = basicSetSetoperation(newSetoperation, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.THRESHOLD_EXPRESSION__SETOPERATION,
					newSetoperation, newSetoperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Variable getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Variable newValue, NotificationChain msgs) {
		Variable oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.THRESHOLD_EXPRESSION__VALUE, oldValue, newValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(Variable newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject) value).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject) newValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.THRESHOLD_EXPRESSION__VALUE, newValue,
					newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PortRef getTriggerSourcePort() {
		return triggerSourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTriggerSourcePort(PortRef newTriggerSourcePort, NotificationChain msgs) {
		PortRef oldTriggerSourcePort = triggerSourcePort;
		triggerSourcePort = newTriggerSourcePort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT, oldTriggerSourcePort, newTriggerSourcePort);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTriggerSourcePort(PortRef newTriggerSourcePort) {
		if (newTriggerSourcePort != triggerSourcePort) {
			NotificationChain msgs = null;
			if (triggerSourcePort != null)
				msgs = ((InternalEObject) triggerSourcePort).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT, null, msgs);
			if (newTriggerSourcePort != null)
				msgs = ((InternalEObject) newTriggerSourcePort).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT, null, msgs);
			msgs = basicSetTriggerSourcePort(newTriggerSourcePort, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT,
					newTriggerSourcePort, newTriggerSourcePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MclPackage.THRESHOLD_EXPRESSION__SETOPERATION:
			return basicSetSetoperation(null, msgs);
		case MclPackage.THRESHOLD_EXPRESSION__VALUE:
			return basicSetValue(null, msgs);
		case MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT:
			return basicSetTriggerSourcePort(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.THRESHOLD_EXPRESSION__SETOPERATION:
			return getSetoperation();
		case MclPackage.THRESHOLD_EXPRESSION__VALUE:
			return getValue();
		case MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT:
			return getTriggerSourcePort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.THRESHOLD_EXPRESSION__SETOPERATION:
			setSetoperation((ExpOperation) newValue);
			return;
		case MclPackage.THRESHOLD_EXPRESSION__VALUE:
			setValue((Variable) newValue);
			return;
		case MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT:
			setTriggerSourcePort((PortRef) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.THRESHOLD_EXPRESSION__SETOPERATION:
			setSetoperation((ExpOperation) null);
			return;
		case MclPackage.THRESHOLD_EXPRESSION__VALUE:
			setValue((Variable) null);
			return;
		case MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT:
			setTriggerSourcePort((PortRef) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.THRESHOLD_EXPRESSION__SETOPERATION:
			return setoperation != null;
		case MclPackage.THRESHOLD_EXPRESSION__VALUE:
			return value != null;
		case MclPackage.THRESHOLD_EXPRESSION__TRIGGER_SOURCE_PORT:
			return triggerSourcePort != null;
		}
		return super.eIsSet(featureID);
	}

} //ThresholdExpressionImpl
