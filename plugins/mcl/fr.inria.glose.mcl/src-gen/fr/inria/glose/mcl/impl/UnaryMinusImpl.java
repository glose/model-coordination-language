/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.UnaryMinus;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UnaryMinusImpl extends UnaryExpressionImpl implements UnaryMinus {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryMinusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.UNARY_MINUS;
	}

} //UnaryMinusImpl
