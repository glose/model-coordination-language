/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getBinaryMinus()
 * @model
 * @generated
 */
public interface BinaryMinus extends BinaryExpression {
} // BinaryMinus
