/**
 */
package fr.inria.glose.mcl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MC Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.MCSpecification#getOwnedConnectors <em>Owned Connectors</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.MCSpecification#getImportedMBI <em>Imported MBI</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getMCSpecification()
 * @model
 * @generated
 */
public interface MCSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Owned Connectors</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mcl.Connector}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Connectors</em>' containment reference list.
	 * @see fr.inria.glose.mcl.MclPackage#getMCSpecification_OwnedConnectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Connector> getOwnedConnectors();

	/**
	 * Returns the value of the '<em><b>Imported MBI</b></em>' containment reference list.
	 * The list contents are of type {@link fr.inria.glose.mcl.ImportInterfaceStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported MBI</em>' containment reference list.
	 * @see fr.inria.glose.mcl.MclPackage#getMCSpecification_ImportedMBI()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportInterfaceStatement> getImportedMBI();

} // MCSpecification
