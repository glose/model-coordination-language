/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.ReadyToRead;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ready To Read</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReadyToReadImpl extends VariableTriggerImpl implements ReadyToRead {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReadyToReadImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.READY_TO_READ;
	}

} //ReadyToReadImpl
