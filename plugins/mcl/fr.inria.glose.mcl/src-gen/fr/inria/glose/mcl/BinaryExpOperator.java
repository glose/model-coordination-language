/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Exp Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.BinaryExpOperator#getBinaryExpActualOperator <em>Binary Exp Actual Operator</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getBinaryExpOperator()
 * @model
 * @generated
 */
public interface BinaryExpOperator extends BinaryExp {
	/**
	 * Returns the value of the '<em><b>Binary Exp Actual Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.inria.glose.mcl.BinaryOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary Exp Actual Operator</em>' attribute.
	 * @see fr.inria.glose.mcl.BinaryOperator
	 * @see #setBinaryExpActualOperator(BinaryOperator)
	 * @see fr.inria.glose.mcl.MclPackage#getBinaryExpOperator_BinaryExpActualOperator()
	 * @model required="true"
	 * @generated
	 */
	BinaryOperator getBinaryExpActualOperator();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.BinaryExpOperator#getBinaryExpActualOperator <em>Binary Exp Actual Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binary Exp Actual Operator</em>' attribute.
	 * @see fr.inria.glose.mcl.BinaryOperator
	 * @see #getBinaryExpActualOperator()
	 * @generated
	 */
	void setBinaryExpActualOperator(BinaryOperator value);

} // BinaryExpOperator
