/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.AssignmentExpression;
import fr.inria.glose.mcl.MclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assignment Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AssignmentExpressionImpl extends AbstractInteractionImpl implements AssignmentExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssignmentExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.ASSIGNMENT_EXPRESSION;
	}

} //AssignmentExpressionImpl
