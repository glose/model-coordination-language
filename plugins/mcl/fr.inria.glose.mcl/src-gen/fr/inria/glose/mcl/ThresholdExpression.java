/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Threshold Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.ThresholdExpression#getSetoperation <em>Setoperation</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.ThresholdExpression#getValue <em>Value</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.ThresholdExpression#getTriggerSourcePort <em>Trigger Source Port</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getThresholdExpression()
 * @model
 * @generated
 */
public interface ThresholdExpression extends TriggeringCondition {
	/**
	 * Returns the value of the '<em><b>Setoperation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Setoperation</em>' containment reference.
	 * @see #setSetoperation(ExpOperation)
	 * @see fr.inria.glose.mcl.MclPackage#getThresholdExpression_Setoperation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ExpOperation getSetoperation();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.ThresholdExpression#getSetoperation <em>Setoperation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Setoperation</em>' containment reference.
	 * @see #getSetoperation()
	 * @generated
	 */
	void setSetoperation(ExpOperation value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Variable)
	 * @see fr.inria.glose.mcl.MclPackage#getThresholdExpression_Value()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Variable getValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.ThresholdExpression#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Variable value);

	/**
	 * Returns the value of the '<em><b>Trigger Source Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger Source Port</em>' containment reference.
	 * @see #setTriggerSourcePort(PortRef)
	 * @see fr.inria.glose.mcl.MclPackage#getThresholdExpression_TriggerSourcePort()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PortRef getTriggerSourcePort();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.ThresholdExpression#getTriggerSourcePort <em>Trigger Source Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger Source Port</em>' containment reference.
	 * @see #getTriggerSourcePort()
	 * @generated
	 */
	void setTriggerSourcePort(PortRef value);

} // ThresholdExpression
