/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.BinaryExpression#getLeftOperand <em>Left Operand</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.BinaryExpression#getRightOperand <em>Right Operand</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getBinaryExpression()
 * @model abstract="true"
 * @generated
 */
public interface BinaryExpression extends AssignmentExpression {
	/**
	 * Returns the value of the '<em><b>Left Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Operand</em>' containment reference.
	 * @see #setLeftOperand(AssignmentExpression)
	 * @see fr.inria.glose.mcl.MclPackage#getBinaryExpression_LeftOperand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AssignmentExpression getLeftOperand();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.BinaryExpression#getLeftOperand <em>Left Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Operand</em>' containment reference.
	 * @see #getLeftOperand()
	 * @generated
	 */
	void setLeftOperand(AssignmentExpression value);

	/**
	 * Returns the value of the '<em><b>Right Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Operand</em>' containment reference.
	 * @see #setRightOperand(AssignmentExpression)
	 * @see fr.inria.glose.mcl.MclPackage#getBinaryExpression_RightOperand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AssignmentExpression getRightOperand();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.BinaryExpression#getRightOperand <em>Right Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Operand</em>' containment reference.
	 * @see #getRightOperand()
	 * @generated
	 */
	void setRightOperand(AssignmentExpression value);

} // BinaryExpression
