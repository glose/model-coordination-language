/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mbilang.ModelTemporalReference;

import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.PeriodicExpression;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Periodic Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.PeriodicExpressionImpl#getTemporalReference <em>Temporal Reference</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.PeriodicExpressionImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PeriodicExpressionImpl extends TriggeringConditionImpl implements PeriodicExpression {
	/**
	 * The cached value of the '{@link #getTemporalReference() <em>Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalReference()
	 * @generated
	 * @ordered
	 */
	protected ModelTemporalReference temporalReference;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final int VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected int value = VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PeriodicExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.PERIODIC_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelTemporalReference getTemporalReference() {
		if (temporalReference != null && temporalReference.eIsProxy()) {
			InternalEObject oldTemporalReference = (InternalEObject) temporalReference;
			temporalReference = (ModelTemporalReference) eResolveProxy(oldTemporalReference);
			if (temporalReference != oldTemporalReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE, oldTemporalReference,
							temporalReference));
			}
		}
		return temporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelTemporalReference basicGetTemporalReference() {
		return temporalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTemporalReference(ModelTemporalReference newTemporalReference) {
		ModelTemporalReference oldTemporalReference = temporalReference;
		temporalReference = newTemporalReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE,
					oldTemporalReference, temporalReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(int newValue) {
		int oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.PERIODIC_EXPRESSION__VALUE, oldValue,
					value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE:
			if (resolve)
				return getTemporalReference();
			return basicGetTemporalReference();
		case MclPackage.PERIODIC_EXPRESSION__VALUE:
			return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE:
			setTemporalReference((ModelTemporalReference) newValue);
			return;
		case MclPackage.PERIODIC_EXPRESSION__VALUE:
			setValue((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE:
			setTemporalReference((ModelTemporalReference) null);
			return;
		case MclPackage.PERIODIC_EXPRESSION__VALUE:
			setValue(VALUE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.PERIODIC_EXPRESSION__TEMPORAL_REFERENCE:
			return temporalReference != null;
		case MclPackage.PERIODIC_EXPRESSION__VALUE:
			return value != VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //PeriodicExpressionImpl
