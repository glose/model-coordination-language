/**
 */
package fr.inria.glose.mcl.util;

import fr.inria.glose.mcl.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.inria.glose.mcl.MclPackage
 * @generated
 */
public class MclSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MclPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MclSwitch() {
		if (modelPackage == null) {
			modelPackage = MclPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case MclPackage.MC_SPECIFICATION: {
			MCSpecification mcSpecification = (MCSpecification) theEObject;
			T result = caseMCSpecification(mcSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.TC_EXPRESSION: {
			TCExpression tcExpression = (TCExpression) theEObject;
			T result = caseTCExpression(tcExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.SYNCHRONIZATION_CONSTRAINT: {
			SynchronizationConstraint synchronizationConstraint = (SynchronizationConstraint) theEObject;
			T result = caseSynchronizationConstraint(synchronizationConstraint);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.CONNECTOR: {
			Connector connector = (Connector) theEObject;
			T result = caseConnector(connector);
			if (result == null)
				result = caseNamedElement(connector);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.NAMED_ELEMENT: {
			NamedElement namedElement = (NamedElement) theEObject;
			T result = caseNamedElement(namedElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.IMPORT_INTERFACE_STATEMENT: {
			ImportInterfaceStatement importInterfaceStatement = (ImportInterfaceStatement) theEObject;
			T result = caseImportInterfaceStatement(importInterfaceStatement);
			if (result == null)
				result = caseNamedElement(importInterfaceStatement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.THRESHOLD_EXPRESSION: {
			ThresholdExpression thresholdExpression = (ThresholdExpression) theEObject;
			T result = caseThresholdExpression(thresholdExpression);
			if (result == null)
				result = caseTriggeringCondition(thresholdExpression);
			if (result == null)
				result = caseTCExpression(thresholdExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.EXP_OPERATION: {
			ExpOperation expOperation = (ExpOperation) theEObject;
			T result = caseExpOperation(expOperation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.GREATER: {
			Greater greater = (Greater) theEObject;
			T result = caseGreater(greater);
			if (result == null)
				result = caseExpOperation(greater);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.LESSER: {
			Lesser lesser = (Lesser) theEObject;
			T result = caseLesser(lesser);
			if (result == null)
				result = caseExpOperation(lesser);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.BINARY_EXP: {
			BinaryExp binaryExp = (BinaryExp) theEObject;
			T result = caseBinaryExp(binaryExp);
			if (result == null)
				result = caseTCExpression(binaryExp);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.BINARY_EXP_OPERATOR: {
			BinaryExpOperator binaryExpOperator = (BinaryExpOperator) theEObject;
			T result = caseBinaryExpOperator(binaryExpOperator);
			if (result == null)
				result = caseBinaryExp(binaryExpOperator);
			if (result == null)
				result = caseTCExpression(binaryExpOperator);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.VARIABLE: {
			Variable variable = (Variable) theEObject;
			T result = caseVariable(variable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.VARIABLE_TRIGGER: {
			VariableTrigger variableTrigger = (VariableTrigger) theEObject;
			T result = caseVariableTrigger(variableTrigger);
			if (result == null)
				result = caseTriggeringCondition(variableTrigger);
			if (result == null)
				result = caseTCExpression(variableTrigger);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.PERIODIC_EXPRESSION: {
			PeriodicExpression periodicExpression = (PeriodicExpression) theEObject;
			T result = casePeriodicExpression(periodicExpression);
			if (result == null)
				result = caseTriggeringCondition(periodicExpression);
			if (result == null)
				result = caseTCExpression(periodicExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.TRIGGERING_CONDITION: {
			TriggeringCondition triggeringCondition = (TriggeringCondition) theEObject;
			T result = caseTriggeringCondition(triggeringCondition);
			if (result == null)
				result = caseTCExpression(triggeringCondition);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.SIMPLE_SYNC: {
			SimpleSync simpleSync = (SimpleSync) theEObject;
			T result = caseSimpleSync(simpleSync);
			if (result == null)
				result = caseSynchronizationConstraint(simpleSync);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.EXTERNAL_SYNC: {
			ExternalSync externalSync = (ExternalSync) theEObject;
			T result = caseExternalSync(externalSync);
			if (result == null)
				result = caseSynchronizationConstraint(externalSync);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.UPDATED: {
			Updated updated = (Updated) theEObject;
			T result = caseUpdated(updated);
			if (result == null)
				result = caseVariableTrigger(updated);
			if (result == null)
				result = caseTriggeringCondition(updated);
			if (result == null)
				result = caseTCExpression(updated);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.READY_TO_READ: {
			ReadyToRead readyToRead = (ReadyToRead) theEObject;
			T result = caseReadyToRead(readyToRead);
			if (result == null)
				result = caseVariableTrigger(readyToRead);
			if (result == null)
				result = caseTriggeringCondition(readyToRead);
			if (result == null)
				result = caseTCExpression(readyToRead);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.EVENT_TRIGGER: {
			EventTrigger eventTrigger = (EventTrigger) theEObject;
			T result = caseEventTrigger(eventTrigger);
			if (result == null)
				result = caseTriggeringCondition(eventTrigger);
			if (result == null)
				result = caseTCExpression(eventTrigger);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.ASSIGNMENT_EXPRESSION: {
			AssignmentExpression assignmentExpression = (AssignmentExpression) theEObject;
			T result = caseAssignmentExpression(assignmentExpression);
			if (result == null)
				result = caseAbstractInteraction(assignmentExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.UNARY_EXPRESSION: {
			UnaryExpression unaryExpression = (UnaryExpression) theEObject;
			T result = caseUnaryExpression(unaryExpression);
			if (result == null)
				result = caseAssignmentExpression(unaryExpression);
			if (result == null)
				result = caseAbstractInteraction(unaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.BINARY_EXPRESSION: {
			BinaryExpression binaryExpression = (BinaryExpression) theEObject;
			T result = caseBinaryExpression(binaryExpression);
			if (result == null)
				result = caseAssignmentExpression(binaryExpression);
			if (result == null)
				result = caseAbstractInteraction(binaryExpression);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.NOT: {
			Not not = (Not) theEObject;
			T result = caseNot(not);
			if (result == null)
				result = caseUnaryExpression(not);
			if (result == null)
				result = caseAssignmentExpression(not);
			if (result == null)
				result = caseAbstractInteraction(not);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.UNARY_MINUS: {
			UnaryMinus unaryMinus = (UnaryMinus) theEObject;
			T result = caseUnaryMinus(unaryMinus);
			if (result == null)
				result = caseUnaryExpression(unaryMinus);
			if (result == null)
				result = caseAssignmentExpression(unaryMinus);
			if (result == null)
				result = caseAbstractInteraction(unaryMinus);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.ASSIGNMENT: {
			Assignment assignment = (Assignment) theEObject;
			T result = caseAssignment(assignment);
			if (result == null)
				result = caseBinaryExpression(assignment);
			if (result == null)
				result = caseAssignmentExpression(assignment);
			if (result == null)
				result = caseAbstractInteraction(assignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.BINARY_PLUS: {
			BinaryPlus binaryPlus = (BinaryPlus) theEObject;
			T result = caseBinaryPlus(binaryPlus);
			if (result == null)
				result = caseBinaryExpression(binaryPlus);
			if (result == null)
				result = caseAssignmentExpression(binaryPlus);
			if (result == null)
				result = caseAbstractInteraction(binaryPlus);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.BINARY_MINUS: {
			BinaryMinus binaryMinus = (BinaryMinus) theEObject;
			T result = caseBinaryMinus(binaryMinus);
			if (result == null)
				result = caseBinaryExpression(binaryMinus);
			if (result == null)
				result = caseAssignmentExpression(binaryMinus);
			if (result == null)
				result = caseAbstractInteraction(binaryMinus);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.PORT_REF: {
			PortRef portRef = (PortRef) theEObject;
			T result = casePortRef(portRef);
			if (result == null)
				result = caseAssignmentExpression(portRef);
			if (result == null)
				result = caseAbstractInteraction(portRef);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MclPackage.ABSTRACT_INTERACTION: {
			AbstractInteraction abstractInteraction = (AbstractInteraction) theEObject;
			T result = caseAbstractInteraction(abstractInteraction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MC Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MC Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMCSpecification(MCSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TC Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TC Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTCExpression(TCExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronization Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronization Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronizationConstraint(SynchronizationConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnector(Connector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import Interface Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import Interface Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImportInterfaceStatement(ImportInterfaceStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Threshold Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Threshold Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThresholdExpression(ThresholdExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exp Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exp Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpOperation(ExpOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Greater</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Greater</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGreater(Greater object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lesser</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lesser</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLesser(Lesser object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Exp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Exp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryExp(BinaryExp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Exp Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Exp Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryExpOperator(BinaryExpOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableTrigger(VariableTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Periodic Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Periodic Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePeriodicExpression(PeriodicExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggering Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggering Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTriggeringCondition(TriggeringCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Sync</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Sync</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleSync(SimpleSync object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Sync</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Sync</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalSync(ExternalSync object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Updated</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Updated</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdated(Updated object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ready To Read</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ready To Read</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadyToRead(ReadyToRead object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventTrigger(EventTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignment Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignment Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssignmentExpression(AssignmentExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryExpression(UnaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryExpression(BinaryExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNot(Not object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Minus</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryMinus(UnaryMinus object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssignment(Assignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Plus</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Plus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryPlus(BinaryPlus object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Minus</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Minus</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryMinus(BinaryMinus object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortRef(PortRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Interaction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Interaction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractInteraction(AbstractInteraction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MclSwitch
