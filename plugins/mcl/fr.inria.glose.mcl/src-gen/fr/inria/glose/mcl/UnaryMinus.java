/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Minus</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getUnaryMinus()
 * @model
 * @generated
 */
public interface UnaryMinus extends UnaryExpression {
} // UnaryMinus
