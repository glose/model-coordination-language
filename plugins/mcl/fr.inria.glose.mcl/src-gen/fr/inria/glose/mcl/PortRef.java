/**
 */
package fr.inria.glose.mcl;

import fr.inria.glose.mbilang.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.PortRef#getRefPort <em>Ref Port</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getPortRef()
 * @model
 * @generated
 */
public interface PortRef extends AssignmentExpression {
	/**
	 * Returns the value of the '<em><b>Ref Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Port</em>' reference.
	 * @see #setRefPort(Port)
	 * @see fr.inria.glose.mcl.MclPackage#getPortRef_RefPort()
	 * @model required="true"
	 * @generated
	 */
	Port getRefPort();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.PortRef#getRefPort <em>Ref Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Port</em>' reference.
	 * @see #getRefPort()
	 * @generated
	 */
	void setRefPort(Port value);

} // PortRef
