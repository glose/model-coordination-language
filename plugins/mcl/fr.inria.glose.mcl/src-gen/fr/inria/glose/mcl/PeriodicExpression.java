/**
 */
package fr.inria.glose.mcl;

import fr.inria.glose.mbilang.ModelTemporalReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Periodic Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.PeriodicExpression#getTemporalReference <em>Temporal Reference</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.PeriodicExpression#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getPeriodicExpression()
 * @model
 * @generated
 */
public interface PeriodicExpression extends TriggeringCondition {
	/**
	 * Returns the value of the '<em><b>Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporal Reference</em>' reference.
	 * @see #setTemporalReference(ModelTemporalReference)
	 * @see fr.inria.glose.mcl.MclPackage#getPeriodicExpression_TemporalReference()
	 * @model required="true"
	 * @generated
	 */
	ModelTemporalReference getTemporalReference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.PeriodicExpression#getTemporalReference <em>Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temporal Reference</em>' reference.
	 * @see #getTemporalReference()
	 * @generated
	 */
	void setTemporalReference(ModelTemporalReference value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see fr.inria.glose.mcl.MclPackage#getPeriodicExpression_Value()
	 * @model required="true"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.PeriodicExpression#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

} // PeriodicExpression
