/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.Connector#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.Connector#getSynchronizationRule <em>Synchronization Rule</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.Connector#getInteractionstatement <em>Interactionstatement</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.Connector#getFrom <em>From</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.Connector#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getConnector()
 * @model
 * @generated
 */
public interface Connector extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' containment reference.
	 * @see #setTrigger(TCExpression)
	 * @see fr.inria.glose.mcl.MclPackage#getConnector_Trigger()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TCExpression getTrigger();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.Connector#getTrigger <em>Trigger</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' containment reference.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(TCExpression value);

	/**
	 * Returns the value of the '<em><b>Synchronization Rule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronization Rule</em>' containment reference.
	 * @see #setSynchronizationRule(SynchronizationConstraint)
	 * @see fr.inria.glose.mcl.MclPackage#getConnector_SynchronizationRule()
	 * @model containment="true"
	 * @generated
	 */
	SynchronizationConstraint getSynchronizationRule();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.Connector#getSynchronizationRule <em>Synchronization Rule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronization Rule</em>' containment reference.
	 * @see #getSynchronizationRule()
	 * @generated
	 */
	void setSynchronizationRule(SynchronizationConstraint value);

	/**
	 * Returns the value of the '<em><b>Interactionstatement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interactionstatement</em>' containment reference.
	 * @see #setInteractionstatement(AbstractInteraction)
	 * @see fr.inria.glose.mcl.MclPackage#getConnector_Interactionstatement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AbstractInteraction getInteractionstatement();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.Connector#getInteractionstatement <em>Interactionstatement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interactionstatement</em>' containment reference.
	 * @see #getInteractionstatement()
	 * @generated
	 */
	void setInteractionstatement(AbstractInteraction value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' containment reference.
	 * @see #setFrom(PortRef)
	 * @see fr.inria.glose.mcl.MclPackage#getConnector_From()
	 * @model containment="true"
	 * @generated
	 */
	PortRef getFrom();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.Connector#getFrom <em>From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' containment reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(PortRef value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' containment reference.
	 * @see #setTo(PortRef)
	 * @see fr.inria.glose.mcl.MclPackage#getConnector_To()
	 * @model containment="true"
	 * @generated
	 */
	PortRef getTo();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.Connector#getTo <em>To</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' containment reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(PortRef value);

} // Connector
