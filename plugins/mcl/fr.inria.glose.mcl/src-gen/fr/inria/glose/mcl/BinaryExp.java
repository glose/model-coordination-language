/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.BinaryExp#getLeftOp <em>Left Op</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.BinaryExp#getRightOp <em>Right Op</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getBinaryExp()
 * @model abstract="true"
 * @generated
 */
public interface BinaryExp extends TCExpression {
	/**
	 * Returns the value of the '<em><b>Left Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Op</em>' containment reference.
	 * @see #setLeftOp(TCExpression)
	 * @see fr.inria.glose.mcl.MclPackage#getBinaryExp_LeftOp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TCExpression getLeftOp();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.BinaryExp#getLeftOp <em>Left Op</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Op</em>' containment reference.
	 * @see #getLeftOp()
	 * @generated
	 */
	void setLeftOp(TCExpression value);

	/**
	 * Returns the value of the '<em><b>Right Op</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Op</em>' containment reference.
	 * @see #setRightOp(TCExpression)
	 * @see fr.inria.glose.mcl.MclPackage#getBinaryExp_RightOp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TCExpression getRightOp();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.BinaryExp#getRightOp <em>Right Op</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Op</em>' containment reference.
	 * @see #getRightOp()
	 * @generated
	 */
	void setRightOp(TCExpression value);

} // BinaryExp
