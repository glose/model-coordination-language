/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triggering Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getTriggeringCondition()
 * @model abstract="true"
 * @generated
 */
public interface TriggeringCondition extends TCExpression {
} // TriggeringCondition
