/**
 */
package fr.inria.glose.mcl;

import fr.inria.glose.mbilang.ModelTemporalReference;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synchronization Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * For the moment, it is possible to specify a synchronization operation and a list of parameters
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.SynchronizationConstraint#getRightTemporalReference <em>Right Temporal Reference</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.SynchronizationConstraint#getLeftTemporalReference <em>Left Temporal Reference</em>}</li>
 * </ul>
 *
 * @see fr.inria.glose.mcl.MclPackage#getSynchronizationConstraint()
 * @model abstract="true"
 * @generated
 */
public interface SynchronizationConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Right Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Temporal Reference</em>' reference.
	 * @see #setRightTemporalReference(ModelTemporalReference)
	 * @see fr.inria.glose.mcl.MclPackage#getSynchronizationConstraint_RightTemporalReference()
	 * @model required="true"
	 * @generated
	 */
	ModelTemporalReference getRightTemporalReference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.SynchronizationConstraint#getRightTemporalReference <em>Right Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Temporal Reference</em>' reference.
	 * @see #getRightTemporalReference()
	 * @generated
	 */
	void setRightTemporalReference(ModelTemporalReference value);

	/**
	 * Returns the value of the '<em><b>Left Temporal Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Temporal Reference</em>' reference.
	 * @see #setLeftTemporalReference(ModelTemporalReference)
	 * @see fr.inria.glose.mcl.MclPackage#getSynchronizationConstraint_LeftTemporalReference()
	 * @model required="true"
	 * @generated
	 */
	ModelTemporalReference getLeftTemporalReference();

	/**
	 * Sets the value of the '{@link fr.inria.glose.mcl.SynchronizationConstraint#getLeftTemporalReference <em>Left Temporal Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Temporal Reference</em>' reference.
	 * @see #getLeftTemporalReference()
	 * @generated
	 */
	void setLeftTemporalReference(ModelTemporalReference value);

} // SynchronizationConstraint
