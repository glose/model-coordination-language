/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ready To Read</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getReadyToRead()
 * @model
 * @generated
 */
public interface ReadyToRead extends VariableTrigger {
} // ReadyToRead
