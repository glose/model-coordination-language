/**
 */
package fr.inria.glose.mcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TC Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * 
 * <!-- end-model-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getTCExpression()
 * @model abstract="true"
 * @generated
 */
public interface TCExpression extends EObject {
} // TCExpression
