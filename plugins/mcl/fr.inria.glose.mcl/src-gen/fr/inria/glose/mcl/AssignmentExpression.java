/**
 */
package fr.inria.glose.mcl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.inria.glose.mcl.MclPackage#getAssignmentExpression()
 * @model abstract="true"
 * @generated
 */
public interface AssignmentExpression extends AbstractInteraction {
} // AssignmentExpression
