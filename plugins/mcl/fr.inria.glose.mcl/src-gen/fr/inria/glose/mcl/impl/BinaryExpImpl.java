/**
 */
package fr.inria.glose.mcl.impl;

import fr.inria.glose.mcl.BinaryExp;
import fr.inria.glose.mcl.MclPackage;
import fr.inria.glose.mcl.TCExpression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Exp</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.inria.glose.mcl.impl.BinaryExpImpl#getLeftOp <em>Left Op</em>}</li>
 *   <li>{@link fr.inria.glose.mcl.impl.BinaryExpImpl#getRightOp <em>Right Op</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class BinaryExpImpl extends TCExpressionImpl implements BinaryExp {
	/**
	 * The cached value of the '{@link #getLeftOp() <em>Left Op</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftOp()
	 * @generated
	 * @ordered
	 */
	protected TCExpression leftOp;

	/**
	 * The cached value of the '{@link #getRightOp() <em>Right Op</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightOp()
	 * @generated
	 * @ordered
	 */
	protected TCExpression rightOp;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MclPackage.Literals.BINARY_EXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TCExpression getLeftOp() {
		return leftOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftOp(TCExpression newLeftOp, NotificationChain msgs) {
		TCExpression oldLeftOp = leftOp;
		leftOp = newLeftOp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.BINARY_EXP__LEFT_OP, oldLeftOp, newLeftOp);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeftOp(TCExpression newLeftOp) {
		if (newLeftOp != leftOp) {
			NotificationChain msgs = null;
			if (leftOp != null)
				msgs = ((InternalEObject) leftOp).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.BINARY_EXP__LEFT_OP, null, msgs);
			if (newLeftOp != null)
				msgs = ((InternalEObject) newLeftOp).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.BINARY_EXP__LEFT_OP, null, msgs);
			msgs = basicSetLeftOp(newLeftOp, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.BINARY_EXP__LEFT_OP, newLeftOp,
					newLeftOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TCExpression getRightOp() {
		return rightOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightOp(TCExpression newRightOp, NotificationChain msgs) {
		TCExpression oldRightOp = rightOp;
		rightOp = newRightOp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MclPackage.BINARY_EXP__RIGHT_OP, oldRightOp, newRightOp);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRightOp(TCExpression newRightOp) {
		if (newRightOp != rightOp) {
			NotificationChain msgs = null;
			if (rightOp != null)
				msgs = ((InternalEObject) rightOp).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.BINARY_EXP__RIGHT_OP, null, msgs);
			if (newRightOp != null)
				msgs = ((InternalEObject) newRightOp).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - MclPackage.BINARY_EXP__RIGHT_OP, null, msgs);
			msgs = basicSetRightOp(newRightOp, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MclPackage.BINARY_EXP__RIGHT_OP, newRightOp,
					newRightOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MclPackage.BINARY_EXP__LEFT_OP:
			return basicSetLeftOp(null, msgs);
		case MclPackage.BINARY_EXP__RIGHT_OP:
			return basicSetRightOp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MclPackage.BINARY_EXP__LEFT_OP:
			return getLeftOp();
		case MclPackage.BINARY_EXP__RIGHT_OP:
			return getRightOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MclPackage.BINARY_EXP__LEFT_OP:
			setLeftOp((TCExpression) newValue);
			return;
		case MclPackage.BINARY_EXP__RIGHT_OP:
			setRightOp((TCExpression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MclPackage.BINARY_EXP__LEFT_OP:
			setLeftOp((TCExpression) null);
			return;
		case MclPackage.BINARY_EXP__RIGHT_OP:
			setRightOp((TCExpression) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MclPackage.BINARY_EXP__LEFT_OP:
			return leftOp != null;
		case MclPackage.BINARY_EXP__RIGHT_OP:
			return rightOp != null;
		}
		return super.eIsSet(featureID);
	}

} //BinaryExpImpl
