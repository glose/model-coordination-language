package fr.inria.glose.mcl.ui.handler;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.generator.GeneratorContext;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class GenerationHandler extends AbstractHandler implements IHandler {
  @Inject
  private IGenerator2 generator;
  
  @Inject
  private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;
  
  @Inject
  private IResourceDescriptions resourceDescriptions;
  
  @Inject
  private IResourceSetProvider resourceSetProvider;
  
  @Override
  public Object execute(final ExecutionEvent event) throws ExecutionException {
    ISelection selection = HandlerUtil.getCurrentSelection(event);
    if ((selection instanceof IStructuredSelection)) {
      IStructuredSelection structuredSelection = ((IStructuredSelection) selection);
      Object firstElement = structuredSelection.getFirstElement();
      if ((firstElement instanceof IFile)) {
        IFile file = ((IFile) firstElement);
        IProject project = file.getProject();
        IFolder srcGenFolder = project.getFolder("src-gen");
        boolean _exists = srcGenFolder.exists();
        boolean _not = (!_exists);
        if (_not) {
          try {
            NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
            srcGenFolder.create(true, true, _nullProgressMonitor);
          } catch (final Throwable _t) {
            if (_t instanceof CoreException) {
              return null;
            } else {
              throw Exceptions.sneakyThrow(_t);
            }
          }
        }
        final EclipseResourceFileSystemAccess2 fsa = this.fileAccessProvider.get();
        fsa.setProject(project);
        fsa.setOutputPath("src-gen");
        NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
        fsa.setMonitor(_nullProgressMonitor);
        URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
        ResourceSet rs = this.resourceSetProvider.get(project);
        Resource r = rs.getResource(uri, true);
        GeneratorContext _generatorContext = new GeneratorContext();
        this.generator.doGenerate(r, fsa, _generatorContext);
      }
    }
    return null;
  }
  
  @Override
  public boolean isEnabled() {
    return true;
  }
}
