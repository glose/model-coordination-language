/*
 * generated by Xtext 2.14.0
 */
package fr.inria.glose.mcl.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.CrossReference
import fr.inria.glose.mbilang.Port
import fr.inria.glose.mbilang.PortDirection
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.xtext.EcoreUtil2
import fr.inria.glose.mcl.Connector
import fr.inria.glose.mcl.MCSpecification

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class MCLProposalProvider extends AbstractMCLProposalProvider {
	
//	override void completeInteraction_SourcePort(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		lookupCrossReference(assignment.getTerminal() as CrossReference, context, acceptor,
//			[
//				filterOutputPort
//			]
//		)
//	}
//	override void completeInteraction_TargetPort(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		lookupCrossReference(assignment.getTerminal() as CrossReference, context, acceptor,
//			[
//				t | filterInputPort(t) && filterAlreadyInUsePorts(t, model) && filterOwnPorts(t, model)
//			]
//		)
//	}

//	override void completeConnector_From(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//	
//	}
//	override void completeConnector_To(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		lookupCrossReference(assignment.getTerminal() as CrossReference, context, acceptor,
//			[
//				t | filterInputPort(t) && filterAlreadyInUsePorts(t, model) && filterOwnPorts(t, model)
//			]
//		)
//	}

	def filterOutputPort(IEObjectDescription p) {
		var rs = new ResourceSetImpl();	
		var res = rs.getEObject(p.EObjectURI, true)
		if (res instanceof Port) {
			return res.direction == PortDirection.OUTPUT
		}
		return false
	}
	
	def filterInputPort(IEObjectDescription p) {
		var rs = new ResourceSetImpl();	
		var res = rs.getEObject(p.EObjectURI, true)
		if (res instanceof Port) {
			return res.direction == PortDirection.INPUT
		}
		return false
	}
	
	def filterAlreadyInUsePorts(IEObjectDescription p, EObject model) {
		var root = EcoreUtil2.getContainerOfType(model, MCSpecification)
		
		for (tp : model.eAllContents.filter[ m | m instanceof Connector].toIterable) {
			if (EcoreUtil2.hasSameURI(p.EObjectOrProxy, (tp as Connector).to))
			return false
		}
		return true
	}
	
	def filterOwnPorts(IEObjectDescription p, EObject model) {
		var rs = new ResourceSetImpl();	
		
		var root = EcoreUtil2.getContainerOfType(model, Connector)
		var sourcePort = root.from
		
		var targetPort = rs.getEObject(p.EObjectURI, true)
			
		return !EcoreUtil2.hasSameURI(EcoreUtil2.getRootContainer(sourcePort), EcoreUtil2.getRootContainer(targetPort))
	}
	
}
