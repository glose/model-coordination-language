/*
 * generated by Xtext 2.14.0
 */
package fr.inria.glose.mcl.ui.quickfix

import org.eclipse.gemoc.gexpressions.xtext.ui.quickfix.GExpressionsQuickfixProvider

/**
 * Custom quickfixes.
 *
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
class MCLQuickfixProvider extends GExpressionsQuickfixProvider {

//	@Fix(MCLValidator.INVALID_NAME)
//	def capitalizeName(Issue issue, IssueResolutionAcceptor acceptor) {
//		acceptor.accept(issue, 'Capitalize name', 'Capitalize the name.', 'upcase.png') [
//			context |
//			val xtextDocument = context.xtextDocument
//			val firstLetter = xtextDocument.get(issue.offset, 1)
//			xtextDocument.replace(issue.offset, 1, firstLetter.toUpperCase)
//		]
//	}
}
