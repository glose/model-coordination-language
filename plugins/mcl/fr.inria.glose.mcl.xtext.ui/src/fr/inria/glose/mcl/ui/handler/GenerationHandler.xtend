package fr.inria.glose.mcl.ui.handler

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.commands.IHandler
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2
import org.eclipse.xtext.generator.GeneratorContext
import org.eclipse.xtext.generator.IGenerator2
import org.eclipse.xtext.resource.IResourceDescriptions
import org.eclipse.xtext.ui.resource.IResourceSetProvider

public class GenerationHandler extends AbstractHandler implements IHandler {
     
      @Inject IGenerator2 generator;

    @Inject Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;

    @Inject
    IResourceDescriptions resourceDescriptions;

    @Inject
    IResourceSetProvider resourceSetProvider;

	override execute(ExecutionEvent event) throws ExecutionException {

        var selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof IStructuredSelection) {
            var structuredSelection = selection as IStructuredSelection;
            var firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IFile) {
                var file = firstElement as IFile;
                var project = file.getProject();
                var srcGenFolder = project.getFolder("src-gen");
                if (!srcGenFolder.exists()) {
                    try {
                        srcGenFolder.create(true, true,
                                new NullProgressMonitor());
                    } catch (CoreException e) {
                        return null;
                    }
                }

                val EclipseResourceFileSystemAccess2 fsa = fileAccessProvider.get();
                fsa.setProject(project);
                fsa.setOutputPath("src-gen");
                fsa.setMonitor(new NullProgressMonitor()); 
                var uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
                var rs = resourceSetProvider.get(project);
                var r = rs.getResource(uri, true);
                generator.doGenerate(r, fsa, new GeneratorContext());

            }
        }
        return null;
    }

    override isEnabled() {
        return true;
    }
 
}