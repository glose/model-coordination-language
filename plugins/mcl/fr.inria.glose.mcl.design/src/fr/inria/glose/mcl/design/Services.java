package fr.inria.glose.mcl.design;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.inria.glose.mbilang.Port;
import fr.inria.glose.mcl.AbstractInteraction;
import fr.inria.glose.mcl.Connector;
import fr.inria.glose.mcl.PortRef;

/**
 * The services class used by VSM.
 */
public class Services {
    
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public EObject myService(EObject self, String arg) {
       // TODO Auto-generated code
      return self;
    }
    
//	public EObject getSourcePort(EObject eo) {
//		if (eo instanceof Connector) {
//			return getSourcePort((Connector)eo);
//		} else if (eo instanceof Interaction) {
//			return getSourcePort((Interaction)eo);
//		}
//		return eo;
//	}
//	
//	protected Port getSourcePort(Connector eo) {
//		System.out.println("là");
//		Connector c = (Connector)eo;
//		ResourceSet rs = c.eContainer().eResource().getResourceSet();
////		if (c.getInteractionstatement() instanceof InteractionStatement) {
////			Port targetPort = ((InteractionStatement)c.getInteractionstatement()).getInteractionRules().get(0).getSourcePort();		
////			Port p = (Port) EcoreUtil.resolve(targetPort, rs);
////			return p;
////		}else {
//			ArrayList<Port> res = new ArrayList<>();
//			c.getTrigger().eAllContents().forEachRemaining(eo2 -> {
//				if (eo2 instanceof PortRef) {
//						res.add(((PortRef)eo).getRefPort());
//				}
//			});
//			c.getInteractionstatement().eAllContents().forEachRemaining(eo2 -> {
//				if (eo2 instanceof PortRef) {
//					if (eo2.eContainingFeature().getName().compareTo("leftOperand") == 0) {
//						res.add(((PortRef)eo).getRefPort());
//					}
//				}
//			});
//			return res.get(0);
////		}
//	}
//	
//	protected EObject getSourcePort(Interaction eo) {
//			System.out.println("toto");
//		return EcoreUtil.resolve(eo.getSourcePort(), eo.eResource().getResourceSet());
//	}
//	
//	
//	public ArrayList<Port> getTargetPorts(EObject eo) {
//		System.out.println("zaza");
//		ArrayList<Port> res = new ArrayList<>();
//		if (eo instanceof Connector) {
//			return getTargetPorts((Connector)eo);
//		} else if (eo instanceof Interaction) {
//			return getTargetPorts((Interaction)eo);
//		} else {
//			return res;
//		}
//	}
//	
//	protected ArrayList<Port> getTargetPorts(Connector eo) {
//		System.out.println("ici");
//		ArrayList<Port> res = new ArrayList<>();
//		ArrayList<Port> resFinal = new ArrayList<>();
//		Connector c = (Connector)eo;
//		EcoreUtil.resolveAll(c.eContainer().eResource());
//		AbstractInteraction i = c.getInteractionstatement();
//		if (i instanceof InteractionStatement) {
//			Interaction ir = ((InteractionStatement)i).getInteractionRules().get(0);
//			res.addAll(ir.getTargetPort());
//			for (Port p : res) {
//				resFinal.add((Port) EcoreUtil.resolve(p, c.eContainer().eResource().getResourceSet()));
//			}
//			return resFinal;
//		}else {
//			i.eAllContents().forEachRemaining(eo2 -> {
//				if (eo2 instanceof PortRef) {
//					if (eo2.eContainingFeature().getName().compareTo("rightOperand") == 0
//							||
//							eo2.eContainingFeature().getName().compareTo("operand") == 0) {
//						resFinal.add(((PortRef)eo).getRefPort());
//					}
//				}
//			});
//		}
//		System.out.println("getTargetPorts() on "+eo.getName()+" returns "+resFinal);
//
//		return resFinal;
//	}
//	protected ArrayList<Port> getTargetPorts(Interaction ir) {
//		ArrayList<Port> res = new ArrayList<>();
//		ArrayList<Port> resFinal = new ArrayList<>();
//		res.addAll(ir.getTargetPort());
//		for (Port p : res) {
//			resFinal.add((Port) EcoreUtil.resolve(p, ir.eResource().getResourceSet()));
//		}
//		return resFinal;
//	}
    
}