/**
 */
package fr.inria.glose.mcl.provider;

import fr.inria.glose.mcl.Connector;
import fr.inria.glose.mcl.MclFactory;
import fr.inria.glose.mcl.MclPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.inria.glose.mcl.Connector} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ConnectorItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MclPackage.Literals.CONNECTOR__TRIGGER);
			childrenFeatures.add(MclPackage.Literals.CONNECTOR__SYNCHRONIZATION_RULE);
			childrenFeatures.add(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT);
			childrenFeatures.add(MclPackage.Literals.CONNECTOR__FROM);
			childrenFeatures.add(MclPackage.Literals.CONNECTOR__TO);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Connector.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Connector"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Connector) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_Connector_type")
				: getString("_UI_Connector_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Connector.class)) {
		case MclPackage.CONNECTOR__TRIGGER:
		case MclPackage.CONNECTOR__SYNCHRONIZATION_RULE:
		case MclPackage.CONNECTOR__INTERACTIONSTATEMENT:
		case MclPackage.CONNECTOR__FROM:
		case MclPackage.CONNECTOR__TO:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER,
				MclFactory.eINSTANCE.createThresholdExpression()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER,
				MclFactory.eINSTANCE.createBinaryExpOperator()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER,
				MclFactory.eINSTANCE.createPeriodicExpression()));

		newChildDescriptors.add(
				createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER, MclFactory.eINSTANCE.createUpdated()));

		newChildDescriptors.add(
				createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER, MclFactory.eINSTANCE.createReadyToRead()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__TRIGGER,
				MclFactory.eINSTANCE.createEventTrigger()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__SYNCHRONIZATION_RULE,
				MclFactory.eINSTANCE.createSimpleSync()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createNot()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createUnaryMinus()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createAssignment()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createBinaryPlus()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createBinaryMinus()));

		newChildDescriptors.add(createChildParameter(MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT,
				MclFactory.eINSTANCE.createPortRef()));

		newChildDescriptors
				.add(createChildParameter(MclPackage.Literals.CONNECTOR__FROM, MclFactory.eINSTANCE.createPortRef()));

		newChildDescriptors
				.add(createChildParameter(MclPackage.Literals.CONNECTOR__TO, MclFactory.eINSTANCE.createPortRef()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify = childFeature == MclPackage.Literals.CONNECTOR__INTERACTIONSTATEMENT
				|| childFeature == MclPackage.Literals.CONNECTOR__FROM
				|| childFeature == MclPackage.Literals.CONNECTOR__TO;

		if (qualify) {
			return getString("_UI_CreateChild_text2",
					new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
