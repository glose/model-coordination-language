/**
 */
package fr.inria.glose.mcl.provider;

import fr.inria.glose.mcl.util.MclAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MclItemProviderAdapterFactory extends MclAdapterFactory
		implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MclItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.MCSpecification} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCSpecificationItemProvider mcSpecificationItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.MCSpecification}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMCSpecificationAdapter() {
		if (mcSpecificationItemProvider == null) {
			mcSpecificationItemProvider = new MCSpecificationItemProvider(this);
		}

		return mcSpecificationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Connector} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectorItemProvider connectorItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Connector}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createConnectorAdapter() {
		if (connectorItemProvider == null) {
			connectorItemProvider = new ConnectorItemProvider(this);
		}

		return connectorItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.ImportInterfaceStatement} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImportInterfaceStatementItemProvider importInterfaceStatementItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.ImportInterfaceStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createImportInterfaceStatementAdapter() {
		if (importInterfaceStatementItemProvider == null) {
			importInterfaceStatementItemProvider = new ImportInterfaceStatementItemProvider(this);
		}

		return importInterfaceStatementItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.ThresholdExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThresholdExpressionItemProvider thresholdExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.ThresholdExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createThresholdExpressionAdapter() {
		if (thresholdExpressionItemProvider == null) {
			thresholdExpressionItemProvider = new ThresholdExpressionItemProvider(this);
		}

		return thresholdExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Greater} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GreaterItemProvider greaterItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Greater}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGreaterAdapter() {
		if (greaterItemProvider == null) {
			greaterItemProvider = new GreaterItemProvider(this);
		}

		return greaterItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Lesser} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LesserItemProvider lesserItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Lesser}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLesserAdapter() {
		if (lesserItemProvider == null) {
			lesserItemProvider = new LesserItemProvider(this);
		}

		return lesserItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.BinaryExpOperator} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryExpOperatorItemProvider binaryExpOperatorItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.BinaryExpOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBinaryExpOperatorAdapter() {
		if (binaryExpOperatorItemProvider == null) {
			binaryExpOperatorItemProvider = new BinaryExpOperatorItemProvider(this);
		}

		return binaryExpOperatorItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Variable} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableItemProvider variableItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Variable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVariableAdapter() {
		if (variableItemProvider == null) {
			variableItemProvider = new VariableItemProvider(this);
		}

		return variableItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.PeriodicExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PeriodicExpressionItemProvider periodicExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.PeriodicExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPeriodicExpressionAdapter() {
		if (periodicExpressionItemProvider == null) {
			periodicExpressionItemProvider = new PeriodicExpressionItemProvider(this);
		}

		return periodicExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.SimpleSync} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleSyncItemProvider simpleSyncItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.SimpleSync}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSimpleSyncAdapter() {
		if (simpleSyncItemProvider == null) {
			simpleSyncItemProvider = new SimpleSyncItemProvider(this);
		}

		return simpleSyncItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Updated} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdatedItemProvider updatedItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Updated}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUpdatedAdapter() {
		if (updatedItemProvider == null) {
			updatedItemProvider = new UpdatedItemProvider(this);
		}

		return updatedItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.ReadyToRead} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReadyToReadItemProvider readyToReadItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.ReadyToRead}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createReadyToReadAdapter() {
		if (readyToReadItemProvider == null) {
			readyToReadItemProvider = new ReadyToReadItemProvider(this);
		}

		return readyToReadItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.EventTrigger} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventTriggerItemProvider eventTriggerItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.EventTrigger}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createEventTriggerAdapter() {
		if (eventTriggerItemProvider == null) {
			eventTriggerItemProvider = new EventTriggerItemProvider(this);
		}

		return eventTriggerItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Not} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotItemProvider notItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Not}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createNotAdapter() {
		if (notItemProvider == null) {
			notItemProvider = new NotItemProvider(this);
		}

		return notItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.UnaryMinus} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryMinusItemProvider unaryMinusItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.UnaryMinus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createUnaryMinusAdapter() {
		if (unaryMinusItemProvider == null) {
			unaryMinusItemProvider = new UnaryMinusItemProvider(this);
		}

		return unaryMinusItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.Assignment} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssignmentItemProvider assignmentItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.Assignment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAssignmentAdapter() {
		if (assignmentItemProvider == null) {
			assignmentItemProvider = new AssignmentItemProvider(this);
		}

		return assignmentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.BinaryPlus} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryPlusItemProvider binaryPlusItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.BinaryPlus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBinaryPlusAdapter() {
		if (binaryPlusItemProvider == null) {
			binaryPlusItemProvider = new BinaryPlusItemProvider(this);
		}

		return binaryPlusItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.BinaryMinus} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryMinusItemProvider binaryMinusItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.BinaryMinus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBinaryMinusAdapter() {
		if (binaryMinusItemProvider == null) {
			binaryMinusItemProvider = new BinaryMinusItemProvider(this);
		}

		return binaryMinusItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.inria.glose.mcl.PortRef} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortRefItemProvider portRefItemProvider;

	/**
	 * This creates an adapter for a {@link fr.inria.glose.mcl.PortRef}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPortRefAdapter() {
		if (portRefItemProvider == null) {
			portRefItemProvider = new PortRefItemProvider(this);
		}

		return portRefItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>) type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void dispose() {
		if (mcSpecificationItemProvider != null)
			mcSpecificationItemProvider.dispose();
		if (connectorItemProvider != null)
			connectorItemProvider.dispose();
		if (importInterfaceStatementItemProvider != null)
			importInterfaceStatementItemProvider.dispose();
		if (thresholdExpressionItemProvider != null)
			thresholdExpressionItemProvider.dispose();
		if (greaterItemProvider != null)
			greaterItemProvider.dispose();
		if (lesserItemProvider != null)
			lesserItemProvider.dispose();
		if (binaryExpOperatorItemProvider != null)
			binaryExpOperatorItemProvider.dispose();
		if (variableItemProvider != null)
			variableItemProvider.dispose();
		if (periodicExpressionItemProvider != null)
			periodicExpressionItemProvider.dispose();
		if (simpleSyncItemProvider != null)
			simpleSyncItemProvider.dispose();
		if (updatedItemProvider != null)
			updatedItemProvider.dispose();
		if (readyToReadItemProvider != null)
			readyToReadItemProvider.dispose();
		if (eventTriggerItemProvider != null)
			eventTriggerItemProvider.dispose();
		if (notItemProvider != null)
			notItemProvider.dispose();
		if (unaryMinusItemProvider != null)
			unaryMinusItemProvider.dispose();
		if (assignmentItemProvider != null)
			assignmentItemProvider.dispose();
		if (binaryPlusItemProvider != null)
			binaryPlusItemProvider.dispose();
		if (binaryMinusItemProvider != null)
			binaryMinusItemProvider.dispose();
		if (portRefItemProvider != null)
			portRefItemProvider.dispose();
	}

}
