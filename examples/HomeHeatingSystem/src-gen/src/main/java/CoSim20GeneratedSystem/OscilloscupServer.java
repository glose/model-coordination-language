package CoSim20GeneratedSystem;

import oscilloscup.Graphics2DPlotter;
import oscilloscup.InteractiveSwingPlotter;
import oscilloscup.SwingPlotter;
import oscilloscup.data.Figure;
import oscilloscup.data.Point;
import oscilloscup.data.rendering.figure.ConnectedLineFigureRenderer;
import oscilloscup.data.rendering.point.CirclePointRenderer;
import java.util.Random;
import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OscilloscupServer {
  private final ServerSocket ss;
  public final Map<String, Graphics2DPlotter> plots = new ConcurrentHashMap<>();

  public OscilloscupServer(int port) throws IOException {
    this.ss = new ServerSocket(port);
    // listens in the background
    new Thread(
            () -> {
              try {
                listen();
              } catch (IOException e) {
                e.printStackTrace();
              }
            })
        .start();
  }

  private Object listen() throws IOException {
    while (true) {
      Socket client = ss.accept();
      // each new client will be processed in
      new Thread(
              () -> {
                try {
                  newClient(client);
                } catch (IOException e) {
                  e.printStackTrace();
                }
              })
          .start();
    }
  }

  private void newClient(Socket client) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
    while (true) {
      String line = br.readLine();
      if (line == null) {
        break;
      } else {
        String[] path = line.split("/");
        Graphics2DPlotter plot = plots.get(path[0]);
        if (path.length != 4) {
          throw new IllegalStateException("malformed command: " + line);
        }
        if (plot == null) {
          throw new IllegalStateException("unknown plot: " + path[0]);
        }
        int figureIndex = Integer.valueOf(path[1]);
        if (figureIndex >= plot.getFigure().getFigureCount()) {
          throw new IllegalStateException("no such figure: " + figureIndex);
        }
        Figure figure = plot.getFigure().getFigureAt(figureIndex);
        double x = Double.valueOf(path[2]);
        double y = Double.valueOf(path[3]);
        figure.addPoint(new Point(x, y));
      }
    }
  }

  public static void createAutoRefreshServer(int port, String plotID, int refreshPeriodInMs)
      throws IOException {
    SwingPlotter sp = new InteractiveSwingPlotter();
    Figure rootFigure = new Figure();
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    CirclePointRenderer renderer = new CirclePointRenderer();
    renderer.setRadius(3);
    rootFigure.getFigureAt(0).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(0).setName("room_temperature");
    rootFigure.getFigureAt(0).setColor();

    rootFigure.getFigureAt(1).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(1).setName("roomTemperature");
    rootFigure.getFigureAt(1).setColor();

    rootFigure.getFigureAt(2).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(2).setName("water_temperature");
    rootFigure.getFigureAt(2).setColor();

    rootFigure.getFigureAt(3).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(3).setName("temperature");
    rootFigure.getFigureAt(3).setColor();

    rootFigure.getFigureAt(4).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(4).setName("heat");
    rootFigure.getFigureAt(4).setColor();

    rootFigure.getFigureAt(5).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(5).setName("temperature");
    rootFigure.getFigureAt(5).setColor();

    sp.getGraphics2DPlotter().getSpace().getYDimension().getOriginAxis().setVisible(false);
    sp.getGraphics2DPlotter().getSpace().getXDimension().getOriginAxis().setVisible(false);
    sp.getGraphics2DPlotter().getSpace().getYDimension().getGrid().setVisible(false);
    //        sp.getGraphics2DPlotter().getSpace().getXDimension().setBounds(0,10000);
    //        sp.getGraphics2DPlotter().getSpace().getXDimension().setMaximumIsAutomatic(false);
    //        sp.getGraphics2DPlotter().getSpace().getXDimension().setMinimumIsAutomatic(false);
    sp.getGraphics2DPlotter().setFigure(rootFigure);
    sp.getGraphics2DPlotter().getSpace().getXDimension().getLegend().setText("time(ms)");
    sp.getGraphics2DPlotter()
        .getSpace()
        .getYDimension()
        .getLegend()
        .setText("CPU Temperature (°C)");
    JFrame f = new JFrame();
    f.setContentPane(sp);
    f.setSize(1200, 600);
    f.setVisible(true);
    // starts the server in background
    OscilloscupServer server = new OscilloscupServer(port);

    new Thread(
            () -> {
              while (true) {
                sp.repaint();
                try {
                  Thread.sleep(refreshPeriodInMs);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            })
        .start();

    // allows the server to control myPlotter, known by the client as "plot"
    server.plots.put(plotID, sp.getGraphics2DPlotter());
  }

  public static void main(String[] args) throws IOException {
    SwingPlotter sp = new SwingPlotter();
    Figure rootFigure = new Figure();
    rootFigure.addFigure(new Figure());
    rootFigure.addFigure(new Figure());
    rootFigure.getFigureAt(0).addRenderer(new ConnectedLineFigureRenderer());
    rootFigure.getFigureAt(1).addRenderer(new ConnectedLineFigureRenderer());
    sp.getGraphics2DPlotter().setFigure(rootFigure);
    JFrame f = new JFrame();
    f.setContentPane(sp);
    f.setSize(500, 500);
    f.setVisible(true);
    // starts the server in background
    OscilloscupServer server = new OscilloscupServer(43444);

    new Thread(
            () -> {
              while (true) {
                sp.repaint();
                try {
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            })
        .start();

    // allows the server to control myPlotter, known by the client as "plot"
    server.plots.put("plot", sp.getGraphics2DPlotter());
  }
}
