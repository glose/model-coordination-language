package SystemBoxControllerAndPID;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.FMIInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

public class PidBI extends CoordinationInterface {
    // ----------------------------------------------------------------
    // FMI Standard
    // ----------------------------------------------------------------
    double startTime = 0.0;
    double stopTime = CONFIG.EndOfSimulation.doubleValue();
    String fmuPath = "src/test/resources/FanController.fmu";

    // -----------------------------------------------------------------
    // Constant port
    // -----------------------------------------------------------------
    Port targetTemperature = new ConstantPort("targetTemperature","targetTemperature", "FanController", 65);
    Port kp = new ConstantPort("Kp", "Kp", "FanController", 5.0);

    // -----------------------------------------------------------------
    // Exported port
    // -----------------------------------------------------------------
    Port CPUfanSpeed = new InitiatorPort("CPUfanSpeed","CPUfanSpeed", "Box", 0); // Output
    FollowerDeterministicPort CPUTemperature = new FollowerDeterministicPort("CPUTemperature","CPUTemperature", "Box", 25, new TemporalPredicate(5)); // Input

    public PidBI() {
        super("PID", "localhost", 26003, 26103, 0, 1);

        // Setup the FMI model
        model = new FMIInterface(fmuPath, startTime, stopTime);

        // Connector between FanController -> Box
        initiatorPorts.add(CPUfanSpeed);
        todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, CPUfanSpeed, "tcp://localhost:26003", now, new TemporalPredicate(5)));

        followerDeterministicPorts.add(CPUTemperature);

        // The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("CPUfanSpeed", CPUfanSpeed);
        portMap.put("CPUTemperatureForPID", CPUTemperature);

        addNewInputPort("CPUTemperatureForPID", "tcp://localhost:26001", "tcp://localhost:26101");

        model.set(targetTemperature.associatedModelVariableName, targetTemperature.getValue());
        model.set(kp.associatedModelVariableName, kp.getValue());

    }

    @Override
    public void onTime(Action currentAction, StopCondition sr) {
        assert (now.compareTo(currentAction.temporalHorizon) == 0);
            // Execute the corresponding action
            if (currentAction.port.compareTo(CPUfanSpeed) == 0) {
                Object value = model.get(CPUfanSpeed.associatedModelVariableName);
                publish(CPUfanSpeed, value, now );
            } else if (currentAction.port.compareTo(CPUTemperature) == 0) {
                model.set(CPUTemperature.associatedModelVariableName, (Double) currentAction.getValue());
            }

            currentAction.setDone();

            if (currentAction instanceof PeriodicAction) {
                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
            }
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onSync(Action currentAction) {
        onTime(currentAction, null);
    }

    @Override
    public void onEnd() {
        System.out.println("PID END");

    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {
        return null;
    }
}
