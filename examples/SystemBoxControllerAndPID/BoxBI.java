package SystemBoxControllerAndPID;

import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.FMIInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class BoxBI extends CoordinationInterface {
    //-------------------------------
    //Plotter stuff
    //-------------------------------
    OscilloscupClient plotterClient;

    // ----------------------------------------------------------------
    // FMI Standard
    // ----------------------------------------------------------------
    double startTime = 0.0;
    double stopTime = CONFIG.EndOfSimulation.doubleValue();
    String fmuPath = "src/test/resources/CPUinBoxWithFanHeatModel.fmu";

    // ----------------------------------------------------------------
    // Exported Variables / Ports
    // ----------------------------------------------------------------
    public Port isStopped = new FollowerPort("CPUprotection::switchCPUState", "isStopped", "CTRL", false); // Input
    public Port CPUTemperatureForController = new FollowerPort("CPUTemperatureForController","CPUTemperature", "CTRL", 0.0); // Output
    public Port CPUTemperatureForPID = new InitiatorPort("CPUTemperatureForPID","CPUTemperature", "CTRL", 0.0); // Output
    public Port BoxTemperature = new FollowerPort("BoxTemperature", "BoxTemperature", "TestBox", 25);  // Input
    public FollowerDeterministicPort CPUfanSpeed = new FollowerDeterministicPort("CPUfanSpeed","CPUfanSpeed", "PID", 0, new TemporalPredicate(5)); // Input

    PrintWriter writer4CPUfanSpeed;

    public BoxBI() {
        super("Box", "localhost", 26001, 26101, 2, 2);

        try {
            writer4CPUfanSpeed = new PrintWriter(new File("boxbi_cpufanspeed.csv"));
            writer4CPUfanSpeed.write("time,CPUfanSpeed\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            plotterClient  = new OscilloscupClient("localhost", 43444);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Setup the FMI model
        model = new FMIInterface(fmuPath, startTime, stopTime);

        // Connector between Box CPUTemperature -> PIDController using dt = 5 seconds
        initiatorPorts.add(CPUTemperatureForPID);
        todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, CPUTemperatureForPID, "tcp://localhost:26001", now, new TemporalPredicate(5)));
        // Connector between Controller -> Box using Updated condition
        followerPorts.add(isStopped);
        // Connector between Test -> Box using dt (fixed in this example)
        followerPorts.add(BoxTemperature);
        // Connector between Box -> Controller using R2R condition on Controller
        followerPorts.add(CPUTemperatureForController);
        // Connector between PID and Box using dt = 5
        followerDeterministicPorts.add(CPUfanSpeed);

        // The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("CPUprotection::switchCPUState", isStopped);
        portMap.put("CPUprotection::cpuTemperature", CPUTemperatureForController);
        portMap.put("CPUTemperature", CPUTemperatureForPID);
        portMap.put("BoxTemperature", BoxTemperature);
        portMap.put("CPUfanSpeed", CPUfanSpeed);

        // Set the input ports
        addNewInputPort("CPUprotection::switchCPUState", "tcp://localhost:26000", "tcp://localhost:26100");
        addNewInputPort("CPUprotection::cpuTemperature", "tcp://localhost:26000", "tcp://localhost:26100");
        addNewInputPort("CPUfanSpeed", "tcp://localhost:26003", "tcp://localhost:26103");

        model.set(BoxTemperature.associatedModelVariableName, 25); //should be 25 since somehow encoded inside the modelica
        model.set(CPUfanSpeed.associatedModelVariableName, 0);
        model.set(isStopped.associatedModelVariableName, false);
    }

    @Override
    public void onTime(Action currentAction, StopCondition sr) {
        assert (now.compareTo(currentAction.temporalHorizon) == 0);
            // Execute the corresponding action
            if (currentAction.port.compareTo(CPUfanSpeed) == 0) {
                model.set(CPUfanSpeed.associatedModelVariableName, (Double) currentAction.getValue());
                try {
                    plotterClient.addPoint("cosimPlotter", 2, now.doubleValue(), (Double) currentAction.getValue());
                    writer4CPUfanSpeed.write(now.doubleValue() + "," + (Double) currentAction.getValue() + "\n");
                    writer4CPUfanSpeed.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (currentAction.port.compareTo(isStopped) == 0) {
                model.set(isStopped.associatedModelVariableName, (Boolean) currentAction.getValue());
                try {
                    plotterClient.addPoint("cosimPlotter", 1, now.doubleValue(), ((Boolean) currentAction.getValue() == false) ? 0 : 20 );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (currentAction.port.compareTo(BoxTemperature) == 0) {
                //TODO check this: it should be a get/publish not a set isn't it ? or is it for testing purpose ?
                model.set(BoxTemperature.associatedModelVariableName, (Double) currentAction.getValue());
            } else if (currentAction.port.compareTo(CPUTemperatureForController) == 0) {
                // Port ID is in the currentAction
                double temperature = (double) model.get(CPUTemperatureForController.associatedModelVariableName);
                publish(CPUTemperatureForController, temperature, now);
                try {
                    plotterClient.addPoint("cosimPlotter", 0, now.doubleValue(), temperature);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (currentAction.port.compareTo(CPUTemperatureForPID) == 0) {
                // Port ID is in the currentAction
                double temperature = (double) model.get(CPUTemperatureForController.associatedModelVariableName);
                publish(CPUTemperatureForPID, (double) temperature, now);
                try {
                    plotterClient.addPoint("cosimPlotter", 0, now.doubleValue(), temperature);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //TODO move it
            doExtraPlot();


            currentAction.setDone();

            if (currentAction instanceof PeriodicAction) {
                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
            }

    }

    private void doExtraPlot() {
        double boxTemp = (double) model.get(BoxTemperature.associatedModelVariableName);
        try {
            plotterClient.addPoint("cosimPlotter", 3, now.doubleValue(), boxTemp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        double cpuIsStopped = (double) model.get(isStopped.associatedModelVariableName);
        try {
            plotterClient.addPoint("cosimPlotter", 1, now.doubleValue(), cpuIsStopped * 20 );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onSync(Action currentAction) {
        model.doStep(new TemporalPredicate(0)); //needed to ensure the previous sets are actually realized
        onTime(currentAction, null);
    }

    @Override
    public void onEnd() {
        System.out.println("BOX END");
        writer4CPUfanSpeed.flush();
    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {
        return null;
    }
}