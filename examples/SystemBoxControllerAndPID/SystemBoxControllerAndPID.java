package SystemBoxControllerAndPID;

import fr.cnrs.i3s.luchogie.oscilloscup.server.OscilloscupServer;
import fr.inria.glose.cosim20.CONFIG;
import fr.inria.glose.cosim20.Utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SystemBoxControllerAndPID {
    // Define the "infinite" time as the time of the end of simulation plus an Epsilon

    public static void main(String[] args) {
        CONFIG.EndOfSimulation = Utils.toBigDecimal(30000);
        CONFIG.showDebugMessage = true;
        try {
            OscilloscupServer.createAutoRefreshServer(43444, "cosimPlotter", 1000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BoxBI ombi = new BoxBI();
        ControllerBI controller = new ControllerBI();
        PidBI pid = new PidBI();

        // Run the simulation
        List<Callable<Object>> calls = new ArrayList<>();
        calls.add(Executors.callable(ombi));
        calls.add(Executors.callable(controller));
        calls.add(Executors.callable(pid));

        ExecutorService executor = Executors.newFixedThreadPool(calls.size());

        System.out.println("Starting the simulation");
        try {
            executor.invokeAll(calls);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
