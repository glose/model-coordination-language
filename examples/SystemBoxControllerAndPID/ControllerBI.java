package SystemBoxControllerAndPID;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.GemocInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.BinaryPredicate;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.EventPredicate;
import org.eclipse.gemoc.execution.commons.predicates.ReadyToReadPredicate;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ControllerBI extends CoordinationInterface {
	// ----------------------------------------------------------------
    // Exported Variables / Ports
    // ----------------------------------------------------------------
    public Port switchCPUState = new InitiatorPort("CPUprotection::switchCPUState","CPUprotection::switchCPUState", "Box", false); // Output
    public Port CPUTemperature = new InitiatorPort("CPUprotection::cpuTemperature","CPUprotection::cpuTemperature", "Box", 25.0); // Input

    public int numberOfCommPoints = 0;

    public ControllerBI() {
        super("CTRL", "localhost", 26000, 26100, 0, 2);
		// -------------------------------------------------------
		// Initialize the model
		// -------------------------------------------------------
    	this.model = new GemocInterface("src/test/resources/overHeatControler.jar", "localhost", 39635);

        // allPorts = Arrays.asList(switchCPUState, CPUTemperature);
        initiatorPorts.add(CPUTemperature);
        initiatorPorts.add(switchCPUState);
        followerPorts = new ArrayList<>();
        followerDeterministicPorts = new ArrayList<>();

    	// The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("isStopped", switchCPUState);
        portMap.put("CPUTemperatureForController", CPUTemperature);
        portMap.put(switchCPUState.ID, switchCPUState);
        portMap.put(CPUTemperature.ID, CPUTemperature);

        addNewInputPort("CPUTemperatureForController", "tcp://localhost:26001", "tcp://localhost:26101");
    }

    @Override
    public void onTime(Action currentAction, StopCondition sc) {
        // now == action.time
        assert (now.compareTo(currentAction.temporalHorizon) == 0);
            // Sync: According to the ID of the current action Port, execute the corresponding action
            if (currentAction.port.ID.compareTo(CPUTemperature.ID) == 0) {
                BigDecimal temp = new BigDecimal(currentAction.getValue().toString());
                model.set("CPUprotection::cpuTemperature::currentValue", new Integer(temp.intValue()));

                if (CONFIG.showDebugMessage) System.out.println("["+ID+"] Temperature = " + model.get("CPUprotection::cpuTemperature::currentValue") + " @ " + now.doubleValue());
            }

            currentAction.setDone();
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sc) {
        // ----------------------------------------------------
        // Propagate the value: 1. Get the value from the model
        // ----------------------------------------------------
        //Object varValue = model.get(sc.objectQualifiedName);
        switchCPUState.setValue(!((boolean)switchCPUState.getValue()));

        // -------------------------------------------------------
        // Propagate the value: 2. Send on the corresponding queue
        // -------------------------------------------------------
        if (CONFIG.showDebugMessage) System.out.println("[Ctrl] Event name: " + sc.objectQualifiedName);
//                        Port port = portMap.get(sc.objectQualifiedName);
        publish(switchCPUState, switchCPUState.getValue(), Utils.toBigDecimal(sc.timeValue) );
    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sc) {
        retrieve(portMap.get(sc.objectQualifiedName), now);
        // Set this action as done, after the doWait there is the new action into the queue with the new value requested.
        // To sync this model with the other model, we need to re-execute the doStep method
        currentAction.setDone();
        // The next action is received,
    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sr) {

    }

    @Override
    public void onSync(Action currentAction) {
       onTime(currentAction, null);
    }

    @Override
    public void onEnd() {
        System.out.println("CTRL END");
    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {
        // ----------------------------------------------------
        // Initiators Predicate
        // ----------------------------------------------------
        EventPredicate e = new EventPredicate("occurs", "CPUprotection::switchCPUState");
        ReadyToReadPredicate p = new ReadyToReadPredicate("currentValue", "CPUprotection::cpuTemperature");
        return new BinaryPredicate(p, e, BinaryPredicate.BooleanBinaryOperator.OR);
    }
}