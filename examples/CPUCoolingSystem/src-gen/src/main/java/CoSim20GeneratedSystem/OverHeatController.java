package CoSim20GeneratedSystem;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.*;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import java.io.IOException;
import fr.inria.glose.cosim20.CONFIG;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;

public class OverHeatController extends CoordinationInterface {

  // -------------------------------
  // Plotter stuff
  // -------------------------------
  OscilloscupClient plotterClient;
  Stopwatch stopwatch = Stopwatch.createStarted();

  public Port SwitchCPUState_ctrl2box =
      new InitiatorPort(
          "SwitchCPUState_ctrl2box", "CPUprotection::switchCPUState", "CPUinBox", false); // OUTPUT
  public Port CPUTemperature_box2ctrl =
      new InitiatorPort(
          "CPUTemperature_box2ctrl", "CPUprotection::cpuTemperature", "CPUinBox", 25); // INPUT

  public OverHeatController() {
    super("overHeatController", "localhost", 36993, 33831, 0, 2);

    // -------------------------------------------------------
    // Initialize the model
    // -------------------------------------------------------
    this.model =
        new GemocInterface(
            "/home/gliboni/projects/mcl/examples/CPUCoolingSystem/su/overHeatControler.jar",
            "localhost",
            39635);

    try {
      plotterClient = new OscilloscupClient("localhost", 40995);
    } catch (IOException e) {
      e.printStackTrace();
    }

    initiatorPorts = new ArrayList<>();
    followerPorts = new ArrayList<>();
    followerDeterministicPorts = new ArrayList<>();

    initiatorPorts.add(SwitchCPUState_ctrl2box);
    initiatorPorts.add(CPUTemperature_box2ctrl);

    // The <key> is the id of the external port linked with a port of this model (<value>)
    portMap.put("isStopped_ctrl2box", SwitchCPUState_ctrl2box);
    portMap.put("SwitchCPUState_ctrl2box", SwitchCPUState_ctrl2box);
    portMap.put("CPUprotection::switchCPUState", SwitchCPUState_ctrl2box);

    portMap.put("CPUTemperature_box2ctrl", CPUTemperature_box2ctrl);
    portMap.put("CPUprotection::cpuTemperature", CPUTemperature_box2ctrl);

    addNewInputPort("CPUTemperature_box2ctrl", "tcp://localhost:41271", "tcp://localhost:41449");

    model.set("CPUprotection::cpuTemperature::currentValue", 25);
  }

  @Override
  public void onTime(Action currentAction, StopCondition sr) {
    if (now.compareTo(currentAction.temporalHorizon) == 0) {
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(SwitchCPUState_ctrl2box.ID) == 0) {
        boolean value = (boolean) SwitchCPUState_ctrl2box.getValue();

        publish(SwitchCPUState_ctrl2box, value, now);
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUTemperature_box2ctrl.ID) == 0) {
        BigDecimal temp = new BigDecimal(currentAction.getValue().toString());
        int value = temp.intValue();

        model.set("CPUprotection::cpuTemperature::currentValue", value);
        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] CPUTemperature_box2ctrl = "
                  + model.get("CPUprotection::cpuTemperature::currentValue")
                  + " @ "
                  + now.doubleValue());
      }

      // Extra plot to show the actual data nature of the variable
      // Remove the current action from the to-do list
      currentAction.setDone();

      if (currentAction instanceof PeriodicAction) {
        todo.add(
            new PeriodicAction(
                currentAction.typeOfAction,
                currentAction.port,
                currentAction.hostSource,
                now,
                ((PeriodicAction) currentAction).getTemporalPredicate()));
      }
    }
  }

  @Override
  public void onEvent(Action currentAction, StopCondition sc) {
    // If there is a Port defined as Transient, then save its changements of state
    SwitchCPUState_ctrl2box.setValue(!((boolean) SwitchCPUState_ctrl2box.getValue()));

    boolean value = (boolean) SwitchCPUState_ctrl2box.getValue();

    publish(SwitchCPUState_ctrl2box, value, Utils.toBigDecimal(sc.timeValue));
  }

  @Override
  public void onReadyToRead(Action currentAction, StopCondition sc) {
    retrieve(portMap.get(sc.objectQualifiedName), now);
    // Set this action as done, after the doWait there is the new action into the queue with the new
    // value requested.
    // To sync this model with the other model, we need to re-execute the doStep method
    currentAction.setDone();
  }

  @Override
  public void onUpdated(Action currentAction, StopCondition sc) {
    // Not implemented yet
  }

  @Override
  public void onSync(Action currentAction) {
    onTime(currentAction, null);
  }

  @Override
  public CoordinationPredicate setInitiatorsPredicate() {
    EventPredicate SwitchCPUState_ctrl2box_predicate =
        new EventPredicate("occurs", "CPUprotection::switchCPUState");
    ReadyToReadPredicate CPUTemperature_box2ctrl_predicate =
        new ReadyToReadPredicate("currentValue", "CPUprotection::cpuTemperature");
    BinaryPredicate binaryPredicate0 =
        new BinaryPredicate(
            SwitchCPUState_ctrl2box_predicate,
            CPUTemperature_box2ctrl_predicate,
            BinaryPredicate.BooleanBinaryOperator.OR);
    return binaryPredicate0;
  }

  @Override
  public void onEnd() {
    stopwatch.stop(); // optional
    System.out.println("[overHeatController] Elapsed time: " + stopwatch.elapsed(TimeUnit.SECONDS));
  }
}
