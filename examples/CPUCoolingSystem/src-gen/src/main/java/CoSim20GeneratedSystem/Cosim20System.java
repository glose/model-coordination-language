package CoSim20GeneratedSystem;

import fr.inria.glose.cosim20.Utils;
import fr.inria.glose.cosim20.CONFIG;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.IOException;

public class Cosim20System {
  // Define the "infinite" time as the time of the end of simulation plus an Epsilon
  public static void main(String[] args) {
    CONFIG.showDebugMessage = false;
    CONFIG.EndOfSimulation = Utils.toBigDecimal(30000);
    try {
      OscilloscupServer.createAutoRefreshServer(40995, "cosimPlotter", 1000);
    } catch (IOException e) {
      e.printStackTrace();
    }

    List<Callable<Object>> calls = new ArrayList<>();
    FanController fancontroller = new FanController();
    calls.add(Executors.callable(fancontroller));
    CPUinBox cpuinbox = new CPUinBox();
    calls.add(Executors.callable(cpuinbox));
    OverHeatController overheatcontroller = new OverHeatController();
    calls.add(Executors.callable(overheatcontroller));
    ExecutorService executor = Executors.newFixedThreadPool(calls.size());

    System.out.println("Starting the simulation");
    try {
      executor.invokeAll(calls);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
