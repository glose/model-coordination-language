package CoSim20GeneratedSystem;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.*;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import java.io.IOException;
import fr.inria.glose.cosim20.CONFIG;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;

public class FanController extends CoordinationInterface {

  // -------------------------------
  // Plotter stuff
  // -------------------------------
  OscilloscupClient plotterClient;
  Stopwatch stopwatch = Stopwatch.createStarted();

  // ----------------------------------------------------------------
  // FMI Standard
  // ----------------------------------------------------------------
  double startTime = 0.0;
  double stopTime = CONFIG.EndOfSimulation.doubleValue();
  String fmuPath = "/home/gliboni/projects/mcl/examples/CPUCoolingSystem/su/FanController.fmu";

  public Port CPUfanSpeed_fanController2CPU =
      new InitiatorPort("CPUfanSpeed_fanController2CPU", "CPUfanSpeed", "CPUinBox", 0); // OUTPUT
  public FollowerDeterministicPort CPUTemperature_CPU2fanController =
      new FollowerDeterministicPort(
          "CPUTemperature_CPU2fanController",
          "CPUTemperature",
          "CPUinBox",
          0,
          new TemporalPredicate(5));

  public FanController() {
    super("fanController", "localhost", 45351, 44003, 0, 1);

    // Setup the FMI model
    model = new FMIInterface(fmuPath, startTime, stopTime);

    try {
      plotterClient = new OscilloscupClient("localhost", 40995);
    } catch (IOException e) {
      e.printStackTrace();
    }

    initiatorPorts = new ArrayList<>();
    followerPorts = new ArrayList<>();
    followerDeterministicPorts = new ArrayList<>();

    initiatorPorts.add(CPUfanSpeed_fanController2CPU);
    todo.add(
        new PeriodicAction(
            Action.TypeOfAction.PUBLISH,
            CPUfanSpeed_fanController2CPU,
            "tcp://localhost:45351",
            now,
            new TemporalPredicate(5)));
    followerDeterministicPorts.add(CPUTemperature_CPU2fanController);

    // The <key> is the id of the external port linked with a port of this model (<value>)
    portMap.put("CPUfanSpeed_fanController2CPU", CPUfanSpeed_fanController2CPU);
    portMap.put("CPUfanSpeed", CPUfanSpeed_fanController2CPU);

    portMap.put("CPUTemperature_CPU2fanController", CPUTemperature_CPU2fanController);
    portMap.put("CPUTemperature", CPUTemperature_CPU2fanController);

    addNewInputPort(
        "CPUTemperature_CPU2fanController", "tcp://localhost:41271", "tcp://localhost:41449");

    model.set("targetTemperature", 65);
    model.set("Kp", 5.0);

    model.set("CPUTemperature", 25);
  }

  @Override
  public void onTime(Action currentAction, StopCondition sr) {
    if (now.compareTo(currentAction.temporalHorizon) == 0) {
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUfanSpeed_fanController2CPU.ID) == 0) {
        double value =
            (double) model.get(CPUfanSpeed_fanController2CPU.associatedModelVariableName);

        publish(CPUfanSpeed_fanController2CPU, value, now);
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUTemperature_CPU2fanController.ID) == 0) {
        double value = (double) currentAction.getValue();

        model.set("CPUTemperature", value);
        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] CPUTemperature_CPU2fanController = "
                  + model.get("CPUTemperature")
                  + " @ "
                  + now.doubleValue());
      }

      // Extra plot to show the actual data nature of the variable
      // Remove the current action from the to-do list
      currentAction.setDone();

      if (currentAction instanceof PeriodicAction) {
        todo.add(
            new PeriodicAction(
                currentAction.typeOfAction,
                currentAction.port,
                currentAction.hostSource,
                now,
                ((PeriodicAction) currentAction).getTemporalPredicate()));
      }
    }
  }

  @Override
  public void onEvent(Action currentAction, StopCondition sc) {}

  @Override
  public void onReadyToRead(Action currentAction, StopCondition sc) {}

  @Override
  public void onUpdated(Action currentAction, StopCondition sc) {
    // Not implemented yet
  }

  @Override
  public void onSync(Action currentAction) {
    // Statically generated ONLY if the BI abstracts a FMU
    // model.doStep(new TemporalPredicate(0));
    onTime(currentAction, null);
  }

  @Override
  public CoordinationPredicate setInitiatorsPredicate() {
    return null;
  }

  @Override
  public void onEnd() {
    stopwatch.stop(); // optional
    System.out.println("[fanController] Elapsed time: " + stopwatch.elapsed(TimeUnit.SECONDS));
  }
}
