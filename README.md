# Models Coordination Language


[![pipeline status](https://gitlab.inria.fr/glose/model-coordination-language/badges/master/pipeline.svg)](https://gitlab.inria.fr/glose/model-coordination-language/commits/master)

The project contains the two languages used to define the Model Behavioral Interface and the Model Coordination Specification.

## MBILang: Model Behavioral Interface (Specification) Language

The Model Behavioral Interface Language (MBILang) allows to define the model behavioral interface that exposes a part of the internal syntax and semantics of the model.
It allows to specify the exposed elements of the internal executable model as an homogeneous interface. The exposed elements can be exploited to specify the possible interactions with the model.

![ ](img/MBILANG_ecore.png  "MBILang Ecore")

### Textual Syntax

- XText Grammar: [MBILANG.xtext](https://gitlab.inria.fr/glose/model-coordination-language/blob/master/plugins/mbilang/fr.inria.glose.mbilang.xtext/src/fr/inria/glose/mbilang/MBILANG.xtext)

## MCL: Model Coordination (Specification) Language

The Model Coordination Language allows to define the Model Coordination Specification based on the Model Behavioral Interface for one or more models. The Model Coordination Language allows to define the Model Coordination Specification based on the Model Behavioral Interface for one or more models. The defined specification describes how to integrate heterogeneous models defining explicitly the coordination among them.

Based on the Model Behavioral Interface, the Coordination Model Specification is composed by a set of Connectors. A connector defines how, when and under which synchronization constraint one or more data are conveyed from a model to another.

![ ](img/MCL_ecore.png  "MCL Ecore")

### Textual Syntax

- XText Grammar: [MCL.xtext](https://gitlab.inria.fr/glose/model-coordination-language/blob/master/plugins/mcl/fr.inria.glose.mcl.xtext/src/fr/inria/glose/mcl/MCL.xtext)


### Graphical Syntax

- Sirius project: [mcl.odesign](https://gitlab.inria.fr/glose/model-coordination-language/blob/master/plugins/mcl/fr.inria.glose.mcl.design/description/mcl.odesign)

# Installation

To try the latest successful build, you can use this Eclipse update site:
[https://glose.gitlabpages.inria.fr/model-coordination-language/updatesite/latest](https://glose.gitlabpages.inria.fr/model-coordination-language/updatesite/latest)

To try a stable release, please visit the [Releases section](https://gitlab.inria.fr/glose/model-coordination-language/-/releases)

## Examples

You can find some examples here:

[https://gitlab.inria.fr/glose/model-coordination-language/tree/master/examples](https://gitlab.inria.fr/glose/model-coordination-language/tree/master/examples)

## Problems

### Java `1.8`

#### JavaFX not found

Hint from [here](https://github.com/JabRef/user-documentation/issues/204) and [here](https://stackoverflow.com/questions/56166267/how-do-i-get-java-fx-running-with-openjdk-8-on-ubuntu-18-04-2-lts)

- Remove the previuous version
```
apt purge openjfx

```

- Install
```
apt install openjfx=8u161-b12-1ubuntu2 libopenjfx-jni=8u161-b12-1ubuntu2 libopenjfx-java=8u161-b12-1ubuntu2
```

- Mark those packages to be not upgraded
```
apt-mark hold openjfx libopenjfx-jni libopenjfx-java
```
